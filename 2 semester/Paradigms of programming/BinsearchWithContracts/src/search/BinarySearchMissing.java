package search;


public class BinarySearchMissing {
    public static int[] sourceIntegers;
    public static int x;

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Usage: BinarySearch <target integer> <descending array of integers>");
            return;
        }

        if (args.length == 1) {
            System.out.println("-1");
            return;
        }

        x = Integer.parseInt(args[0]);


        sourceIntegers = new int[args.length - 1];
        for (int i = 0; i < args.length - 1; i++) {
            sourceIntegers[i] = Integer.parseInt(args[i + 1]);
        }


        int minimum_index = binarySearch(sourceIntegers, x);

        System.out.println(minimum_index);
    }


    // Invariant:   BaseInv === ((a != empty) && (key : integer) && (key != null)) && (\forall i \in [0..a.length-1) a[i] >= a[i+1])

    // Invariant:   RetvInv === ((retval=0 && a[0] <= key) XOR
    //              XOR (retval=a.length && a[a.length-1] >= key) XOR
    //              XOR (0 < retval < a.length ==> a[retval-1] > key >= a[retval]) XOR
    //              XOR (retval < 0 ==> a[-retval-2] > key > a[-retval-1]))

    // Requires:    BaseInv
    // Ensures:     RetvInv && BaseInv
    public static int binarySearch(int[] a, int key) {
        //return recursiveBinarySearch(a, key);
        return iterativeBinarySearch(a, key);
    }


    // Requires:    BaseInv
    // Ensures:     RetvInv && BaseInv
    private static int recursiveBinarySearch(int[] a, int key) {
        return internalRecursiveBinarySearch(a, key, -1, a.length);
    }


    // Invariant:   RecInv === (BaseInv && -1 <= l < r <= a.length && r > 0 && a[l] > key >= a[r])
    // Requires:    RecInv
    // Ensures:     RetvInv && BaseInv
    private static int internalRecursiveBinarySearch(int[] a, int key, int l, int r) {

        // Requires:    RecInv
        // Ensures:     a[0] >= key && RecInv
        if (a[0] < key) {
            return -1;
        }


        // Requires: a[0] >= key && RecInv
        // Ensures:  a[0] > key
        if (a[0] == key) {
            return 0;
        }


        // Requires:    a[0] > key && RecInv
        // Ensures:     a[0] >= key >= a[a.length] && RecInv
        if (key < a[a.length - 1]) {
            return -(a.length) - 1;
        }


        // Requires:    RecInv
        // Ensures:     r > l+1
        if (l == r - 1) {
            //Ensures:     l >= 0 && r < a.length && l==r+1 && ((a[l] > key >= a[r]) ==> ((key=a[r]) ||(a[l] > key > a[r])))
            int ans = -1;

            if (a[r] == key) {
                // Ensures:
                // (r < a.length && l > 0 && r == l+1 && (a[l] > key <== RecInv) && (a[r] == key)) ==>
                // ==> (a[ans-1] > key == a[ans] >= a[ans+1])
                ans = r;
            } else { //if ((a[l] > key) && (key > a[r])) {

                // Ensures:
                // (r < a.length && l > 0 && r == l+1 && (a[l] > key <== RecInv) && (a[r] > key)) ==>
                // ==> (a[l] > key > a[l+1] == a[r]) ==>
                // ==> \not \exists i = [0..a.length) : a[i] == key ==> "key is not in array" ==>
                // ==> r == (insertion point) ==>
                // ==> ans = -r-1 ==>
                // ==> a[-ans-2] > key > a[-ans-1]
                ans = (-r - 1);
            }

            // Ensures:
            // RetvInv [retval := ans] && BaseInv
            return ans;
        }


        // Requires: RecInv
        // Ensures:  RecInv[l := newL]
        int newL = l;
        // Requires: RecInv
        // Ensures:  RecInv[r := newR]
        int newR = r;

        // Requires: RecInv
        // Ensures:  0 < median < a.length-1 && RecInv
        int median = (l + r) / 2;


        // Requires: RecInv
        if (a[median] <= key) {
            // Ensures:
            // l' == l && r' == median < r ==>
            // ==> a[l] == a[l'] > key >= a[r'] >= a[r]
            newR = median;
        } else {
            // Ensures:
            // l' == median > l && r' == r ==>
            // ==> a[l] >= a[l'] > key >= a[r'] == a[r]
            newL = median;
        }

        // Requires:    RecInv
        // Ensures:     RetvInv && BaseInv
        return internalRecursiveBinarySearch(a, key, newL, newR);
    }


    // Invariant:   BaseInv === ((a != empty) && (key : integer) && (key != null)) && (\forall i \in [0..a.length-1) a[i] >= a[i+1])
    // Requires:    BaseInv
    // Ensures:     RetvInv && BaseInv
    private static int iterativeBinarySearch(int[] a, int key) {

        // Requires:    RecInv
        // Ensures:     a[0] >= key && RecInv
        if (a[0] < key) {
            return -1;
        }


        // Requires: a[0] >= key && RecInv
        // Ensures:  a[0] > key
        if (a[0] == key) {
            return 0;
        }


        // Requires:    a[0] > key && RecInv
        // Ensures:     a[0] >= key >= a[a.length] && RecInv
        if (key < a[a.length - 1]) {
            return -(a.length) - 1;
        }

        //Ensures: leftBorder == -1 && rightBorder == a.length && median == -1
        int leftBorder = 0;
        int rightBorder = a.length - 1;
        int median = -1;


        // Invariant CycleInv: ((0 <= median < a.length) && (a[leftBorder] > key >= a[rightBorder]))
        // Requires: 1 < leftBorder+1 < rightBorder < a.length && CycleInv
        // Ensures : leftBorder == rightBorder-1 && CycleInv
        while (leftBorder < rightBorder - 1) {
            // leftBorder < rightBorder-1 && CycleInv

            // Requires: -1 <= leftBorder < rightBorder <= a.length && CycleInv
            // Ensures:  0 <= median < a.length && (leftBorder < median < rightBorder)
            median = (leftBorder + rightBorder) / 2;

            // Requires: 0 <= median < a.length && (leftBorder < median < rightBorder)
            // Ensures:  CycleInv
            if (a[median] > key) {
                // 0 <= median < a.length && a[median] > key ==> leftBorder:=median ==> CycleInv[leftBorder := median]
                leftBorder = median;
            } else {
                // 0 <= median < a.length && key >= a[median] ==> rightBorder:=median ==> CycleInv[rightBorder := median]
                rightBorder = median;
            }
        }

        // Requires: 0 < leftBorder == rightBorder-1 < a.length-1 && CycleInv && (a[leftBorder] > key >= a[rightBorder])
        // Ensures:  a[rightBorder-1] > key == a[rightBorder]
        if (a[rightBorder] != key) {
            // (leftBorder == rightBorder-1 && a[leftBorder] > key >= a[rightBorder] && a[rightBorder] != key) ==>
            // ==> 0 < leftBorder && 1 < rightBorder ==>
            // ==> a[rightBorder-1] > key > a[rightBorder] ==>
            // ==> "-(rightBorder) - 1 is insertion point"
            // ==> "RetvInv[retval := -(rightBorder) - 1] is satisfied"
            return -(rightBorder) - 1;
        }


        // (leftBorder == rightBorder-1 && a[leftBorder] > key >= a[rightBorder] && !(a[rightBorder] != key)) ==>
        // (leftBorder == rightBorder-1 && a[leftBorder] > key >= a[rightBorder] && a[rightBorder] == key) ==>
        // ==> 0 < leftBorder && 1 < rightBorder ==>
        // ==> a[rightBorder-1] > key >= a[rightBorder] ==>
        // ==> "RetvInv[retval := rightBorder] is satisfied"
        return rightBorder;

    }
}
