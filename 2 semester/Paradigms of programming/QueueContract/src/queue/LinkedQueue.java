package queue;

import java.util.function.Function;
import java.util.function.Predicate;

public class LinkedQueue extends AbstractQueue {
    private Node head;
    private Node tail;

    @Override
    protected Queue doFilter(Predicate<Object> predicate) {
        LinkedQueue filteredQueue = new LinkedQueue();

        Node copyingPointer = head;

        while (copyingPointer != null) {
            if (predicate.test(copyingPointer.data)) {
                filteredQueue.enqueue(copyingPointer.data);
            }
            copyingPointer = copyingPointer.next;
        }

        return filteredQueue;
    }

    @Override
    protected Queue doMap(Function<Object, Object> function) {
        LinkedQueue filteredQueue = new LinkedQueue();

        Node copyingPointer = head;

        while (copyingPointer != null) {

            filteredQueue.enqueue(function.apply(copyingPointer.data));

            copyingPointer = copyingPointer.next;
        }

        return filteredQueue;
    }

    @Override
    protected void initializeFields() {
        length = 0;
    }

    @Override
    protected void doEnqueue(Object element) {
        if (length == 0) {
            head = new Node(element);
            tail = head;
        } else {
            tail = new Node(tail, element);
        }

        length++;
    }

    @Override
    protected Object doDequeue() {
        Object answer;
        answer = head.data;
        head = head.next;
        length--;
        return answer;
    }

    @Override
    protected Object doElement() {
        return head.data;
    }

    @Override
    public LinkedQueue makeCopy() {
        LinkedQueue copy = new LinkedQueue();

        Node copyingPointer = head;

        while (copyingPointer != null) {
            copy.enqueue(copyingPointer.data);
            copyingPointer = copyingPointer.next;
        }

        return copy;
    }

    static class Node {
        Node prev = null;
        Node next = null;
        Object data;

        public Node() {
        }

        public Node(Object _data) {
            data = _data;
        }

        public Node(Node _prev, Object _data) {
            data = _data;
            if (_prev != null) {
                prev = _prev;
                prev.next = this;
            }
        }
    }
}


