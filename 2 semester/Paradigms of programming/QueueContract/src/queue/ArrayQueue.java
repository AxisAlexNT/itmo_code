package queue;

import java.util.function.Function;
import java.util.function.Predicate;

public class ArrayQueue extends AbstractQueue {
    private int currentArraySize = 8;
    private int headPointer = BASE_QUEUE_SIZE - 1, tailPointer = BASE_QUEUE_SIZE - 1;
    private Object[] queue = new Object[BASE_QUEUE_SIZE];


    @Override
    protected void initializeFields() {
        queue = new Object[BASE_QUEUE_SIZE];
        currentArraySize = BASE_QUEUE_SIZE;
        headPointer = BASE_QUEUE_SIZE - 1;
        tailPointer = BASE_QUEUE_SIZE - 1;
    }

    @Override
    protected void doEnqueue(Object element) {
        ensureCapacity();

        queue[tailPointer] = element;

        if (tailPointer > 0) {
            tailPointer--;
        } else {
            tailPointer = currentArraySize - 1;
        }

        length++;
    }


    @Override
    protected Object doDequeue() {
        Object result = queue[headPointer];

        if (headPointer > 0) {
            headPointer--;
        } else {
            headPointer = currentArraySize - 1;
        }

        length--;

        ensureCapacity();

        return result;
    }

    @Override
    protected Object doElement() {
        return queue[headPointer];
    }

    @Override
    protected Queue doFilter(Predicate<Object> predicate) {
        ArrayQueue filteredQueue = new ArrayQueue();

        int copyingIndex = headPointer;

        while (copyingIndex != tailPointer) {
            if (predicate.test(queue[copyingIndex])) {
                filteredQueue.enqueue(queue[copyingIndex]);
            }
            copyingIndex--;

            if (copyingIndex < 0) {
                copyingIndex = currentArraySize - 1;
            }
        }

        return filteredQueue;
    }

    @Override
    protected Queue doMap(Function<Object, Object> function) {
        ArrayQueue mappedQueue = new ArrayQueue();

        int copyingIndex = headPointer;

        while (copyingIndex != tailPointer) {
            mappedQueue.enqueue(function.apply(queue[copyingIndex]));
            copyingIndex--;

            if (copyingIndex < 0) {
                copyingIndex = currentArraySize - 1;
            }
        }

        return mappedQueue;
    }

    @Override
    public ArrayQueue makeCopy() {
        ArrayQueue copy = new ArrayQueue();

        int copyingIndex = headPointer;

        while (copyingIndex != tailPointer) {
            copy.enqueue(queue[copyingIndex]);
            copyingIndex--;

            if (copyingIndex < 0) {
                copyingIndex = currentArraySize - 1;
            }
        }

        return copy;
    }


    private void ensureCapacity() {
        if (length >= currentArraySize / 2) {
            Object[] queueToExchange;
            queueToExchange = new Object[currentArraySize * 2];

            int copyingIndex = headPointer;
            int destIndex = currentArraySize * 2 - 1;

            while (copyingIndex != tailPointer) {
                queueToExchange[destIndex] = queue[copyingIndex];
                destIndex--;
                copyingIndex--;

                if (copyingIndex == -1) {
                    copyingIndex = currentArraySize - 1;
                }
            }


            tailPointer = destIndex;
            headPointer = currentArraySize * 2 - 1;


            queue = queueToExchange;
            currentArraySize *= 2;
        } else if ((length <= currentArraySize / 4) && (length > BASE_QUEUE_SIZE)) {
            Object[] queueToExchange;
            queueToExchange = new Object[currentArraySize / 2];

            int copyingIndex = headPointer;
            int destIndex = currentArraySize / 2 - 1;

            while (copyingIndex != tailPointer) {
                queueToExchange[destIndex] = queue[copyingIndex];
                destIndex--;
                copyingIndex--;

                if (copyingIndex == -1) {
                    copyingIndex = currentArraySize - 1;
                }
            }

            tailPointer = destIndex;
            headPointer = currentArraySize / 2 - 1;


            queue = queueToExchange;
            currentArraySize /= 2;
        }
    }
}
