package queue;

public interface Copiable {

    // Requires: this != null
    // Ensures: retval is a copy of this
    /*public*/ Copiable makeCopy();
}
