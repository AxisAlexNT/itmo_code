package queue;

import java.util.function.Function;
import java.util.function.Predicate;

public interface Queue extends Copiable {
    // Requires: (this != null) && (element != null) && (this.size() >= 0)
    // Ensures:  (this' != null) && (this'.size() = this.size()+1 > 0) && (element is the tail of the queue)
    /*public*/ void enqueue(Object element);

    // Requires: (this != null) && (this.size() > 0)
    // Ensures:  (this' != null) && (retval is a head of the queue) && (this'.size() = this.size()-1 >= 0)
    /*public*/ Object dequeue();

    // Requires: (this != null) && (this.size() > 0)
    // Ensures:  (this' != null) && (retval is a head of the queue) && (this'.size() = this.size() > 0)
    /*public*/ Object element();

    // Requires: (this != null)
    // Ensures:  (this' != null) && (retval is the length of queue represented by this')
    /*public*/ int size();

    // Requires: (this != null)
    // Ensures:  (this' != null) && (retval == true <=> if and only if the queue represented by this' is empty) && (retval == true ==> this'.size() == 0)
    /*public*/ boolean isEmpty();

    // Requires: (this != null)
    // Ensures:  (this' != null) && (queue represented by this' is empty) && (this'.size() == 0) && (this'.isEmpty() == true)
    /*public*/ void clear();

    // Requires: (this != null) && (predicate != null) && (type<Predicate> == Object)
    // Ensures:  (this' = this) && (retval is a subqueue of this) && (\forall x in retval predicate(x))
    /*public*/ Queue filter(Predicate<Object> predicate);

    // Requires: (this != null) && (function != null) && (function: <Object> --> <Object>)
    // Ensures:  (this' = this) && (\forall x in this: function(x) in retval)
    /*public*/ Queue map(Function<Object, Object> function);
}
