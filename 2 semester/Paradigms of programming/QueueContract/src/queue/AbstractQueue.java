package queue;

import java.util.function.Function;
import java.util.function.Predicate;

public abstract class AbstractQueue implements Queue {
    protected final int BASE_QUEUE_SIZE = 8;
    protected int length = 0;

    public AbstractQueue() {
        clear();
    }


    // Requires: (this != null) && (element != null)
    // Ensures:  (this' != null) && (element is in the tail of this')
    @Override
    public void enqueue(Object element) {
        assert (element != null);
        doEnqueue(element);
    }


    // Requires: (this != null) && (length > 0)
    // Ensures:  (this' != null) && (length' >= 0) && (retval == this.element())
    @Override
    public Object dequeue() {
        assert (length > 0);
        return doDequeue();
    }


    // Requires: (this != null) && (length > 0)
    // Ensures:  (this' == this != null) && (length' == length ) && (retval is a head of queue represented by this')
    @Override
    public Object element() {
        assert (length > 0);
        return doElement();
    }


    // Requires: (this != null) && (length >= 0)
    // Ensures:  (this' == this != null) && (length' == length) && (retval == length) && (length is a size of queue represented by this')
    @Override
    public int size() {
        assert (length >= 0);
        return length;
    }


    // Requires: (this != null) && (length >= 0)
    // Ensures:  (this' == this != null) && (retval == true <=> queue represented by this' is empty) && (retval === (length == 0))
    @Override
    public boolean isEmpty() {
        assert (length >= 0);
        return (length == 0);
    }


    // Requires: (this != null)
    // Ensures:  (this' represents an empty queue) && (length' == 0) && (this'.isEmpty() == true)
    @Override
    public void clear() {
        initializeFields();
        length = 0;
    }


    // Requires: (this != null) && (predicate != null)
    // Ensures:  (this' = this) && (retval is a subqueue of this) && (\forall x in retval predicate(x))
    @Override
    public Queue filter(Predicate<Object> predicate) {
        assert (predicate != null);
        return doFilter(predicate);
    }


    // Requires: (this != null) && (function != null)
    // Ensures:  (this' = this) && (\forall x in this: function(x) in retval)
    @Override
    public Queue map(Function<Object, Object> function) {
        assert (function != null);
        return doMap(function);
    }


    // Requires: (this != null)
    // Ensures:  (this' represents an empty queue) && (length' == 0) && (this'.isEmpty() == true)
    protected abstract void initializeFields();

    // Requires: (this != null) && (element != null)
    // Ensures:  (this' != null) && (element is in the tail of this')
    protected abstract void doEnqueue(Object element);

    // Requires: (this != null) && (length > 0)
    // Ensures:  (this' != null) && (length' >= 0) && (retval == this.element())
    protected abstract Object doDequeue();

    // Requires: (this != null) && (length > 0)
    // Ensures:  (this' == this != null) && (length' == length ) && (retval is a head of queue represented by this')
    protected abstract Object doElement();

    // Requires: (this != null) && (predicate != null)
    // Ensures:  (this' = this) && (retval is a subqueue of this) && (\forall x in retval predicate(x))
    protected abstract Queue doFilter(Predicate<Object> predicate);

    // Requires: (this != null) && (function != null)
    // Ensures:  (this' = this) && (\forall x in this: function(x) in retval)
    protected abstract Queue doMap(Function<Object, Object> function);
}
