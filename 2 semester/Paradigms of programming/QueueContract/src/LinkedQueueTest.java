import queue.LinkedQueue;

public class LinkedQueueTest {
    public static void main(String[] args) {
        LinkedQueue linkedQueue = new LinkedQueue();

        for (int i = 0; i < 65536; i++) {
            linkedQueue.enqueue(i % 6);
        }

        System.out.println("SIZE after adding: " + linkedQueue.size());
        System.out.flush();

        for (int i = 0; i < 65539; i++) {
            Object o = linkedQueue.dequeue();

            if ((Integer) o != i % 6) {
                System.out.println("WARNING!!!" + i % 6 + "!=" + o + " at point #" + i);
                System.out.flush();
                break;
            }
        }

    }
}
