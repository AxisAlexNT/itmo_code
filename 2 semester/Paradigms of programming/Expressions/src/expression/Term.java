package expression;

import expression.exceptions.ArithmeticEvaluationException;

public abstract class Term implements CommonExpression {
    protected abstract int doEvaluate(int x) throws ArithmeticEvaluationException;

    protected abstract double doEvaluate(double x);

    protected abstract int doEvaluate(int x, int y, int z) throws ArithmeticEvaluationException;

    @Override
    public int evaluate(int x) throws ArithmeticEvaluationException {
        return doEvaluate(x);
    }

    @Override
    public double evaluate(double x) {
        return doEvaluate(x);
    }

    @Override
    public int evaluate(int x, int y, int z) throws ArithmeticEvaluationException {
        return doEvaluate(x, y, z);
    }

    @Override
    public String toString() {
        return doToString();
    }

    protected abstract String doToString();


    protected int sign(int x) {
        if (x == 0) {
            return 0;
        } else {
            return (x > 0) ? 1 : (-1);
        }
    }

    protected int abs(int x) {
        return (x < 0) ? (-x) : x;
    }

}
