package expression;

import expression.exceptions.ArithmeticEvaluationException;

public abstract class UnaryOperator extends Term {
    protected Term argument;

    protected String operationSymbol;

    public UnaryOperator(Term argument, String operationSymbol) {
        this.operationSymbol = operationSymbol;
        this.argument = argument;
    }

    @Override
    protected int doEvaluate(int x) throws ArithmeticEvaluationException {
        return operation(argument.evaluate(x));
    }

    protected abstract int operation(int x) throws ArithmeticEvaluationException;

    @Override
    protected double doEvaluate(double x) {
        return operation(argument.evaluate(x));
    }

    protected abstract double operation(double x);

    @Override
    protected int doEvaluate(int x, int y, int z) throws ArithmeticEvaluationException {
        return operation(argument.evaluate(x, y, z));
    }

    @Override
    protected String doToString() {
        return (new StringBuilder()).append('(').append(operationSymbol).append(argument.toString()).append(')').toString();
    }
}
