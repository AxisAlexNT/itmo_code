package expression;

public class Const extends Term {
    protected int valueInt;
    protected double valueDouble;
    protected String displayedValue;

    public Const(int _value) {
        valueInt = _value;
        valueDouble = (double) valueInt;
        displayedValue = ((new StringBuilder()).append(valueInt)).toString();
    }

    public Const(double _value) {
        valueDouble = _value;
        valueInt = (int) valueDouble;
        displayedValue = ((new StringBuilder()).append(valueDouble)).toString();
    }

    @Override
    protected int doEvaluate(int x) {
        return valueInt;
    }

    @Override
    protected double doEvaluate(double x) {
        return valueDouble;
    }

    @Override
    protected int doEvaluate(int x, int y, int z) {
        return valueInt;
    }

    @Override
    protected String doToString() {
        if (valueInt < 0) {
            return "(" + displayedValue + ")";
        } else {
            return displayedValue;
        }
    }


}
