package expression.exceptions;

public class GrammarMismatchException extends AbstractExpressionException {
    protected static final String currentGrammar =
            "\nValid Grammar is:\n" +
                    "\n" +
                    "O' --> |XO' | empty\n" +
                    "O  --> XO'\n" +
                    "X' --> ^AX' | empty\n" +
                    "X  --> AX'\n" +
                    "A' --> &FA' | empty\n" +
                    "A  --> FA'\n" +
                    "F' --> (+|-)SF' | empty\n" +
                    "F  --> SF'\n" +
                    "S' --> ( *|/ )TS' | empty\n" +
                    "S  --> TS'|empty\n" +
                    "T  --> I|-T|~T|count T|(O)|x|y|z|O\n" +
                    "I  --> 0..9 | 0..9I\n" +
                    "\n" +
                    "Where token I represents a valid integer in range -2147483648 <= int < 2147483648\n" +
                    "\n" +
                    "\n" +
                    "Starting token: O\n\n";


    public GrammarMismatchException(String message, int position) {
        super(message, position);
    }

    public GrammarMismatchException(String message, int position, boolean needPrintGrammar) {
        super(message, position);
        //super(needPrintGrammar ? (message + currentGrammar) : message, position);
    }


    public GrammarMismatchException(String message) {
        super(message + currentGrammar);
    }


}
