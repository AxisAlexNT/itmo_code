package expression.exceptions;

public class UnexpectedEndedExpressionException extends GrammarMismatchException {
    public UnexpectedEndedExpressionException(String message, int position) {
        super(message, position);
    }

    public UnexpectedEndedExpressionException(int position)
    {
        super("The input string contains more than a valid expression", position);
    }

}
