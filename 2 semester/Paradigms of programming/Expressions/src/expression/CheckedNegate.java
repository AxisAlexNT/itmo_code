package expression;

import expression.exceptions.ArithmeticEvaluationException;
import expression.exceptions.IntegerOverflowException;

public class CheckedNegate extends UnaryOperator {
    public CheckedNegate(Term argument) {
        super(argument, "-");
    }

    @Override
    protected int operation(int x) throws ArithmeticEvaluationException {
        if (x == Integer.MIN_VALUE) {
            throw new IntegerOverflowException("Negating Integer.MIN_VALUE will exceed the integer range", this, true);
        }

        return (-x);
    }

    @Override
    protected double operation(double x) {
        return -x;
    }
}
