package expression;

import expression.exceptions.ArithmeticEvaluationException;

public class UnaryHigh extends UnaryOperator {
    public UnaryHigh(Term argument) {
        super(argument, "high");
    }

    @Override
    protected int operation(int x) throws ArithmeticEvaluationException {
        return Integer.highestOneBit(x);
    }

    @Override
    protected double operation(double x) {
        return 0;
    }
}
