package expression.parser;

import expression.TripleExpression;
import expression.exceptions.GrammarMismatchException;


public interface Parser {
    TripleExpression parse(String expression) throws GrammarMismatchException;
}