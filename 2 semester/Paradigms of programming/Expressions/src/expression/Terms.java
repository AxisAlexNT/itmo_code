package expression;

public enum Terms {
    UNARY_MINUS,
    BRACKET_OPEN,
    BRACKET_CLOSE,
    MULTIPLY,
    DIVIDE,
    ADD,
    SUBTRACT,
    CONST,
    VAR
}

