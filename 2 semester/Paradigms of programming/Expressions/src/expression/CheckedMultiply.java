package expression;

import expression.exceptions.ArithmeticEvaluationException;
import expression.exceptions.IntegerOverflowException;

public class CheckedMultiply extends BinaryOperator {
    public CheckedMultiply(Term _left, Term _right) {
        super(_left, _right, "*");
    }

    @Override
    protected int operation(int l, int r) throws ArithmeticEvaluationException {
        if (l > r) {
            int t = l;
            l = r;
            r = t;
        }
        // l <= r


        if (l == Integer.MIN_VALUE) {
            if ((r != 0) && (r != 1)) {
                throw new IntegerOverflowException("Integer overflow in multiplication during evaluation", this, true);
            }
        }

        if (r == Integer.MAX_VALUE) {
            if ((l == Integer.MAX_VALUE) || (abs(l) > 1)) {
                throw new IntegerOverflowException("Integer overflow in multiplication during evaluation", this, true);
            }
        }

        if ((l == 1) || (r == 1) || (l == 0) || (r == 0) || (r == -1)) {
            return l * r;
        }


        int preRes = l * r;

        if ((preRes / l != r)) {
            throw new IntegerOverflowException("Integer overflow in multiplication during evaluation", this, true);
        }

        return l * r;
    }

    @Override
    protected double operation(double l, double r) {
        return l * r;
    }
}
