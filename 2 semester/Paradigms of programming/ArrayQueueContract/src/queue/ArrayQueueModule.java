package queue;


public class ArrayQueueModule {
    private static final int BASE_QUEUE_SIZE = 8;
    private static int length = 0, currentArraySize = 0;
    private static int headPointer = BASE_QUEUE_SIZE - 1, tailPointer = BASE_QUEUE_SIZE - 1;
    private static Object[] queue;


    public ArrayQueueModule() {
        init();
    }


    private static void ensureCapacity() {
        if (queue == null) {
            init();
        }


        if (length >= currentArraySize / 2) {
            Object[] queueToExchange;
            queueToExchange = new Object[currentArraySize * 2];

            int copyingIndex = headPointer;
            int destIndex = currentArraySize * 2 - 1;

            while (copyingIndex != tailPointer) {
                queueToExchange[destIndex] = queue[copyingIndex];
                destIndex--;
                copyingIndex--;

                if (copyingIndex == -1) {
                    copyingIndex = currentArraySize - 1;
                }
            }


            tailPointer = destIndex;
            headPointer = currentArraySize * 2 - 1;


            queue = queueToExchange;
            currentArraySize *= 2;
        } else if ((length <= currentArraySize / 4) && (length > BASE_QUEUE_SIZE)) {
            Object[] queueToExchange;
            queueToExchange = new Object[currentArraySize / 2];

            int copyingIndex = headPointer;
            int destIndex = currentArraySize / 2 - 1;

            while (copyingIndex != tailPointer) {
                queueToExchange[destIndex] = queue[copyingIndex];
                destIndex--;
                copyingIndex--;

                if (copyingIndex == -1) {
                    copyingIndex = currentArraySize - 1;
                }
            }

            tailPointer = destIndex;
            headPointer = currentArraySize / 2 - 1;


            queue = queueToExchange;
            currentArraySize /= 2;
        }
    }


    public static void enqueue(Object element) {
        assert (element != null);

        ensureCapacity();
        queue[tailPointer] = element;

        if (tailPointer > 0) {
            tailPointer--;
        } else {
            tailPointer = currentArraySize - 1;
        }

        length++;
    }


    public static Object dequeue() {
        assert (length > 0);

        Object result = queue[headPointer];

        if (headPointer > 0) {
            headPointer--;
        } else {
            headPointer = currentArraySize - 1;
        }

        length--;

        ensureCapacity();

        return result;
    }


    public static Object element() {
        assert (length > 0);
        return queue[headPointer];
    }


    public static int size() {
        assert (length >= 0);
        return length;
    }


    public static boolean isEmpty() {
        assert (length >= 0);
        return (length == 0);
    }


    public static void clear() {
        init();
    }


    private static void init() {
        queue = new Object[BASE_QUEUE_SIZE];
        currentArraySize = BASE_QUEUE_SIZE;
        length = 0;
        headPointer = BASE_QUEUE_SIZE - 1;
        tailPointer = BASE_QUEUE_SIZE - 1;
    }


    public static void push(Object element) {
        assert (element != null);

        ensureCapacity();

        headPointer++;
        if (headPointer == currentArraySize) {
            headPointer = 0;
        }

        queue[headPointer] = element;

        length++;
    }


    public static Object peek() {
        assert (length > 0);

        int previousTailPointer = tailPointer;
        previousTailPointer++;

        if (previousTailPointer == currentArraySize) {
            previousTailPointer = 0;
        }

        return queue[previousTailPointer];
    }


    public static Object remove() {
        assert (length > 0);

        int previousTailPointer = tailPointer;
        previousTailPointer++;

        if (previousTailPointer == currentArraySize) {
            previousTailPointer = 0;
        }

        Object answer = queue[previousTailPointer];
        tailPointer = previousTailPointer;
        length--;

        ensureCapacity();

        return answer;
    }
}
