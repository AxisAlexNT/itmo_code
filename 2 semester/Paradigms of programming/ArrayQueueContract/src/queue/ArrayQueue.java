package queue;

public class ArrayQueue {
    private final int BASE_QUEUE_SIZE = 8;
    private int length = 0, currentArraySize = 8;
    private int headPointer = BASE_QUEUE_SIZE - 1, tailPointer = BASE_QUEUE_SIZE - 1;
    private Object[] queue = new Object[BASE_QUEUE_SIZE];


    //Inv: queue != null && ((length <= BASE_QUEUE_SIZE) || (currentArraySize/4 <= length <= currentArraySize/2))
    //Inv  && |headPointer|+(|currentArraySize-tailPointer| mod currentArraySize)==length
    //Inv  && (currentArraySize > BASE_QUEUE_SIZE) && (0 <= headPointer, tailPointer < currentArraySize)
    //Inv  && queue[headPointer] represents a head of the queue
    //Inv  && tailPointer points the next empty cell in queue array that represents a queue
    //Requires: Inv
    //Ensures: Inv && ((currentArraySize' = currentArraySize) XOR ((currentArraySize' == currentArraySize/4) || (currentArraySize' == 2*currentArraySize)))
    private void ensureCapacity() {
        // Requires: queue
        // Ensures:  queue != null
        if (queue == null) {
            init();
        }


        if (length >= currentArraySize / 2) {
            // Ensures: length >= currentArraySize / 2

            Object[] queueToExchange;
            // Requires: Inv
            // Ensures:  (queueToExchange != null) && (|queueToExchange| == 2*currentArraySize)
            queueToExchange = new Object[currentArraySize * 2];

            // Requires: Inv
            // Ensures:  copyingIndex = headPointer
            int copyingIndex = headPointer;

            // Requires: Inv
            // Ensures:  destIndex = currentArraySize * 2 - 1
            int destIndex = currentArraySize * 2 - 1;

            // Invariant CycleINV: Inv && 0 <= copyingIndex <= currentArraySize-1 && currentArraySize-1 <= destIndex <= 2*currentArraySize-1
            // Requires: CycleINV
            // Ensures:  copyingIndex == tailPointer
            while (copyingIndex != tailPointer) {
                // Requires: CycleINV
                // Ensures:  queueToExchange[destIndex] = queue[copyingIndex];
                queueToExchange[destIndex] = queue[copyingIndex];
                // Requires: CycleINV
                // Ensures:  CycleINV && currentArraySize-1 <= destIndex <= 2*currentArraySize-1
                destIndex--;

                // Requires: CycleINV
                // Ensures:  -1 <= copyingIndex <= currentArraySize-1
                copyingIndex--;

                // Requires: -1 <= copyingIndex <= currentArraySize-1
                // Ensures:  0 <= copyingIndex <= currentArraySize-1
                if (copyingIndex == -1) {
                    copyingIndex = currentArraySize - 1;
                }
            }

            // Requires: Inv && currentArraySize-1 <= destIndex <= 2*currentArraySize-1
            // Ensures:  Inv && tailPointer = destIndex
            tailPointer = destIndex;

            // Requires: Inv
            // Ensures:  headPointer = currentArraySize * 2 - 1;
            headPointer = currentArraySize * 2 - 1;

            // Requires: queueToExchange != null && (queueToExchange length = 2*(queue length)) && Inv
            // Ensures:  (queue' != null) && (queue' represents the same queue order as queue)
            queue = queueToExchange;

            // Requires: Inv
            // Ensures:  currentArraySize' = 2*currentAraySize && Inv
            currentArraySize *= 2;
        } else if ((length <= currentArraySize / 4) && (length > BASE_QUEUE_SIZE)) {
            //Ensures: (length <= currentArraySize / 4) && (length > BASE_QUEUE_SIZE)

            Object[] queueToExchange;
            // Requires: Inv
            // Ensures:  (queueToExchange != null) && (|queueToExchange| == currentArraySize / 2)
            queueToExchange = new Object[currentArraySize / 2];

            // Requires: Inv
            // Ensures:  copyingIndex = headPointer
            int copyingIndex = headPointer;

            // Requires: Inv
            // Ensures:  destIndex = currentArraySize/2 - 1
            int destIndex = currentArraySize / 2 - 1;

            // Invariant CycleINV: Inv && 0 <= copyingIndex <= currentArraySize-1 && currentArraySize-1 <= destIndex <=currentArraySize/2-1
            // Requires: CycleINV
            // Ensures:  copyingIndex == tailPointer
            while (copyingIndex != tailPointer) {
                // Requires: CycleINV
                // Ensures:  queueToExchange[destIndex] = queue[copyingIndex];
                queueToExchange[destIndex] = queue[copyingIndex];
                // Requires: CycleINV
                // Ensures:  CycleINV && currentArraySize-1 <= destIndex <= currentArraySize/2-1
                destIndex--;

                // Requires: CycleINV
                // Ensures:  -1 <= copyingIndex <= currentArraySize-1
                copyingIndex--;

                // Requires: -1 <= copyingIndex <= currentArraySize-1
                // Ensures:  0 <= copyingIndex <= currentArraySize-1
                if (copyingIndex == -1) {
                    copyingIndex = currentArraySize - 1;
                }
            }

            // Requires: Inv && currentArraySize-1 <= destIndex <= currentArraySize/2-1
            // Ensures:  Inv && tailPointer = destIndex
            tailPointer = destIndex;

            // Requires: Inv
            // Ensures:  headPointer = currentArraySize / 2 - 1;
            headPointer = currentArraySize / 2 - 1;


            // Requires: queueToExchange != null && (queueToExchange length = (queue length)/2) && Inv
            // Ensures:  (queue' != null) && (queue' represents the same queue order as queue)
            queue = queueToExchange;

            // Requires: Inv
            // Ensures:  currentArraySize' = currentAraySize/2 && Inv
            currentArraySize /= 2;
        }
    }

    public void enqueue(Object element) {
        assert (element != null);

        ensureCapacity();


        queue[tailPointer] = element;

        if (tailPointer > 0) {
            tailPointer--;
        } else {
            tailPointer = currentArraySize - 1;
        }

        length++;
    }


    public Object dequeue() {
        assert (length > 0);

        Object result = queue[headPointer];

        if (headPointer > 0) {
            headPointer--;
        } else {
            headPointer = currentArraySize - 1;
        }

        length--;

        ensureCapacity();

        return result;
    }


    public Object element() {

        assert (length > 0);
        return queue[headPointer];
    }


    public int size() {
        assert (length >= 0);
        return length;
    }


    public boolean isEmpty() {
        assert (length >= 0);
        return (length == 0);
    }


    public void clear() {
        init();
    }


    private void init() {
        queue = new Object[BASE_QUEUE_SIZE];
        currentArraySize = BASE_QUEUE_SIZE;
        length = 0;
        headPointer = BASE_QUEUE_SIZE - 1;
        tailPointer = BASE_QUEUE_SIZE - 1;
        // ==> Inv: |headPointer-tailPointer| == length
    }


    public void push(Object element) {
        assert (element != null);

        ensureCapacity();

        headPointer++;
        if (headPointer == currentArraySize) {
            headPointer = 0;
        }

        queue[headPointer] = element;

        length++;
    }


    public Object peek() {
        assert (length > 0);

        int previousTailPointer = tailPointer;
        previousTailPointer++;

        if (previousTailPointer == currentArraySize) {
            previousTailPointer = 0;
        }

        return queue[previousTailPointer];
    }


    public Object remove() {
        assert (length > 0);

        int previousTailPointer = tailPointer;
        previousTailPointer++;

        if (previousTailPointer == currentArraySize) {
            previousTailPointer = 0;
        }

        Object answer = queue[previousTailPointer];
        tailPointer = previousTailPointer;
        length--;

        ensureCapacity();

        return answer;
    }

}
