package queue;

public class ArrayQueueADT {
    private final int BASE_QUEUE_SIZE = 8;
    private int length = 0, currentArraySize = 0;
    private int headPointer = BASE_QUEUE_SIZE - 1, tailPointer = BASE_QUEUE_SIZE - 1;
    private Object[] queue;


    public ArrayQueueADT() {
        queue = new Object[BASE_QUEUE_SIZE];
        currentArraySize = BASE_QUEUE_SIZE;
        length = 0;
    }


    private static void ensureCapacity(ArrayQueueADT queueADT) {
        if (queueADT.length >= queueADT.currentArraySize / 2) {
            Object[] queueToExchange;
            queueToExchange = new Object[queueADT.currentArraySize * 2];

            int copyingIndex = queueADT.headPointer;
            int destIndex = queueADT.currentArraySize * 2 - 1;

            while (copyingIndex != queueADT.tailPointer) {
                queueToExchange[destIndex] = queueADT.queue[copyingIndex];
                destIndex--;
                copyingIndex--;

                if (copyingIndex == -1) {
                    copyingIndex = queueADT.currentArraySize - 1;
                }
            }


            queueADT.tailPointer = destIndex;
            queueADT.headPointer = queueADT.currentArraySize * 2 - 1;


            queueADT.queue = queueToExchange;
            queueADT.currentArraySize *= 2;
        } else if ((queueADT.length <= queueADT.currentArraySize / 4) && (queueADT.length > queueADT.BASE_QUEUE_SIZE)) {
            Object[] queueToExchange;
            queueToExchange = new Object[queueADT.currentArraySize / 2];

            int copyingIndex = queueADT.headPointer;
            int destIndex = queueADT.currentArraySize / 2 - 1;

            while (copyingIndex != queueADT.tailPointer) {
                if (copyingIndex == -1) {
                    copyingIndex = queueADT.currentArraySize - 1;
                }

                queueToExchange[destIndex] = queueADT.queue[copyingIndex];
                destIndex--;
                copyingIndex--;
            }

            queueADT.tailPointer = destIndex;
            queueADT.headPointer = queueADT.currentArraySize / 2 - 1;


            queueADT.queue = queueToExchange;
            queueADT.currentArraySize /= 2;
        }
    }


    public static void enqueue(ArrayQueueADT queueADT, Object element) {
        assert (element != null);
        ensureCapacity(queueADT);

        queueADT.queue[queueADT.tailPointer] = element;

        if (queueADT.tailPointer > 0) {
            queueADT.tailPointer--;
        } else {
            queueADT.tailPointer = queueADT.currentArraySize - 1;
        }

        queueADT.length++;
    }


    public static Object dequeue(ArrayQueueADT queueADT) {
        assert (queueADT.length > 0);

        Object result = queueADT.queue[queueADT.headPointer];

        if (queueADT.headPointer > 0) {
            queueADT.headPointer--;
        } else {
            queueADT.headPointer = queueADT.currentArraySize - 1;
        }

        queueADT.length--;

        ensureCapacity(queueADT);

        return result;
    }


    public static Object element(ArrayQueueADT queueADT) {
        assert (queueADT != null);
        assert (queueADT.length > 0);

        return queueADT.queue[queueADT.headPointer];
    }


    public static boolean isEmpty(ArrayQueueADT queueADT) {
        assert (queueADT != null);
        assert (queueADT.length >= 0);

        return (queueADT.length == 0);
    }


    public static int size(ArrayQueueADT queueADT) {
        assert (queueADT != null);

        return queueADT.length;
    }


    public static void clear(ArrayQueueADT queueADT) {
        queueADT.queue = new Object[queueADT.BASE_QUEUE_SIZE];
        queueADT.currentArraySize = queueADT.BASE_QUEUE_SIZE;
        queueADT.length = 0;
        queueADT.headPointer = queueADT.BASE_QUEUE_SIZE - 1;
        queueADT.tailPointer = queueADT.BASE_QUEUE_SIZE - 1;
    }


    public static void push(ArrayQueueADT queueADT, Object element) {
        assert (element != null);

        ensureCapacity(queueADT);

        queueADT.headPointer++;
        if (queueADT.headPointer == queueADT.currentArraySize) {
            queueADT.headPointer = 0;
        }

        queueADT.queue[queueADT.headPointer] = element;

        queueADT.length++;
    }


    public static Object peek(ArrayQueueADT queueADT) {
        assert (queueADT.length > 0);

        int previousTailPointer = queueADT.tailPointer;
        previousTailPointer++;

        if (previousTailPointer == queueADT.currentArraySize) {
            previousTailPointer = 0;
        }

        return queueADT.queue[previousTailPointer];
    }


    public static Object remove(ArrayQueueADT queueADT) {
        assert (queueADT != null);
        assert (queueADT.length > 0);

        int previousTailPointer = queueADT.tailPointer;
        previousTailPointer++;

        if (previousTailPointer == queueADT.currentArraySize) {
            previousTailPointer = 0;
        }

        Object answer = queueADT.queue[previousTailPointer];
        queueADT.tailPointer = previousTailPointer;
        queueADT.length--;

        ensureCapacity(queueADT);

        return answer;
    }


}
