import queue.ArrayQueue;
import queue.ArrayQueueADT;
import queue.ArrayQueueModule;

public class QueuesTest {

    public static void main(String[] args) {
        ArrayQueue q = new ArrayQueue();
        ArrayQueueADT queueADT = new ArrayQueueADT();
        //ArrayQueueModule queueModule = new ArrayQueueModule();

        //ArrayQueueModule.clear();

        for (int i = 0; i < 512; i++) {
            //q.enqueue(i);
            //ArrayQueueADT.enqueue(queueADT, i);
            ArrayQueueModule.push(i % 6);
        }

        //System.out.println("SIZE after adding: " + ArrayQueueADT.size(queueADT));
        //System.out.println("SIZE after adding: " + q.size());
        System.out.println("SIZE after adding: " + ArrayQueueModule.size());
        System.out.flush();

        for (int i = 0; i < 512; i++) {
            //int t = (Integer) ArrayQueueADT.queue(queueADT);
            //int t = (Integer) q.queue();
            //int t = (Integer) ArrayQueueModule.queue();
            //System.out.println(ArrayQueueModule.queue());
            //System.out.println(t);

            Object o = ArrayQueueModule.remove();

            if ((Integer) o != i % 6) {
                System.out.println("WARNING!!!" + i % 6 + "!=" + o + " at point #" + i);
                break;
            }
        }

    }
}
