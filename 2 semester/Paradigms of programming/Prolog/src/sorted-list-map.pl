% map_get(ListMap, Key, Value)
% map_put(ListMap, Key, Value, Result)
% map_remove(ListMap, Key, Result)

% contains(What, Where)
contains(V, [V|_]) :- !.
contains(V, [H|T]) :- contains(V,T).

% concat(L1, L2, Result)
concat([], B, B).
concat([H|T], B, R) :- concat(T, B, NT), R=[H|NT],!.

%emplace(What, Where, Result) -- appends What to the head of Where and returns as Result
emplace(What, Where, Result) :- concat([What], Where, Result),!.

%insert(What, List, Result)
insert(What, [], [What]).
insert(What, ListMap, Result) :- concat(ListMap, [What], Result),!.

%sorted-by-key-insert: sorted_insert((K,V), Where, Result)
sorted_insert((K,V), [], R) :- R = [(K,V)],!.
sorted_insert((K,V), [(K, _) | T], Result) :- emplace((K,V), T, Result),!.
sorted_insert((K,V), [(LK, LV) | T], Result) :- (LK =< K), sorted_insert((K,V), T, IT), emplace((LK, LV), IT, Result),!.
sorted_insert((K,V), [(GK, GV) | T], Result) :- (GK > K), emplace((GK, GV), T, PR), emplace((K,V), PR, Result),!.


% map_get(ListMap, Key, Value)
map_get(ListMap, Key, Value) :- contains((Key,Value), ListMap).

% map_put(ListMap, Key, Value, Result)
map_put([], Key, Value, Result) :- Result = [(Key, Value)],!.	%� ������ map ������ ����� ������ �������� ����
map_put([(Key,_) | T], Key, Value, Result) :- emplace((Key,Value), T, Result),!. %���� ��������� ���� -- �������� ���
map_put(ListMap, Key, Value, Result) :- not (contains((Key, _), ListMap)), sorted_insert((Key, Value), ListMap, Result),!. %���� ����� � map ���, �� ��������� ����� ����
map_put([H|T], Key, Value, Result) :- (contains((Key, _), T)), map_put(T, Key, Value, IM), concat([H], IM, Result),!. %���� ���� � map ���� -- �� ���� ��� � ������, ����� ����� ��������

% map_remove(ListMap, Key, Result)
map_remove(M, K, R) :- not (contains((K,_), M)), R=M,!.
map_remove([], _, []),!.
map_remove([(Key, _) | T], Key, R) :- R=T,!.
map_remove([H | T], Key, Result) :- map_remove(T, Key, RT), emplace(H, RT, Result),!.

%map_remove([(-3,c),(0,u)],0,V).


%1: map_put([], 2, 10, R).
%2: map_put([(6,11)], 2, 10, R).
%map_put([(11,40), (13, 30), (15,20), (16,50)], 2, 10, R).
%insert((10,20), [(15,60),(2,4)], Mp).
% Test: map_get([(5,2),(6,11),(10,4),(11,40),(13,10)], 10, 4).
% map_put([(2,10)], 4, 16, R).
% sorted_insert((4,16), [(2,10)], R).
% map_put([(8,11),(2,10)], 4, 16, R).
% map_put([(8,11), (4,15), (2,10)], 4, 16, R).
%contains((2,_),[(-3,c),(0,u)]).