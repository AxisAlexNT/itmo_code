all_distinct([]).
all_distinct([H | T]) :- \+ member(H, T), all_distinct(T).

choose([], _).
choose([H | T], Values) :- member(H, Values), choose(T, Values), \+ member(H, T).

left(V1, V2, [V1, V2 | _]) :- !.
left(V1, V2, [H | T]) :- left(V1, V2, T).

next(V1, V2, R) :- left(V1, V2, R).
next(V1, V2, R) :- left(V2, V1, R).

solve(W, Z, R) :-
  R = [house(N1, C1, P1, D1, S1), house(N2, C2, P2, D2, S2), house(N3, C3, P3, D3, S3), house(N4, C4, P4, D4, S4), house(N5, C5, P5, D5, S5)], % �� ����� ����� ���� �����.
  D3 = mlk,                                     % � ����������� ���� ���� ������.
  N1 = nrg,                                     % �������� ���� � ������ ����.
  member(house(eng, red, _  ,   _, _  ), R),    % ���������� ���� � ������� ����.
  member(house(spn, _  , dog,   _, _  ), R),    % � ������� ���� ������.
  member(house(_  , grn, _  , cof, _  ), R),    % � ������ ���� ���� ����.
  member(house(ukr, _  , _  , tea, _  ), R),    % �������� ���� ���.
  member(house(_  , _  , slu, _  , old), R),    % ���, ��� ����� Old Gold, �������� ������.
  member(house(_  , yel, _  , _  , koo), R),    % � ����� ���� ����� Kool.
  member(house(_  , _  , _  , ora, luk), R),    % ���, ��� ����� Lucky Strike, ���� ������������ ���.
  member(house(jap, _  , _  , _  , par), R),    % ������ ����� Parliament.
  left(house(_  , whi, _  , _  , _  ),
       house(_  , grn, _  , _  , _  ), R),  % ������ ��� ����� ����� ������ �� ������ ����.
  next(house(_  , _  , _  , _  , che),
       house(_  , _  , fox, _  , _  ), R),  % ����� ����, ��� ����� Chesterfield, ������ ����.
  next(house(_  , _  , hrs, _  , _  ),
       house(_  , _  , _  , _  , koo), R),  % � ���� �� ��������� � ���, � ������� ������ ������, ����� Kool.
  next(house(nrg, _  , _  , _  , _  ),
       house(_  , blu, _  , _  , _  ), R),  % �������� ���� ����� � ����� �����.
  choose([N1, N2, N3, N4, N5], [eng, spn, ukr, jap, nrg]),
  choose([C1, C2, C3, C4, C5], [red, grn, yel, whi, blu]),
  choose([P1, P2, P3, P4, P5], [dog, slu, fox, hrs, zeb]),
  choose([D1, D2, D3, D4, D5], [mlk, cof, tea, ora, wat]),
  choose([S1, S2, S3, S4, S5], [old, koo, luk, par, che]),
  member(house(W  , _  , _  ,  wat, _  ), R), % ��� ���� ����?
  member(house(Z  , _  , zeb,  _  , _  ), R). % ��� ������ �����?
