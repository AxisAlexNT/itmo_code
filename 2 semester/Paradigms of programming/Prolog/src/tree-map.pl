% map_put(ListMap, Key, Value, Result)
% map_remove(ListMap, Key, Result)

% contains(What, Where)
contains(V, [V|_]) :- !.
contains(V, [H|T]) :- contains(V,T).

% concat(L1, L2, Result)
concat([], B, B).
concat([H|T], B, R) :- concat(T, B, NT), R=[H|NT],!.

%emplace(What, Where, Result) -- appends What to the head of Where and returns as Result
emplace(What, Where, Result) :- concat([What], Where, Result),!.

%insert(What, List, Result)
insert(What, [], [What]).
insert(What, ListMap, Result) :- concat(ListMap, [What], Result),!.

%sorted-by-key-insert: sorted_insert((K,V), Where, Result)
sorted_insert((K,V), [], R) :- R = [(K,V)],!.
sorted_insert((K,V), [(K, _) | T], Result) :- emplace((K,V), T, Result),!.
sorted_insert((K,V), [(LK, LV) | T], Result) :- (LK =< K), sorted_insert((K,V), T, IT), emplace((LK, LV), IT, Result),!.
sorted_insert((K,V), [(GK, GV) | T], Result) :- (GK > K), emplace((GK, GV), T, PR), emplace((K,V), PR, Result),!.


% map_put(ListMap, Key, Value, Result)
%map_put([], Key, Value, Result) :- Result = [(Key, Value)],!.	%� ������ map ������ ����� ������ �������� ����
%map_put([(Key,_) | T], Key, Value, Result) :- emplace((Key,Value), T, Result),!. %���� ��������� ���� -- �������� ���
%map_put(ListMap, Key, Value, Result) :- not (contains((Key, _), ListMap)), sorted_insert((Key, Value), ListMap, Result),!. %���� ����� � map ���, �� ��������� ����� ����
%map_put([H|T], Key, Value, Result) :- (contains((Key, _), T)), map_put(T, Key, Value, IM), concat([H], IM, Result),!. %���� ���� � map ���� -- �� ���� ��� � ������, ����� ����� ��������

% map_remove(ListMap, Key, Result)
%map_remove(M, K, R) :- not (contains((K,_), M)), R=M,!.
%map_remove([], _, []),!.
%map_remove([(Key, _) | T], Key, R) :- R=T,!.
%map_remove([H | T], Key, Result) :- map_remove(T, Key, RT), emplace(H, RT, Result),!.

%map_remove([(-3,c),(0,u)],0,V).


%1: map_put([], 2, 10, R).
%2: map_put([(6,11)], 2, 10, R).
%map_put([(11,40), (13, 30), (15,20), (16,50)], 2, 10, R).
%insert((10,20), [(15,60),(2,4)], Mp).
% Test: map_get([(5,2),(6,11),(10,4),(11,40),(13,10)], 10, 4).
% map_put([(2,10)], 4, 16, R).
% sorted_insert((4,16), [(2,10)], R).
% map_put([(8,11),(2,10)], 4, 16, R).
% map_put([(8,11), (4,15), (2,10)], 4, 16, R).
%contains((2,_),[(-3,c),(0,u)]).


wrap_cart([], R) :- R=[],!.
wrap_cart([(K,V) | T], R) :- wrap_cart(T,NT), rand_int(99999999999999999,RND), emplace((K, RND, V), NT, R),!.

% wrap_cart([(10,5), (4, 3), (2,1), (1, t)],R).

%[(X, Y, Data, Left, Right)]
% node is (X, Y, Data, Left, Right), X -- Key, Y -- random priority, Data -- map's value, left/right -- subtrees

%tree_build(ListMap, TreeMap)
tree_build([], TreeMap) :- TreeMap=[],!.
tree_build([(K,V) | T], TreeMap) :- tree_build(T, PT), add(PT, K, V, TreeMap),!.

% tree_build([], T).
% tree_build([(2,10)], T).


split([], K, R) :- R=[[], []],!.
split([(X, Y, Data, Left, Right)], K, R) :-
			(K > X), split(Right, K, [T1, T2]), R=[[(X, Y, Data, Left, T1)], T2],!.
split([(X, Y, Data, Left, Right)], K, R) :-
			(K =< X), split(Left, K, [T1, T2]), R=[T1, [(X, Y, Data, T2, Right)]],!.

merge(T1, [], R) :- R=T1,!.
merge([], T2, R) :- R=T2,!.
merge([(T1X, T1Y, T1Data, T1Left, T1Right)], [(T2X, T2Y, T2Data, T2Left, T2Right)], R) :-
						(T1Y > T2Y),
						merge(T1Right, [(T2X, T2Y, T2Data, T2Left, T2Right)], NT1R),
						R=[(T1X, T1Y, T1Data, T1Left, NT1R)],!.
merge([(T1X, T1Y, T1Data, T1Left, T1Right)], [(T2X, T2Y, T2Data, T2Left, T2Right)], R) :-
						(T1Y =< T2Y),
						merge([(T1X, T1Y, T1Data, T1Left, T1Right)], T2Left, NT2L),
						R=[(T2X, T2Y, T2Data, NT2L, T2Right)],!.


insert([], (X,Y,D), R) :- R=[(X,Y,D,[], [])],!.
insert(T, (NX, NY, ND), Result) :- 
	split(T, NX, [L, R]),
	merge(L, [(NX, NY, ND, [], [])] , MLN),
	merge(MLN, R, Result),!.


add(T, K, V, R) :- rand_int(99999999999999999,RND), insert(T, (K, RND, V), R),!.

tree_contains([(Key, _, Data, _, _)], Key) :- !.
tree_contains([(K, _, Data, Left, _)], Key) :- (Key < K), tree_contains(Left, Key),!.
tree_contains([(K, _, Data, _, Right)], Key) :- (K < Key), tree_contains(Right, Key),!.


% add([], 5, 6, C1).
% add([], 5, 6, C1), add(C1, 4,8, C2).

% tree_build(ListMap, TreeMap), �������� ������ �� �������������� ������ ��� ����-��������;
% map_get(TreeMap, Key, Value). 


% insert(T, (NX, NY, ND), Result)


% map_get(TreeMap, Key, Value) -- �� map � key ������� value? ��, ��������� ��������������

map_get([(Key, _, Data, _, _)], Key, Value) :- Value=Data,!.
map_get([(K, _, Data, Left, _)], Key, Value) :- (Key < K), map_get(Left, Key, Value),!.
map_get([(K, _, Data, _, Right)], Key, Value) :- (K < Key), map_get(Right, Key, Value),!.

% add([], 5, 6, C1), add(C1, 4,8, C2), tree_map_get(C2, 4, V).

% tree_build([(7,u), (6,af)], TM).
% tree_build([(7,u), (6,af)], TM), map_get(TM, 6,V6), map_get(TM, 7,V7).
% add([], 7, u, C1), add(C1, 6,af, C2), map_get(TM, 6,V6), map_get(TM, 7,V7).

% tree_build([(-9, i), (-5, ku), (9,n)], TM), map_get(TM, -9, V).

%map_put(TreeMap, Key, Value, Result)
map_put(TreeMap, Key, Value, Result) :- map_remove(TreeMap, Key, TM), add(TM, Key, Value, PR), Result=PR,!.


%map_remove(TreeMap, Key, Result)
map_remove(T, K, Result) :-
			PK is K-1,
			NK is K+1,
			split(T, K, [LL, LR]),
			split(LR, NK, [_, GR]),
			merge(LL, GR, Result),!.


% tree_build([(-9, xnuz), (2, uwbs)], T), map_remove(T, 2, T2), map_get(T2, 2, V).
% tree_build([(-9, xnuz), (2, uwbs)], T), map_remove(T, 2, T2), map_get(T2, -9, V).
% tree_build([(-9, xnuz), (2, uwbs)], T), map_remove(T, 2, T2), map_get(T2, -9, V), map_remove(T2, 9, T3), BV=(T2=T3).
% tree_build([(-9, xnuz), (2, uwbs)], T), map_remove(T, 2, T2), map_get(T2, -9, V), map_remove(T2, 9, T3), map_put()

map_replace(T, K, V, Result) :- tree_contains(T, K),
	map_remove(T, K, PT),
	map_put(PT, K, V, Result),!.

map_replace(T, K, V, Result) :- not (tree_contains(T, K)), Result=T,!.