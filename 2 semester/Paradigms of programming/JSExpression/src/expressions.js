"use strict";

let expressions = (function () {
    const variablesNamesIndices =
        {
            "x": 0,
            "y": 1,
            "z": 2
        };

    function GeneralExpression(_evaluationMethod, symbol) {
        this.evaluate = _evaluationMethod;
        this.symbol = symbol;
    }

    GeneralExpression.prototype.toString = function () {
        return this.symbol
    };

    GeneralExpression.prototype.prefix = function () {
        return this.symbol
    };

    const defaultOperationEvaluationMethod = function (...vals) {
        return this.operation(...this.subterms.map(function (term) {
            return term.evaluate(...vals);
        }))
    };


    function Operator(operation, operationSymbol, derivationMethod, ...subterms) {
        GeneralExpression.call(this, defaultOperationEvaluationMethod, operationSymbol);
        this.operation = operation;
        this.subterms = subterms;
        this.symbol = operationSymbol;
        this.derivationMethod = derivationMethod;
    }


    Operator.prototype.toString = function () {
        return this.subterms.reduce((preString, currentElement) => {
            return preString + currentElement.toString() + " "
        }, "") + this.symbol;
    };

    Operator.prototype.prefix = function () {
        let argsPrefix = this.subterms.reduce((preString, currentElement) => {
            return preString + currentElement.prefix() + " "
        }, "");


        if (this.subterms.length < 1) {
            return (this.symbol + " " + argsPrefix).trim();
        } else {
            return "(" + (this.symbol + " " + argsPrefix).trim() + ")";
        }
    };

    Operator.prototype.diff = function (dv) {
        return this.derivationMethod(dv, ...this.subterms);
    };


    function Add(x, y) {
        Operator.call(this, (a, b) => Number(a) + Number(b), "+", (dv, a, b) => new Add(a.diff(dv), b.diff(dv)), x, y);
    }

    function Subtract(x, y) {
        Operator.call(this, (a, b) => Number(a) - Number(b), "-", (dv, a, b) => new Subtract(a.diff(dv), b.diff(dv)), x, y);
    }

    function Divide(x, y) {
        Operator.call(this, (a, b) => Number(a) / Number(b), "/", (dv, a, b) => new Divide(new Subtract(new Multiply(a.diff(dv), b), new Multiply(b.diff(dv), a)), new Multiply(b, b)), x, y);
    }

    function Multiply(x, y) {
        Operator.call(this, (a, b) => Number(a) * Number(b), "*", (dv, a, b) => new Add(new Multiply(a.diff(dv), b), new Multiply(a, b.diff(dv))), x, y);
    }

    function Negate(x) {
        Operator.call(this, (a) => -Number(a), "negate", (dv, a) => new Negate(a.diff(dv)), x);
    }

    function Const(value) {
        Operator.call(this, () => value, value.toString(), () => new Const(0));
    }


    Add.prototype = Operator.prototype;
    Subtract.prototype = Operator.prototype;
    Multiply.prototype = Operator.prototype;
    Divide.prototype = Operator.prototype;
    Negate.prototype = Operator.prototype;
    Const.prototype = Operator.prototype;

    function Variable(name) {
        GeneralExpression.call(this,
            function (...args) {
                return args[variablesNamesIndices[name]];
            },
            name.toString());

        this.diff = function (dv) {
            if (dv === this.symbol) {
                return new Const(1);
            } else {
                return new Const(0);
            }
        };
    }

    Variable.prototype = GeneralExpression.prototype;


    function tokenize(str) {
        return str.split(" ");
    }


    function parseObjectExpression(str) {
        let tokens = tokenize(str);

        let stack = [];
        let pointer = 0;

        tokens.map((token) => {
            if (pointer < 0) {
                console.log("Stack underflow!")
            }
            switch (token) {
                case "+":
                    stack[pointer - 2] = new Add(stack[pointer - 2], stack[pointer - 1]);
                    --pointer;
                    break;
                case "-":
                    stack[pointer - 2] = new Subtract(stack[pointer - 2], stack[pointer - 1]);
                    --pointer;
                    break;
                case "*":
                    stack[pointer - 2] = new Multiply(stack[pointer - 2], stack[pointer - 1]);
                    --pointer;
                    break;
                case "/":
                    stack[pointer - 2] = new Divide(stack[pointer - 2], stack[pointer - 1]);
                    --pointer;
                    break;
                case "negate":
                    stack[pointer - 1] = new Negate(stack[pointer - 1]);
                    break;
                case "x":
                case "y":
                case "z":
                    stack[pointer] = new Variable(token);
                    ++pointer;
                    break;
                case "":
                    break;
                default:
                    stack[pointer++] = new Const(Number(token));
                    break;
            }
        });


        if (pointer === 1) {
            return stack[pointer - 1];
        } else {
            console.log("Invalid expression! Stack Overflow");
        }
    }


    function PrefixParseError(message, position) {
        this.position = position;
        this.message = "At position: " + position.toString() + " Error:" + message;
    }

    PrefixParseError.prototype = Object.create(Error.prototype);
    PrefixParseError.prototype.constructor = PrefixParseError;


    function ExpectationFailedError(expected, found, position) {
        PrefixParseError.call(this, "Expected token " + expected.toString() + ", but found " + found.toString(), position);
    }

    ExpectationFailedError.prototype = PrefixParseError;
    ExpectationFailedError.constructor = ExpectationFailedError;


    function expectSymbol(str, expectation, pos) {
        if (str.toString() !== expectation.toString()) {
            throw new ExpectationFailedError(expectation.toString(), str.toString(), pos);
        }
    }


    function skipSpaces(str, pos) {
        while (str[pos] === " ") {
            pos++
        }

        return pos;
    }


    function nextToken(str, pos) {

        pos = skipSpaces(str, pos);

        if ((str[pos] === "(") || (str[pos] === ")")) {
            return {token: str[pos], pos: pos + 1};
        }

        let token = "";

        while ((str[pos] !== " ") && (str[pos] !== "(") && (str[pos] !== ")") && (pos < str.length)) {
            token += str[pos++];
        }

        return {token: token, pos: pos};

    }


    function innerParsePrefix(expression, position) {
        if (position >= expression.length) {
            throw new PrefixParseError("Expression index out of bounds", position);
        }

        let token = nextToken(expression, position);

        let left;
        let right;
        let argument;

        switch (token.token) {
            case "(":
                argument = innerParsePrefix(expression, token.pos);
                let pos = skipSpaces(expression, argument.pos);
                expectSymbol(expression[pos], ")", pos);
                return {term: argument.term, pos: pos + 1};
            case ")":
                throw new PrefixParseError("Unexpected closing bracket ')'", position);
            case "+":
                left = innerParsePrefix(expression, token.pos);
                right = innerParsePrefix(expression, left.pos);
                return {term: new Add(left.term, right.term), pos: right.pos};
            case "-":
                left = innerParsePrefix(expression, token.pos);
                right = innerParsePrefix(expression, left.pos);
                return {term: new Subtract(left.term, right.term), pos: right.pos};
            case "/":
                left = innerParsePrefix(expression, token.pos);
                right = innerParsePrefix(expression, left.pos);
                return {term: new Divide(left.term, right.term), pos: right.pos};
            case "*":
                left = innerParsePrefix(expression, token.pos);
                right = innerParsePrefix(expression, left.pos);
                return {term: new Multiply(left.term, right.term), pos: right.pos};
            case "negate":
                argument = innerParsePrefix(expression, token.pos);
                return {term: new Negate(argument.term), pos: argument.pos};
            case "x":
            case "y":
            case "z":
                return {term: new Variable(token.token), pos: token.pos};
            default:
                let num;
                try {
                    num = Number(token.token);
                    if (Number.isFinite(num)) {
                        return {term: new Const(Number(num)), pos: token.pos};
                    } else {
                        throw new PrefixParseError("Parsed number is not a finite number!", token.pos);
                    }
                } catch (e) {
                    throw new PrefixParseError("Failed to parse numeric constant!", token.pos);
                }
        }
    }


    function parsePrefix(str) {
        let result;
        if (str.length > 0) {
            result = innerParsePrefix(str, 0);
        }

        let pos = skipSpaces(str, result.pos);

        if (pos < str.length) {
            throw new PrefixParseError("Expression is incorrect, can not parsed wholly: " + pos + " " + str.length, pos);
        }

        return result.term;
    }


    const parse = (str) => parseObjectExpression(str);


    return {
        Add: Add,
        Subtract: Subtract,
        Multiply: Multiply,
        Divide: Divide,
        Negate: Negate,
        Const: Const,
        Variable: Variable,
        parse: parse,
        parsePrefix: parsePrefix
    };
})();