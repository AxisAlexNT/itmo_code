"use strict";

function ternaryOperation(x, y, z, operation) {
    return function (a, b, c) {
        return operation(x(...arguments), y(...arguments), z(...arguments));
    }
}

function binaryOperation(x, y, operation) {
    return function () {
        return operation(x(...arguments), y(...arguments));
    }
}

function unaryOperation(x, operation) {
    return function () {
        return operation(x(...arguments));
    }
}


// function generalOperation(operation, ...args)
// {
//     let applied = args.map(operation)
// }


const negate = x => unaryOperation(x, t => -t);

const add = (x, y) => binaryOperation(x, y, (a, b) => (a + b));

const multiply = (x, y) => binaryOperation(x, y, (a, b) => (a * b));

const divide = (x, y) => binaryOperation(x, y, (a, b) => (a / b));

const subtract = (x, y) => binaryOperation(x, y, (a, b) => (a - b));

const iff = (x, y, z) => ternaryOperation(x, y, z, (a, b, c) => ((a >= 0) ? b : c));

const abs = x => unaryOperation(x, t => Math.abs(t));


const cnst = function (val) {
    return () => val;
};

function variable(name) {
    return (x, y, z) => {
        switch (name) {
            case "x":
                return x;
            case "y":
                return y;
            case "z":
                return z;
            default:
                console.log("Unrecognized variable name!");
                break;
        }
    }
}

const one = cnst(1);
const two = cnst(2);


function tokenize(str) {
    return str.split(" ");

    // let result = [];
    // let pointer = -1;
    // let currentToken = "";
    // for (const s of str) {
    //     if ((s === " ") || (s === "")) {
    //         result[pointer++] = currentToken;
    //         currentToken = "";
    //     } else {
    //         currentToken += s;
    //     }
    // }
    //
    // result[pointer] = currentToken;
    // return result;
}


function parse(str) {
    let tokens = tokenize(str);

    let stack = [];
    let pointer = 0;

    tokens.map((token) => {
        if (pointer < 0) {
            console.log("Stack underflow!")
        }
        switch (token) {
            case "+":
                stack[pointer - 2] = add(stack[pointer - 2], stack[pointer - 1]);
                --pointer;
                break;
            case "-":
                stack[pointer - 2] = subtract(stack[pointer - 2], stack[pointer - 1]);
                --pointer;
                break;
            case "*":
                stack[pointer - 2] = multiply(stack[pointer - 2], stack[pointer - 1]);
                --pointer;
                break;
            case "/":
                stack[pointer - 2] = divide(stack[pointer - 2], stack[pointer - 1]);
                --pointer;
                break;
            case "e":
                stack[pointer++] = cnst(Math.PI);
                break;
            case "pi":
                stack[pointer++] = cnst(Math.E);
                break;
            case "negate":
                stack[pointer - 1] = negate(stack[pointer - 1]);
                break;
            case "abs":
                stack[pointer - 1] = abs(stack[pointer - 1]);
                break;
            case "iff":
                stack[pointer - 3] = iff(stack[pointer - 3], stack[pointer - 2], stack[pointer - 1]);
                pointer -= 2;
                break;
            case "one":
                stack[pointer++] = cnst(1);
                break;
            case "two":
                stack[pointer++] = cnst(2);
                break;
            case "x":
            case "y":
            case "z":
                stack[pointer] = variable(token);
                ++pointer;
                break;
            case "":
                break;
            default:
                stack[pointer++] = cnst(Number(token));
                break;
        }
    });


    if (pointer === 1) {
        return stack[pointer - 1];
    } else {
        console.log("Invalid expression! Stack Overflow");
    }
}