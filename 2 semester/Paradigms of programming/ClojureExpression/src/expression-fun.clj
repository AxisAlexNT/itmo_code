(defn generalOperator [f]
  (fn [& terms]
    (fn [vals]
      (apply f (map #(%1 vals) terms)))))




;(def terms [['add + '+] ['subtract - '-] ['multiply * '*] ['divide / '/]])

(def add (generalOperator +))
(def subtract (generalOperator -))
(def multiply (generalOperator *))
(def negate (generalOperator #(- %)))
(def divide (generalOperator #(/ %1 (double %2))))


(defn inneravg [& xs] (/ (apply + xs) (count xs)))
(defn innermed [& xs] (nth (sort xs) (Math/floor (/ (count xs) 2))))

(defn innermin [& xs] (reduce #(Math/min %1 %2) xs))
(defn innermax [& xs] (reduce #(Math/max %1 %2) xs))

(def avg (generalOperator inneravg))
(def med (generalOperator innermed))
(def min (generalOperator innermin))
(def max (generalOperator innermax))



(def terms {'+ add '- subtract '* multiply '/ divide 'negate negate 'avg avg 'med med 'min min 'max max})

(defn constant [x]
  (fn [vals] x))


(defn variable [name]
  (fn [vals]
    (double (get vals name))))





(letfn [(parseTerm [expr]
          (cond
            (and (or (coll? expr) (seq? expr)) (empty? expr)) nil
            (and (list? expr) (not (empty? expr)) (contains? terms (first expr))) (apply (parseArg (first expr)) (map parseArg (rest expr))) ; When it is a form ==> Apply it
            :else (parseArg expr)))



        (parseArg [arg]
          (cond
            (list? arg) (parseTerm arg)
            (number? arg) (constant (double (num arg)))     ; When it is a numeric constant
            (and (symbol? arg) (contains? terms arg)) (get terms arg) ; When it is a form name
            (and (symbol? arg) (not (contains? terms arg))) (variable (str arg)) ; When it is a variable name
            :else (parseTerm arg)))]
  (defn parseFunction [st] (parseTerm (read-string st))))



; ((parseFunction "(med 1 2 6)") {"x" 5})