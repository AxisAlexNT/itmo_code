(definterface IOperator
  (evaluate [args])
  (diff [dv])
  (getSymbol [])
  (toStr []))



;(deftype GeneralOperator [f symbol subterms]
;  IOperator
;  (evaluate [this args] (apply f (map #(.evaluate % args) subterms)))
;
;  (getSymbol [this] symbol)
;  (toStr [this] (concat "(" (.getSymbol this) " " (map #(.toStr %) subterms) ")")))


(deftype JConstant [x]
  IOperator
  (evaluate [this args] x)

  (getSymbol [this] (if (double? x) (format "%.1f" x) (str x)))

  (toStr [this] (if (double? x) (format "%.1f" x) (str x)))

  (diff [this dv] (JConstant. 0)))


(defn mydivbin [a b] (/ a (double b)))

(defn mydiv [& xs] (reduce mydivbin xs))


(deftype JAdd [subterms]
  IOperator
  (evaluate [this args] (apply + (map #(.evaluate % args) subterms)))

  (getSymbol [this] "+")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) (apply str (map #(apply str " " (.toStr %)) subterms)) ")"))

  (diff [this dv] (JAdd. (map #(.diff % dv) subterms))))

(deftype JSubtract [subterms]
  IOperator
  (evaluate [this args] (apply - (map #(.evaluate % args) subterms)))

  (getSymbol [this] "-")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) (apply str (map #(apply str " " (.toStr %)) subterms)) ")"))

  (diff [this dv] (JSubtract. (map #(.diff % dv) subterms))))

(deftype JMultiply [subterms]
  IOperator
  (evaluate [this args] (apply * (map #(.evaluate % args) subterms)))

  (getSymbol [this] "*")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) (apply str (map #(apply str " " (.toStr %)) subterms)) ")"))
  (diff [this dv] (JAdd. [(JMultiply. [(.diff (nth subterms 0) dv) (nth subterms 1)]) (JMultiply. [(.diff (nth subterms 1) dv) (nth subterms 0)])])))

(deftype JDivide [subterms]
  IOperator
  (evaluate [this args] (apply mydiv (map #(double (.evaluate % args)) subterms)))

  (getSymbol [this] "/")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) (apply str (map #(apply str " " (.toStr %)) subterms)) ")"))
  (diff [this dv] (JDivide. [
                             (JSubtract. [(JMultiply. [(.diff (first subterms) dv) (last subterms)]) (JMultiply. [(.diff (last subterms) dv) (first subterms)])])

                             (JMultiply. [(last subterms) (last subterms)])])))

(deftype JNegate [arg]
  IOperator
  (evaluate [this args] (- (.evaluate arg args)))

  (getSymbol [this] "negate")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) " " (.toStr arg) ")"))

  (diff [this dv] (JNegate. (.diff arg dv))))


(deftype JSgn [arg]
  IOperator
  (evaluate [this args] (Math/signum (.evaluate arg args)))

  (getSymbol [this] "negate")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) " " (.toStr arg) ")"))

  (diff [this dv] (JSgn. (.diff arg dv))))


(deftype JSqrt [arg]
  IOperator
  (evaluate [this args] (Math/sqrt (Math/abs (.evaluate arg args))))

  (getSymbol [this] "sqrt")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) " " (.toStr arg) ")"))

  (diff [this dv] (JDivide. [(JMultiply. [(JSgn. arg) (.diff arg dv)])
                             (JMultiply. [(JConstant. 2) (JSqrt. arg)])])))


(deftype JSqr [arg]
  IOperator
  (evaluate [this args] (* (.evaluate arg args) (.evaluate arg args)))

  (getSymbol [this] "square")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) " " (.toStr arg) ")"))

  (diff [this dv] (JMultiply. [(JConstant. 2) (.diff arg dv) arg])))


(declare Sinh Cosh)

(deftype JSinh [arg]
  IOperator
  (evaluate [this args] (Math/sinh (.evaluate arg args)))

  (getSymbol [this] "sinh")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) " " (.toStr arg) ")"))

  (diff [this dv] (JMultiply. [(Cosh arg) (.diff arg dv)])))


(deftype JCosh [arg]
  IOperator
  (evaluate [this args] (Math/cosh (.evaluate arg args)))

  (getSymbol [this] "cosh")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) " " (.toStr arg) ")"))

  (diff [this dv] (JMultiply. [(Sinh arg) (.diff arg dv)])))



(deftype JVariable [name]
  IOperator
  (evaluate [this args] (get args name))

  (getSymbol [this] name)

  (toStr [this] name)

  (diff [this dv] (if (= dv name) (JConstant. 1) (JConstant. 0))))






(defn Constant [x] (JConstant. x))
(def Const Constant)
(defn Add [& parts] (JAdd. parts))
(defn Subtract [& parts] (JSubtract. parts))
(defn Multiply [& parts] (JMultiply. parts))
(defn Divide [& parts] (JDivide. parts))
(defn Negate [x] (JNegate. x))
(defn Sqrt [x] (JSqrt. x))
(defn Sqr [x] (JSqr. x))
(defn Square [x] (JSqr. x))
(defn Sinh [x] (JSinh. x))
(defn Cosh [x] (JCosh. x))
(defn Variable [name] (JVariable. name))


(defn toString [x] (.toStr x))

(defn evaluate [expression vars] (.evaluate expression vars))

(defn diff [expr dv] (.diff expr dv))


(def terms {'+ Add '- Subtract '* Multiply '/ Divide 'negate Negate 'sqrt Sqrt 'sqr Sqr 'square Square 'sinh Sinh 'cosh Cosh})

(letfn [(parseTerm [expr]
          (cond
            (and (or (coll? expr) (seq? expr)) (empty? expr)) nil
            (and (list? expr) (not (empty? expr)) (contains? terms (first expr))) (apply (parseArg (first expr)) (map parseArg (rest expr))) ; When it is a form ==> Apply it
            :else (parseArg expr)))



        (parseArg [arg]
          (cond
            (list? arg) (parseTerm arg)
            (number? arg) (Constant (double (num arg)))     ; When it is a numeric constant
            (and (symbol? arg) (contains? terms arg)) (get terms arg) ; When it is a form name
            (and (symbol? arg) (not (contains? terms arg))) (Variable (str arg)) ; When it is a variable name
            :else (parseTerm arg)))]
  (defn parseObject [st] (parseTerm (read-string st))))
