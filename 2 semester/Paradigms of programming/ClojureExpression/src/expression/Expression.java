package expression;

import expression.exceptions.ArithmeticEvaluationException;

public interface Expression {
    int evaluate(int x) throws ArithmeticEvaluationException;
}
