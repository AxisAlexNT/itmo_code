package expression.parser;

/*
Grammar:


F' --> (+|-)SF' | empty
F  --> SF'
S' --> (*|/)TS' | empty
S  --> TS'|empty
T  --> -T|high T|low T|~T|(F)|x|y|z|I|F
I  --> 0..9 | 0..9I


Starting token: O
*/


import expression.*;
import expression.exceptions.GrammarMismatchException;
import expression.exceptions.InvalidIntegerException;
import expression.exceptions.UnexpectedSymbolException;

public class ExpressionParser implements Parser {
    private static final String highToken = "high";
    private static final String lowToken = "low";
    private static final char[] validTokenStartingSymbols = {'(', ')', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'x', 'y', 'z', '+', '-', '*', '/', ' ', 'l', 'h'};

    private static final int errorSubstringLength = 7;
    private static final String insertedErrorMessage = " >ERROR HERE> ";

    private String expression;

    private static int max(int a, int b) {
        return (a > b) ? a : b;
    }

    private static int min(int a, int b) {
        return (a < b) ? a : b;
    }

    @Override
    public TripleExpression parse(String inputString) throws GrammarMismatchException {
        expression = inputString;
        if (expression == null) {
            throw new GrammarMismatchException("Cannot parse NULL expression! Please ensure that expression is not null!");
        }

        if (expression.isEmpty()) {
            throw new GrammarMismatchException("Empty expression does not contain any tokens.", -1);
        }

        ParseResult parsedExpression = startParse();

        if (parsedExpression.endedPosition != inputString.length()) {
            //String msg = generateGrammarMismatchMessage("Unexpected symbol found which is not a valid prefix of any token", parsedExpression.endedPosition);
            throw new GrammarMismatchException("Unexpected symbol found which is not a valid prefix of any token", parsedExpression.endedPosition);
        }

        return parsedExpression.expression;
    }

    private ParseResult startParse() throws GrammarMismatchException {
        return parseFirstPriority(0);
    }


    private ParseResult parsePreFirstPriority(int position, Term leftS) throws GrammarMismatchException {
        position = skipWhiteSpace(position);

        if (position == expression.length()) {
            return new ParseResult(leftS, position);
        }

        ParseResult S;
        ParseResult preFRight;

        switch (expression.charAt(position)) {
            case '+':
                S = parseSecondPriority(position + 1);
                preFRight = parsePreFirstPriority(S.endedPosition, new CheckedAdd(leftS, S.expression));
                return new ParseResult(preFRight.expression, preFRight.endedPosition);
            case '-':
                S = parseSecondPriority(position + 1);
                preFRight = parsePreFirstPriority(S.endedPosition, new CheckedSubtract(leftS, S.expression));
                return new ParseResult(preFRight.expression, preFRight.endedPosition);
            default:
                return new ParseResult(leftS, position);

        }
    }


    private ParseResult parseThirdPriority(int position) throws GrammarMismatchException {
        position = skipWhiteSpace(position);

        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected token T (a number, negation, parentheses, high/low with an argument or a variable), but reached EOS", position);
        }

        ParseResult T, F;

        char currentSymbol = expression.charAt(position);

        switch (currentSymbol) {
            case '-':
                if (position < expression.length() - 1) {
                    if (Character.isDigit(expression.charAt(position + 1))) {
                        return tryParseInt(position);
                    } else {
                        T = parseThirdPriority(position + 1);
                        return new ParseResult(new CheckedNegate(T.expression), T.endedPosition);
                    }
                } else {
                    throw new GrammarMismatchException("Found an unary minus without a valid argument!", position);
                }
            case '(':
                int openingBracketPosition = position;
                T = parseFirstPriority(position + 1);
                position = skipWhiteSpace(T.endedPosition);
                if (position == expression.length()) {
                    throw new GrammarMismatchException("Expected closing bracket ')' for pairing '(' at position " + openingBracketPosition + ", but reached EOS", T.endedPosition, false);
                }

                if (expression.charAt(position) != ')') {
                    if (!isValidTokenBeginnig(expression.charAt(position))) {
                        throw new GrammarMismatchException("Unexpected symbol found which is not a valid prefix of any token", position);
                    }
                    throw new GrammarMismatchException("Expected closing bracket ')' for pairing '(' at position " + openingBracketPosition + ", but none was found", position, false);
                }

                return new ParseResult(T.expression, position + 1);
            case 'x':
            case 'y':
            case 'z':
                return new ParseResult(new Variable(String.valueOf(expression.charAt(position))), position + 1);
            case 'h':
                ensureToken(position, highToken);
                position += highToken.length();
                T = parseThirdPriority(position);
                return new ParseResult(new UnaryHigh(T.expression), T.endedPosition);
            case 'l':
                ensureToken(position, lowToken);
                position += lowToken.length();
                T = parseThirdPriority(position);
                return new ParseResult(new UnaryLow(T.expression), T.endedPosition);
            case ')':
                throw new GrammarMismatchException("Unexpected unpaired closing bracket ')'", position, false);
            default:
                if (Character.isDigit(expression.charAt(position))) {
                    return tryParseInt(position);
                } else {
                    throw new UnexpectedSymbolException("Unexpected symbol '" + expression.charAt(position) + "' starting new token", position);
                }
        }
    }

    private ParseResult parseFirstPriority(int position) throws GrammarMismatchException {
        position = skipWhiteSpace(position);

        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected token F (a sum or subtraction), but reached EOS", position, false);
        }

        ParseResult S = parseSecondPriority(position);
        return parsePreFirstPriority(S.endedPosition, S.expression);
    }

    private void ensureToken(int position, String testingToken) throws GrammarMismatchException {
        for (int i = 0; i < testingToken.length(); i++) {
            if (expression.charAt(position + i) != testingToken.charAt(i)) {
                throw new UnexpectedSymbolException("Probably misspelled '" + testingToken + "'", position);
            }
        }

        if (position + testingToken.length() < expression.length()) {
            if (Character.isAlphabetic(expression.charAt(position + testingToken.length()))) {
                throw new UnexpectedSymbolException("Token name " + testingToken + " is followed by an alphabetic character, probably misspelled", position);
            }
        }
    }

    private ParseResult parsePreSecondPriority(int position, Term leftT) throws GrammarMismatchException {
        position = skipWhiteSpace(position);

        if (position == expression.length()) {
            return new ParseResult(leftT, position);
        }

        ParseResult T;
        ParseResult preSRight;

        switch (expression.charAt(position)) {
            case '*':
                T = parseThirdPriority(position + 1);
                preSRight = parsePreSecondPriority(T.endedPosition, new CheckedMultiply(leftT, T.expression));
                return new ParseResult(preSRight.expression, preSRight.endedPosition);
            case '/':
                T = parseThirdPriority(position + 1);
                preSRight = parsePreSecondPriority(T.endedPosition, new CheckedDivide(leftT, T.expression));
                return new ParseResult(preSRight.expression, preSRight.endedPosition);
            default:
                return new ParseResult(leftT, position);
        }
    }

    private ParseResult parseSecondPriority(int position) throws GrammarMismatchException {
        position = skipWhiteSpace(position);
        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected token T, but reached EOS", position, false);
        }

        ParseResult T = parseThirdPriority(position);
        return parsePreSecondPriority(T.endedPosition, T.expression);
    }

    private boolean isValidTokenBeginnig(char testingChar) {
        for (char validChar : validTokenStartingSymbols) {
            if (testingChar == validChar) {
                return true;
            }
        }

        return false;
    }


    private String generateGrammarMismatchMessage(String message, int position) {
        String errorContainingSubstring = generateErrorSubstring(position);

        StringBuilder errorMessageBuilder = new StringBuilder();

        errorMessageBuilder.append(errorContainingSubstring).append("\r\n").append(message).append("\r\n");

        return errorMessageBuilder.toString();
    }


    private String generateErrorSubstring(int errorPosition) {
        StringBuilder errorSubstringBuilder = new StringBuilder();

        errorSubstringBuilder.append(expression);
        errorSubstringBuilder.insert(errorPosition, insertedErrorMessage);

        int errorSubstringStartPosition = max(0, errorPosition - errorSubstringLength);
        int errorSubstringEndPosition = min(expression.length() + insertedErrorMessage.length(), errorPosition + errorSubstringLength + insertedErrorMessage.length());

        errorSubstringBuilder.substring(errorSubstringStartPosition, errorSubstringEndPosition);

        return errorSubstringBuilder.toString();
    }


    private ParseResult tryParseInt(int position) throws GrammarMismatchException {
        int answer = 0;
        int previousAnswer = 1;
        boolean needNegation = false;
        boolean answerWasModified = false;

        if (expression.charAt(position) == '-') {
            needNegation = true;
            position++;
            previousAnswer = -1;
        }

        if (position == expression.length()) {
            //throw new GrammarMismatchException("Expected a negative integer, but only minus was parsed before EOS", position);
            throw new UnexpectedSymbolException(generateGrammarMismatchMessage("Expected a negative integer, but only minus was parsed before EOS", position), position);
        }

        while ((position < expression.length()) && Character.isDigit(expression.charAt(position))) {
            if (!needNegation) {
                answer = answer * 10 + (expression.charAt(position) - '0');
            } else {
                answer = answer * 10 - (expression.charAt(position) - '0');
            }

            if (!answerWasModified) {
                previousAnswer = answer;
                answerWasModified = true;
            }

            if (sign(answer) != sign(previousAnswer)) {
                throw new InvalidIntegerException("Number does not represent a valid integer. Valid integers range: -2147483648 <= int < 2147483648.", position);
            }

            position++;
        }

        if (!answerWasModified) {
            throw new GrammarMismatchException("Expected token I (a valid integer), but no number is represented", position);
        } else {
            return new ParseResult(new Const(answer), position);
        }
    }

    private int skipWhiteSpace(int position) {
        while ((position < expression.length()) && Character.isWhitespace(expression.charAt(position))) {
            position++;
        }

        return position;
    }


    private void raiseGrammarMismatchException(String message, int position) throws GrammarMismatchException {
        String exceptionMessage = String.format("%28s \t GrammarMismatchException: %s", generateErrorSubstring(position), message);

        throw new GrammarMismatchException(exceptionMessage, position);
    }


    private int sign(int x) {
        if (x == 0) {
            return 0;
        } else {
            return (x > 0) ? 1 : (-1);
        }
    }

    private enum MinOrMax {
        MIN,
        MAX
    }

    private class ParseResult {
        Term expression;
        int endedPosition;

        public ParseResult(Term _expression, int _endedPosition) {
            expression = _expression;
            endedPosition = _endedPosition;
        }
    }


}