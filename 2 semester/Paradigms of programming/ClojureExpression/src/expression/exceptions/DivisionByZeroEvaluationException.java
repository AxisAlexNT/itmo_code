package expression.exceptions;

import expression.Term;

public class DivisionByZeroEvaluationException extends ArithmeticEvaluationException {
    public DivisionByZeroEvaluationException(String message, Term nonEvaluatingTerm) {
        super(message, "Integer division by zero", nonEvaluatingTerm);
    }

    public DivisionByZeroEvaluationException(String message, Term nonEvaluatingTerm, boolean needToPrintRanges) {
        super(message, "Integer division by zero", nonEvaluatingTerm, needToPrintRanges);
    }
}
