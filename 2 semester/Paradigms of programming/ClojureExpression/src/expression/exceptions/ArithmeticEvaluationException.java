package expression.exceptions;

import expression.Term;

public class ArithmeticEvaluationException extends AbstractExpressionException {
    protected String shortMessage;

    public ArithmeticEvaluationException(String message, int errorPosition) {
        super(message, errorPosition);
    }

    public ArithmeticEvaluationException(String message, String _shortMessage, Term nonEvaluatingTerm) {
        super(message, nonEvaluatingTerm);
        shortMessage = _shortMessage;
    }

    public ArithmeticEvaluationException(String message, String _shortMessage, Term nonEvaluatingTerm, boolean needToPrintRanges) {
        super(message + ((needToPrintRanges) ? ("\nValid integers range: -2147483648 <= int < 2147483648\n") : ("")), nonEvaluatingTerm);
        shortMessage = _shortMessage;
    }

    @Override
    public String getShortMessage() {
        return shortMessage;
    }
}
