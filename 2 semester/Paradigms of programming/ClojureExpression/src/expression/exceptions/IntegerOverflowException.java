package expression.exceptions;

import expression.Term;

public class IntegerOverflowException extends ArithmeticEvaluationException {
    public IntegerOverflowException(String message, Term nonEvaluatingTerm) {
        super(message, "Integer overflow", nonEvaluatingTerm);
    }

    public IntegerOverflowException(String message, Term nonEvaluatingTerm, boolean needToPrintRanges) {
        super(message, "Integer overflow", nonEvaluatingTerm, needToPrintRanges);
    }
}
