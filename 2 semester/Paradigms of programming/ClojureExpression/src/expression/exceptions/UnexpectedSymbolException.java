package expression.exceptions;

public class UnexpectedSymbolException extends GrammarMismatchException {
    public UnexpectedSymbolException(String message, int position) {
        super(message, position);
    }
}
