package expression;

import expression.exceptions.ArithmeticEvaluationException;

public class UnaryLow extends UnaryOperator {
    public UnaryLow(Term argument) {
        super(argument, "low");
    }

    @Override
    protected int operation(int x) throws ArithmeticEvaluationException {
        return Integer.lowestOneBit(x);
    }

    @Override
    protected double operation(double x) {
        return 0;
    }
}
