package expression;

public class Variable extends Term {
    protected String name;

    public Variable(String _name) {
        this.name = _name;
    }

    @Override
    protected int doEvaluate(int x) {
        return x;
    }

    @Override
    protected double doEvaluate(double x) {
        return x;
    }

    @Override
    protected int doEvaluate(int x, int y, int z) {
        switch (name) {
            case "x":
                return x;
            case "y":
                return y;
            case "z":
                return z;
            default:
                //throw new Exception("Unrecognized variable name!");
                return 0;
        }
    }

    @Override
    protected String doToString() {
        return name;
    }
}
