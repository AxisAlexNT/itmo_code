(defn v+bin [va vb]
  {:pre [(== (count va) (count vb))]}
  (vec (map + va vb)))


(defn v-bin [va vb]
  {:pre [(== (count va) (count vb))]}
  (vec (map - va vb)))


(defn v*bin [va vb]
  {:pre [(== (count va) (count vb))]}
  (vec (map * va vb)))


(defn v*s [v & scalars]
  {:pre [(coll? v)]}
  (if (empty? scalars)
    v
    (vec (map (fn [c] (* (reduce * 1 scalars) c)) v))))


(defn v+ [& vs]
  {:pre [(apply == (map count vs))]}
  (vec (reduce v+bin vs)))


(defn v- [& vs]
  (if (== (count vs) 1)
    (v*s (first vs) (- 1))
    (vec (reduce v-bin vs))))



(defn v* [& vs]
  {:pre [(apply == (map count vs))]}
  (vec (reduce v*bin vs)))


(defn scalar [va vb]
  {:pre [(== (count va) (count vb))]}
  (apply + (map * va vb)))


(defn vectLen [v] (Math/sqrt (scalar v v)))

(defn vect*bin [[ax, ay, az] [bx, by, bz]] [(- (* ay bz) (* az by)), (- (* az bx) (* ax bz)), (- (* ax by) (* ay bx))])
(defn vect [& vs]
  {:pre [(apply == (map count vs))]}
  (vec (reduce vect*bin vs)))


(defn transpose [m] (vec (apply map vector m)))

(defn m+bin [ma mb]
  {:pre [
         (and
           (== (count ma) (count mb))

           (and (map #(== (count %1) (count %2)) ma mb)))]}


  (vec (map v+ ma mb)))


(defn m+ [& ms]
  (vec (reduce m+bin ms)))



(defn m*s [m & scalars]
  (if (empty? scalars)
    m
    (vec (map #(v*s % (reduce * 1 scalars)) m))))




(defn m-bin [ma mb]
  {:pre [
         (and
           (== (count ma) (count mb))

           (and (map #(== (count %1) (count %2)) ma mb)))]}


  (vec (map v- ma mb)))


(defn m- [& ms]
  (if (== (count ms) 1)
    (m*s (first ms) (- 1))
    (vec (reduce m-bin ms))))





(defn m*bin [ma mb]
  {:pre [
         (and
           (== (count ma) (count mb))
           (and (map #(== (count %1) (count %2)) ma mb)))]}


  (vec (map v* ma mb)))


(defn m* [& ms] (vec (reduce m*bin ms)))


(defn m*v [m v] (vec (map (partial scalar v) m)))

(defn m*m*bin [ma mb]
  {:pre [
         (and
           (apply == (map count ma))
           (apply == (map count mb))
           (vector? ma)
           (vector? (first ma))
           (== (count (first ma)) (count mb)))]}


  (transpose (map #(m*v ma %1) (transpose mb))))

(defn m*m [& ms] (vec (reduce m*m*bin ms)))

(defn reduceBooleanCollection [v]
  {:pre [(coll? v)]}
  (if (>= (count v) 1)
    (and
      (first v)
      (reduceBooleanCollection (rest v)))

    true))



(defn tensorDepth [t]
  (if (coll? t)
    (+ 1 (apply max (map tensorDepth t)))
    0))



(defn checkEqualDepths [t]
  (if (vector? t)
    (if (and (>= (count t) 1) (vector? (first t)))
      (and
        (apply == (map count t))
        (and (map checkEqualDepths t))
        (apply == (map tensorDepth t)))

      true)

    true))




(defn checkEqualSizesBin [ta tb]
  {:pre [(and (coll? ta) (coll? tb))]}
  (if (reduceBooleanCollection (map coll? ta))
    (if (reduceBooleanCollection (map coll? tb))
      (and
        (== (count ta) (count tb))
        (reduceBooleanCollection (map checkEqualSizesBin ta tb)))

      false)

    (if (reduceBooleanCollection (map coll? tb))
      false
      true)))




(defn checkEqualSizes [& ts]
  {:pre [(coll? ts)]}
  (if (<= (count ts) 1)
    true
    (if (== (count ts) 2)
      (apply checkEqualSizesBin ts)
      (and (apply checkEqualSizesBin [(first ts), (nth ts 1)]) (apply checkEqualSizesBin (rest ts))))))







(defn innerCheckRect [te t]
  (if (and (coll? t) (not (empty? t)) (coll? (first t)))
    (and
      (coll? te)
      (not (empty? te))
      (reduceBooleanCollection (map #(== (count (first te)) (count %1)) t))
      (reduceBooleanCollection (map (partial innerCheckRect (first te)) t)))
    true))



(defn checkRect [t]
  (if (coll? t)
    (innerCheckRect t t)
    true))




(defn binaryTensorContract [ta tb]
  (map checkRect [ta tb]))


(defn naryTensorContract [& ts]
  (and
    (apply checkEqualSizes ts)
    (reduceBooleanCollection (map checkEqualDepths ts))
    (reduceBooleanCollection (map checkRect ts))))






(defn t+bin [ta tb]
  {:pre [(binaryTensorContract ta tb)]}
  (if (and (vector? ta) (vector? (first ta)))
    (vec (map t+bin ta tb))
    (vec (v+ ta tb))))


(defn t-bin [ta tb]
  {:pre [(binaryTensorContract ta tb)]}
  (if (and (vector? ta) (vector? (first ta)))
    (vec (map t-bin ta tb))
    (vec (v- ta tb))))


(defn t*bin [ta tb]
  {:pre [(binaryTensorContract ta tb)]}
  (if (and (vector? ta) (vector? (first ta)))
    (vec (map t*bin ta tb))
    (vec (v* ta tb))))


(defn t*s [t scalar]
  {:pre [(checkRect t)]}
  (if (and (vector? t) (vector? (first t)))
    (vec (map #(t*s %1 scalar) t))
    (vec (v*s t scalar))))



(defn t- [& ts]
  {:pre [(apply naryTensorContract ts)]}
  (if (== 1 (count ts))
    (t*s (first ts) (- 1))
    (vec (reduce t-bin ts))))



(defn t* [& ts]
  {:pre [(apply naryTensorContract ts)]}
  (vec (reduce t*bin ts)))

(defn t+ [& ts]
  {:pre [(apply naryTensorContract ts)]}
  (vec (reduce t+bin ts)))