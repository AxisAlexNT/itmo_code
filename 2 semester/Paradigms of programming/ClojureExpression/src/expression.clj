(definterface IOperator
  (evaluate [args])
  (diff [dv])
  (getSymbol [])
  (toSuffix [])
  (toInfix [])
  (toStr []))



;(deftype GeneralOperator [f symbol subterms]
;  IOperator
;  (evaluate [this args] (apply f (map #(.evaluate % args) subterms)))
;
;  (getSymbol [this] symbol)
;  (toStr [this] (concat "(" (.getSymbol this) " " (map #(.toStr %) subterms) ")")))


(deftype JConstant [x]
  IOperator
  (evaluate [this args] (read-string (str x)))

  (getSymbol [this] (if (double? x) (format "%.1f" x) (str x)))

  (toStr [this] (if (double? x) (format "%.1f" x) (str x)))

  (toSuffix [this] (if (double? x) (format "%.1f" x) (str x)))

  (toInfix [this] (if (double? x) (format "%.1f" x) (str x)))

  (diff [this dv] (JConstant. 0)))


(defn mydivbin [a b] (/ a (double b)))

(defn mydiv [& xs] (reduce mydivbin xs))


(deftype JAdd [subterms]
  IOperator
  (evaluate [this args] (apply + (map #(.evaluate % args) subterms)))

  (getSymbol [this] "+")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) (apply str (map #(apply str " " (.toStr %)) subterms)) ")"))

  (toSuffix [this] (apply str "(" (apply str (map #(apply str (.toSuffix %) " ") subterms)) (apply str (.getSymbol this)) ")"))

  (toInfix [this] (apply str "(" (.toInfix (first subterms)) (apply str (map #(apply str (apply str " " (.getSymbol this) " ") (.toInfix %)) (rest subterms))) ")"))

  (diff [this dv] (JAdd. (map #(.diff % dv) subterms))))

(deftype JSubtract [subterms]
  IOperator
  (evaluate [this args] (apply - (map #(.evaluate % args) subterms)))

  (getSymbol [this] "-")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) (apply str (map #(apply str " " (.toStr %)) subterms)) ")"))

  (toSuffix [this] (apply str "(" (apply str (map #(apply str (.toSuffix %) " ") subterms)) (apply str (.getSymbol this)) ")"))

  (toInfix [this] (apply str "(" (.toInfix (first subterms)) (apply str (map #(apply str (apply str " " (.getSymbol this) " ") (.toInfix %)) (rest subterms))) ")"))

  (diff [this dv] (JSubtract. (map #(.diff % dv) subterms))))

(deftype JMultiply [subterms]
  IOperator
  (evaluate [this args] (apply * (map #(.evaluate % args) subterms)))

  (getSymbol [this] "*")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) (apply str (map #(apply str " " (.toStr %)) subterms)) ")"))

  (toSuffix [this] (apply str "(" (apply str (map #(apply str (.toSuffix %) " ") subterms)) (apply str (.getSymbol this)) ")"))

  (toInfix [this] (apply str "(" (.toInfix (first subterms)) (apply str (map #(apply str (apply str " " (.getSymbol this) " ") (.toInfix %)) (rest subterms))) ")"))

  (diff [this dv] (JAdd. [(JMultiply. [(.diff (nth subterms 0) dv) (nth subterms 1)]) (JMultiply. [(.diff (nth subterms 1) dv) (nth subterms 0)])])))

(deftype JDivide [subterms]
  IOperator
  (evaluate [this args] (apply mydiv (map #(double (.evaluate % args)) subterms)))

  (getSymbol [this] "/")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) (apply str (map #(apply str " " (.toStr %)) subterms)) ")"))

  (toSuffix [this] (apply str "(" (apply str (map #(apply str (.toSuffix %) " ") subterms)) (apply str (.getSymbol this)) ")"))

  (toInfix [this] (apply str "(" (.toInfix (first subterms)) (apply str (map #(apply str (apply str " " (.getSymbol this) " ") (.toInfix %)) (rest subterms))) ")"))

  (diff [this dv] (JDivide. [
                             (JSubtract. [(JMultiply. [(.diff (first subterms) dv) (last subterms)]) (JMultiply. [(.diff (last subterms) dv) (first subterms)])])

                             (JMultiply. [(last subterms) (last subterms)])])))

(deftype JNegate [arg]
  IOperator
  (evaluate [this args] (- (.evaluate arg args)))

  (getSymbol [this] "negate")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) " " (.toStr arg) ")"))

  (toSuffix [this] (apply str "(" (apply str (.toSuffix arg)) " " (apply str (.getSymbol this)) ")"))

  (toInfix [this] (apply str "(" (apply str (.getSymbol this)) " " (apply str (.toInfix arg)) ")"))


  (diff [this dv] (JNegate. (.diff arg dv))))


(deftype JSgn [arg]
  IOperator
  (evaluate [this args] (Math/signum (.evaluate arg args)))

  (getSymbol [this] "negate")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) " " (.toStr arg) ")"))

  (toSuffix [this] (apply str "(" (apply str (.toInfix arg)) (apply str (.getSymbol this)) ")"))

  (toInfix [this] (apply str "(" (apply str (.getSymbol this)) " " (apply str (.toInfix arg)) ")"))

  (diff [this dv] (JSgn. (.diff arg dv))))


(deftype JSqrt [arg]
  IOperator
  (evaluate [this args] (Math/sqrt (Math/abs (.evaluate arg args))))

  (getSymbol [this] "sqrt")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) " " (.toStr arg) ")"))

  (toSuffix [this] (apply str "(" (apply str (.toInfix arg)) (apply str (.getSymbol this)) ")"))

  (toInfix [this] (apply str "(" (apply str (.getSymbol this)) " " (apply str (.toInfix arg)) ")"))

  (diff [this dv] (JDivide. [(JMultiply. [(JSgn. arg) (.diff arg dv)])
                             (JMultiply. [(JConstant. 2) (JSqrt. arg)])])))


(deftype JSqr [arg]
  IOperator
  (evaluate [this args] (* (.evaluate arg args) (.evaluate arg args)))

  (getSymbol [this] "square")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) " " (.toStr arg) ")"))

  (toSuffix [this] (apply str "(" (apply str (.toInfix arg)) (apply str (.getSymbol this)) ")"))

  (toInfix [this] (apply str "(" (apply str (.getSymbol this)) " " (apply str (.toInfix arg)) ")"))

  (diff [this dv] (JMultiply. [(JConstant. 2) (.diff arg dv) arg])))


(defn myand [& xs] (Double/longBitsToDouble (apply bit-and (map #(java.lang.Double/doubleToLongBits %) xs))))
(defn myor [& xs] (Double/longBitsToDouble (apply bit-or (map #(java.lang.Double/doubleToLongBits %) xs))))
(defn myxor [& xs] (Double/longBitsToDouble (apply bit-xor (map #(java.lang.Double/doubleToLongBits %) xs))))


(deftype JAnd [subterms]
  IOperator
  (evaluate [this args] (apply myand (map #(double (.evaluate % args)) subterms)))

  (getSymbol [this] "&")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) (apply str (map #(apply str " " (.toStr %)) subterms)) ")"))

  (toSuffix [this] (apply str "(" (apply str (map #(apply str (.toSuffix %) " ") subterms)) (apply str (.getSymbol this)) ")"))

  (toInfix [this] (apply str "(" (.toInfix (first subterms)) (apply str (map #(apply str (apply str " " (.getSymbol this) " ") (.toInfix %)) (rest subterms))) ")"))

  (diff [this dv] (JDivide. [
                             (JSubtract. [(JMultiply. [(.diff (first subterms) dv) (last subterms)]) (JMultiply. [(.diff (last subterms) dv) (first subterms)])])

                             (JMultiply. [(last subterms) (last subterms)])])))
(deftype JOr [subterms]
  IOperator
  (evaluate [this args] (apply myor (map #(double (.evaluate % args)) subterms)))

  (getSymbol [this] "|")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) (apply str (map #(apply str " " (.toStr %)) subterms)) ")"))

  (toSuffix [this] (apply str "(" (apply str (map #(apply str (.toSuffix %) " ") subterms)) (apply str (.getSymbol this)) ")"))

  (toInfix [this] (apply str "(" (.toInfix (first subterms)) (apply str (map #(apply str (apply str " " (.getSymbol this) " ") (.toInfix %)) (rest subterms))) ")"))

  (diff [this dv] (JDivide. [
                             (JSubtract. [(JMultiply. [(.diff (first subterms) dv) (last subterms)]) (JMultiply. [(.diff (last subterms) dv) (first subterms)])])

                             (JMultiply. [(last subterms) (last subterms)])])))
(deftype JXor [subterms]
  IOperator
  (evaluate [this args] (apply myxor (map #(double (.evaluate % args)) subterms)))

  (getSymbol [this] "^")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) (apply str (map #(apply str " " (.toStr %)) subterms)) ")"))

  (toSuffix [this] (apply str "(" (apply str (map #(apply str (.toSuffix %) " ") subterms)) (apply str (.getSymbol this)) ")"))

  (toInfix [this] (apply str "(" (.toInfix (first subterms)) (apply str (map #(apply str (apply str " " (.getSymbol this) " ") (.toInfix %)) (rest subterms))) ")"))

  (diff [this dv] (JDivide. [
                             (JSubtract. [(JMultiply. [(.diff (first subterms) dv) (last subterms)]) (JMultiply. [(.diff (last subterms) dv) (first subterms)])])

                             (JMultiply. [(last subterms) (last subterms)])])))




(declare Sinh Cosh)

(deftype JSinh [arg]
  IOperator
  (evaluate [this args] (Math/sinh (.evaluate arg args)))

  (getSymbol [this] "sinh")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) " " (.toStr arg) ")"))

  (toSuffix [this] (apply str "(" (apply str (.toInfix arg)) (apply str (.getSymbol this)) ")"))

  (toInfix [this] (apply str "(" (apply str (.getSymbol this)) " " (apply str (.toInfix arg)) ")"))

  (diff [this dv] (JMultiply. [(Cosh arg) (.diff arg dv)])))


(deftype JCosh [arg]
  IOperator
  (evaluate [this args] (Math/cosh (.evaluate arg args)))

  (getSymbol [this] "cosh")

  (toStr [this] (apply str "(" (apply str (.getSymbol this)) " " (.toStr arg) ")"))

  (toSuffix [this] (apply str "(" (apply str (.toInfix arg)) (apply str (.getSymbol this)) ")"))

  (toInfix [this] (apply str "(" (apply str (.getSymbol this)) " " (apply str (.toInfix arg)) ")"))

  (diff [this dv] (JMultiply. [(Sinh arg) (.diff arg dv)])))



(deftype JVariable [name]
  IOperator
  (evaluate [this args] (get args name))

  (getSymbol [this] name)

  (toStr [this] name)

  (toSuffix [this] name)

  (toInfix [this] name)

  (diff [this dv] (if (= dv name) (JConstant. 1) (JConstant. 0))))



(deftype JVariableFN [name]
  IOperator
  (evaluate [this args] (get args (str (first (str name)))))

  (getSymbol [this] name)

  (toStr [this] name)

  (toSuffix [this] name)

  (toInfix [this] name)

  (diff [this dv] (if (= dv name) (JConstant. 1) (JConstant. 0))))





(defn Constant [x] (JConstant. x))
(def Const Constant)
(defn Add [& parts] (JAdd. parts))
(defn Subtract [& parts] (JSubtract. parts))
(defn Multiply [& parts] (JMultiply. parts))
(defn Divide [& parts] (JDivide. parts))
(defn Negate [x] (JNegate. x))
(defn Sqrt [x] (JSqrt. x))
(defn Sqr [x] (JSqr. x))
(defn Square [x] (JSqr. x))
(defn And [& parts] (JAnd. parts))
(defn Or [& parts] (JOr. parts))
(defn Xor [& parts] (JXor. parts))
(defn Sinh [x] (JSinh. x))
(defn Cosh [x] (JCosh. x))
;(defn Variable [name] (JVariable. (str (first (str name)))))
(defn Variable [name] (JVariableFN. (str name)))


(defn toString [x] (.toStr x))

(defn toStringSuffix [x] (.toSuffix x))

(defn toStringInfix [x] (.toInfix x))

(defn evaluate [expression vars] (.evaluate expression vars))

(defn diff [expr dv] (.diff expr dv))
;
;(println (toStringSuffix (Add (Const 2) (Const 3) (Const 4))))
;(println (toStringInfix (Add (Const 2) (Const 3) (Const 4))))


(def terms {"add" Add "+" Add "-" Subtract "*" Multiply "/" Divide "!" Negate "negate" Negate "e" Negate "sqrt" Sqrt "sqr" Sqr "square" Square "sinh" Sinh "cosh" Cosh "&" And "|" Or "^" Xor})

(defn -return [value tail] {:value value :tail tail})
(def -valid? boolean)
(def -value :value)
(def -tail :tail)

(defn _show [result]
  (if (-valid? result) (str "-> " (pr-str (-value result)) " | " (pr-str (apply str (-tail result))))
                       "!"))
(defn tabulate [parser inputs]
  (run! (fn [input] (printf "    %-10s %s\n" input (_show (parser input)))) inputs))

(defn _empty [value] (partial -return value))

(defn _char [p] (fn [[c & cs]] (if (and c (p c)) (-return c cs))))

(defn _map [f] (fn [result] (if (-valid? result) (-return (f (-value result)) (-tail result)))))

(defn _combine [f a b] (fn [str] (let [ar ((force a) str)] (if (-valid? ar) ((_map (partial f (-value ar))) ((force b) (-tail ar)))))))

(defn _either [a b] (fn [str] (let [ar ((force a) str)] (if (-valid? ar) ar ((force b) str)))))

(defn +char [chars] (_char (set chars)))

(defn +char-not [chars] (_char (comp not (set chars))))

(defn +map [f parser] (comp (_map f) parser))

(def +ignore (partial +map (constantly (quote ignore))))

(defn iconj [coll value] (if (= value (quote ignore)) coll (conj coll value)))

(defn +seq [& ps] (reduce (partial _combine iconj) (_empty []) ps))

(defn +seqf [f & ps] (+map (partial apply f) (apply +seq ps)))

(defn +seqn [n & ps] (apply +seqf (fn [& vs] (nth vs n)) ps))

(defn +or [p & ps] (reduce (partial _either) p ps))

(defn +opt [p] (+or p (_empty nil)))

(defn +star [p] (letfn [(rec [] (+or (+seqf cons p (delay (rec))) (_empty ())))] (rec)))

(defn +plus [p] (+seqf cons p (+star p)))

(defn +str [p] (+map (partial apply str) p))

(defmacro infix [[a op b]] (list op a b))

(defmacro suffixBin [[a b op]] (list op a b))

(defmacro suffix [args] (cons (last args) (apply list (take (- (count args) 1) args))))

(defn +collect [defs] (cond (empty? defs) () (seq? (first defs)) (let [[[key args body] & tail] defs] (cons {:key key, :args args, :body body} (+collect tail))) :else (let [[key body & tail] defs] (cons {:key key, :args [], :synth true, :body body} (+collect tail)))))

(defmacro grammar [& defs]
  (let
    [collected (+collect defs) keys (set (map :key (filter :synth collected)))]
    (letfn
      [(rule [r]
         (clojure.core/seq
           (clojure.core/concat
             (clojure.core/list (:key r))
             (clojure.core/list (:args r))
             (clojure.core/list (convert (:body r))))))
       (convert [value]
         (cond (seq? value)
               (map convert value)
               (char? value)
               (clojure.core/seq
                 (clojure.core/concat
                   (clojure.core/list
                     (quote user/+char))
                   (clojure.core/list (str value))))
               (keys value)
               (clojure.core/seq
                 (clojure.core/concat
                   (clojure.core/list value))) :else value))]
      (clojure.core/seq (clojure.core/concat
                          (clojure.core/list
                            (quote clojure.core/letfn))
                          (clojure.core/list (mapv rule collected))
                          (clojure.core/list (clojure.core/seq (clojure.core/concat (clojure.core/list (quote user/+parser))
                                                                                    (clojure.core/list (clojure.core/seq (clojure.core/concat (clojure.core/list (:key (last collected))))))))))))))

(defn _parser [p]
  (fn [input]
    (-value ((_combine (fn [v _] v) p (_char #{\u0000})) (str input \u0000)))))

(def +parser _parser)


(def json
  (grammar
    *null (+seqf (constantly nil) \n \u \l \l)
    *all-chars (mapv char (range 0 128))
    *letter (+char (apply str (filter #(Character/isLetter %) *all-chars)))
    *digit (+char (apply str (filter #(Character/isDigit %) *all-chars)))
    *space (+char (apply str (filter #(Character/isWhitespace %) *all-chars)))
    *ws (+ignore (+star *space))
    *number (+map read-string (+str (+plus *digit)))
    *identifier (+str (+seqf cons *letter (+star (+or *letter *digit))))
    *string (+seqn 1 \" (+str (+star (+char-not "\""))) \")
    (*seq [begin p end]
          (+seqn 1 begin (+opt (+seqf cons *ws p (+star (+seqn 1 *ws \, *ws p)))) *ws end))
    *array (*seq \[ (delay *value) \])
    *member (+seq *identifier *ws (+ignore \:) *ws (delay *value))
    *object (+map (partial reduce #(apply assoc %1 %2) {}) (*seq \{ *member \}))
    *value (+or *null *number *string *object *array)
    *json (+seqn 0 *ws *value)))


;Grammar:
;Start symbol X
;O' --> |XO' | empty
;O  --> XO'
;X' --> ^AX' | empty
;X  --> AX'
;A' --> &FA' | empty
;A  --> FA'
;F' --> (+|-)SF' | empty
;F  --> SF'
;S' --> (*|/)TS' | empty
;S  --> TS'|empty




;(def infixParser
;  (grammar
;    ;*null (+seqf (constantly nil) \n\u\l\l)
;    *all-chars (mapv char (range 0 128))
;    *letter (+char (apply str (filter #(Character/isLetter %) *all-chars)))
;    *digit (+char (apply str (filter #(Character/isDigit %) *all-chars)))
;    *space (+char (apply str (filter #(Character/isWhitespace %) *all-chars)))
;    *ws (+ignore (+star *space))
;    *number (+map read-string (+str (+plus *digit)))
;    ;*identifier (+str (+seqf cons *letter (+star (+or *letter *digit))))
;    *string (+seqn 1 \" (+str (+star (+char-not "\""))) \")
;    (*seq [begin p end]
;          (+seqn 1 begin (+opt (+seqf cons *ws p (+star (+seqn 1 *ws \, *ws p)))) *ws end))
;    (*seqb [begin p]
;           (+seqn 1 begin (+opt (+seqf cons *ws p (+star (+seqn 1 *ws \, *ws p)))) *ws))
;    ;*array (*seq \[ (delay *value) \])
;    ;*member (+seq *identifier *ws (+ignore \:) *ws (delay *value))
;    ;*object (+map (partial reduce #(apply assoc %1 %2) {}) (*seq \{ *member \}))
;    ;*value (+or *null *number *string *object *array)
;    ;*orp (+or (*seqb \| *ws *xor *ws *orp *ws))
;    ;*or (+seq *ws *xor *ws *orp *ws)
;    ;*xorp (+or (*seqb \^ *and *ws *xorp *ws))
;    ;*xor (+seq *ws *and *ws *xorp *ws)
;    ;*andp (+or (*seqb \& *ws *fst *ws *andp *ws))
;    ;*and (+seq *fst *ws *fstp)
;    ;*fstp (+or (*seqb \+ *snd *ws *fstp *ws) (*seqb \- *snd *ws *fstp *ws))
;    ;*fst (+seq *ws *snd *ws *fstp *ws)
;    *sndp (+or (*seqb \* *t *ws *sndp *ws) (*seqb \/ *t *ws *sndp *ws) *ws)
;    *snd (+or (+seq *ws *t *sndp *ws) *ws)
;    *t (+seq (+or *number *string) *ws)
;    *infixParser (+seqn 0 *ws *snd)))


;F' --> (+|-)SF' | empty
;F  --> SF'
;S' --> (*|/)TS' | empty
;S  --> (TS')|empty
;T  --> number | value


;S -> (M) | M
;M -> V + M | V
;V -> num/let


;(defn flatten [& xs] xs)

(defn exval [fst snd]
  (if (coll? snd)
    (if (>= (count snd) 3)
      (apply (read-string (first snd)) fst (exval (nth 1 snd) (rest (rest snd))))
      (first (rest snd)))
    0))



;S' --> (*|/)TS' | empty
;S  --> (TS')|empty
;T  --> number | value



;(def infixParser
;  (grammar
;    ;*null (+seqf (constantly nil) \n\u\l\l)
;    *all-chars (mapv char (range 0 128))
;    *letter (+char (apply str (filter #(Character/isLetter %) *all-chars)))
;    *digit (+char (apply str (filter #(Character/isDigit %) *all-chars)))
;    *space (+char (apply str (filter #(Character/isWhitespace %) *all-chars)))
;    *ws (+ignore (+star *space))
;    *number (+map read-string (+str (+plus *digit)))
;    ;*identifier (+str (+seqf cons *letter (+star (+or *letter *digit))))
;    *variable (+str (+star (+or *letter *digit)))
;    *string (+seqn 1 \" (+str (+star (+char-not "\""))) \")
;    (*seq [begin p end]
;          (+seqn 1 begin (+opt (+seqf cons *ws p (+star (+seqn 1 *ws \, *ws p)))) *ws end))
;    (*seqb [begin p]
;           (+seqn 1 begin (+opt (+seqf cons *ws p (+star (+seqn 1 *ws \, *ws))))))
;    ;*sum (+seq \( *value *ws (+or \+ \-) *ws *value \))
;    *pre_second_priority (+or (+seq *ws (+char "+-") *ws *value *ws (delay *pre_second_priority)) *value *ws)
;    *second_priority (+or (+seq \( *value *ws *pre_second_priority \)) *value *ws)
;    *value (+or *number *string *variable)
;    *infixParser (+seqn 0 *ws *second_priority *ws)))
;
;
;(def t1 (infixParser "2"))
;(println t1)
;(def t1 (infixParser "x"))
;(println t1)
;(def t1 (infixParser "(2 + 3)"))
;(println t1)
;(def t1 (infixParser "(2 + y)"))
;(println t1)
;(def t1 (infixParser "(2 + var)"))
;(println t1)
;(def t1 (infixParser "(2 + 3 + 4)"))
;(println t1)

;(def t2 (infixParser "(2 + 3)"))


;(println t2)



; T --> (args op )
; args --> *value *ws (delay *args) | *ws
; value --> number | variable
; op --> + | - | * | / | binary...



(defn tryflatten [x & xs]
  (if (empty? xs)
    x
    (cons x (flatten xs))))

;(defn tryapply [xs oper]
;  (println xs) (println oper))

;(apply (read-string (str oper)) xs)


;(defn mkTerm [op args]
;  (case (str op)
;    "" (Constant 0)
;    "+" (apply Add args)
;    "-" (apply Subtract args)
;    :else (println "Case is not concerned! For op=" (str op) "!")))

;(defn mkTerm [op args]
;  (if (contains? terms (str op))
;    (apply (get terms (str op)) args)
;    (println "Cannot find term:" (str op))))

;(defn mkFunOrVar [op]
;  (println "mkVarOrFun! Op:" op "!")
;  (if (contains? terms (str op))
;    (get terms (str op))
;    (Variable (str op))))
;
;
;
;(defn mkTerm [op args]
;  (println "mkTerm!")
;  (if (or (nil? op) (not (contains? terms (str op))))
;    (println args)
;    (apply (last args) (take (- (count args) 1) args))
;    (if (contains? terms (str op))
;      (println op args)
;      (apply (get terms (str op)) args)
;      (println "Cannot find term:" (str op)))))


(defn mkTerm [op args]
  (if (contains? terms (str op))
    (apply (get terms (str op)) args)
    (println "Cannot find term:" (str op))))





(def suffixParser
  (grammar
    *all-chars (mapv char (range 0 128))
    *letter (+char (apply str (filter #(Character/isLetter %) *all-chars)))
    *digit (+char (apply str (filter #(Character/isDigit %) *all-chars)))
    *space (+char (apply str (filter #(Character/isWhitespace %) *all-chars)))
    *ws (+ignore (+opt (+star *space)))
    *number (+map read-string (+str (+seqf cons (+opt (+char "-")) (+plus *digit))))

    *variable (+str (+star *letter))
    *string (+seqn 1 \" (+str (+star (+char-not "\""))) \")

    (*seq [begin p end]
          (+seqn 1 begin (+opt (+seqf cons *ws p (+star (+seqn 1 *ws \, *ws p)))) *ws end))

    *arg (+star (+seqf tryflatten *ws (+or *value (delay *term)) *ws))

    *inner_term (+seqf #(mkTerm %2 %1) (delay *arg) *ws (force *operation))

    *term (+or (+seqf #(Negate %) (+ignore \() *ws (+seqn 0 (+or *constant (delay *term) *variable-not-negate)) *ws (+ignore *negate) (+ignore \))) (+seqn 1 *ws \( *inner_term *ws \) *ws) (+seqn 0 *value *ws))

    *value (+or *constant *variable)

    *variable (+seqf Variable (+str (+plus *letter)))

    *variable-not-negate (+seqf Variable (+str (+seqf str (+char-not "n") (+opt (+char-not "e")) (+opt (+char-not "g")) (+opt (+char-not "a")) (+opt (+char-not "t")) (+opt (+char-not "e")))))

    *constant (+seqf Const *double)

    *double (+seqf str *number (+opt \.) (+opt *number))

    *negate (+seqf (constantly "negate") *ws \n \e \g \a \t \e *ws)

    *operation (+char "+-/*|&^!~")
    *suffixParser (+seqn 0 (+ignore *ws) *term (+ignore *ws))))



(def parseObjectSuffix suffixParser)

;(def t (parseObjectSuffix "(2 x *)"))
;(def t (parseObjectSuffix "(2 x y 7 +)"))
;(def t (parseObjectSuffix "(2 7 (3 2 x -) 6 +)"))
;(println t)
;(println (toStringInfix t))
;(println (toStringSuffix t))
;(def t (parseObjectSuffix "(2 7 (3 2 xy -) 6 +)"))
;(println t)
;(println (toStringInfix t))
;(println (toStringSuffix t))
;;[[2 [x [y]]] +]
;
;(def t (parseObjectSuffix "(x 2.0 +)"))
;(def t (Constant 10.0))
;(println t)
;(println (evaluate t {"z" 0.0, "x" 0.0, "y" 0.0}))
;(evaluate t {"z" 0.0, "x" 0.0, "y" 0.0})
;(println (toStringSuffix  (parseObjectSuffix "( x    2.0  +)")))
;(def expr (parseObjectSuffix "((x negate) 2.0 /)"))
;(println (get terms "negate"))
;(def expr (parseObjectSuffix " negate "))
;(println \" expr \")
;(println (get terms expr))
;(println (toStringSuffix (mkTerm expr [(Const 10.0)])))
;(def expr (parseObjectSuffix "(x negate)"))
;(println expr)
;(println (type expr))
;(println (toStringSuffix expr))
;(println (evaluate expr {"z" 0.0, "x" 0.0, "y" 0.0}))



;S' --> (*|/)TS' | empty
;S  --> (TS')|empty
;T  --> number | value


;V --> [-]number[.number] | var | N
;M'--> (*|/)VM' | empty
;M --> VM' | V | empty
;S'--> (+|-)MS' | empty
;S --> MS' | M | empty
;A'--> &SA' | empty
;A --> SA' | S | empty
;O'--> |AO' | empty
;O --> AO' | A | empty
;X'--> ^OX' | empty
;X --> OX' | O | empty
;N --> negate X | X | (N)
;Starting token: N, may be no brackets!!!
(def infixParser
  (grammar
    ;*null (+seqf (constantly nil) \n\u\l\l)
    *all-chars (mapv char (range 0 128))
    *letter (+char (apply str (filter #(Character/isLetter %) *all-chars)))
    *digit (+char (apply str (filter #(Character/isDigit %) *all-chars)))
    *space (+char (apply str (filter #(Character/isWhitespace %) *all-chars)))
    *ws (+ignore (+star *space))
    *number (+map read-string (+str (+plus *digit)))
    ;*identifier (+str (+seqf cons *letter (+star (+or *letter *digit))))
    *string (+seqn 1 \" (+str (+star (+char-not "\""))) \")
    (*seq [begin p end]
          (+seqn 1 begin (+opt (+seqf cons *ws p (+star (+seqn 1 *ws \, *ws p)))) *ws end))
    (*seqb [begin p]
           (+seqn 1 begin (+opt (+seqf cons *ws p (+star (+seqn 1 *ws \, *ws))))))

    *variable (+seqf Variable (+str (+plus *letter)))

    *constant (+seqf Const *double)

    *double (+seqf str *number (+opt \.) (+opt *number))

    *negate (+seqf (constantly "negate") *ws \n \e \g \a \t \e *ws)

    *vall (+or *constant *variable *ws)

    *valfix (+seqn 0 *vall)


    *predivide (+seqf cons (+ignore \/) *ws *valfix
                      (+opt (+seqn 0 *ws (delay *predivide))))

    *divide (+or (+seqf #(apply Divide %1 %2) *ws *valfix *ws *predivide) (+opt *valfix))

    *premult (+seqf cons (+ignore \*) *ws *valfix
                    (+opt (+seqn 0 *ws (delay *premult))))
    *mult (+or
            (+seqf #(apply Multiply %1 %2) *ws *valfix *ws *premult)
            (+opt *divide)
            (+opt *valfix))


    *presum (+seqf cons (+ignore \+) *ws *valfix
                   (+opt (+seqn 0 *ws (delay *presum))))
    *presubtract (+or (+seqf cons (+ignore \-) *ws *valfix
                             (+opt (+seqn 0 *ws (delay *presubtract))) *mult *divide))
    *sum (+or
           (+seqf #(apply Add %1 %2) *ws *valfix *ws *presum)
           (+seqf #(apply Subtract %1 %2) *ws *valfix *ws *presubtract)
           (+opt *mult)
           (+opt *divide)
           (+opt *valfix))



    *preand (+seqf #(apply And %1 %2) *ws (+ignore \&) *sum *ws (delay *preand))

    *and (+or (+seqf #(apply And %1 %2) *ws *sum *ws *preand *ws) *sum *ws)

    *preor (+seqf #(apply Or %1 %2) *ws (+ignore \|) *and *ws (delay *preor))

    *or (+or (+seqf #(apply Or %1 %2) *ws *and *ws *preor *ws) *and *ws)

    *prexor (+seqf #(apply Xor %1 %2) *ws (+ignore \^) *or *ws (delay *prexor))

    *xor (+or (+seqf #(apply Xor %1 %2) *ws *or *ws *prexor *ws) *or *ws)


    *negate_priority (+opt (+or
                             (+seqf #(apply Negate %2) *negate *ws *mult)
                             *xor))

    *infixParser (+seqn 0 *ws *negate_priority *ws)))


(def parseObjectInfix infixParser)



;(def t1 (infixParser "2"))
;(println t1)
;(println (toStringInfix t1))
;(def t1 (infixParser "x"))
;(println t1)
;(println (toStringInfix t1))
;(def t1 (infixParser "3 - 2 * 6"))
;(println t1)
;(println (toStringInfix t1))
;(def t1 (infixParser "(2 + y)"))
;(println t1)
;(def t1 (infixParser "(2 + var)"))
;(println t1)
;(def t1 (infixParser "(2 + 3 + 4)"))
;(println t1)

;(def t2 (infixParser "(2 + 3)"))


;(println t2)