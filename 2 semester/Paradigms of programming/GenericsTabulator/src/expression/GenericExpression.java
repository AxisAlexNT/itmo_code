package expression;

import expression.exceptions.ArithmeticEvaluationException;

public interface GenericExpression<N extends Number> {
    N evaluate(N x, N y, N z) throws ArithmeticEvaluationException;
}
