package expression.exceptions;

import expression.terms.GenericTerm;

public class IntegerOverflowException extends ArithmeticEvaluationException {
    public IntegerOverflowException(String message, GenericTerm nonEvaluatingGenericTerm) {
        super(message, "Integer overflow", nonEvaluatingGenericTerm);
    }

    public IntegerOverflowException(String message) {
        super(message, "Integer overflow");
    }


    public IntegerOverflowException(String message, GenericTerm nonEvaluatingGenericTerm, boolean needToPrintRanges) {
        super(message, "Integer overflow", nonEvaluatingGenericTerm, needToPrintRanges);
    }
}
