package expression.exceptions;

import expression.terms.GenericTerm;

public class DivisionByZeroEvaluationException extends ArithmeticEvaluationException {
    public DivisionByZeroEvaluationException(String message, GenericTerm nonEvaluatingGenericTerm) {
        super(message, "Integer division by zero", nonEvaluatingGenericTerm);
    }

    public DivisionByZeroEvaluationException(String message) {
        super(message, "Integer division by zero");
    }

    public DivisionByZeroEvaluationException(String message, GenericTerm nonEvaluatingGenericTerm, boolean needToPrintRanges) {
        super(message, "Integer division by zero", nonEvaluatingGenericTerm, needToPrintRanges);
    }
}
