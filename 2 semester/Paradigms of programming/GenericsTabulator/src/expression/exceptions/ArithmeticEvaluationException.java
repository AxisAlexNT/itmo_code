package expression.exceptions;

import expression.terms.GenericTerm;

public class ArithmeticEvaluationException extends AbstractExpressionException {
    protected String shortMessage;

    public ArithmeticEvaluationException(String message, int errorPosition) {
        super(message, errorPosition);
    }

    public ArithmeticEvaluationException(String message, String _shortMessage, GenericTerm nonEvaluatingGenericTerm) {
        super(message, nonEvaluatingGenericTerm);
        shortMessage = _shortMessage;
    }

    public ArithmeticEvaluationException(String message, String _shortMessage) {
        super(message);
        shortMessage = _shortMessage;
    }

    public ArithmeticEvaluationException(String message, String _shortMessage, GenericTerm nonEvaluatingGenericTerm, boolean needToPrintRanges) {
        super(message + ((needToPrintRanges) ? ("\nValid integers range: -2147483648 <= int < 2147483648\n") : ("")), nonEvaluatingGenericTerm);
        shortMessage = _shortMessage;
    }

    @Override
    public String getShortMessage() {
        return shortMessage;
    }
}
