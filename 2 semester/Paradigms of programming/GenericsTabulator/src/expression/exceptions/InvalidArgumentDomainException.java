package expression.exceptions;

import expression.terms.GenericTerm;

public class InvalidArgumentDomainException extends ArithmeticEvaluationException {
    public InvalidArgumentDomainException(String message, GenericTerm nonEvaluatingGenericTerm) {
        super(message, "Invalid argument domain", nonEvaluatingGenericTerm);
    }

    public InvalidArgumentDomainException(String message, GenericTerm nonEvaluatingGenericTerm, boolean needToPrintRanges) {
        super(message, "Invalid argument domain", nonEvaluatingGenericTerm, needToPrintRanges);
    }
}
