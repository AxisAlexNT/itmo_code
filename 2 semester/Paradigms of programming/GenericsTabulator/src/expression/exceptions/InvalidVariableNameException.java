package expression.exceptions;

import expression.terms.GenericTerm;

public class InvalidVariableNameException extends ArithmeticEvaluationException {
    public InvalidVariableNameException(String message, String _shortMessage, GenericTerm nonEvaluatingGenericTerm) {
        super(message, _shortMessage, nonEvaluatingGenericTerm);
    }
}
