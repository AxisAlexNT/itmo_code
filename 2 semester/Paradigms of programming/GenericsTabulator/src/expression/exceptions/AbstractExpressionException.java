package expression.exceptions;

import expression.terms.GenericTerm;

public abstract class AbstractExpressionException extends Exception {
    public AbstractExpressionException() {
        super();
    }


    public AbstractExpressionException(String message) {
        super(message);
    }


    public AbstractExpressionException(String message, int errorPosition) {
        //super((new StringBuilder()).append("\nAt position: ").append(errorPosition).append("\nException during exception parsing: \t").append(message).append("\n\n").toString());
        super(String.format("At position: %d \t\t%s", errorPosition, message));
    }


    public AbstractExpressionException(String message, GenericTerm nonEvaluatingGenericTerm) {
        //super((new StringBuilder().append("\n\nIn term: ").append(nonEvaluatingGenericTerm.toString()).append("\nArithmetic exception during evaluation: \t").append(message).append("\n\n")).toString());
        super(String.format("In term: %s \t\t Exception: %s", nonEvaluatingGenericTerm.toString(), message));
    }

    public String getShortMessage() {
        return getMessage();
    }

}
