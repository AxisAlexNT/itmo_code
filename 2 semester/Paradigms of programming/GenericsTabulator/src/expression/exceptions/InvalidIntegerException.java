package expression.exceptions;

public class InvalidIntegerException extends GrammarMismatchException {
    public InvalidIntegerException(String message, int position) {
        super(message, position);
    }
}
