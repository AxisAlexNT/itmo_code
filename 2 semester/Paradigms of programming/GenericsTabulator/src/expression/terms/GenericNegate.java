package expression.terms;

import expression.exceptions.ArithmeticEvaluationException;
import expression.generic.evaluators.Evaluator;

public class GenericNegate<N extends Number> extends GenericUnaryOperator<N> {
    public GenericNegate(GenericTerm<N> argument, final Evaluator<N> expressionEvaluator) {
        super(argument, "-", expressionEvaluator);
    }

    @Override
    protected N operation(N argument) throws ArithmeticEvaluationException {
        return evaluator.negate(argument);
    }
}
