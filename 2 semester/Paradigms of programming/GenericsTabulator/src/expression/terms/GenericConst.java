package expression.terms;

import expression.exceptions.ArithmeticEvaluationException;
import expression.generic.evaluators.Evaluator;

public class GenericConst<N extends Number> extends GenericTerm<N> {
    protected N value;

    public GenericConst(final N _value, final Evaluator<N> expressionEvaluator) {
        super(expressionEvaluator);
        value = _value;
    }

    public GenericConst(final String _value, final Evaluator<N> expressionEvaluator) throws NumberFormatException {
        super(expressionEvaluator);
        value = evaluator.ofString(_value);
    }



    @Override
    protected String doToString() {
        return String.format("(%s)", value.toString());
    }

    @Override
    protected N doEvaluate(N x, N y, N z) throws ArithmeticEvaluationException {
        return value;
    }


}
