package expression.terms;

import expression.exceptions.ArithmeticEvaluationException;
import expression.generic.evaluators.Evaluator;

public abstract class GenericBinaryOperator<N extends Number> extends GenericTerm<N> {
    protected GenericTerm<N> left;
    protected GenericTerm<N> right;
    protected String operationSymbol;


    public GenericBinaryOperator(GenericTerm<N> _left, GenericTerm<N> _right, String _operationSymbol, final Evaluator<N> expressionEvaluator) {
        super(expressionEvaluator);
        left = _left;
        right = _right;
        operationSymbol = _operationSymbol;
    }

    protected abstract N operation(N l, N r) throws ArithmeticEvaluationException;

    @Override
    protected N doEvaluate(N x, N y, N z) throws ArithmeticEvaluationException {
        return operation(left.evaluate(x, y, z), right.evaluate(x, y, z));
    }

    @Override
    protected String doToString() {
        return String.format("(%s %s %s)", left.toString(), operationSymbol, right.toString());
    }


}
