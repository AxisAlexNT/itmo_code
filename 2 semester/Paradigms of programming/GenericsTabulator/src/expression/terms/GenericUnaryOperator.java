package expression.terms;

import expression.exceptions.ArithmeticEvaluationException;
import expression.generic.evaluators.Evaluator;

public abstract class GenericUnaryOperator<N extends Number> extends GenericTerm<N> {
    protected GenericTerm<N> argument;

    protected String operationSymbol;

    public GenericUnaryOperator(GenericTerm<N> argument, String operationSymbol, final Evaluator<N> expressionEvaluator) {
        super(expressionEvaluator);
        this.operationSymbol = operationSymbol;
        this.argument = argument;
    }

    @Override
    protected N doEvaluate(N x, N y, N z) throws ArithmeticEvaluationException {
        return operation(argument.evaluate(x, y, z));
    }

    protected abstract N operation(N argument) throws ArithmeticEvaluationException;

    @Override
    protected String doToString() {
        return String.format("(%s %s)", operationSymbol, argument.toString());
    }
}
