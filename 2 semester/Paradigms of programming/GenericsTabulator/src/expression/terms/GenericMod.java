package expression.terms;

import expression.exceptions.ArithmeticEvaluationException;
import expression.generic.evaluators.Evaluator;

public class GenericMod<N extends Number> extends GenericBinaryOperator<N> {
    public GenericMod(GenericTerm<N> _left, GenericTerm<N> _right, final Evaluator<N> expressionEvaluator) {
        super(_left, _right, "mod", expressionEvaluator);
    }


    @Override
    protected N operation(N l, N r) throws ArithmeticEvaluationException {
        return evaluator.mod(l, r);
    }
}
