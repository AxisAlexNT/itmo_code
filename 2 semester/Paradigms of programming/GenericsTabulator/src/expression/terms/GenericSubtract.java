package expression.terms;

import expression.exceptions.ArithmeticEvaluationException;
import expression.generic.evaluators.Evaluator;

public class GenericSubtract<N extends Number> extends GenericBinaryOperator<N> {
    public GenericSubtract(GenericTerm<N> _left, GenericTerm<N> _right, final Evaluator<N> expressionEvaluator) {
        super(_left, _right, "-", expressionEvaluator);
    }


    @Override
    protected N operation(N l, N r) throws ArithmeticEvaluationException {
        return evaluator.subtract(l, r);
    }
}
