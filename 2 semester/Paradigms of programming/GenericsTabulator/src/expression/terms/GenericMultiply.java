package expression.terms;

import expression.exceptions.ArithmeticEvaluationException;
import expression.generic.evaluators.Evaluator;

public class GenericMultiply<N extends Number> extends GenericBinaryOperator<N> {
    public GenericMultiply(GenericTerm<N> _left, GenericTerm<N> _right, final Evaluator<N> expressionEvaluator) {
        super(_left, _right, "*", expressionEvaluator);
    }


    @Override
    protected N operation(N l, N r) throws ArithmeticEvaluationException {
        return evaluator.multiply(l, r);
    }
}
