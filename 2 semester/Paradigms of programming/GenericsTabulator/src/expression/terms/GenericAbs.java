package expression.terms;

import expression.exceptions.ArithmeticEvaluationException;
import expression.generic.evaluators.Evaluator;

public class GenericAbs<N extends Number> extends GenericUnaryOperator<N> {
    public GenericAbs(GenericTerm<N> argument, final Evaluator<N> expressionEvaluator) {
        super(argument, "abs", expressionEvaluator);
    }

    @Override
    protected N operation(N argument) throws ArithmeticEvaluationException {
        return evaluator.abs(argument);
    }
}
