package expression.terms;

import expression.GenericExpression;
import expression.exceptions.ArithmeticEvaluationException;
import expression.generic.evaluators.Evaluator;

public abstract class GenericTerm<N extends Number> implements GenericExpression<N> {
    protected Evaluator<N> evaluator;

    @Override
    public String toString() {
        return doToString();
    }

    protected abstract String doToString();


    @Override
    public N evaluate(N x, N y, N z) throws ArithmeticEvaluationException {
        return doEvaluate(x, y, z);
    }

    protected abstract N doEvaluate(N x, N y, N z) throws ArithmeticEvaluationException;

    public GenericTerm(final Evaluator<N> expressionEvaluator) {
        evaluator = expressionEvaluator;
    }

//    public GenericTerm()
//    {
//    }

    public void setEvaluator(final Evaluator<N> expressionEvaluator) {
        evaluator = expressionEvaluator;
    }
}
