package expression.terms;

import expression.exceptions.InvalidVariableNameException;
import expression.generic.evaluators.Evaluator;

public class GenericVariable<N extends Number> extends GenericTerm<N> {
    protected String name;

    public GenericVariable(final String _name, final Evaluator<N> expressionEvaluator) {
        super(expressionEvaluator);
        name = _name;
    }

    @Override
    protected N doEvaluate(N x, N y, N z) throws InvalidVariableNameException {
        switch (name) {
            case "x":
                return x;
            case "y":
                return y;
            case "z":
                return z;
            default:
                throw new InvalidVariableNameException(String.format("GenericVariable name %s is invalid", name), String.format("GenericVariable name %s is invalid", name), this);

        }
    }

    @Override
    protected String doToString() {
        return name;
    }
}
