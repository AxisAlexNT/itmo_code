package expression.terms;

import expression.exceptions.ArithmeticEvaluationException;
import expression.generic.evaluators.Evaluator;

public class GenericDivide<N extends Number> extends GenericBinaryOperator<N> {
    public GenericDivide(GenericTerm<N> _left, GenericTerm<N> _right, final Evaluator<N> expressionEvaluator) {
        super(_left, _right, "/", expressionEvaluator);
    }

    @Override
    protected N operation(N l, N r) throws ArithmeticEvaluationException {
        return evaluator.divide(l, r);
    }
}
