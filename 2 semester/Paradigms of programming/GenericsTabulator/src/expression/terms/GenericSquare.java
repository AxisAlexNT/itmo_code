package expression.terms;

import expression.exceptions.ArithmeticEvaluationException;
import expression.generic.evaluators.Evaluator;

public class GenericSquare<N extends Number> extends GenericUnaryOperator<N> {
    public GenericSquare(GenericTerm<N> argument, final Evaluator<N> expressionEvaluator) {
        super(argument, "square", expressionEvaluator);
    }

    @Override
    protected N operation(N argument) throws ArithmeticEvaluationException {
        return evaluator.square(argument);
    }
}
