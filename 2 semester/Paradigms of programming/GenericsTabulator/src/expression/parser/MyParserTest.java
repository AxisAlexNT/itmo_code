package expression.parser;

import expression.GenericExpression;
import expression.exceptions.GrammarMismatchException;
import expression.generic.evaluators.UncheckedByteEvaluator;

public class MyParserTest {
    public static void main(String[] args) {
        GenericExpressionParser<Byte> parser = new GenericExpressionParser<>(new UncheckedByteEvaluator());

        GenericExpression<Byte> expr;

        try {
            expr = parser.parse("1 + 5 mod 3");
            System.out.println(expr.toString());
        } catch (GrammarMismatchException e) {
            e.printStackTrace();
        }


    }
}
