package expression.parser;

/*
Grammar:


F' --> (+|-)SF' | empty
F  --> SF'
S' --> (*|/|mod)TS' | empty
S  --> TS'|empty
T  --> -T|square T|abs T|~T|(F)|x|y|z|I|F
I  --> 0..9 | 0..9I


Starting token: O
*/


import expression.GenericExpression;
import expression.exceptions.GrammarMismatchException;
import expression.exceptions.UnexpectedSymbolException;
import expression.generic.evaluators.Evaluator;
import expression.terms.*;

public class GenericExpressionParser<N extends Number> implements Parser<N> {
    private static final char[] validTokenStartingSymbols = {'(', ')', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'x', 'y', 'z', '+', '-', '*', '/', ' ', 'a', 'm', 's'};

    private static final int errorSubstringLength = 7;
    private static final String insertedErrorMessage = " >ERROR HERE> ";
    private static final String modToken = "mod";
    private static final String absToken = "abs";
    private static final String squareToken = "square";
    public final Evaluator<N> expressionEvaluator;
    private String expression;


    public GenericExpressionParser(final Evaluator<N> _expressionEvaluator) {
        expressionEvaluator = _expressionEvaluator;
    }

    private static int max(int a, int b) {
        return (a > b) ? a : b;
    }

    private static int min(int a, int b) {
        return (a < b) ? a : b;
    }

    @Override
    public GenericExpression<N> parse(String inputString) throws GrammarMismatchException {
        expression = inputString;
        if (expression == null) {
            throw new GrammarMismatchException("Cannot parse NULL expression! Please ensure that expression is not null!");
        }

        if (expression.isEmpty()) {
            throw new GrammarMismatchException("Empty expression does not contain any tokens.", -1);
        }

        ParseResult<N> parsedExpression = startParse();

        if (parsedExpression.endedPosition != inputString.length()) {
            //String msg = generateGrammarMismatchMessage("Unexpected symbol found which is not a valid prefix of any token", parsedExpression.endedPosition);
            throw new GrammarMismatchException("Unexpected symbol found which is not a valid prefix of any token", parsedExpression.endedPosition);
        }

        return parsedExpression.expression;
    }

    private ParseResult<N> startParse() throws GrammarMismatchException {
        return parseFirstPriority(0);
    }


    private ParseResult<N> parsePreFirstPriority(int position, GenericTerm<N> leftS) throws GrammarMismatchException {
        position = skipWhiteSpace(position);

        if (position == expression.length()) {
            return new ParseResult<N>(leftS, position);
        }

        ParseResult<N> S;
        ParseResult<N> preFRight;

        switch (expression.charAt(position)) {
            case '+':
                S = parseSecondPriority(position + 1);
                preFRight = parsePreFirstPriority(S.endedPosition,
                        new GenericAdd<N>(leftS, S.expression, expressionEvaluator));
                return new ParseResult<N>(preFRight.expression, preFRight.endedPosition);
            case '-':
                S = parseSecondPriority(position + 1);
                preFRight = parsePreFirstPriority(S.endedPosition, new GenericSubtract<N>(leftS, S.expression, expressionEvaluator));
                return new ParseResult<N>(preFRight.expression, preFRight.endedPosition);
            default:
                return new ParseResult<N>(leftS, position);

        }
    }


    private ParseResult<N> parseThirdPriority(int position) throws GrammarMismatchException {
        position = skipWhiteSpace(position);

        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected token T (a number, negation, parentheses, high/low with an argument or a variable), but reached EOS", position);
        }

        ParseResult<N> T, F;

        char currentSymbol = expression.charAt(position);

        switch (currentSymbol) {
            case '-':
                if (position < expression.length() - 1) {
                    if (Character.isDigit(expression.charAt(position + 1))) {
                        return tryParseInt(position);
                    } else {
                        T = parseThirdPriority(position + 1);
                        return new ParseResult<N>(new GenericNegate<N>(T.expression, expressionEvaluator), T.endedPosition);
                    }
                } else {
                    throw new GrammarMismatchException("Found an unary minus without a valid argument!", position);
                }
            case '(':
                int openingBracketPosition = position;
                T = parseFirstPriority(position + 1);
                position = skipWhiteSpace(T.endedPosition);
                if (position == expression.length()) {
                    throw new GrammarMismatchException("Expected closing bracket ')' for pairing '(' at position " + openingBracketPosition + ", but reached EOS", T.endedPosition, false);
                }

                if (expression.charAt(position) != ')') {
                    if (!isValidTokenBeginnig(expression.charAt(position))) {
                        throw new GrammarMismatchException("Unexpected symbol found which is not a valid prefix of any token", position);
                    }
                    throw new GrammarMismatchException("Expected closing bracket ')' for pairing '(' at position " + openingBracketPosition + ", but none was found", position, false);
                }

                return new ParseResult<N>(T.expression, position + 1);
            case 's':
                ensureToken(position, squareToken);
                position += squareToken.length() + 1;
                T = parseThirdPriority(position);
                return new ParseResult<N>(new GenericSquare<N>(T.expression, expressionEvaluator), T.endedPosition);
            case 'a':
                ensureToken(position, absToken);
                position += absToken.length() + 1;
                T = parseThirdPriority(position);
                return new ParseResult<N>(new GenericAbs<N>(T.expression, expressionEvaluator), T.endedPosition);
            case 'x':
            case 'y':
            case 'z':
                return new ParseResult<N>(new GenericVariable<N>(String.valueOf(expression.charAt(position)), expressionEvaluator), position + 1);
            case ')':
                throw new GrammarMismatchException("Unexpected unpaired closing bracket ')'", position, false);
            default:
                if (Character.isDigit(expression.charAt(position))) {
                    return tryParseInt(position);
                } else {
                    throw new UnexpectedSymbolException("Unexpected symbol '" + expression.charAt(position) + "' starting new token", position);
                }
        }
    }

    private ParseResult<N> parseFirstPriority(int position) throws GrammarMismatchException {
        position = skipWhiteSpace(position);

        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected token F (a sum or subtraction), but reached EOS", position, false);
        }

        ParseResult<N> S = parseSecondPriority(position);
        return parsePreFirstPriority(S.endedPosition, S.expression);
    }

    private void ensureToken(int position, String testingToken) throws GrammarMismatchException {
        for (int i = 0; i < testingToken.length(); i++) {
            char actual_char = expression.charAt(position + i);
            char testing_char = testingToken.charAt(i);
            if (testing_char != actual_char) {
                throw new UnexpectedSymbolException("Probably misspelled '" + testingToken + "'", position);
            }
        }

        if (position + testingToken.length() < expression.length()) {
            if (Character.isAlphabetic(expression.charAt(position + testingToken.length()))) {
                throw new UnexpectedSymbolException("Token name " + testingToken + " is followed by an alphabetic character, probably misspelled", position);
            }
        }
    }

    private ParseResult<N> parsePreSecondPriority(int position, GenericTerm<N> leftT) throws GrammarMismatchException {
        position = skipWhiteSpace(position);

        if (position == expression.length()) {
            return new ParseResult<N>(leftT, position);
        }

        ParseResult<N> T;
        ParseResult<N> preSRight;

        switch (expression.charAt(position)) {
            case '*':
                T = parseThirdPriority(position + 1);
                preSRight = parsePreSecondPriority(T.endedPosition,
                        new GenericMultiply<N>(leftT, T.expression, expressionEvaluator));
                return new ParseResult<N>(preSRight.expression, preSRight.endedPosition);
            case '/':
                T = parseThirdPriority(position + 1);
                preSRight = parsePreSecondPriority(T.endedPosition, new GenericDivide<N>(leftT, T.expression, expressionEvaluator));
                return new ParseResult<N>(preSRight.expression, preSRight.endedPosition);
            case 'm':
                ensureToken(position, modToken);
                position += modToken.length();
                T = parseThirdPriority(position);
                preSRight = parsePreSecondPriority(T.endedPosition, new GenericMod<N>(leftT, T.expression, expressionEvaluator));
                return new ParseResult<N>(preSRight.expression, preSRight.endedPosition);
            default:
                return new ParseResult<N>(leftT, position);
        }
    }

    private ParseResult<N> parseSecondPriority(int position) throws GrammarMismatchException {
        position = skipWhiteSpace(position);
        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected token T, but reached EOS", position, false);
        }

        ParseResult<N> T = parseThirdPriority(position);
        return parsePreSecondPriority(T.endedPosition, T.expression);
    }

    private boolean isValidTokenBeginnig(char testingChar) {
        for (char validChar : validTokenStartingSymbols) {
            if (testingChar == validChar) {
                return true;
            }
        }

        return false;
    }


    private String generateGrammarMismatchMessage(final String message, int position) {
        String errorContainingSubstring = generateErrorSubstring(position);

        return String.format("%s%n%s%n", errorContainingSubstring, message);
    }


    private String generateErrorSubstring(int errorPosition) {
        StringBuilder errorSubstringBuilder = new StringBuilder();

        errorSubstringBuilder.append(expression);
        errorSubstringBuilder.insert(errorPosition, insertedErrorMessage);

        int errorSubstringStartPosition = max(0, errorPosition - errorSubstringLength);
        int errorSubstringEndPosition = min(expression.length() + insertedErrorMessage.length(), errorPosition + errorSubstringLength + insertedErrorMessage.length());

        errorSubstringBuilder.substring(errorSubstringStartPosition, errorSubstringEndPosition);

        return errorSubstringBuilder.toString();
    }


    private ParseResult<N> tryParseInt(int position) throws GrammarMismatchException {
        StringBuilder numberStringBuilder = new StringBuilder();

        if (expression.charAt(position) == '-') {
            position++;
            numberStringBuilder.append('-');
        }

        if (position == expression.length()) {
            throw new UnexpectedSymbolException(generateGrammarMismatchMessage("Expected a negative integer, but only minus was parsed before EOS", position), position);
        }

        while ((position < expression.length()) && Character.isDigit(expression.charAt(position))) {
            numberStringBuilder.append(expression.charAt(position));
            position++;
        }

        GenericConst<N> answer = new GenericConst<N>(numberStringBuilder.toString(), expressionEvaluator);

        return new ParseResult<N>(answer, position);

    }

    private int skipWhiteSpace(int position) {
        while ((position < expression.length()) && Character.isWhitespace(expression.charAt(position))) {
            position++;
        }
        return position;
    }


    private void raiseGrammarMismatchException(String message, int position) throws GrammarMismatchException {
        String exceptionMessage = String.format("%28s \t GrammarMismatchException: %s", generateErrorSubstring(position), message);

        throw new GrammarMismatchException(exceptionMessage, position);
    }


    private class ParseResult<K extends N> {
        final GenericTerm<K> expression;
        final int endedPosition;

        public ParseResult(GenericTerm<K> _expression, int _endedPosition) {
            expression = _expression;
            endedPosition = _endedPosition;
        }
    }


}