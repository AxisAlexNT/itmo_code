package expression.parser;

import expression.GenericExpression;
import expression.exceptions.GrammarMismatchException;


public interface Parser<N extends Number> {
    GenericExpression<N> parse(String expression) throws GrammarMismatchException;
}