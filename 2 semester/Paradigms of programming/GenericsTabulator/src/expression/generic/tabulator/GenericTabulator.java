package expression.generic.tabulator;

import expression.GenericExpression;
import expression.exceptions.ArithmeticEvaluationException;
import expression.exceptions.DivisionByZeroEvaluationException;
import expression.exceptions.GrammarMismatchException;
import expression.exceptions.IntegerOverflowException;
import expression.generic.evaluators.*;
import expression.parser.GenericExpressionParser;

public class GenericTabulator implements Tabulator {
    @Override
    public Object[][][] tabulate(String mode, String expression, int x1, int x2, int y1, int y2, int z1, int z2) throws Exception {
        return doTabulate(selectEvaluatorByMode(mode), expression, x1, x2, y1, y2, z1, z2);
    }

    private Evaluator<? extends Number> selectEvaluatorByMode(String mode) throws Exception {
        switch (mode) {
            case "i":
                return new CheckedIntegerEvaluator();
            case "d":
                return new DoubleEvaluator();
            case "bi":
                return new BigIntegerEvaluator();
            case "u":
                return new UncheckedIntegerEvaluator();
            case "f":
                return new UncheckedFloatEvaluator();
            case "b":
                return new UncheckedByteEvaluator();
            case "l":
                return new UncheckedLongEvaluator();
            case "s":
                return new UncheckedShortEvaluator();
            default:
                throw new Exception("Unrecognized mode string!");
        }
    }

    private <N extends Number> Object[][][] doTabulate(Evaluator<N> ev, String expression, int x1, int x2, int y1, int y2, int z1, int z2) throws ArithmeticEvaluationException {
        GenericExpression<N> expressionTerm;
        GenericExpressionParser<N> parser = new GenericExpressionParser<>(ev);

        try {
            expressionTerm = parser.parse(expression);
        } catch (GrammarMismatchException e) {
            return null;
        }

        Object[][][] table = new Object[x2 - x1 + 1][y2 - y1 + 1][z2 - z1 + 1];


        for (int x = x1; x <= x2; ++x) {
            for (int y = y1; y <= y2; ++y) {
                for (int z = z1; z <= z2; ++z) {
                    Object answer;

                    try {
                        N xg = ev.ofString(Integer.toString(x));
                        N yg = ev.ofString(Integer.toString(y));
                        N zg = ev.ofString(Integer.toString(z));
                        answer = expressionTerm.evaluate(xg, yg, zg);
                    } catch (IntegerOverflowException | DivisionByZeroEvaluationException | ArithmeticException e) {
                        answer = null;
                    }

                    table[x - x1][y - y1][z - z1] = answer;
                }
            }
        }


        return table;

    }
}
