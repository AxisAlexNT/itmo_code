package expression.generic.tabulator;

import expression.generic.evaluators.CheckedIntegerEvaluator;
import expression.generic.evaluators.Evaluator;

import java.util.Arrays;

public class MyTabulatorTest {
    public static void main(String[] args) {
        GenericTabulator tabulator = new GenericTabulator();

        System.out.println((-4));
        System.out.println(Integer.parseInt("-19"));

        Evaluator<Integer> ev = new CheckedIntegerEvaluator();

        System.out.println(ev.ofString(Integer.toString(-19)));

        try {
            System.out.println(Arrays.deepToString(tabulator.tabulate("i", "1 + 5 mod 3", -11, 6, -19, 4, -4, 12)));
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
