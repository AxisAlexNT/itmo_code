package expression.generic.evaluators;

import expression.exceptions.ArithmeticEvaluationException;
import expression.exceptions.DivisionByZeroEvaluationException;
import expression.exceptions.IntegerOverflowException;

public interface Evaluator<N extends Number> {
    N sum(N l, N r) throws ArithmeticEvaluationException;

    N multiply(N l, N r) throws ArithmeticEvaluationException;

    N divide(N l, N r) throws ArithmeticEvaluationException;

    N subtract(N l, N r) throws ArithmeticEvaluationException;

    N negate(N x) throws ArithmeticEvaluationException;

    N ofString(final String src) throws NumberFormatException;

    N mod(N l, N r) throws ArithmeticException, DivisionByZeroEvaluationException;

    N square(N l) throws ArithmeticException, ArithmeticEvaluationException;

    N abs(N l) throws ArithmeticException, IntegerOverflowException;
}
