package expression.generic.evaluators;

import expression.exceptions.ArithmeticEvaluationException;

public class UncheckedIntegerEvaluator implements Evaluator<Integer> {

    @Override
    public Integer sum(Integer l, Integer r) throws ArithmeticEvaluationException {
        return l + r;
    }

    @Override
    public Integer multiply(Integer l, Integer r) throws ArithmeticEvaluationException {
        return l * r;
    }

    @Override
    public Integer divide(Integer l, Integer r) throws ArithmeticEvaluationException {
        return l / r;
    }

    @Override
    public Integer subtract(Integer l, Integer r) throws ArithmeticEvaluationException {
        return l - r;
    }

    @Override
    public Integer negate(Integer x) throws ArithmeticEvaluationException {
        return -x;
    }

    @Override
    public Integer ofString(String src) throws NumberFormatException {
        return Integer.parseInt(src);
    }

    @Override
    public Integer mod(Integer l, Integer r) throws ArithmeticException {
        return (l % r);
    }

    @Override
    public Integer square(Integer l) throws ArithmeticException {
        return (l * l);
    }

    @Override
    public Integer abs(Integer l) throws ArithmeticException {
        return (l >= 0) ? l : (-l);
    }
}
