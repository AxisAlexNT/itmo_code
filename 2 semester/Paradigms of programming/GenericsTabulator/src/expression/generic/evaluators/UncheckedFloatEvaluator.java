package expression.generic.evaluators;

import expression.exceptions.ArithmeticEvaluationException;

public class UncheckedFloatEvaluator implements Evaluator<Float> {
    @Override
    public Float sum(Float l, Float r) throws ArithmeticEvaluationException {
        return l + r;
    }

    @Override
    public Float multiply(Float l, Float r) throws ArithmeticEvaluationException {
        return l * r;
    }

    @Override
    public Float divide(Float l, Float r) throws ArithmeticEvaluationException {
        return l / r;
    }

    @Override
    public Float subtract(Float l, Float r) throws ArithmeticEvaluationException {
        return l - r;
    }

    @Override
    public Float negate(Float x) throws ArithmeticEvaluationException {
        return -x;
    }

    @Override
    public Float ofString(final String src) throws NumberFormatException {
        return Float.parseFloat(src);
    }


    @Override
    public Float mod(Float l, Float r) throws ArithmeticException {
        return (l % r);
    }

    @Override
    public Float square(Float l) throws ArithmeticException {
        return (l * l);
    }

    @Override
    public Float abs(Float l) throws ArithmeticException {
        return Math.abs(l);
    }
}
