package expression.generic.evaluators;

import expression.exceptions.ArithmeticEvaluationException;

public class UncheckedShortEvaluator implements Evaluator<Short> {

    @Override
    public Short sum(Short l, Short r) throws ArithmeticEvaluationException {
        return (short) (l + r);
    }

    @Override
    public Short multiply(Short l, Short r) throws ArithmeticEvaluationException {
        return (short) (l * r);
    }

    @Override
    public Short divide(Short l, Short r) throws ArithmeticEvaluationException {
        return (short) (l / r);
    }

    @Override
    public Short subtract(Short l, Short r) throws ArithmeticEvaluationException {
        return (short) (l - r);
    }

    @Override
    public Short negate(Short x) throws ArithmeticEvaluationException {
        return (short) (-x);
    }

    @Override
    public Short ofString(String src) throws NumberFormatException {
        return (short) (Long.parseLong(src));
    }

    @Override
    public Short mod(Short l, Short r) throws ArithmeticException {
        return (short) (l % r);
    }

    @Override
    public Short square(Short l) throws ArithmeticException {
        return (short) (l * l);
    }

    @Override
    public Short abs(Short l) throws ArithmeticException {
        return (short) ((l >= 0) ? l : (-l));
    }
}
