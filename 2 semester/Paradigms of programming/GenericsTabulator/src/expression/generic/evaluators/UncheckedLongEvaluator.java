package expression.generic.evaluators;

import expression.exceptions.ArithmeticEvaluationException;

public class UncheckedLongEvaluator implements Evaluator<Long> {

    @Override
    public Long sum(Long l, Long r) throws ArithmeticEvaluationException {
        return l + r;
    }

    @Override
    public Long multiply(Long l, Long r) throws ArithmeticEvaluationException {
        return l * r;
    }

    @Override
    public Long divide(Long l, Long r) throws ArithmeticEvaluationException {
        return l / r;
    }

    @Override
    public Long subtract(Long l, Long r) throws ArithmeticEvaluationException {
        return l - r;
    }

    @Override
    public Long negate(Long x) throws ArithmeticEvaluationException {
        return -x;
    }

    @Override
    public Long ofString(String src) throws NumberFormatException {
        return Long.parseLong(src);
    }


    @Override
    public Long mod(Long l, Long r) throws ArithmeticException {
        return (l % r);
    }

    @Override
    public Long square(Long l) throws ArithmeticException {
        return (l * l);
    }

    @Override
    public Long abs(Long l) throws ArithmeticException {
        return ((l >= 0) ? l : (-l));
    }
}
