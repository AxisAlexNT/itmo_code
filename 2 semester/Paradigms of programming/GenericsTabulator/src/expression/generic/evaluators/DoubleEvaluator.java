package expression.generic.evaluators;

import expression.exceptions.ArithmeticEvaluationException;

public class DoubleEvaluator implements Evaluator<Double> {
    @Override
    public Double sum(Double l, Double r) throws ArithmeticEvaluationException {
        return l + r;
    }

    @Override
    public Double multiply(Double l, Double r) throws ArithmeticEvaluationException {
        return l * r;
    }

    @Override
    public Double divide(Double l, Double r) throws ArithmeticEvaluationException {
        return l / r;
    }

    @Override
    public Double subtract(Double l, Double r) throws ArithmeticEvaluationException {
        return l - r;
    }

    @Override
    public Double negate(Double x) throws ArithmeticEvaluationException {
        return -x;
    }

    @Override
    public Double ofString(final String src) throws NumberFormatException {
        return Double.parseDouble(src);
    }


    @Override
    public Double mod(Double l, Double r) throws ArithmeticException {
        return (l % r);
    }

    @Override
    public Double square(Double l) throws ArithmeticException {
        return (l * l);
    }

    @Override
    public Double abs(Double l) throws ArithmeticException {
        return Math.abs(l);
    }
}
