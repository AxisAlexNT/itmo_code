package expression.generic.evaluators;

import expression.exceptions.ArithmeticEvaluationException;

import java.math.BigInteger;

public class BigIntegerEvaluator implements Evaluator<BigInteger> {
    @Override
    public BigInteger sum(BigInteger l, BigInteger r) throws ArithmeticEvaluationException {
        return l.add(r);
    }

    @Override
    public BigInteger multiply(BigInteger l, BigInteger r) throws ArithmeticEvaluationException {
        return l.multiply(r);
    }

    @Override
    public BigInteger divide(BigInteger l, BigInteger r) throws ArithmeticEvaluationException {
        return l.divide(r);
    }

    @Override
    public BigInteger subtract(BigInteger l, BigInteger r) throws ArithmeticEvaluationException {
        return l.subtract(r);
    }

    @Override
    public BigInteger negate(BigInteger x) throws ArithmeticEvaluationException {
        return x.negate();
    }

    @Override
    public BigInteger ofString(final String src) throws NumberFormatException {
        return new BigInteger(src);
    }

    @Override
    public BigInteger mod(BigInteger l, BigInteger r) throws ArithmeticException {
        return l.mod(r);
    }

    @Override
    public BigInteger square(BigInteger l) throws ArithmeticException {
        return l.multiply(l);
    }

    @Override
    public BigInteger abs(BigInteger l) throws ArithmeticException {
        return l.abs();
    }
}
