package expression.generic.evaluators;

import expression.exceptions.ArithmeticEvaluationException;
import expression.exceptions.DivisionByZeroEvaluationException;
import expression.exceptions.IntegerOverflowException;

public class CheckedIntegerEvaluator implements Evaluator<Integer> {

    @Override
    public Integer sum(Integer l, Integer r) throws ArithmeticEvaluationException {
        if (r > 0) {
            if (l > Integer.MAX_VALUE - r) {
                throw new IntegerOverflowException("Integer overflow in sum during evaluation");
            }
        } else {
            if (l < Integer.MIN_VALUE - r) {
                throw new IntegerOverflowException("Integer overflow in sum during evaluation");
            }
        }

        return l + r;
    }

    @Override
    public Integer multiply(Integer l, Integer r) throws ArithmeticEvaluationException {
        if (l > r) {
            int t = l;
            l = r;
            r = t;
        }
        // l <= r


        if (l == Integer.MIN_VALUE) {
            if ((r != 0) && (r != 1)) {
                throw new IntegerOverflowException("Integer overflow in multiplication during evaluation");
            }
        }

        if (r == Integer.MAX_VALUE) {
            if ((l == Integer.MAX_VALUE) || (abs(l) > 1)) {
                throw new IntegerOverflowException("Integer overflow in multiplication during evaluation");
            }
        }

        if ((l == 1) || (r == 1) || (l == 0) || (r == 0) || (r == -1)) {
            return l * r;
        }


        int preRes = l * r;

        if ((preRes / l != r)) {
            throw new IntegerOverflowException("Integer overflow in multiplication during evaluation");
        }

        return l * r;
    }

    @Override
    public Integer divide(Integer l, Integer r) throws ArithmeticEvaluationException {
        if (r == 0) {
            throw new DivisionByZeroEvaluationException("Division by zero occured when evaluating expression");
        }

        if ((l == Integer.MIN_VALUE) && (r == -1)) {
            throw new IntegerOverflowException("Integer overflow in division during evaluation");
        }

        return l / r;
    }

    @Override
    public Integer subtract(Integer l, Integer r) throws ArithmeticEvaluationException {
        if (r > 0) {
            if (l < Integer.MIN_VALUE + r) {
                throw new IntegerOverflowException("Integer overflow in subtraction during evaluation");
            }
        } else {
            if (l > Integer.MAX_VALUE + r) {
                throw new IntegerOverflowException("Integer overflow in subtraction during evaluation");
            }
        }
        return l - r;
    }

    @Override
    public Integer negate(Integer x) throws ArithmeticEvaluationException {
        if (x == Integer.MIN_VALUE) {
            throw new IntegerOverflowException("Negating Integer.MIN_VALUE will exceed the integer range");
        }

        return (-x);
    }

    @Override
    public Integer ofString(final String src) throws NumberFormatException {
        return Integer.valueOf(src);
    }

    @Override
    public Integer mod(Integer l, Integer r) throws ArithmeticException, DivisionByZeroEvaluationException {
        if (r == 0) {
            throw new DivisionByZeroEvaluationException("Division by zero");
        }
        return l % r;
    }

    @Override
    public Integer square(Integer l) throws ArithmeticException, ArithmeticEvaluationException {
        return multiply(l, l);
    }

    @Override
    public Integer abs(Integer l) throws ArithmeticException, IntegerOverflowException {
        if (l == Integer.MIN_VALUE) {
            throw new IntegerOverflowException("Integer overflow in abs value");
        }
        return (l >= 0) ? l : (-l);
    }
}
