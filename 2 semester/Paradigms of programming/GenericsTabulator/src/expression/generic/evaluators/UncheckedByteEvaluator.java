package expression.generic.evaluators;

import expression.exceptions.ArithmeticEvaluationException;

public class UncheckedByteEvaluator implements Evaluator<Byte> {

    @Override
    public Byte sum(Byte l, Byte r) throws ArithmeticEvaluationException {
        return (byte) (l + r);
    }

    @Override
    public Byte multiply(Byte l, Byte r) throws ArithmeticEvaluationException {
        return (byte) (l * r);
    }

    @Override
    public Byte divide(Byte l, Byte r) throws ArithmeticEvaluationException {
        return (byte) (l / r);
    }

    @Override
    public Byte subtract(Byte l, Byte r) throws ArithmeticEvaluationException {
        return (byte) (l - r);
    }

    @Override
    public Byte negate(Byte x) throws ArithmeticEvaluationException {
        return (byte) (-x);
    }

    @Override
    public Byte ofString(String src) throws NumberFormatException {
        return (byte) (Long.parseLong(src));
    }


    @Override
    public Byte mod(Byte l, Byte r) throws ArithmeticException {
        return (byte) (l % r);
    }

    @Override
    public Byte square(Byte l) throws ArithmeticException {
        return (byte) (l * l);
    }

    @Override
    public Byte abs(Byte l) throws ArithmeticException {
        return (byte) ((l > 0) ? l : (-l));
    }
}
