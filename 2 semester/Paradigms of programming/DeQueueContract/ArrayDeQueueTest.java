import queue.*;

public class ArrayDeQueueTest {
    public static void main(String[] args)
    {
        ArrayDeQueue arrayQueue = new ArrayDeQueue();

        for (int i = 0; i < 65536; i++) {
            arrayQueue.enqueue(i%6);
        }

        System.out.println("SIZE after adding: " + arrayQueue.size());
        System.out.flush();

        for (int i = 0; i < 512; i++) {
            Object o = arrayQueue.dequeue();

            if ((Integer) o != i % 6) {
                System.out.println("WARNING!!!" + i % 6 + "!=" + o + " at point #" + i);
                System.out.flush();
                break;
            }
        }

    }
}
