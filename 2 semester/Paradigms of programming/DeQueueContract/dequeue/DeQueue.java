package queue;

public interface DeQueue extends Copiable {
    /*public*/ void enqueue(Object element);
    /*public*/ void push(Object element);
    /*public*/ Object dequeue();
    /*public*/ Object remove();
    /*public*/ Object element();
    /*public*/ Object peek();
    /*public*/ int size();
    /*public*/ boolean isEmpty();
    /*public*/ void clear();
}
