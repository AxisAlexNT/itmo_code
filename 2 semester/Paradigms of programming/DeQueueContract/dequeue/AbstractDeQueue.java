package queue;

public abstract class AbstractDeQueue implements DeQueue {
    protected final int BASE_QUEUE_SIZE = 8;
    protected int length = 0;

    public AbstractDeQueue() {
        clear();
    }


    @Override
    public void enqueue(Object element) {
        assert (element != null);

        doEnqueue(element);
    }


    @Override
    public void push(Object element) {
        assert (element != null);
        doPush(element);
    }


    @Override
    public Object dequeue() {
        assert (length > 0);
        return doDequeue();
    }

    @Override
    public Object remove() {
        assert (length > 0);
        return doRemove();
    }


    @Override
    public Object element() {
        assert (length > 0);
        return doElement();
    }


    @Override
    public Object peek() {
        assert (length > 0);
        return doPeek();
    }


    @Override
    public int size() {
        assert (length >= 0);
        return length;
    }

    @Override
    public boolean isEmpty() {
        assert (length >= 0);
        return (length == 0);
    }


    @Override
    public void clear() {
        initializeFields();
        length = 0;
    }


    protected abstract void initializeFields();

    protected abstract void doEnqueue(Object element);

    protected abstract void doPush(Object element);

    protected abstract Object doDequeue();

    protected abstract Object doRemove();

    protected abstract Object doElement();

    protected abstract Object doPeek();


}
