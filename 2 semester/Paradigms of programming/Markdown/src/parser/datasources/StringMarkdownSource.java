package parser.datasources;


import parser.exceptions.MarkdownException;

public class StringMarkdownSource extends MarkdownSource {
    private final String data;

    public StringMarkdownSource(final String data) throws MarkdownException {
        this.data = data + END;
    }

    @Override
    protected char readChar() {
        return data.charAt(pos);
    }
}
