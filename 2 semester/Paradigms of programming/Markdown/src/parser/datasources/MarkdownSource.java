package parser.datasources;

import expressions.MarkdownExpression;
import parser.exceptions.MarkdownException;

import java.io.IOException;

public abstract class MarkdownSource {
    public static char END = '\0';
    public int line = 1;
    public int posInLine = -1;
    protected int pos = -1;
    private char currentChar;
    private char previousChar = '\0';
    private char followingChar;

    protected abstract char readChar() throws IOException;

    public char getChar() {
        return currentChar;
    }

    public char getPreviousChar() {
        return previousChar;
    }

    public char getFollowingChar() {
        return followingChar;
    }

    public char nextChar() throws MarkdownException {
        try {
            if (currentChar == '\n') {
                line++;
                posInLine = 0;
            }
            previousChar = currentChar;
            currentChar = followingChar;
            followingChar = readChar();
            if (followingChar == '\r') {
                followingChar = readChar();
            }
            pos++;
            posInLine++;
            return currentChar;
        } catch (final IOException e) {
            throw error("Source read error", e.getMessage());
        }
    }


    public void init() throws MarkdownException {
        nextChar();
        nextChar();
    }

    public MarkdownException error(final String format, final Object... args) throws MarkdownException {
        return new MarkdownException(line, posInLine, String.format("%d:%d: %s", line, posInLine, String.format(format, args)));
    }
}
