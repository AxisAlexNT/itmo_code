package parser.datasources;

import parser.exceptions.MarkdownException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class FileMarkdownSource extends MarkdownSource {
    private BufferedReader reader;

    public FileMarkdownSource(final String fileName) throws MarkdownException {
        try {
            reader = new BufferedReader(new FileReader(fileName, StandardCharsets.UTF_8));
        } catch (final IOException e) {
            throw error("Error opening input file '%s': %s", fileName, e.getMessage());
        }
    }

    @Override
    protected char readChar() throws IOException {
        int read = -1;
        if (reader != null) {
            read = reader.read();
            if ((read == -1)) {
                reader.close();
                reader = null;
            }
        }
        return read == -1 ? END : (char) read;
    }
}
