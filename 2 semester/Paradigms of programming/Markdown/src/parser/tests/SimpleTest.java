package parser.tests;

import parser.MarkdownParser;
import parser.datasources.FileMarkdownSource;
import parser.datasources.MarkdownSource;
import parser.exceptions.MarkdownException;

public class SimpleTest {
    public static void main(String[] args) {

        //System.out.println(Character.isSpaceChar('\n'));
        //System.out.println(Character.isWhitespace('\n'));

        MarkdownSource src = null;

        try {
            src = new FileMarkdownSource("input.txt");
        } catch (MarkdownException e) {
            e.printStackTrace();
        }

        MarkdownParser p = new MarkdownParser(src);

        String result = null;
        try {
            result = p.parse().getHTMLCode();
        } catch (MarkdownException e) {
            e.printStackTrace();
        }

        System.out.println(result);

    }
}
