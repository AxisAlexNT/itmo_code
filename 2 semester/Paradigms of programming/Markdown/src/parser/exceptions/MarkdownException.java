package parser.exceptions;

public class MarkdownException extends Exception {
    private final int pos;
    private final int line;

    public MarkdownException(final int line, final int pos, final String message) {
        super(message);
        this.line = line;
        this.pos = pos;
    }

    public int getPosition() {
        return pos;
    }

    public int getLine() {
        return line;
    }
}
