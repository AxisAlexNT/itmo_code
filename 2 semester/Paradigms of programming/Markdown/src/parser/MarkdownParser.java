package parser;

import expressions.MarkdownExpression;
import expressions.TextContents;
import expressions.structureblocks.EmptyElement;
import expressions.structureblocks.FormattedContainer;
import expressions.structureblocks.Header;
import expressions.structureblocks.Paragraph;
import expressions.structureblocks.highlights.*;
import parser.datasources.MarkdownSource;
import parser.exceptions.MarkdownException;

import java.util.HashSet;
import java.util.LinkedList;


/*

Text, header, paragraph, highlight

Block elements -- Paragraphs, Headers, // Blockquotes, Lists, Code blocks, Horizontal Rules
Span elements -- Highlights // Images, Links
Misc -- Backslash escapes


Stream  --> Block Stream | END
Block   --> [Heading] [\r]\n Paragraph [\r]\n Block
Heading --> {#}n <space> FormattedText <space> [{#}n] [\r]\n
Paragraph   --> FormattedText [\r]\n
FormattedText --> (**Text** | __Text__ | *Text* | _Text_ | `Text`) [FormattedText]
Text    --> {IsLetterOrDigit | Space | ScreenSymbols}+ sequence


Contract: Each parse<Element> function returns a MarkdownExpression and guarantees that it eats all symbols, including \r\n at the end of its line.
Функции оставляют позицию на первом символе, который должны парсить не они.
*/

public final class MarkdownParser {
    private MarkdownSource input;


    private HashSet<Character> keyChars;

    private static final EmptyElement emptyElement = new EmptyElement();


    public MarkdownParser(final MarkdownSource inputSource) {
        input = inputSource;
        this.keyChars = new HashSet<>();
        keyChars.add('*');
        keyChars.add('-');
        keyChars.add('_');
        keyChars.add('~');
        keyChars.add('`');
        keyChars.add('\\');
        //keyChars.add('&');
    }


    public MarkdownExpression parse() throws MarkdownException {
        assert (input != null) : "No input is given to parse it";
        input.init();
        return parseBlock();
    }

    private MarkdownExpression parseBlock() throws MarkdownException {
        LinkedList<MarkdownExpression> contents = new LinkedList<>();

        skipNL();

        while (!isEOS()) {
            contents.addLast(parseHeader());
        }

        return new FormattedContainer(contents);
    }


    private String parseTextLine() throws MarkdownException {
        StringBuilder textBuilder = new StringBuilder();

        while (!isEOS() && noEmptyLineAhead() && (allowedWordChar(input.getChar()))) {
            textBuilder.append(translateCharToHTMLCode(input.getChar()));
            input.nextChar();
        }

        return textBuilder.toString();
    }

    private MarkdownExpression parseHighlight(String unpairedCharacters) throws MarkdownException {
        if (unpairedCharacters.contains(make(input.getChar(), 1)) || isEOS()) {
            return emptyElement;
        }

        char prev = input.getPreviousChar();
        char cur = input.getChar();
        char next = input.getFollowingChar();

        MarkdownExpression result = emptyElement;

        switch (cur) {
            case '`':
                input.nextChar();
                result = parseHighlight(unpairedCharacters + '`');
                expect('`');
                result = new CodeHighlight(result);
                break;
            case '*':
                if (next == '*') {
                    input.nextChar();
                    input.nextChar();
                    result = parseHighlight(unpairedCharacters + '*');
                    expect('*');
                    expect('*');
                    result = new StrongHighlight(result);
                } else if (Character.isLetterOrDigit(next)) {
                    input.nextChar();
                    result = parseHighlight(unpairedCharacters + '*');
                    expect('*');
                    result = new EmphasizeHighlight(result);
                } else if (Character.isWhitespace(prev) && Character.isWhitespace(next)) {
                    input.nextChar();
                    result = new TextContents(make(cur, 1));
                }
                break;
            case '_':
                if (next == '_') {
                    input.nextChar();
                    input.nextChar();
                    result = parseHighlight(unpairedCharacters + '_');
                    expect('_');
                    expect('_');
                    result = new StrongHighlight(result);
                } else if (Character.isLetterOrDigit(next)) {
                    input.nextChar();
                    result = parseHighlight(unpairedCharacters + '_');
                    expect('_');
                    result = new EmphasizeHighlight(result);
                } else if (Character.isWhitespace(prev) && Character.isWhitespace(next)) {
                    input.nextChar();
                    result = new TextContents(make(cur, 1));
                }
                break;
            case '-':
                if (next == '-') {
                    input.nextChar();
                    input.nextChar();
                    result = parseHighlight(unpairedCharacters + '-');
                    expect('-');
                    expect('-');
                    result = new StrikeoutHighlight(result);
                } else {
                    input.nextChar();
                    result = new TextContents(make(cur, 1));
                }
                break;
            case '~':
                input.nextChar();
                result = parseHighlight(unpairedCharacters + '~');
                expect('~');
                result = new MarkHighlight(result);
                break;
            case '\\':
                if (keyChars.contains(next)) {
                    result = new TextContents(make(next, 1));
                    input.nextChar();
                    input.nextChar();
                } else {
                    result = new TextContents(make(cur, 1));
                    input.nextChar();
                }
                break;
            default:
                break;
        }

        String followedText = parseTextLine();
        LinkedList<MarkdownExpression> contents = new LinkedList<>();
        contents.addLast(result);
        contents.addLast(new TextContents(followedText));

        if (!unpairedCharacters.isEmpty()) {
            MarkdownExpression followingExpr = parseHighlight(unpairedCharacters);
            contents.addLast(followingExpr);
        }

        return new FormattedContainer(contents);

    }


    private MarkdownExpression parseFormattedText(MarkdownExpression... previous) throws MarkdownException {
        LinkedList<MarkdownExpression> contents = new LinkedList<>();

        for (MarkdownExpression p : previous) {
            contents.addLast(p);
        }


        while (noEmptyLineAhead() && !isEOS()) {
            MarkdownExpression text = parseHighlight("");
            contents.addLast(text);
        }

        return new FormattedContainer(contents);

    }


    private MarkdownExpression parseParagraph(MarkdownExpression... previous) throws MarkdownException {
        LinkedList<MarkdownExpression> contents = new LinkedList<>();

        for (MarkdownExpression p : previous) {
            contents.addLast(p);
        }

        while (noEmptyLineAhead() && !isEOS()) {
            MarkdownExpression formattedElement = parseFormattedText();
            contents.addLast(formattedElement);
        }

        skipNL();

        return new Paragraph(new FormattedContainer(contents));
    }


    private MarkdownExpression parseHeader() throws MarkdownException {

        if (isEOS()) {
            return emptyElement;
        }

        if ((input.posInLine > 1) || !test('#')) {
            return parseParagraph();
        }


        int level = 0;

        while (test('#')) {
            level++;
            input.nextChar();
        }

        if (!test(' ')) {
            return parseParagraph(new TextContents(make('#', level)));
        } else {
            input.nextChar();
        }

        MarkdownExpression headerText = parseFormattedText();

        skipNL();

        return new Header(headerText, level);
    }


    private boolean test(char c) {
        return (input.getChar() == c);
    }

    private boolean ahead(char c) {
        return (input.getFollowingChar() == c);
    }

    private boolean isEOS() {
        return test(MarkdownSource.END);
    }

    private boolean isNewLineHere() {
        return test('\n');
    }

    private boolean allowedWordChar(char c) {
        return !keyChars.contains(c);
    }


    private boolean noEmptyLineAhead() {
        return !test('\n') || !ahead('\n');
    }

    private void expect(char c) throws MarkdownException {
        if (!test(c)) {
            throw input.error("Expected char %c, found char %c", c, input.getChar());
        }

        input.nextChar();
    }


    private String translateCharToHTMLCode(char c) {
        switch (c) {
            case '<':
                return "&lt;";
            case '>':
                return "&gt;";
            case '&':
                return "&amp;";
            default:
                return Character.toString(c);
        }
    }

    private String make(char ch, int count) {
        return (Character.toString(ch)).repeat(count);
    }


    private void skipNL() throws MarkdownException {
        while (isNewLineHere()) {
            input.nextChar();
        }
    }

}
