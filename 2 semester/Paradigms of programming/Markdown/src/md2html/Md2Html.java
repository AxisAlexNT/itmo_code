package md2html;

import expressions.Expression;
import parser.MarkdownParser;
import parser.datasources.FileMarkdownSource;
import parser.datasources.MarkdownSource;
import parser.exceptions.MarkdownException;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;

public class Md2Html {
    public static void main(String[] args) {

        if (args.length != 2) {
            System.out.println("Usage: java Md2Html <input file> <output file>");
            return;
        }

        MarkdownSource src;
        MarkdownParser parser;
        Expression expr;
        String HTMLCode = "";

        try {
            src = new FileMarkdownSource(args[0]);
            parser = new MarkdownParser(src);
            expr = parser.parse();
            HTMLCode = expr.getHTMLCode();
        } catch (MarkdownException e) {
            System.out.println("Exception during parsing the source data:");
            e.printStackTrace();
        }


        String outCode = HTMLCode.trim().replace("\n</", "</");

        try (PrintWriter outputWriter = new PrintWriter(new File(args[1]), Charset.forName("utf8"))) {
            outputWriter.print(outCode);
            outputWriter.flush();
        } catch (IOException e) {
            System.out.println("Exception during writing the result:");
            e.printStackTrace();
        }
    }
}
