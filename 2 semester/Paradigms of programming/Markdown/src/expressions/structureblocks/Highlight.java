package expressions.structureblocks;

import expressions.Expression;

import java.util.LinkedList;

public abstract class Highlight extends StructureBlock {
    protected final String highlightHTMLCodename;

    public Highlight(Expression _contents, final String _highlightHTMLCodename) {
        super(_contents, _highlightHTMLCodename);
        highlightHTMLCodename = _highlightHTMLCodename;
    }


    public Highlight(LinkedList<Expression> _contents, final String _highlightHTMLCodename) {
        super(_contents, _highlightHTMLCodename);
        highlightHTMLCodename = _highlightHTMLCodename;
    }

    @Override
    public String doGetHTMLCode() {
        return String.format("<%s>%s</%s>", highlightHTMLCodename, combineContentHTML(), highlightHTMLCodename);
    }


    @Override
    public String getHTMLCode() {
        assert (HTMLBlockCodename != null);
        String innerHTML = combineContentHTML();
        return String.format("<%s>%s</%s>", HTMLBlockCodename, innerHTML, HTMLBlockCodename);
    }

}
