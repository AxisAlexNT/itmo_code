package expressions.structureblocks;

import expressions.Expression;

public class Paragraph extends StructureBlock {
    public Paragraph(Expression _contents) {
        super(_contents, "p");
    }

    @Override
    public String getHTMLCode() {
        String innerHtml = combineContentHTML();

        if (innerHtml.isBlank()) {
            return "";
        } else {
            return String.format("<p>%s</p>%n", innerHtml);
        }
    }

}
