package expressions.structureblocks;

import expressions.MarkdownExpression;

public final class EmptyElement extends MarkdownExpression {
    private static final String emptyBlockHTMLCode = "";

    public EmptyElement() {

    }

    @Override
    protected String doGetHTMLCode() {
        return emptyBlockHTMLCode;
    }
}
