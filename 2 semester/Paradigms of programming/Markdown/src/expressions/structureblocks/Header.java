package expressions.structureblocks;

import expressions.Expression;
import expressions.TextContents;

import java.util.LinkedList;

public class Header extends StructureBlock {
    private int level = 1;


    public Header(final Header anotherHeader) {
        contents = anotherHeader.contents;
        level += anotherHeader.level;
        HTMLBlockCodename = String.format("h%d", level);
    }

    public Header(final Expression _contents) {
        super(_contents);
        HTMLBlockCodename = String.format("h%d", level);
    }

    public Header(final Expression _contents, int _level) {
        super(_contents);
        level = _level;
        HTMLBlockCodename = String.format("h%d", level);
    }

    public Header(final String headerText) {
        contents = new LinkedList<>();
        contents.addLast(new TextContents(headerText));
        HTMLBlockCodename = String.format("h%d", level);
    }


    public Header(final String headerText, final int _level) {
        contents = new LinkedList<>();
        contents.addLast(new TextContents(headerText));
        level = _level;
        HTMLBlockCodename = String.format("h%d", level);
    }
}
