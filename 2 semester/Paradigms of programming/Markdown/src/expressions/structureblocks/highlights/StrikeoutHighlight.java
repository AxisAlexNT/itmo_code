package expressions.structureblocks.highlights;

import expressions.Expression;
import expressions.structureblocks.Highlight;

public class StrikeoutHighlight extends Highlight {
    public StrikeoutHighlight(Expression _contents) {
        super(_contents, "s");
    }
}
