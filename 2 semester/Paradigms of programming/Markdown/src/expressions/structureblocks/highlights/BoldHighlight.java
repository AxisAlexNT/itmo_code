package expressions.structureblocks.highlights;

import expressions.Expression;
import expressions.structureblocks.Highlight;

public class BoldHighlight extends Highlight {
    public BoldHighlight(Expression _contents) {
        super(_contents, "b");
    }
}
