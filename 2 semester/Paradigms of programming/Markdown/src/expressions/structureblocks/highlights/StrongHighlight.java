package expressions.structureblocks.highlights;

import expressions.Expression;
import expressions.structureblocks.Highlight;

import java.util.LinkedList;

public class StrongHighlight extends Highlight {
    public StrongHighlight(Expression _contents) {
        super(_contents, "strong");
    }

    public StrongHighlight(LinkedList<Expression> _contents) {
        super(_contents, "strong");
    }
}
