package expressions.structureblocks.highlights;

import expressions.Expression;
import expressions.structureblocks.Highlight;

public class CodeHighlight extends Highlight {
    public CodeHighlight(Expression _contents) {
        super(_contents, "code");
    }

//    @Override
//    public String doGetHTMLCode() {
//        String innerPreHTML = combineContentHTML();
//        innerPreHTML = innerPreHTML.replace("<", "&lt;");
//        innerPreHTML = innerPreHTML.replace(">", "&gt;");
//        innerPreHTML = innerPreHTML.replace("&", "&amp;");
//        return innerPreHTML;
//    }
}
