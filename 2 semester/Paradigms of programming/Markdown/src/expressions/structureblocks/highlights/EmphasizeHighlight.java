package expressions.structureblocks.highlights;

import expressions.Expression;
import expressions.structureblocks.Highlight;

public class EmphasizeHighlight extends Highlight {
    public EmphasizeHighlight(Expression _contents) {
        super(_contents, "em");
    }
}
