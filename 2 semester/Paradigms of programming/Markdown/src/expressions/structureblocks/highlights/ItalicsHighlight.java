package expressions.structureblocks.highlights;

import expressions.Expression;
import expressions.structureblocks.Highlight;

public class ItalicsHighlight extends Highlight {
    public ItalicsHighlight(Expression _contents) {
        super(_contents, "i");
    }
}
