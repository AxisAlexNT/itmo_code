package expressions.structureblocks.highlights;

import expressions.Expression;
import expressions.structureblocks.Highlight;

public class MarkHighlight extends Highlight {
    public MarkHighlight(Expression _contents) {
        super(_contents, "mark");
    }
}
