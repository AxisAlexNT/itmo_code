package expressions.structureblocks;

import expressions.Expression;
import expressions.MarkdownExpression;

import java.util.LinkedList;

public abstract class StructureBlock extends MarkdownExpression {
    protected String HTMLBlockCodename;

    public StructureBlock(Expression _contents, final String _HTMLBlockCodename) {
        super(_contents);
        HTMLBlockCodename = _HTMLBlockCodename;
    }

    public StructureBlock(LinkedList<Expression> _contents, final String _HTMLBlockCodename) {
        super(_contents);
        HTMLBlockCodename = _HTMLBlockCodename;
    }

    public StructureBlock(Expression _contents) {
        super(_contents);
    }

    protected StructureBlock() {
        super();
    }

    @Override
    public String getHTMLCode() {
        assert (HTMLBlockCodename != null);
        String innerHTML = combineContentHTML();
        return String.format("<%s>%s</%s>\n", HTMLBlockCodename, innerHTML, HTMLBlockCodename);
    }

    @Override
    public String doGetHTMLCode() {
        assert (HTMLBlockCodename != null);
        String innerHTML = combineContentHTML();
        return String.format("<%s>%s</%s>\n", HTMLBlockCodename, innerHTML, HTMLBlockCodename);
    }
}
