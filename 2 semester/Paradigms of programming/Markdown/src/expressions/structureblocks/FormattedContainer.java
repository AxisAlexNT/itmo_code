package expressions.structureblocks;

import expressions.Expression;
import expressions.MarkdownExpression;

import java.util.LinkedList;

public class FormattedContainer extends MarkdownExpression {

    public FormattedContainer(LinkedList<MarkdownExpression> _contents) {
        contents = new LinkedList<>();
        for (Expression e : _contents) {
            contents.addLast(e);
        }
    }

    @Override
    protected String doGetHTMLCode() {
        return combineContentHTML();
    }
}
