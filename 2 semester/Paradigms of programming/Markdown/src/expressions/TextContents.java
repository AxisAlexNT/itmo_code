package expressions;

public class TextContents extends MarkdownExpression {
    private String text;

    public TextContents(String _text) {
        text = _text;
    }

    @Override
    protected String doGetHTMLCode() {
        return text;
    }
}
