package expressions;

import java.util.LinkedList;

public abstract class MarkdownExpression implements Expression {
    public LinkedList<Expression> contents;

    public MarkdownExpression() {
        contents = new LinkedList<>();
    }

    public MarkdownExpression(Expression _contents) {
        contents = new LinkedList<>();
        contents.addLast(_contents);
    }

    public MarkdownExpression(LinkedList<Expression> _contents) {
        contents = _contents;
    }

    @Override
    public String getHTMLCode() {
        return doGetHTMLCode();
    }

    protected abstract String doGetHTMLCode();

    protected String combineContentHTML() {
        StringBuilder codeBuilder = new StringBuilder();

        if (contents != null) {
            for (Expression element : contents) {
                codeBuilder.append(element.getHTMLCode());
            }
        }

        return codeBuilder.toString();
    }
}
