package expressions;

import java.util.LinkedList;

public interface Expression {
    public String getHTMLCode();
}
