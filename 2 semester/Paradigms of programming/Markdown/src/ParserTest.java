import expressions.MarkdownExpression;
import parser.MarkdownParser;
import parser.datasources.MarkdownSource;
import parser.datasources.StringMarkdownSource;
import parser.exceptions.MarkdownException;

public class ParserTest {
    public static void main(String[] args) {

        String result = "";

        try {
            MarkdownSource src = new StringMarkdownSource("# Hello #");

            MarkdownParser parser = new MarkdownParser(src);

            MarkdownExpression expr = parser.parse();

            result = expr.getHTMLCode();

        } catch (MarkdownException e) {
            e.printStackTrace();
        }


        System.out.println(result);

    }
}
