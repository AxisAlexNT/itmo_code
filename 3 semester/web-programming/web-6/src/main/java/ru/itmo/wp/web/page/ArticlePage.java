package ru.itmo.wp.web.page;

import com.google.common.base.Strings;
import ru.itmo.wp.model.domain.Article;
import ru.itmo.wp.model.domain.User;
import ru.itmo.wp.model.exception.ValidationException;
import ru.itmo.wp.model.repository.ArticleRepository;
import ru.itmo.wp.model.repository.impl.ArticleRepositoryImpl;
import ru.itmo.wp.model.service.ArticleService;
import ru.itmo.wp.model.service.UserService;
import ru.itmo.wp.web.exception.RedirectException;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/** @noinspection unused*/
public class ArticlePage {
    private final UserService userService = new UserService();
    private final ArticleService articleService = new ArticleService();
    private final ArticleRepository articleRepository = new ArticleRepositoryImpl();

    private void action(HttpServletRequest request, Map<String, Object> view) {
        if (request.getSession().getAttribute("user") == null)
        {
            request.getSession().setAttribute("message", "You have to be registered in order to create articles");
            throw new RedirectException("/index");
        }
    }

    private void addArticle(HttpServletRequest request, Map<String, Object> view) throws ValidationException {
        String title = request.getParameter("title");
        String text = request.getParameter("text");

        User user = (User) request.getSession().getAttribute("user");

        Article article = new Article();
        article.setTitle(title);
        article.setText(text);
        article.setUserId(user.getId());

        articleService.validateArticle(article);

        articleRepository.save(article);

        request.getSession().setAttribute("message", "Article created successfully!");

        throw new RedirectException("/index");
    }
}
