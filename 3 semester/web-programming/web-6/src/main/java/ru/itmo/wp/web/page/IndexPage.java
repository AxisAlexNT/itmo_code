package ru.itmo.wp.web.page;

import com.google.common.base.Strings;
import ru.itmo.wp.model.domain.Article;
import ru.itmo.wp.model.domain.User;
import ru.itmo.wp.model.repository.ArticleRepository;
import ru.itmo.wp.model.repository.UserRepository;
import ru.itmo.wp.model.repository.impl.ArticleRepositoryImpl;
import ru.itmo.wp.model.repository.impl.UserRepositoryImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @noinspection unused
 */
public class IndexPage {
    private ArticleRepository articleRepository = new ArticleRepositoryImpl();
    private UserRepository userRepository = new UserRepositoryImpl();

    private void action(HttpServletRequest request, Map<String, Object> view) {
        putMessage(request, view);
    }

    private void putMessage(HttpServletRequest request, Map<String, Object> view) {
        String message = (String) request.getSession().getAttribute("message");
        if (!Strings.isNullOrEmpty(message)) {
            view.put("message", message);
            request.getSession().removeAttribute("message");
        }
    }


    private void fetchArticles(HttpServletRequest request, Map<String, Object> view) {
        List<Article> articles = articleRepository.findAllVisibleArticles();
        view.put("articles", articles);
        HashMap<Long, String> userNames = new HashMap<>();

        for (User u : userRepository.findAll()) {
            userNames.put(u.getId(), u.getLogin());
        }

        view.put("userNames", userNames);
    }
}
