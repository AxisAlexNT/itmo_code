package ru.itmo.wp.web.page;

import ru.itmo.wp.model.domain.Article;
import ru.itmo.wp.model.domain.User;
import ru.itmo.wp.model.exception.ValidationException;
import ru.itmo.wp.model.repository.ArticleRepository;
import ru.itmo.wp.model.repository.impl.ArticleRepositoryImpl;
import ru.itmo.wp.model.service.ArticleService;
import ru.itmo.wp.model.service.UserService;
import ru.itmo.wp.web.exception.RedirectException;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @noinspection unused
 */
public class MyArticlesPage {
    private final UserService userService = new UserService();
    private final ArticleService articleService = new ArticleService();
    private final ArticleRepository articleRepository = new ArticleRepositoryImpl();

    private void action(HttpServletRequest request, Map<String, Object> view) {
        if (request.getSession().getAttribute("user") == null) {
            request.getSession().setAttribute("message", "You have to be registered in order to create articles");
            throw new RedirectException("/index");
        }

        User currentUser = (User) request.getSession().getAttribute("user");
        long currentUserId = currentUser.getId();

        view.put("userArticles", articleRepository.findArticlesByUserId(currentUserId));
    }

    private void toggleArticleVisibility(HttpServletRequest request, Map<String, Object> view) throws ValidationException {
        User currentUser = (User) request.getSession().getAttribute("user");

        if (currentUser == null) {
            throw new ValidationException("You have to be authorized");
        }

        long currentUserId = currentUser.getId();

        long articleId = Long.parseLong(request.getParameter("articleId"));

        Article processingArticle = articleRepository.findById(articleId);

        if (processingArticle == null) {
            throw new ValidationException("No such article");
        }

        if (processingArticle.getUserId() != currentUserId) {
            throw new ValidationException("You do not have rights to change visibility of this article");
        }

        processingArticle.setHidden(!processingArticle.isHidden());

        articleRepository.updateVisibility(processingArticle);

        view.put("buttonText", processingArticle.isHidden() ? "Show" : "Hide");
    }
}
