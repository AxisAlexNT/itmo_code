package ru.itmo.wp.web.page;

import ru.itmo.wp.model.domain.User;
import ru.itmo.wp.model.exception.ValidationException;
import ru.itmo.wp.model.repository.UserRepository;
import ru.itmo.wp.model.repository.impl.UserRepositoryImpl;
import ru.itmo.wp.model.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @noinspection unused
 */
public class UsersPage {
    private final UserService userService = new UserService();
    private final UserRepository userRepository = new UserRepositoryImpl();

    private void action(HttpServletRequest request, Map<String, Object> view) {
        User currentUser = (User) request.getSession().getAttribute("user");

        if (currentUser != null) {
            currentUser = userService.find(currentUser.getId());
            view.put("user", currentUser);
            request.getSession().setAttribute("user", currentUser);
        }
    }

    private void findAll(HttpServletRequest request, Map<String, Object> view) {
        view.put("users", userService.findAll());
    }

    private void findUser(HttpServletRequest request, Map<String, Object> view) {
        view.put("user",
                userService.find(Long.parseLong(request.getParameter("userId"))));
    }

    private void toggleAdmin(HttpServletRequest request, Map<String, Object> view) throws ValidationException {
        User currentUser = (User) request.getSession().getAttribute("user");

        if (currentUser == null) {
            throw new ValidationException("You have to be authorized");
        }


        if (!currentUser.isAdmin()) {
            throw new ValidationException("You have to be admin to promote/demote other users");
        }

        User victim = userService.find(Long.parseLong(request.getParameter("userId")));

        if (victim == null) {
            throw new ValidationException("Requested user cannot be found");
        }

        victim.setAdmin(!victim.isAdmin());

        userRepository.updateAdmin(victim);

        if (victim.getId() == currentUser.getId()) {
            request.getSession().setAttribute("user", victim);
        }

        view.put("buttonText", victim.isAdmin() ? "Demote" : "Promote");
    }
}
