package ru.itmo.wp.model.service;

import com.google.common.base.Strings;
import ru.itmo.wp.model.domain.Article;
import ru.itmo.wp.model.exception.ValidationException;
import ru.itmo.wp.model.repository.ArticleRepository;
import ru.itmo.wp.model.repository.impl.ArticleRepositoryImpl;

import java.util.List;

/**
 * @noinspection UnstableApiUsage
 */
public class ArticleService {
    private final ArticleRepository articleRepository = new ArticleRepositoryImpl();

    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    public List<Article> findAllReverseOrder() {
        return articleRepository.findAllReverseOrder();
    }

    public Article findById(long id) {
        return articleRepository.findById(id);
    }

    public void validateArticle(Article article) throws ValidationException {
        if (article == null)
        {
            throw new ValidationException("Article cannot be null");
        }

        if (Strings.isNullOrEmpty(article.getTitle()))
        {
            throw new ValidationException("Article title cannot be empty");
        }

        if (Strings.isNullOrEmpty(article.getText()))
        {
            throw new ValidationException("Article text cannot be empty");
        }
    }
}
