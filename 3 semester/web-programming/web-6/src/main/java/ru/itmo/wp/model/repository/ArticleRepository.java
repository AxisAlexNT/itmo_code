package ru.itmo.wp.model.repository;

import ru.itmo.wp.model.domain.Article;

import java.util.List;

public interface ArticleRepository {
    Article findById(long id);

    List<Article> findArticlesByUserId(long userId);

    List<Article> findAll();

    List<Article> findAllReverseOrder();

    List<Article> findAllVisibleArticles();

    void save(Article article);

    void updateVisibility(Article article);
}
