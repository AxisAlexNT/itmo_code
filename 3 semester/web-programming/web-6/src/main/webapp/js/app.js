window.notify = function (message) {
    $.notify(message, {position: "bottom right"})
};


ajax = function (data, onSuccess) {
    $.ajax({
        type: "POST",
        url: "",
        dataType: "json",
        data: data,
        success: function(response)
        {
            onSuccess(response);
            if (response["error"]) {
                notify(response["error"]);
            }

            if (response["redirect"])
            {
                location.href = response["redirect"];
            }
        }
    });
};