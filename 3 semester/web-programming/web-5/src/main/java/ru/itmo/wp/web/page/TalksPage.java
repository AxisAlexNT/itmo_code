package ru.itmo.wp.web.page;

import com.google.common.base.Strings;
import ru.itmo.wp.model.domain.Talk;
import ru.itmo.wp.model.domain.User;
import ru.itmo.wp.model.exception.RepositoryException;
import ru.itmo.wp.model.exception.ValidationException;
import ru.itmo.wp.web.exception.RedirectException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public class TalksPage extends Page {
    @Override
    protected void action() {
        super.action();
    }

    @Override
    protected void before(HttpServletRequest request, Map<String, Object> view) {
        super.before(request, view);
        if (getUser() == null) {
            setMessage("You have to be authorized in order to access talks section");
            throw new RedirectException("/index");
        }
        List<Talk> userTalks = talkRepository.findUserTalks(getUser().getId());
        view.put("messages", userTalks);
    }

    protected void sendMessage(HttpServletRequest request) throws ValidationException {
        User sourceUser = getUser();
        User targetUser;
        long targetUserId = Long.parseLong(request.getParameter("targetUserId"));
        try {
            targetUser = userRepository.find(targetUserId);
        } catch (RepositoryException e) {
            throw new ValidationException("Target user cannot be found");
        }
        String message = request.getParameter("message");

        if (Strings.isNullOrEmpty(message)) {
            throw new ValidationException("Message should not be empty");
        }

        if (message.length() > 250) {
            throw new ValidationException("Message should contain less than 250 characters");
        }

        Talk talk = new Talk();
        talk.setSourceUserId(sourceUser.getId());
        talk.setTargetUserId(targetUser.getId());
        talk.setText(message);

        talkRepository.save(talk);

        throw new RedirectException("/talks");
    }


}
