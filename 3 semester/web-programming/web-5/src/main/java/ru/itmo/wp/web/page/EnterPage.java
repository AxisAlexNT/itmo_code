package ru.itmo.wp.web.page;

import ru.itmo.wp.model.domain.Event;
import ru.itmo.wp.model.domain.User;
import ru.itmo.wp.model.exception.ValidationException;
import ru.itmo.wp.web.exception.RedirectException;

import javax.servlet.http.HttpServletRequest;

/**
 * @noinspection unused
 */
public class EnterPage extends Page {
    //private final UserService userService = new UserService();


    private void enter(HttpServletRequest request) throws ValidationException {
        String loginOrEmail = request.getParameter("loginOrEmail");
        String password = request.getParameter("password");

        User user = userService.validateAndFindByLoginOrEmailAndPassword(loginOrEmail, password);
        setUser(user);
        setMessage("Hello, " + user.getLogin());
        Event enterEvent = new Event();
        enterEvent.setUserId(user.getId());
        enterEvent.setType(Event.MessageType.ENTER);
        eventRepository.save(enterEvent);
        throw new RedirectException("/index");
    }
}
