package ru.itmo.wp.web.page;

import com.google.common.base.Strings;
import ru.itmo.wp.model.domain.User;
import ru.itmo.wp.model.repository.EventRepository;
import ru.itmo.wp.model.repository.TalkRepository;
import ru.itmo.wp.model.repository.UserRepository;
import ru.itmo.wp.model.repository.impl.EventRepositoryImpl;
import ru.itmo.wp.model.repository.impl.TalkRepositoryImpl;
import ru.itmo.wp.model.repository.impl.UserRepositoryImpl;
import ru.itmo.wp.model.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class Page {
    public static final String USER_COUNT = "userCount";
    protected final UserService userService = new UserService();
    protected final EventRepository eventRepository = new EventRepositoryImpl();
    protected final UserRepository userRepository = new UserRepositoryImpl();
    protected final TalkRepository talkRepository = new TalkRepositoryImpl();
    protected HttpServletRequest request;
    protected Map<String, Object> view;


    protected void before(HttpServletRequest request, Map<String, Object> view) {
        this.request = request;
        this.view = view;
        view.put(USER_COUNT, userService.findCount());
        putUsers();
        putCurrentUser();
        putMessage();
    }

    protected void action() {
        //No operations
    }

    protected void after(HttpServletRequest request, Map<String, Object> view) {
        //putMessage();
    }


    protected void putCurrentUser() {
        User user = getUser();
        if (user != null) {
            view.put("user", user);
        }
    }


    protected void putMessage() {
        String message = (String) request.getSession().getAttribute("message");
        if (!Strings.isNullOrEmpty(message)) {
            view.put("message", message);
            request.getSession().removeAttribute("message");
        }
    }

    protected void setMessage(String message) {
        request.getSession().setAttribute("message", message);
    }

    protected User getUser() {
        return (User) request.getSession().getAttribute("user");
    }

    protected void setUser(User user) {
        request.getSession().setAttribute("user", user);
    }

    protected void putUsers() {
        view.put("users", userService.findAll());
    }

}
