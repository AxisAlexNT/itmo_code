package ru.itmo.wp.web.page;

import ru.itmo.wp.model.domain.User;
import ru.itmo.wp.model.exception.ValidationException;
import ru.itmo.wp.web.exception.RedirectException;

import javax.servlet.http.HttpServletRequest;

/**
 * @noinspection unused
 */
public class RegisterPage extends Page {
    //private final UserService userService = new UserService();

    protected void action() {
        // No operations.
    }

    private void register(HttpServletRequest request) throws ValidationException {
        User user = new User();
        user.setLogin(request.getParameter("login"));
        user.setEmail(request.getParameter("email"));
        String password = request.getParameter("password");
        String confirmation = request.getParameter("confirmation");


        userService.validateRegistration(user, password, confirmation);
        userService.register(user, password);

        request.getSession().setAttribute("message", "You are successfully registered!");
        throw new RedirectException("/index");
    }
}
