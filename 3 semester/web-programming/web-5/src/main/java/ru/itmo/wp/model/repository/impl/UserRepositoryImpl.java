package ru.itmo.wp.model.repository.impl;

import ru.itmo.wp.model.database.DatabaseUtils;
import ru.itmo.wp.model.domain.User;
import ru.itmo.wp.model.exception.RepositoryException;
import ru.itmo.wp.model.repository.UserRepository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;

public class UserRepositoryImpl extends BasicRepositoryImpl implements UserRepository {
    private final DataSource DATA_SOURCE = DatabaseUtils.getDataSource();

    @Override
    public User find(long id) {
        return findOneQueryMatch(User.class, "SELECT * FROM User WHERE id=?", id);
    }

    @Override
    public User findByLogin(String login) {
        return findOneQueryMatch(User.class, "SELECT * FROM User WHERE login=?", login);
    }

    @Override
    public User findByEmail(String email) {
        return findOneQueryMatch(User.class, "SELECT * FROM User WHERE email=?", email);
    }


    @Override
    public User findByLoginOrEmailAndPasswordSha(String loginOrEmail, String passwordSha) {
        return findOneQueryMatch(User.class, "SELECT * FROM User WHERE ((login=? OR email=?) AND passwordSha=?)", loginOrEmail, loginOrEmail, passwordSha);
    }


    @Override
    public List<User> findAll() {
        return findAllQueryMatch(User.class, "SELECT * FROM User ORDER BY id DESC");
    }


    private User toUser(ResultSetMetaData metaData, ResultSet resultSet) throws SQLException {
        return construct(User.class, resultSet, metaData);
    }

    @Override
    public void save(User user, String passwordSha) {
        ResultSet generatedKeys = executeQueryAndGetGeneratedKeys("INSERT INTO `User` (`login`, `passwordSha`, `email`, `creationTime`) VALUES (?, ?, ?, NOW())", 1, user.getLogin(), passwordSha, user.getEmail());
        try {
            if (generatedKeys.next()) {
                user.setId(generatedKeys.getLong(1));
                user.setCreationTime(find(user.getId()).getCreationTime());
            } else {
                throw new RepositoryException("Can't save User [no autogenerated fields].");
            }
        } catch (SQLException e) {
            throw new RepositoryException("Can't save User", e);
        }
    }


    @Override
    public long findCount() {
        return executeCountQuery("SELECT COUNT(*) FROM User");
    }
}
