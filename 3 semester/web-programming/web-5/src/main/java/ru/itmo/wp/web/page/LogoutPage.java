package ru.itmo.wp.web.page;

import ru.itmo.wp.model.domain.Event;
import ru.itmo.wp.model.domain.User;
import ru.itmo.wp.web.exception.RedirectException;

import javax.servlet.http.HttpServletRequest;

/**
 * @noinspection unused
 */
public class LogoutPage extends Page {
    private void action(HttpServletRequest request) {
        User user = getUser();

        request.getSession().removeAttribute("user");

        setMessage("Good bye. Hope to see you soon!");

        if (user != null) {
            Event logoutEvent = new Event();
            logoutEvent.setUserId(user.getId());
            logoutEvent.setType(Event.MessageType.LOGOUT);
            eventRepository.save(logoutEvent);
        }

        throw new RedirectException("/index");
    }
}
