package ru.itmo.wp.web.page;

/**
 * @noinspection unused
 */
public class NotFoundPage extends Page {
    protected void action() {
        // No operations.
    }
}
