package ru.itmo.wp.model.repository.impl;

import ru.itmo.wp.model.database.DatabaseUtils;
import ru.itmo.wp.model.domain.Event;
import ru.itmo.wp.model.exception.RepositoryException;

import javax.sql.DataSource;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BasicRepositoryImpl {
    private final DataSource DATA_SOURCE = DatabaseUtils.getDataSource();

    public ResultSet executeQueryAndGetGeneratedKeys(String queryString, int expectedUpdateCount, Object... args) {
        try (Connection connection = DATA_SOURCE.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(queryString, Statement.RETURN_GENERATED_KEYS)) {
                for (int i = 1; i <= args.length; i++) {
                    statement.setString(i, args[i - 1].toString());
                }

                if (statement.executeUpdate() != expectedUpdateCount) {
                    throw new RepositoryException("Can't execute insert query: it has updated less fields than expected.");
                } else {
                    return statement.getGeneratedKeys();
                }
            }
        } catch (SQLException e) {
            throw new RepositoryException("Can't execute insert query.", e);
        }
    }


    public Pair<ResultSet, ResultSetMetaData> executeStatementAndGetResult(String queryString, Object... args) {
        try (Connection connection = DATA_SOURCE.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(queryString, Statement.RETURN_GENERATED_KEYS)) {
                for (int i = 1; i <= args.length; i++) {
                    statement.setString(i, args[i - 1].toString());
                }

                try (ResultSet resultSet = statement.executeQuery()) {
                    if (!resultSet.next()) {
                        return null;
                    }
                    return new Pair<>(resultSet, statement.getMetaData());
                }
            }
        } catch (SQLException e) {
            throw new RepositoryException("Can't execute insert query.", e);
        }
    }

    public <T> List<T> findAllQueryMatch(Class<T> clazz, String queryString, Object... args) {
        List<T> answer = new ArrayList<>();
        try (Connection connection = DATA_SOURCE.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(queryString, Statement.RETURN_GENERATED_KEYS)) {
                for (int i = 1; i <= args.length; i++) {
                    statement.setString(i, args[i - 1].toString());
                }

                T t;
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (!resultSet.next()) {
                        return null;
                    }
                    while (((t = construct(clazz, resultSet, resultSet.getMetaData())) != null)) {
                        answer.add(t);
                        if (!resultSet.next()) {
                            break;
                        }
                    }
                }
            }
        } catch (SQLException e) {
            throw new RepositoryException("Can't execute insert query.", e);
        }

        return answer;
    }


    public <T> T findOneQueryMatch(Class<T> clazz, String queryString, Object... args) {
        try (Connection connection = DATA_SOURCE.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(queryString, Statement.RETURN_GENERATED_KEYS)) {
                for (int i = 1; i <= args.length; i++) {
                    statement.setString(i, args[i - 1].toString());
                }

                try (ResultSet resultSet = statement.executeQuery()) {
                    if ((resultSet == null) || (!resultSet.next())) {
                        return null;
                    }
                    T t = construct(clazz, resultSet, resultSet.getMetaData());
                    return t;
                }
            }

        } catch (SQLException e) {
            throw new RepositoryException("Can't execute insert query.", e);
        }
    }


    public <T> T construct(Class<T> clazz, ResultSet resultSet, ResultSetMetaData metaData) throws SQLException {
        if (clazz == null || resultSet == null || metaData == null) {
            return null;
        }

        T constructedObject;
        try {
            //noinspection deprecation
            constructedObject = clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RepositoryException("Cannot construct basic object.", e);
        }

        String columnName;
        String setterMethodName;

        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            columnName = metaData.getColumnName(i);
            columnName = Character.toUpperCase(columnName.charAt(0)) + columnName.substring(1);
            setterMethodName = "set" + columnName;

            Method setter = null;


//            if (!resultSet.isClosed() && !resultSet.next())
//            {
//                continue;
//            }

            String fieldValue = "";

            try {
                fieldValue = resultSet.getString(i);
            } catch (Exception e) {
                continue;
            }

            Object value = null;

            try {
                switch (metaData.getColumnType(i)) {
                    case Types.BIGINT:
                        value = Long.parseLong(fieldValue);
                        setter = clazz.getDeclaredMethod(setterMethodName, long.class);
                        break;
                    case Types.BOOLEAN:
                        value = Boolean.parseBoolean(fieldValue);
                        setter = clazz.getDeclaredMethod(setterMethodName, boolean.class);
                        break;
                    case Types.VARCHAR:
                    case Types.LONGVARCHAR:
                        value = String.valueOf(fieldValue);
                        setter = clazz.getDeclaredMethod(setterMethodName, String.class);
                        break;
                    case Types.DATE:
                    case Types.TIMESTAMP:
                    case Types.TIME:
                        java.util.Date d = resultSet.getTimestamp(i);
                        value = d;
                        setter = clazz.getDeclaredMethod(setterMethodName, java.util.Date.class);
                        break;
                    case Types.CHAR:
                        String charName = resultSet.getString(i);
                        switch (charName) {
                            case "ENTER":
                                value = Event.MessageType.ENTER;
                                break;
                            case "LOGOUT":
                                value = Event.MessageType.LOGOUT;
                                break;
                            default:
                                break;
                        }
                        setter = clazz.getDeclaredMethod(setterMethodName, Event.MessageType.class);
                        break;
                    case Types.OTHER:
                    case Types.JAVA_OBJECT:
                        String typeName = metaData.getColumnTypeName(i);
                        throw new RepositoryException("Parser for other/java objects is not implemented");
                    default:
                        typeName = metaData.getColumnTypeName(i);
                        break;
                }
            } catch (NoSuchMethodException e) {
                continue;
                //throw new RepositoryException("Cannot construct object: field setter method not found.", e);
            }

            if (setter != null) {
                try {
                    setter.setAccessible(true);
                    setter.invoke(constructedObject, value);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    throw new RepositoryException("Cannot construct object: setter is not invokable.", e);
                }
            }

        }

        return constructedObject;
    }


    public long executeCountQuery(String queryString, Object... args) {
        try (Connection connection = DATA_SOURCE.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(queryString)) {
                for (int i = 1; i <= args.length; i++) {
                    statement.setString(i, args[i - 1].toString());
                }

                try (ResultSet resultSet = statement.executeQuery()) {
                    if (!resultSet.next()) {
                        throw new RepositoryException("No users are counted by SQL request.");
                    }

                    ResultSetMetaData metaData = statement.getMetaData();

                    for (int i = 1; i <= metaData.getColumnCount(); ++i) {
                        if ("COUNT(*)".equals(metaData.getColumnName(i))) {
                            return resultSet.getLong(i);
                        }
                    }
                }
            }
            throw new RepositoryException("No users are counted by SQL request.");
        } catch (SQLException e) {
            throw new RepositoryException("Can't count users.", e);
        }
    }


    private static class Pair<K, V> {
        K first;
        V second;

        public Pair(K first, V second) {
            this.first = first;
            this.second = second;
        }

        public K getFirst() {
            return first;
        }

        public void setFirst(K first) {
            this.first = first;
        }

        public V getSecond() {
            return second;
        }

        public void setSecond(V second) {
            this.second = second;
        }
    }
}