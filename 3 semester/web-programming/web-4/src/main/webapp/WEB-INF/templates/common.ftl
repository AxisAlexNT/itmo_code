<#-- @ftlvariable name="answer" type="java.util.Map<java.lang.String, ru.itmo.tpl.model.User>" -->
<#-- @ftlvariable name="sect" type="java.lang.String" -->
<#-- @ftlvariable name="requestUri" type="java.lang.String" -->
<#-- @ftlvariable name="url" type="java.lang.String" -->
<#-- @ftlvariable name="caption" type="java.lang.String" -->
<#-- @ftlvariable name="name" type="java.lang.String" -->
<#-- @ftlvariable name="user" type="ru.itmo.tpl.model.User" -->
<#-- @ftlvariable name="post" type="ru.itmo.tpl.model.Post" -->
<#-- @ftlvariable name="logged_user_id" type="java.lang.Long" -->
<#-- @ftlvariable name="userId" type="java.lang.Long" -->


<#macro header>
    <header>
        <a href="/index"><img src="/img/logo.png" alt="Codeforces" title="Codeforces"/></a>
        <div class="languages">
            <a href="#"><img src="/img/gb.png" alt="In English" title="In English"/></a>
            <a href="#"><img src="/img/ru.png" alt="In Russian" title="In Russian"/></a>
        </div>

        <#assign user=findBy(users, "id", logged_user_id!(-1))!>


        <div class="enter-or-register-box">
            <#if user?? && user?has_content>
                <@userlink userToLink=user/>
                |
                <a href="#">Logout</a>
            <#else>
                <a href="#">Enter</a>
                |
                <a href="#">Register</a>
            </#if>
        </div>


        <#assign menuItems=[{"url":"/index", "caption":"Index"},{"url":"/misc/help", "caption":"Help"}, {"url":"/users", "caption":"Users"}, {"url":"/posts", "caption":"Blogs"}]>

        <#assign indexCounter=0>
        <#list menuItems as item>
            <#assign indexCounter=indexCounter+1>
            <#assign url=item["url"]>
            <#assign caption=item["caption"]>
            <#if requestUri?starts_with(url?string)>
                <#assign sect=indexCounter>
                <#break>
            </#if>
        </#list>

        <#if sect??>
            <style>
                nav > ul > li:nth-child(${sect}) {
                    border-bottom: 3px solid #3B5998;
                }
            </style>
        </#if>

        <nav>
            <ul>
                <#list menuItems as item>
                    <#assign indexCounter=indexCounter+1>
                    <#assign url=item["url"]>
                    <#assign caption=item["caption"]>
                    <li><a href="${url}">${caption}</a></li>
                </#list>
            </ul>
        </nav>
    </header>
</#macro>



<#macro sidebar>
    <aside>
        <#list posts as post>
            <@sidebarPostBlock post=post/>
        </#list>
    </aside>
</#macro>

<#macro footer>
    <footer>
        <a href="#">Codeforces</a> &copy; 2010-2019 by Mike Mirzayanov
    </footer>
</#macro>

<#macro page>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Codeforces</title>
        <link rel="stylesheet" type="text/css" href="/css/normalize.css">
        <link rel="stylesheet" type="text/css" href="/css/style.css">
        <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    </head>
    <body>
    <@header/>
    <div class="middle">
        <@sidebar/>
        <main>
            <#nested/>
        </main>
    </div>
    <@footer/>
    </body>
    </html>
</#macro>

<#macro userlink userToLink nameOnly=false>
    <#if nameOnly?has_content && nameOnly==true>
        <a href="/user?handle=${userToLink.handle}">${userToLink.name}</a>
    <#else>
        <a href="/user?handle=${userToLink.handle}" class="userlink-${userToLink.color}">${userToLink.name}</a>
    </#if>
</#macro>

<#macro sidebarPostBlock post>
    <#if post?? && post?has_content>
        <section>
            <div class="header">
                Post #${post.id}
            </div>
            <div class="body">
                ${post.getPreviewText()}
            </div>
            <div class="footer">
                <a href="/post?post_id=${post.id}">View all</a>
            </div>
        </section>
    </#if>
</#macro>


<#macro showPost post previewMode=false>
    <#assign authorUser=findBy(users, "id", post.user_id!(-1))!>
    <#assign authorName=authorUser.handle!"">
    <article>
        <div class="title"><a href="/post?post_id=${post.id!}">${post.title!}</a></div>
        <div class="information">By <@userlink userToLink=authorUser/></div>
        <div class="body">
            <#if previewMode?has_content && previewMode==true>
                ${post.getPreviewText()!}
            <#else>
                ${post.getText()!}
            </#if>
        </div>
        <div class="footer">
            <div class="left">
                <img src="img/voteup.png" title="Vote Up" alt="Vote Up"/>
                <span class="positive-score">+173</span>
                <img src="img/votedown.png" title="Vote Down" alt="Vote Down"/>
            </div>
            <div class="right">
                <img src="img/date_16x16.png" title="Publish Time" alt="Publish Time"/>
                `2 days ago`
                <img src="img/comments_16x16.png" title="Comments" alt="Comments"/>
                <a href="#">0</a>
            </div>
        </div>
    </article>
</#macro>

<#macro listPosts previewMode=false onlyByUserId=-1>
    <#assign anyPostFound=false>
    <ul class="blogs-list">
        <#list posts as p>
            <#if p?? && p?has_content && (onlyByUserId==-1 || p.user_id==onlyByUserId)>
                <#assign anyPostFound=true>
                <li class="blog-item">
                    <@showPost post=p previewMode=previewMode/>
                </li>
            </#if>
        </#list>
    </ul>
    <#if anyPostFound==false>
        <h1>No posts found</h1>
    </#if>
</#macro>


<#function findBy items key id>
    <#list items as item>
        <#if item[key]==id>
            <#return item/>
        </#if>
    </#list>
</#function>

<#function findWithNeighbours items key id>
    <#assign answer={"prev":"null", "current":"null", "next":"null"}>

    <#list items as item>
        <#if answer["next"]=="shouldbe">
            <#assign answer={"prev":answer["prev"], "current":answer["current"], "next":item}>
            <#return answer>
        </#if>

        <#if item[key]==id>
            <#if !item?has_next>
                <#assign answer={"prev":answer["prev"], "current":item, "next":"null"}>
                <#return answer>
            </#if>
            <#assign answer={"prev":answer["prev"], "current":item, "next":"shouldbe"}>
        </#if>

        <#if answer["current"]=="null">
            <#assign answer={"prev":item, "current":"null", "next":"null"}>
        </#if>
    </#list>

    <#return answer>
</#function>

<#macro maybeuserlink maybeuser text>
    <#if maybeuser??>
        <#if maybeuser!="null">
            <#if maybeuser.id??>
                <a href="/user?handle=${maybeuser.handle}">${text}</a>
                <#return>
            </#if>
        </#if>
    </#if>
    ${text}
</#macro>


<#macro usertable caption>
    <div class="datatable">
        <div class="caption">${caption}</div>
        <table>
            <thead>
            <tr>
                <th>Id</th>
                <th>Handle</th>
                <th>Full name</th>
            </tr>
            </thead>
            <tbody>
            <#list users as user>
                <#if user?? && user?has_content>
                    <tr>
                        <td>${user.id}</td>
                        <td>${user.handle}</td>
                        <td><@userlink userToLink=user/></td>
                    </tr>
                </#if>
            </#list>
            </tbody>
        </table>
    </div>
</#macro>

<#function countUserPosts userId=-1>
    <#assign counter=0>
    <#list posts as p>
        <#if p?? && p?has_content && p.user_id==userId>
            <#assign counter=counter+1>
        </#if>
    </#list>
    <#return counter>
</#function>