package ru.itmo.tpl.util;

import ru.itmo.tpl.model.Color;
import ru.itmo.tpl.model.Post;
import ru.itmo.tpl.model.User;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class DataUtil {
    private static Color defaultUserColor = Color.BLUE;

    
    private static final List<User> USERS = Arrays.asList(
            new User(1, "MikeMirayanov", "Mikhail Mirzayanov", Color.RED),
            new User(2, "tourist", "Genady Korotkevich", Color.GREEN),
            new User(3, "emusk", "Elon Musk", defaultUserColor),
            new User(5, "pashka", "Pavel Mavrin", Color.RED),
            new User(7, "geranazavr555", "Georgiy Nazarov", defaultUserColor),
            new User(11, "cannon147", "Erofey Bashunov", Color.GREEN)
    );

    private static final List<Post> POSTS = Arrays.asList(
            new Post(1, "The standard Lorem Ipsum passage, used since the 1500s", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", 1),
            new Post(7, "Section 1.10.32 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC", "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?", 5),
            new Post(18, "1914 translation by H. Rackham", "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?", 7),
            new Post(14, "Задание 5", "В сайдбаре в блоках Pay attention тоже отображайте тексты постов (тоже не более 250 символов). Фразу Pay attention замените на “Post #?” (его id). По ссылке View all уводите на страницу /post?post_id=?, на которой должен быть уже один не сокращённый пост. Таким образом, посты у вас будут отображаться на двух страницах (/index и /post?post_id=?). Не допускайте copy-paste: они обе должны пользоваться одним макросом отображения одного поста. На странице с одним постом текст поста сокращать не надо (ваш макрос должен поддерживать как сокращенный способ отображения, так и полный).\n" + "\n" + "Добавьте в профиль пользователя количество его постов ссылкой на новую страницу /posts?user_id=?, по которой осуществляется переход на список всех постов пользователя. Она должна выглядеть совсем как главная, но фильтровать посты по заданному пользователю. Вам надо снова переиспользовать макрос для отображения постов.\n", 1)
    );

    private static List<User> getUsers() {
        return USERS;
    }

    private static List<Post> getPosts() {
        return POSTS;
    }

    public static void putData(Map<String, Object> data) {
        data.put("users", getUsers());
        data.put("posts", getPosts());

        for (User user : getUsers()) {
            if (Long.toString(user.getId()).equals(data.get("logged_user_id"))) {
                data.put("user", user);
            }
        }
    }
}
