package ru.itmo.wp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itmo.wp.domain.Notice;
import ru.itmo.wp.form.NoticeContents;
import ru.itmo.wp.service.NoticeService;
import ru.itmo.wp.service.UserService;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
public class NoticeAddPage extends Page {
    private final NoticeService noticeService;


    public NoticeAddPage(UserService userService, NoticeService noticeService) {
        this.noticeService = noticeService;
    }

    @GetMapping("/notice/add")
    public String registerGet(Model model, HttpSession httpSession) {
        model.addAttribute("noticeContents", new NoticeContents());
        return "NoticeAddPage";
    }

    @PostMapping("/notice/add")
    public String addNotice(@Valid @ModelAttribute("addNoticeForm") NoticeContents addNoticeForm,
                            BindingResult bindingResult,
                            HttpSession httpSession) {
        if (bindingResult.hasErrors()) {
            return "NoticeAddPage";
        }

        Notice notice = new Notice();
        notice.setContent(addNoticeForm.getContent());
        noticeService.save(notice);

        putMessage(httpSession, "Notice was added!");

        return "redirect:/";
    }
}
