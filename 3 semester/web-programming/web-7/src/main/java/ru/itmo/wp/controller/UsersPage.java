package ru.itmo.wp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itmo.wp.domain.User;
import ru.itmo.wp.service.UserService;

import javax.servlet.http.HttpServletRequest;

@Controller
public class UsersPage extends Page {
    private final UserService userService;

    public UsersPage(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users/all")
    public String users(Model model) {
        model.addAttribute("users", userService.findAll());
        return "UsersPage";
    }

    @PostMapping("/users/toggleUser")
    public String toggleUser(HttpServletRequest request, Model model) {
        try {
            long id = Long.parseLong(request.getParameter("toggleId"));
            User user = userService.findById(id);
            user.setEnabled(!user.isEnabled());
            userService.save(user);
        } catch (NumberFormatException | NullPointerException ignored) {
            putMessage(request.getSession(), "Wrong user id specified for toggling");
        } finally {
            model.addAttribute("users", userService.findAll());
        }
        return "UsersPage";
    }
}
