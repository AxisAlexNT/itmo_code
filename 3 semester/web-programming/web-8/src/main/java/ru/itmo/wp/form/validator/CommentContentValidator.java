package ru.itmo.wp.form.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.itmo.wp.form.CommentTextIncapsulator;

@Component
public class CommentContentValidator implements Validator {

    public CommentContentValidator() {
    }

    public boolean supports(Class<?> clazz) {
        return CommentTextIncapsulator.class.equals(clazz);
    }

    public void validate(Object target, Errors errors) {
        if (!errors.hasErrors()) {
            CommentTextIncapsulator content = (CommentTextIncapsulator) target;

            if (content.getText().isBlank()) {
                errors.rejectValue("text", "text.is-blank", "comment cannot be blank");
            }
        }
    }
}
