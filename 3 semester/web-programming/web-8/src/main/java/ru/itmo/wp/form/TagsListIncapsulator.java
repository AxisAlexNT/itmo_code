package ru.itmo.wp.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class TagsListIncapsulator {
    @NotNull
    @Pattern(regexp = "[a-z\\s]*", message = "Expected lowercase Latin letters")
    private String tagsList = "";

    public String getTagsList() {
        return tagsList;
    }

    public void setTagsList(String tagsList) {
        this.tagsList = tagsList;
    }
}
