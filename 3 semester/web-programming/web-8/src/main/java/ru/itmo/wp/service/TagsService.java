package ru.itmo.wp.service;

import org.springframework.stereotype.Service;
import ru.itmo.wp.domain.Tag;
import ru.itmo.wp.repository.TagsRepository;

import java.util.HashSet;
import java.util.Set;

@Service
public class TagsService {
    private final TagsRepository tagsRepository;

    public TagsService(TagsRepository tagsRepository) {
        this.tagsRepository = tagsRepository;
    }

    public Set<Tag> parseTags(String rawTagsList) {
        HashSet<Tag> result = new HashSet<>();
        if (rawTagsList == null) {
            return result;
        }

        String[] tagTokens = rawTagsList.split("\\s");
        for (String tagToken : tagTokens) {
            if (!tagToken.isBlank()) {
                result.add(getOrCreateTagByName(tagToken.trim()));
            }
        }

        return result;
    }

    public Tag getOrCreateTagByName(String name) {
        Tag tag = tagsRepository.findByName(name);
        if (tag == null) {
            tag = new Tag();
            tag.setName(name);
            return tagsRepository.save(tag);
        } else {
            return tag;
        }
    }
}
