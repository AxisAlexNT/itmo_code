package ru.itmo.wp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ru.itmo.wp.domain.Comment;
import ru.itmo.wp.domain.Post;
import ru.itmo.wp.domain.User;
import ru.itmo.wp.form.CommentTextIncapsulator;
import ru.itmo.wp.form.validator.CommentContentValidator;
import ru.itmo.wp.security.Guest;
import ru.itmo.wp.service.PostService;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
public class PostPage extends Page {
    private final PostService postService;
    private final CommentContentValidator commentContentValidator;

    public PostPage(PostService postService, CommentContentValidator commentContentValidator) {
        this.postService = postService;
        this.commentContentValidator = commentContentValidator;
    }

    @InitBinder("commentForm")
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(commentContentValidator);
    }

    @Guest
    @GetMapping("/post/{id}")
    public String postPreview(Model model, @PathVariable("id") String id_String) {
        try {
            long id = Long.parseLong(id_String);
            model.addAttribute("post", postService.findById(id));
            model.addAttribute("commentForm", new CommentTextIncapsulator());
        } catch (NumberFormatException e) {
            model.addAttribute("post", null);
        }
        return "PostPage";
    }

    @PostMapping("/post/{id}/addComment")
    public String addComment(@PathVariable("id") String post_id_String,
                             @Valid @ModelAttribute("commentForm") CommentTextIncapsulator commentTextIncapsulator,
                             BindingResult bindingResult,
                             HttpSession httpSession) {
        if (bindingResult.hasErrors()) {
            putMessage(httpSession, "Your comment cannot be blank");
            return "redirect:/post/" + post_id_String;
        }

        try {
            long post_id = Long.parseLong(post_id_String);
            Post post = postService.findById(post_id);
            if (post == null){
                putMessage(httpSession, "No post with id " + post_id);
                return "PostPage";
            }

            User user = getUser(httpSession);


            Comment comment = new Comment();
            comment.setText(commentTextIncapsulator.getText());
            comment.setUser(user);
            postService.addComment(post, comment);
            putMessage(httpSession, "Your comment was added successfully");

            return "redirect:/post/" + post_id_String;
        }catch (NumberFormatException e){
            putMessage(httpSession, "Wrong post id");
            return "PostPage";
        }
    }

}
