package ru.itmo.wp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itmo.wp.domain.Post;
import ru.itmo.wp.domain.Role;
import ru.itmo.wp.domain.Tag;
import ru.itmo.wp.form.TagsListIncapsulator;
import ru.itmo.wp.form.validator.TagsListValidator;
import ru.itmo.wp.security.AnyRole;
import ru.itmo.wp.service.TagsService;
import ru.itmo.wp.service.UserService;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Set;

@Controller
public class WritePostPage extends Page {
    private final UserService userService;
    private final TagsService tagService;
    private final TagsListValidator tagsListValidator;

    public WritePostPage(UserService userService, TagsService tagService, TagsListValidator tagsListValidator) {
        this.userService = userService;
        this.tagService = tagService;
        this.tagsListValidator = tagsListValidator;
    }

    @InitBinder("tagsList")
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(tagsListValidator);
    }

    @AnyRole({Role.Name.WRITER, Role.Name.ADMIN})
    @GetMapping("/writePost")
    public String writePostGet(Model model) {
        model.addAttribute("post", new Post());
        model.addAttribute("tagsList", new TagsListIncapsulator());
        return "WritePostPage";
    }

    @AnyRole({Role.Name.WRITER, Role.Name.ADMIN})
    @PostMapping("/writePost")
    public String writePostPost(@Valid @ModelAttribute("post") Post post,
                                BindingResult bindingResultPost,
                                @Valid @ModelAttribute("tagsList") TagsListIncapsulator tagsList,
                                BindingResult bindingResultTagsList,
                                HttpSession httpSession) {
        if (bindingResultPost.hasErrors() || bindingResultTagsList.hasErrors()) {
            return "WritePostPage";
        }

        Set<Tag> tags = tagService.parseTags(tagsList.getTagsList());
        post.setTags(tags);

        userService.writePost(getUser(httpSession), post);
        putMessage(httpSession, "You published new post");

        return "redirect:/posts";
    }
}