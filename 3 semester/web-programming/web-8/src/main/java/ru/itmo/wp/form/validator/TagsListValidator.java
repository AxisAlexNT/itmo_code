package ru.itmo.wp.form.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.itmo.wp.form.TagsListIncapsulator;

@Component
public class TagsListValidator implements Validator {

    public TagsListValidator() {
    }

    public boolean supports(Class<?> clazz) {
        return TagsListIncapsulator.class.equals(clazz);
    }

    public void validate(Object target, Errors errors) {
        if (!errors.hasErrors()) {
            String rawTagsList = ((TagsListIncapsulator) target).getTagsList();
            String[] tags = rawTagsList.split("\\s");
            for (String tag : tags) {
                if (!tag.isBlank()) {
                    if (tag.chars().filter(Character::isAlphabetic).count() != tag.length()) {
                        errors.rejectValue("tagsList", "tagsList.invalid-tag-symbols", "Tags should contain only alphabetic symbols");
                    }
                }
            }
        }
    }
}
