package ru.itmo.wp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itmo.wp.domain.Tag;

public interface TagsRepository extends JpaRepository<Tag, Long> {
    Tag findByName(String name);

    Tag findById(long id);
}
