package ru.itmo.wp.servlet;

import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.Map;

public class MessengerServlet extends HttpServlet {
    private static final LinkedList<Message> messageDB = new LinkedList<>();
    private static final URLDecoder urlDecoder = new URLDecoder();


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding(StandardCharsets.UTF_8.displayName());
        PrintWriter outputWriter = response.getWriter();
        HttpSession session = request.getSession();
        Gson jsoner = new Gson();

        String uri = request.getRequestURI();

        Map<String, String[]> params = request.getParameterMap();


        String responseBody = "";

        switch (uri) {
            case "/message/auth": {
                String userNameFromRequest = "";
                String responseData = "";

                response.setContentType("application/json");

                if (params.containsKey("user")) { // && (params.get("user").length == 1)) { // Note: might be changed depending on the behaviour required
                    userNameFromRequest = params.get("user")[0];
                    session.setAttribute("user", userNameFromRequest);
                    responseData = userNameFromRequest;
                } else if (session.getAttribute("user") != null) {
                    responseData = session.getAttribute("user").toString();
                } else {
                    responseData = "";
                }

                responseBody = String.format("\"%s\"", responseData);
            }
            break;


            case "/message/findAll": {
                responseBody = jsoner.toJson(messageDB);
            }
            break;

            case "/message/add": {
                if (session.getAttribute("user") != null) {
                    String userNameFromSession = session.getAttribute("user").toString();
                    if (params.containsKey("text")) {
                        messageDB.add(new Message(userNameFromSession, params.get("text")[0]));
                    } else {
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    }

                } else {
                    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                }
            }
            break;

            default: {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
            break;

        }

        outputWriter.print(responseBody);
        outputWriter.flush();
    }


    private static class Message {
        private String user = "";
        private String text = "";

        public Message(String username, String text) {
            this.user = username;
            this.text = text;
            return;
        }

        @Override
        public String toString() {
            return String.format("{\"user\":\"%s\",\"text\":\"%s\"}", URLEncoder.encode(user, StandardCharsets.UTF_8), URLEncoder.encode(text, StandardCharsets.UTF_8));
        }
    }
}

