package ru.itmo.wp.servlet;

import ru.itmo.wp.util.ImageUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

public class CaptchaFilter extends HttpFilter {
    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            HttpSession session = request.getSession();
            if (("GET".equals(request.getMethod())) && (!"true".equals(session.getAttribute("captchaPassed")))) {
                String requestUri = request.getRequestURI();
                PrintWriter outputWriter = response.getWriter();

                int cpt = 0;

                if ((session.getAttribute("captchaPassed") == null) || (session.getAttribute("userCaptcha") == null)) {
                    Random rnd = new Random();
                    cpt = 100 + rnd.nextInt(899);
                    session.setAttribute("captchaPassed", "false");
                    session.setAttribute("userCaptcha", cpt);
                    session.setAttribute("desiredUrl", requestUri);
                }

                cpt = (int) session.getAttribute("userCaptcha");


                if (requestUri.contains("favicon.ico")) {
                    System.out.println("Favicon is asked");
                    return;
                }

                if (requestUri.startsWith("/img/captcha")) {
                    response.setStatus(HttpServletResponse.SC_OK);
                    response.setContentType("image/png");
                    response.getOutputStream().write(ImageUtils.toPng(String.valueOf(cpt)));
                    response.getOutputStream().flush();
                    System.out.println("Captcha image is asked");
                    return;
                }

                if (requestUri.startsWith("/captcha")) {
                    int recievedCaptcha = 0;

                    try {
                        recievedCaptcha = Integer.parseInt(request.getParameter("captcha_field"));
                        cpt = Integer.parseInt(session.getAttribute("userCaptcha").toString());
                    } catch (Exception e) {
                        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                        outputWriter.flush();
                        // System.out.println("user captcha is wrong in session");
                        throw e;
                    } finally {
                        String desiredUrl = "";
                        if (session.getAttribute("desiredUrl") != null) {
                            desiredUrl = session.getAttribute("desiredUrl").toString();
                        }

                        if (cpt == recievedCaptcha) {
                            session.setAttribute("captchaPassed", "true");
                            // System.out.println("Captcha OK");
                            session.removeAttribute("desiredUrl");
                        } else {
                            // outputWriter.printf("Captcha is wrong");
                            session.removeAttribute("userCaptcha");
                            session.removeAttribute("captchaPassed");
                        }
                        response.sendRedirect(desiredUrl);
                        outputWriter.flush();
                    }
                } else {
                    response.setStatus(HttpServletResponse.SC_OK);
                    response.setContentType("text/html");
                    outputWriter.print(
                            "<html><head>Please, enter captcha</head><body>\n" +
                                    "<img src=\"img/captcha.png\">" +
                                    "<form action=\"captcha\" method=\"get\">\n" +
                                    "\t<label for=\"captcha_field\">Enter CAPTCHA:</label>\n" +
                                    "\t<input id=\"captcha_field\" name=\"captcha_field\">\n"
                                    + "</form></body></html>"
                    );
                    outputWriter.flush();
                    // System.out.println("response sent");
                }
            } else {
                super.doFilter(request, response, chain);
            }
        } finally {
            // System.out.println("Captcha filter finished");
        }
    }
}
