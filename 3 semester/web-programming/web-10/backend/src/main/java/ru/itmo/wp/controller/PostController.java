package ru.itmo.wp.controller;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.itmo.wp.domain.Comment;
import ru.itmo.wp.domain.Post;
import ru.itmo.wp.domain.User;
import ru.itmo.wp.exception.ValidationException;
import ru.itmo.wp.form.CommentContentsIncapsulator;
import ru.itmo.wp.form.PostContentsIncapsulator;
import ru.itmo.wp.repository.PostRepository;
import ru.itmo.wp.service.JwtService;
import ru.itmo.wp.service.PostService;
import ru.itmo.wp.util.BindingResultUtils;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/1")
public class PostController extends ApiController {
    private final PostService postService;
    private final PostRepository postRepository;
    private final JwtService jwtService;

    public PostController(PostService postService, PostRepository postRepository, JwtService jwtService) {
        this.postService = postService;
        this.postRepository = postRepository;
        this.jwtService = jwtService;
    }

    @GetMapping("posts")
    public List<Post> findPosts() {
        return postService.findAll();
    }

    @PostMapping("posts")
    public void addComment(@RequestBody @Valid PostContentsIncapsulator postContents, BindingResult bindingResult, @RequestAttribute User user) {
        if (bindingResult.hasErrors()) {
            throw new ValidationException(BindingResultUtils.getErrorMessage(bindingResult));
        }

        if (user == null) {
            throw new ValidationException("You should be authorized in order to publish posts");
        }

        Post post = new Post();
        post.setTitle(postContents.getTitle());
        post.setText(postContents.getText());
        post.setUser(user);
        user.getPosts().add(post);

        postRepository.save(post);
    }


    @PostMapping("addComment")
    public void addComment(@RequestBody @Valid CommentContentsIncapsulator commentContents, BindingResult bindingResult, @RequestAttribute User user) {
        if (bindingResult.hasErrors()) {
            throw new ValidationException(BindingResultUtils.getErrorMessage(bindingResult));
        }

        if (user == null) {
            throw new ValidationException("You should be authorized in order to publish comments");
        }

        Post post = postRepository.findById(commentContents.getPostId()).orElseThrow(() -> new ValidationException("Wrong post id"));

        Comment comment = new Comment();
        comment.setText(commentContents.getText());

        comment.setUser(user);
        post.addComment(comment);
        postRepository.save(post);
    }
}
