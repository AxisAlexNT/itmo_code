package ru.itmo.wp.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CommentContentsIncapsulator {
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 250, message = "Comment should be 1-250 symbols long")
    private String text;

    private long postId;


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getPostId() {
        return postId;
    }

    public void setPostId(long postId) {
        this.postId = postId;
    }
}
