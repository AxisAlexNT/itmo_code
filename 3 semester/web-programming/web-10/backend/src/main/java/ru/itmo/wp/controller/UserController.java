package ru.itmo.wp.controller;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ru.itmo.wp.domain.User;
import ru.itmo.wp.exception.ValidationException;
import ru.itmo.wp.form.RegisterUserCredentials;
import ru.itmo.wp.form.validator.UserCredentialsRegisterValidator;
import ru.itmo.wp.repository.UserRepository;
import ru.itmo.wp.service.JwtService;
import ru.itmo.wp.service.UserService;
import ru.itmo.wp.util.BindingResultUtils;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/1")
public class UserController extends ApiController {
    private final UserRepository userRepository;
    private final UserService userService;
    private final JwtService jwtService;
    private final UserCredentialsRegisterValidator userCredentialsRegisterValidator;

    public UserController(UserRepository userRepository, UserService userService, JwtService jwtService, UserCredentialsRegisterValidator userCredentialsRegisterValidator) {
        this.userRepository = userRepository;
        this.userService = userService;
        this.jwtService = jwtService;
        this.userCredentialsRegisterValidator = userCredentialsRegisterValidator;
    }

    @GetMapping("users/authorized")
    public User findAuthorized(User user) {
        return user;
    }

    @GetMapping("users")
    public List<User> getAllUsers()
    {
        return userRepository.findAll();
    }

    @InitBinder("registerUserCredentials")
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(userCredentialsRegisterValidator);
    }

    @PostMapping("users")
    public String registerAndIssueNewJwt(@RequestBody @Valid RegisterUserCredentials registerUserCredentials, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new ValidationException(BindingResultUtils.getErrorMessage(bindingResult));
        }

        userService.createNewUser(registerUserCredentials);

        return jwtService.create(registerUserCredentials.getLogin(), registerUserCredentials.getPassword());
    }
}
