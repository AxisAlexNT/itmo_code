package ru.itmo.wp.form.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.itmo.wp.form.RegisterUserCredentials;
import ru.itmo.wp.repository.UserRepository;

@Component
public class UserCredentialsRegisterValidator implements Validator {
    private final UserRepository userRepository;

    public UserCredentialsRegisterValidator(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean supports(Class<?> clazz) {
        return RegisterUserCredentials.class.equals(clazz);
    }

    public void validate(Object target, Errors errors) {
        if (!errors.hasErrors()) {
            RegisterUserCredentials registerForm = (RegisterUserCredentials) target;
            if (!userRepository.findByLogin(registerForm.getLogin()).isEmpty())
            {
                errors.reject("login.login-is-occupied", "This login is occupied already");
            }
        }
    }
}
