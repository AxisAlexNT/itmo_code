package ru.itmo.wp.service;

import org.springframework.stereotype.Service;
import ru.itmo.wp.domain.User;
import ru.itmo.wp.form.RegisterUserCredentials;
import ru.itmo.wp.repository.UserRepository;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User findByLoginAndPassword(String login, String password) {
        return login == null || password == null ? null : userRepository.findByLoginAndPassword(login, password);
    }

    public User createNewUser(RegisterUserCredentials credentials)
    {
        User user = new User();
        user.setName(credentials.getName());
        user.setLogin(credentials.getLogin());
        user.setAdmin(false);
        user = userRepository.save(user);
        userRepository.updatePasswordSha(user.getId(), user.getLogin(), credentials.getPassword());

        return findByLoginAndPassword(credentials.getLogin(), credentials.getPassword());
    }
}
