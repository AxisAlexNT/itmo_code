import java.io.IOException;

public class ConsoleFastScanner {


    public static void main(String[] args) {


        //try (CharacterFastScanner fs = new CharacterFastScanner("1 2 3")) {

        try (CharacterFastScanner fs = new CharacterFastScanner(System.in)) {

            System.out.println("Asking hasNext()");
            while (fs.hasNext()) {
                System.out.println("Has next passed");

                if (fs.nextTokenIsNewLine()) {
                    System.out.println("New line char!");
                }

                if (fs.hasNextInt()) {
                    System.out.println("Int: " + fs.nextInt());
                    continue;
                } else {
                    //System.out.println("Next token is not an int");
                }
                if (fs.nextTokenIsNewLine()) {
                    System.out.println("New line char!");
                } else {
                    //System.out.println("Not INT not CRLF. Skipping. nextToken was " + fs.nextToken);
                    fs.skipCurrentToken();
                }
                //System.out.println("Asking hasNext()");
            }

            System.out.println("No next data");
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}

