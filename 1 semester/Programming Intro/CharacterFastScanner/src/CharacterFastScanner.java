import java.io.*;
import java.util.InputMismatchException;

/*
    Grammar:

    S --> <type><del>S
    S --> <del>S
    <type> --> <int> | <float> | <string>
    <nat> --> {0..9}+
    <int> --> {+-}*<nat>
    <float> --> <float.parse()> //<int>.<nat> | <int>,<nat>

    <del> <-> is any whitespace char (delimeter)

 */


class nextStruct<T> {
    T next;
    String baseToken;

    public nextStruct() {

    }

    public nextStruct(T _next, String _baseToken) {
        next = _next;
        baseToken = _baseToken;
    }
}


public class CharacterFastScanner implements Closeable {

    public String nextToken = null;
    private int BUFFER_SIZE = 1024;
    private String charsetName = "UTF8";
    private InputStreamReader inputStreamReader = null;
    private char[] buffer;
    private int currentPosition = -1;
    private int readSize;
    private boolean streamClosed = false;
    private boolean streamFinished = false;
    private boolean needToCheckBufferSize = true;
    private StringBuilder tokenBuilder = null;
    private StringBuilder lineBuilder = null;
    private nextStruct<Integer> _nextInt;
    private nextStruct<String> _nextString;
    private nextStruct<Float> _nextFloat;
    private String nextLine;


    public CharacterFastScanner(String s) throws IOException {
        if (s == null) {
            throw new IOException("Cannot scan empty string!");
        }

        BUFFER_SIZE = s.length();
        needToCheckBufferSize = false;

        buffer = s.toCharArray();
        currentPosition = 0;
        readSize = BUFFER_SIZE;
        streamClosed = false;
        streamFinished = true;


    }


    CharacterFastScanner(InputStream inputStream) {
        inputStreamReader = new InputStreamReader(inputStream);
        buffer = new char[BUFFER_SIZE];
        try {
            bufferize();
        } catch (IOException e) {

        }
    }

    CharacterFastScanner(InputStream inputStream, String _charsetName) throws IOException {
        if (needToCheckBufferSize && (BUFFER_SIZE <= 512)) {
            throw new IOException("CharacterFastScanner's buffer size must be more than 512 bytes (a minimum HDD sector size)!");
        }
        charsetName = _charsetName;
        inputStreamReader = new InputStreamReader(inputStream, charsetName);
        buffer = new char[BUFFER_SIZE];
        bufferize();
    }

    CharacterFastScanner(InputStream _inputStream, int bufferSize) throws IOException {
        BUFFER_SIZE = bufferSize;
        if (needToCheckBufferSize && (BUFFER_SIZE <= 512)) {
            throw new IOException("CharacterFastScanner's buffer size must be more than 512 bytes (a minimum HDD sector size)!");
        }
        inputStreamReader = new InputStreamReader(_inputStream);
        buffer = new char[BUFFER_SIZE];
        bufferize();
        _inputStream.
    }


    CharacterFastScanner(File f, String _charsetName) throws IOException {
        buffer = new char[BUFFER_SIZE];
        charsetName = _charsetName;
        inputStreamReader = new InputStreamReader(new FileInputStream(f), charsetName);
        bufferize();
    }


    @Override
    public void close() throws IOException {
        if (inputStreamReader != null) {
            inputStreamReader.close();
            inputStreamReader = null;
        }
    }


    private void bufferize() throws IOException {
        // System.out.println("bufferize()");
        if ((!streamClosed) && (!streamFinished) && (inputStreamReader != null)) {
            try {
                if (buffer == null) {
                    buffer = new char[BUFFER_SIZE];
                }


                readSize = inputStreamReader.read(buffer);

                currentPosition = 0;

                if (readSize == -1) {
                    streamFinished = true;
                    inputStreamReader.close();
                }

            } catch (IOException e) {
                try {
                    inputStreamReader.close();
                } catch (IOException closingException) {
                    throw new IOException("CharacterFastScanner failed to get next bytes: " + e.getMessage() + "\n and failed to close the stream: " + closingException.getMessage());
                } finally {
                    streamFinished = true;
                    streamClosed = true;
                    inputStreamReader.close();
                    inputStreamReader = null;
                }
                throw new IOException("CharacterFastScanner failed to get next bytes: " + e.getMessage());
            }
        }
    }

    private void ifNeedBufferize() throws IOException {
        if (((currentPosition == -1) || (currentPosition >= readSize)) && (!streamFinished) && (!streamClosed)) {
            bufferize();
            currentPosition = 0;
        }
    }


    private void skipToNextTokenStart() throws IOException {
        ifNeedBufferize();

        // System.out.println("buffed");

        while ((currentPosition < readSize) && (Character.isWhitespace(buffer[currentPosition]))) {
            ifNeedBufferize();
            currentPosition++;
        }

        // System.out.println("exited skip nts");
        // System.out.println("next token starts at #" + currentPosition);
    }


    private String getNextToken() throws IOException {
        skipToNextTokenStart();
        // System.out.println("skipped to ns");

        tokenBuilder = new StringBuilder();

        // // System.out.println("Ready to build tokens");

        while ((currentPosition < readSize) && (!Character.isWhitespace(buffer[currentPosition]))) {
            tokenBuilder.append(buffer[currentPosition]);
            currentPosition++;
            ifNeedBufferize();
        }

        nextToken = tokenBuilder.toString();
        return nextToken;
    }


    private boolean updateToken() {
        if (nextToken == null) {
            try {
                nextToken = getNextToken();
                if (nextToken.length() <= 0) {
                    return false;
                }
                return true;
            } catch (IOException ioe) {
                // // System.out.println();
                return false;
            }
        }

        return true;
    }


    public boolean hasNextInt() {
        return hasNextInt(10);
    }


    public boolean hasNextInt(int radix) {
        // // System.out.println("hasNextInt()");
        if (!updateToken()) {
            // System.out.println("Cannot update token");
            return false;
        }


        try {
            _nextInt = new nextStruct<>(Integer.parseInt(nextToken, radix), nextToken);
            // System.out.println("Yes, it has nextInteger: " + _nextInt.next);
            return true;
        } catch (NumberFormatException nfe) {
            // System.out.println("Not an integer: " + nextToken);
            return false;
        }
    }


    public int nextInt(int radix) throws InputMismatchException {
        if ((_nextInt == null) || (!_nextInt.baseToken.equals(nextToken))) {
            if (hasNextInt(radix)) {
                int res = _nextInt.next;
                _nextInt = null;
                nextToken = null;
                return res;
            } else {
                throw new InputMismatchException("Stream does not contain a valid number!");
            }
        }

        int res = _nextInt.next;
        _nextInt = null;
        nextToken = null;
        return res;
    }

    public int nextInt() throws InputMismatchException {
        return nextInt(10);
    }


    public boolean hasNextFloat() {
        if (!updateToken()) {
            return false;
        }

        try {
            _nextFloat = new nextStruct<>(Float.parseFloat(nextToken), nextToken);
            return true;
        } catch (NumberFormatException nfe) {
            //// System.out.println("Not a float: " + nextToken);
            return false;
        }
    }


    public float nextFloat() throws InputMismatchException {
        if ((_nextFloat == null) || (!_nextFloat.baseToken.equals(nextToken))) {
            if (hasNextFloat()) {
                float res = _nextFloat.next;
                _nextFloat = null;
                nextToken = null;
                return res;
            } else {
                throw new InputMismatchException("Stream does not contain a valid number!");
            }
        }

        float res = _nextFloat.next;
        _nextFloat = null;
        nextToken = null;
        return res;
    }


    public boolean hasNextByte() {
        try {
            ifNeedBufferize();
            return (currentPosition < readSize);
        } catch (IOException e) {
            return false;
        }
    }


    public byte nextByte() throws InputMismatchException {
        if (hasNextByte()) {
            char nextChar = buffer[currentPosition];
            String symb = Character.toString(nextChar);
            byte[] thisCharInBytes = symb.getBytes();

            byte answ = thisCharInBytes[0];

            if (thisCharInBytes.length > 1) {

                for (int i = 0; i < thisCharInBytes.length - 1; i++) {
                    thisCharInBytes[i] = thisCharInBytes[i + 1];
                }
                thisCharInBytes[thisCharInBytes.length - 1] = 0;

                buffer[currentPosition] = (new String(thisCharInBytes).toCharArray())[0];
            } else {
                currentPosition++;
            }


            return answ;
        }

        throw new InputMismatchException("Stream does not contain a valid byte!");
    }


    public void skipCurrentToken() {
        nextToken = null;
        try {
            ifNeedBufferize();
            getNextToken();
        } catch (IOException e) {
        }
    }

    public boolean hasNext() {
        // System.out.println("Called hasNext() at #" + currentPosition);
//        if (currentPosition < readSize)
//        {
//            // System.out.println("Buffer first element ord: " + Character.getNumericValue(buffer[currentPosition]));
//        }
        try {
            ifNeedBufferize();
            // System.out.println("hasNext() <-> " + ((currentPosition < readSize) || (nextToken.length() > 0) || (getNextToken().length() > 0)));
            return ((currentPosition < readSize) || (nextToken.length() > 0) || (getNextToken().length() > 0));
        } catch (Exception e) {
            // System.out.println("hasNext() exn <-> false");
            return false;
        }
    }


    public String nextLine() throws IOException {
        ifNeedBufferize();
        lineBuilder = new StringBuilder();
        while (currentPosition < readSize) {
            if ((buffer[currentPosition] == '\r') || (buffer[currentPosition] == '\n')) {
                break;
            }

            lineBuilder.append(buffer[currentPosition]);
            currentPosition++;
            ifNeedBufferize();
        }
        return lineBuilder.toString();
    }


    public boolean nextTokenIsNewLine() {
        // System.out.println("called nextTokenIsNewLine()");
        try {
            if ((readSize > 0) && (currentPosition >= 0) && ((buffer[0] == '\n') || (buffer[0] == '\r'))) {
                return true;
            }
            ifNeedBufferize();

            if ((currentPosition < readSize) && ((buffer[currentPosition] == '\n') || (buffer[currentPosition] == '\r'))) {
                return true;
            }

            while ((currentPosition < readSize) && (Character.isSpaceChar(buffer[currentPosition]))) {
                currentPosition++;
                ifNeedBufferize();
            }

            // Whitespace, but not just space
            return (currentPosition < readSize) && ((buffer[currentPosition] == '\n') || (buffer[currentPosition] == '\r')); //Character.isWhitespace(buffer[currentPosition]);
        } catch (IOException e) {
            return false;
        }
    }


}



/*

        new CharacterFastScanner(new InputStream() {

            private String str = new String(s);
            private int pos = 0;

            @Override
            public int read() {
                if (pos < str.length()) {
                    pos++;
                    return str.charAt(pos - 1);
                } else {
                    return -1;
                }
            }
        });

 */




/*

    public boolean hasNextLine()
    {
        try{
            ifNeedBufferize();

            StringBuilder lineBuilder = new StringBuilder();

            while(currentPosition < readSize)
            {
                if ((buffer[currentPosition] == '\r') || (buffer[currentPosition] == '\n'))
                {
                    break;
                }
            }
        } catch (IOException e) {
            return false;
        }
    }
 */