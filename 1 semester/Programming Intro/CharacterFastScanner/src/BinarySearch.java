package search;


public class BinarySearch {
    public static int[] sourceIntegers;
    public static int x;

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Usage: BinarySearch <target integer> <descending array of integers>");
            return;
        }

        if (args.length == 1) {
            System.out.println("0");
            return;
        }

        x = Integer.parseInt(args[0]);


        sourceIntegers = new int[args.length - 1];
        for (int i = 0; i < args.length - 1; i++) {
            sourceIntegers[i] = Integer.parseInt(args[i + 1]);
        }


        int minimum_index = binarySearch(sourceIntegers, x);

        System.out.println(minimum_index);
    }


    // Invariant:   BaseInv === ((a != empty) && (key : integer))
    // Requires:    BaseInv
    // Ensures:     ((retval=0 && a[0] <= key) || (retval=a.length && a[a.length-1] >= key) || (a[retval-1] > key >= a[retval])) && BaseInv
    public static int binarySearch(int[] a, int key) {
        return recursiveBinarySearch(a, key);
        //return iterativeBinarySearch(a, key);
    }


    // Requires:    BaseInv
    // Ensures:     ((retval=0 && a[0] <= key) || (retval=a.length && a[a.length-1] >= key) || (a[retval-1] > key >= a[retval])) && BaseInv
    public static int recursiveBinarySearch(int[] a, int key) {
        return internalRecursiveBinarySearch(a, key, -1, a.length);
    }


    // Invariant:   RecInv === (BaseInv && -1 <= l < r <= a.length && r > 0)
    // Requires:    RecInv
    // Ensures:     ((retval=0 && a[0] <= key) || (retval=a.length && a[a.length-1] >= key) || (a[retval-1] > key >= a[retval])) && BaseInv
    private static int internalRecursiveBinarySearch(int[] a, int key, int l, int r) {

        // Requires:    RecInv
        // Ensures:     a[0] > key && RecInv
        if (a[0] <= key) {
            return 0;
        }

        // Requires:    a[0] > key && RecInv
        // Ensures:     a[0] > key > a[a.length] && RecInv
        if (key < a[a.length - 1]) {
            return a.length;
        }


        // Requires:    RecInv
        // Ensures:     r > l+1
        if (l == r - 1) {
            // Requires:    RecInv
            // Ensures:     l >= 0 && r < a.length && (retval=a.length && a[a.length-1] >= key)
            if (a[l] <= key) {
                return l;
            } else {
                return r;
            }
        }


        int newL = l;
        int newR = r;


        int median = (l + r) / 2;


        if (a[median] > key) {
            newL = median;
        } else {
            newR = median;
        }


        return internalRecursiveBinarySearch(a, key, newL, newR);
    }

    // Invariant Inv: a != null && a != empty && 0 <= l <= r < a.length && \forall i from 0 to a.length: a[i] >= a[i+1]
    // Requires: Inv
    // ->?<- // Ensures: Inv && a[l]>=x>a[r]
    // Ensures: Inv && a[retval] <= x < a[retval+1]
    private static int oldinternalRecursiveBinarySearch(int[] a, int x, int l, int r) {
        // Requires: l, r >= 0
        // Ensures: l != r
        if (l == r) {
            return l;
        }


        // Requires: Inv
        // Ensures: r-l>1
        if (r - l == 1) {
            if (a[l] <= x) {
                return l;
            } else {
                return r;
            }
        }

        // Requires: l, r >= 0
        // Ensures: m = ceil((l+r)/2) > 0
        int m = (l + r) / 2 + 1;


        // Requires: 0 <= l < r < a.length
        // Ensures: newL = l && newR = r && 0 <= newL < newR < a.length
        int newL = l, newR = r;

        // Requires: Inv
        // Ensures: 0 <= newL <= newR < a.length && a[newL] >= a[newR] && a[newL] <= a[l] && a[newR] >= a[r]
        if (a[m] > x) {
            // Requires: m > 0
            // Ensures: newL = m+1 > 1
            newL = m + 1;
        } else {
            // Requires: m > 0
            // Ensures: newR = m-1 >= 0
            newR = m - 1;
        }


        // Requires: a != null && a != empty && 0 <= l <= r < a.length && \forall i from 0 to a.length: a[i] >= a[i+1]
        // Ensures: a[retval] <= x < a[retval+1]
        return oldinternalRecursiveBinarySearch(a, x, newL, newR);
    }


    public static int iterativeBinarySearch(int[] a, int key) {
        if (key >= a[0]) {
            return 0;
        }

        if (key < a[a.length - 1]) {
            return a.length;
        }

        int leftBorder = -1;
        int rightBorder = a.length;

        int median = -1;


        while (rightBorder > leftBorder + 1) {
            median = (leftBorder + rightBorder) / 2;

            //System.out.println("lB: " + leftBorder + " rB: " + rightBorder + " med: " + median);

            if (a[median] > key) {
                leftBorder = median;
            } else {
                rightBorder = median;
            }
        }


        if (a[leftBorder] <= key) {
            return leftBorder;
        } else {
            return rightBorder;
        }
    }
}
