import java.io.IOException;
import java.util.ArrayList;

public class Reverse {

    public static void main(String[] args) {

        ArrayList<ArrayList<Integer>> inputMatrix = new ArrayList<>();
        int currentLineNumber = 0;

        try (CharacterFastScanner fastInputScanner = new CharacterFastScanner(System.in)) {
            inputMatrix.add(new ArrayList<>());

            while (fastInputScanner.hasNext()) {
                // System.out.println("I'm in while!");
                boolean nl = fastInputScanner.nextTokenIsNewLine();
                //System.out.println("Checked nl");
                if (nl) {
                    // System.out.println("next token is NL");
                    currentLineNumber++;
                    inputMatrix.add(new ArrayList<>());
                    // System.out.println("skipping");
                    fastInputScanner.skipCurrentToken();
                    // System.out.println("skipped");
                    nl = fastInputScanner.nextTokenIsNewLine();
                }

                // System.out.println("exited nl check");

                while ((!fastInputScanner.hasNextInt()) && fastInputScanner.hasNext() && !fastInputScanner.nextTokenIsNewLine()) {
                    fastInputScanner.skipCurrentToken();
                }
                // System.out.println("Exited from while-2. Checking hasInt()");

                boolean hi = fastInputScanner.hasNextInt();

                // System.out.println("Checked whether int");

                if (hi) {
                    // System.out.println("next token is int, adding it");
                    inputMatrix.get(currentLineNumber).add(fastInputScanner.nextInt());
                }

                // System.out.println("checked once");
            }

            //System.out.println("FS does not have next");

            for (int i = currentLineNumber - 1; i >= 0; i--) {
                for (int j = inputMatrix.get(i).size() - 1; j >= 0; j--) {
                    System.out.print(inputMatrix.get(i).get(j));
                    System.out.print(" ");
                }
                System.out.println();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
