package expression.parser;

/*
Grammar:

(1) F --> S+F | S-F | S
(2) S --> T*S | T/S | T
(T) T --> -T | Q
(3) Q --> I | x | y | z | (F)
(4) I --> 0..9 | 0..9I

Starting token: F
*/


import expression.*;

public class ExpressionParserLL implements Parser {
    @Override
    public TripleExpression parse(String expression) {
        assert (expression.length() > 0);

        try {
            return parseFirstPriority(expression, 0).expression;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    protected ParseResult parseFirstPriority(String expression, int position) throws GrammarMismatchException {
        ParseResult S = parseSecondPriority(expression, position);

        position = S.endedPosition;

        while ((position < expression.length()) && Character.isWhitespace(expression.charAt(position))) {
            position++;
        }

        if (position == expression.length()) {
            return new ParseResult(S.expression, position);
        }

        ParseResult F;

        switch (expression.charAt(position)) {
            case '+':
                F = parseFirstPriority(expression, position + 1);
                return new ParseResult(new Add(S.expression, F.expression), F.endedPosition);
            case '-':
                F = parseFirstPriority(expression, position + 1);
                return new ParseResult(new Subtract(S.expression, F.expression), F.endedPosition);
            default:
                return S;
        }
    }

    protected ParseResult parseSecondPriority(String expression, int position) throws GrammarMismatchException {
        ParseResult T = parseThirdPriority(expression, position);

        position = T.endedPosition;

        while ((position < expression.length()) && Character.isWhitespace(expression.charAt(position))) {
            position++;
        }

        if (position == expression.length()) {
            return new ParseResult(T.expression, position);
        }

        ParseResult S;

        switch (expression.charAt(position)) {
            case '*':
                S = parseSecondPriority(expression, position + 1);
                return new ParseResult(new Multiply(T.expression, S.expression), S.endedPosition);
            case '/':
                S = parseSecondPriority(expression, position + 1);
                return new ParseResult(new Divide(T.expression, S.expression), S.endedPosition);
            default:
                return T;
        }
    }


    protected ParseResult parseThirdPriority(String expression, int position) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected term T, found EOS", position);
        }


        if (Character.getType(expression.charAt(position)) == Character.DASH_PUNCTUATION) {
            ParseResult T = parseThirdPriority(expression, position + 1);
            return new ParseResult(new UnaryMinus(T.expression), T.endedPosition);
        } else {
            return parseQuadPriority(expression, position);
        }

    }


    protected ParseResult parseQuadPriority(String expression, int position) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected term Q, found EOS", position);
        }

        ParseResult result;

        switch (expression.charAt(position)) {
            case '(':
                result = parseFirstPriority(expression, position + 1);

                position = result.endedPosition;

                while ((position < expression.length()) && Character.isWhitespace(expression.charAt(position))) {
                    position++;
                }

                if (position == expression.length()) {
                    throw new GrammarMismatchException("Expected paired closing bracket ')' but reached the end of expressions", result.endedPosition);
                } else if (expression.charAt(position) != ')') {
                    throw new GrammarMismatchException("Expected paired closing bracket ')' but none found, symbol is: " + expression.charAt(result.endedPosition), position);
                } else {
                    return new ParseResult(result.expression, position + 1);
                }
            case 'x':
            case 'y':
            case 'z':
                return new ParseResult(new Variable(String.valueOf(expression.charAt(position))), position + 1);
            default:
                return tryParseInt(expression, position);
        }
    }


    private ParseResult tryParseInt(String expression, int position) throws GrammarMismatchException {
        int answer = -1;

        while ((position < expression.length()) && Character.isDigit(expression.charAt(position))) {
            if (answer == -1) {
                answer = 0;
            }

            answer = answer * 10 + (expression.charAt(position) - '0');
            position++;
        }

        if (answer == -1) {
            throw new GrammarMismatchException("Expected token I, but no number is represented", position);
        } else {
            return new ParseResult(new Const(answer), position);
        }
    }


    private int skipWhiteSpace(String expression, int position) {
        while ((position < expression.length()) && Character.isWhitespace(expression.charAt(position))) {
            position++;
        }

        return position;
    }

    private class ParseResult {
        Term expression;
        int endedPosition;

        public ParseResult(Term _expression, int _endedPosition) {
            expression = _expression;
            endedPosition = _endedPosition;
        }
    }
}