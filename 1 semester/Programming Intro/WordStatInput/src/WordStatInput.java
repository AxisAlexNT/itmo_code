import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Pattern;

public class WordStatInput {
    public static void main(String[] args) {

        System.setProperty("file.encoding", "UTF-8");

        if (args.length != 2) {
            System.out.println("Usage: SumHexFile <input file> <output file>");
            return;
        }


        LinkedHashMap<String, Integer> stats = new LinkedHashMap<>();

        try (Scanner inputScanner = new Scanner(new FileInputStream(new File(args[0])), "UTF8")) {
            try (PrintWriter outputWriter = new PrintWriter(new File(args[1]), "UTF8")) {


                Locale.setDefault(Locale.US);

                Pattern wordPattern = Pattern.compile("([^\\p{Pd}\\p{Lower}'])", Pattern.UNICODE_CHARACTER_CLASS);

                while (inputScanner.hasNextLine()) {
                    String s = inputScanner.nextLine();
                    s = s.toLowerCase();
                    String[] words = wordPattern.split(s);

                    for (String word : words) {
                        if (word.length() > 0) {
                            if (stats.containsKey(word)) {
                                stats.put(word, stats.get(word) + 1);
                            } else {
                                stats.put(word, 1);
                            }
                        }
                    }
                }


                stats.forEach((word, count) ->
                {
                    outputWriter.println(word + " " + count);
                });


            } catch (Exception ex) {
                System.out.println("Failed to create an output file.");
                ex.printStackTrace();
            }
        } catch (Exception e) {
            System.out.println("Cannot find input file!");
            e.printStackTrace();
        }
    }
}
