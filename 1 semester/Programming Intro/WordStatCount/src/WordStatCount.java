import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Pattern;


class WordFreqPos implements Comparable<WordFreqPos> {
    public int wordOccurrenceFrequency = -1;
    public long firstOccurrencePosition = -1;
    public String word = "";


    WordFreqPos(String _word, int frequency, long firstPosition) {
        wordOccurrenceFrequency = frequency;
        firstOccurrencePosition = firstPosition;
        word = _word;
    }

    @Override
    public int compareTo(WordFreqPos anotherWord) {
        if (this.wordOccurrenceFrequency == anotherWord.wordOccurrenceFrequency) {
            return Long.compare(this.firstOccurrencePosition, anotherWord.firstOccurrencePosition);
        } else {
            return Integer.compare(this.wordOccurrenceFrequency, anotherWord.wordOccurrenceFrequency);
        }
    }
}


public class WordStatCount {

    public static void main(String[] args) {

        System.setProperty("file.encoding", "UTF-8");


        if (args.length != 2) {
            System.out.println("Usage: java WordStatCount <input file> <output file>");
            return;
        }

        LinkedHashMap<String, WordFreqPos> wordOccurrenceStats = new LinkedHashMap<>();


        try (Scanner inputScanner = new Scanner(new FileInputStream(new File(args[0])), "UTF8")) {

            Pattern wordSplitPattern = Pattern.compile("([^\\p{Pd}\\p{Lower}'])", Pattern.UNICODE_CHARACTER_CLASS);

            long currentWordPosition = 0;

            while (inputScanner.hasNextLine()) {
                String s = inputScanner.nextLine().toLowerCase();
                String[] words = wordSplitPattern.split(s);

                for (String word : words) {
                    if (!word.isEmpty()) {
                        if (wordOccurrenceStats.containsKey(word)) {
                            wordOccurrenceStats.get(word).wordOccurrenceFrequency++;
                        } else {
                            wordOccurrenceStats.put(word, new WordFreqPos(word, 1, currentWordPosition));
                        }
                        currentWordPosition++;
                    }
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Cannot find input file <" + args[0] + ">");
        } catch (Exception e) {
            System.out.println("The following error occurred:" + e.getMessage());
        }

        List<WordFreqPos> sortedWords = new ArrayList<>();


        wordOccurrenceStats.forEach((word, wordAndStat) ->
        {
            sortedWords.add(wordAndStat);
        });

        Collections.sort(sortedWords);

        StringBuilder outputBuilder = new StringBuilder();

        sortedWords.forEach(wordFreqPos ->
        {
            outputBuilder.append(wordFreqPos.word);
            outputBuilder.append(' ');
            outputBuilder.append(wordFreqPos.wordOccurrenceFrequency);
            outputBuilder.append('\n');
        });


        try (PrintWriter outputWriter = new PrintWriter(new File(args[1]), "utf8")) {
            outputWriter.print(outputBuilder.toString());
        } catch (IOException e) {
            System.out.println("Failed to create an output file <" + args[1] + "> with th following error:");
            System.out.println(e.getMessage());
        }
    }
}
