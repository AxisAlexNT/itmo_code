import expression.*;
import expression.exceptions.ArithmeticEvaluationException;

public class TestBasicMod {
    public static void main(String[] args) {
        int res =
                0;
        try {
            res = new CheckedAdd(
                    new CheckedSubtract(
                            new CheckedMultiply(
                                    new Variable("x"),
                                    new Variable("x")),
                            new CheckedMultiply(
                                    new Const(2),
                                    new Variable("x"))),
                    new Const(1)
            ).evaluate(Integer.parseInt(args[0]));
        } catch (ArithmeticEvaluationException e) {
            e.printStackTrace();
        }


        System.out.println(res);
    }
}
