import expression.TripleExpression;
import expression.exceptions.ArithmeticEvaluationException;
import expression.exceptions.GrammarMismatchException;
import expression.parser.ExpressionParser;

public class RangeTests {
    public static void main(String[] args) {
        ExpressionParser parser = new ExpressionParser();

        TripleExpression parseResult;
        String evaluationResult;
        int result;

        try {
            parseResult = parser.parse("-" + Integer.toString(Integer.MIN_VALUE));
            result = parseResult.evaluate(1, 0, 0);
            evaluationResult = Integer.toString(result);
            System.out.println(evaluationResult);
        } catch (GrammarMismatchException | ArithmeticEvaluationException e) {
            e.printStackTrace();
        }


    }
}
