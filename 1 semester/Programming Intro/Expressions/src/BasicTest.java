import expression.CheckedSqrt;
import expression.Const;
import expression.exceptions.ArithmeticEvaluationException;

public class BasicTest {
    public static void main(String[] args) {
        int res = 0;
        try {
            res = new CheckedSqrt(new Const(24)).evaluate(0, 0, 0);
        } catch (ArithmeticEvaluationException e) {
            e.printStackTrace();
        }

        System.out.println(res);
    }
}
