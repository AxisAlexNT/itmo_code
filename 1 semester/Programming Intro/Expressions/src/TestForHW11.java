import expression.TripleExpression;
import expression.exceptions.ArithmeticEvaluationException;
import expression.exceptions.GrammarMismatchException;
import expression.parser.ExpressionParser;

public class TestForHW11 {
    public static void main(String[] args) {
        ExpressionParser parser = new ExpressionParser();

        TripleExpression parseResult;
        String evaluationResult;

        System.out.println("x:\t\tf(x):");

        for (int i = 0; i < 11; i++) {
            try {
                parseResult = parser.parse("1000000*x*x*x*x*x/(x-1)");
                evaluationResult = Integer.toString(parseResult.evaluate(i, 0, 0));
            } catch (GrammarMismatchException | ArithmeticEvaluationException e) {
                evaluationResult = e.getShortMessage();
            }

            System.out.printf("%d\t\t%s\n", i, evaluationResult);
        }


    }
}
