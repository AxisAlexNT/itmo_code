package expression;

import expression.exceptions.ArithmeticEvaluationException;
import expression.exceptions.DivisionByZeroEvaluationException;
import expression.exceptions.IntegerOverflowException;

public class CheckedDivide extends BinaryOperator {
    public CheckedDivide(Term _left, Term _right) {
        super(_left, _right, "/");
    }

    @Override
    protected int operation(int l, int r) throws ArithmeticEvaluationException {
        if (r == 0) {
            throw new DivisionByZeroEvaluationException("Division by zero occured when evaluating expression", this, false);
        }

        if ((l == Integer.MIN_VALUE) && (r == -1)) {
            throw new IntegerOverflowException("Integer overflow in division during evaluation", this, true);
        }

        return l / r;
    }

    @Override
    protected double operation(double l, double r) throws ArithmeticEvaluationException {
        if (r == 0.0f) {
            throw new DivisionByZeroEvaluationException("Division by zero occured when evaluating expression", this, false);
        }

        return l / r;
    }
}
