package expression;

import expression.exceptions.ArithmeticEvaluationException;

public interface TripleExpression {
    int evaluate(int x, int y, int z) throws ArithmeticEvaluationException;
}