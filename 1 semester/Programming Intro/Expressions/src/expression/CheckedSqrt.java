package expression;

import expression.exceptions.ArithmeticEvaluationException;
import expression.exceptions.InvalidArgumentDomainException;

public class CheckedSqrt extends UnaryOperator {
    public CheckedSqrt(Term argument) {
        super(argument, "sqrt ");
    }

    @Override
    protected int operation(int x) throws ArithmeticEvaluationException {
        if (x < 0) {
            throw new InvalidArgumentDomainException("Square root of negative integer is undefined!", this, false);
        }

        if (x == 0) {
            return 0;
        }

        if (x < 4) {
            return 1;
        }

        for (int i = 2; i < x; i++) {
            if (i * i > x) {
                return i - 1;
            }
        }

        return 0;
    }

    @Override
    protected double operation(double x) {
        return 0;
    }
}
