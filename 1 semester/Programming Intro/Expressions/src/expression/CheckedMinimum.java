package expression;

import expression.exceptions.ArithmeticEvaluationException;

public class CheckedMinimum extends BinaryOperator {
    public CheckedMinimum(Term _left, Term _right) {
        super(_left, _right, " min ");
    }

    @Override
    protected int operation(int l, int r) throws ArithmeticEvaluationException {
        return (l < r) ? l : r;
    }

    @Override
    protected double operation(double l, double r) throws ArithmeticEvaluationException {
        return (l < r) ? l : r;
    }
}
