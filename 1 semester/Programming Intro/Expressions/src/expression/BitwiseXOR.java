package expression;

public class BitwiseXOR extends BinaryOperator {
    public BitwiseXOR(Term _left, Term _right) {
        super(_left, _right, "^");
    }

    @Override
    protected int operation(int l, int r) {
        return l ^ r;
    }

    @Override
    protected double operation(double l, double r) {
        // throw new Exception();
        return ((int) l) ^ ((int) r);
    }
}
