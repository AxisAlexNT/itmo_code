package expression.parser;

/*
Grammar:

M' --> (min | max)OM' | empty
M  --> OM'
O' --> |XO' | empty
O  --> XO'
X' --> ^AX' | empty
X  --> AX'
A' --> &FA' | empty
A  --> FA'
F' --> (+|-)SF' | empty
F  --> SF'
S' --> (*|/)TS' | empty
S  --> TS'|empty
T  --> -T|sqrt T|abs T|~T|count T|(O)|x|y|z|I|O
I  --> 0..9 | 0..9I


Starting token: O
*/


import expression.*;
import expression.exceptions.GrammarMismatchException;
import expression.exceptions.UnexpectedSymbolException;

public class ExpressionParser implements Parser {
    public static final String countToken = "count";
    public static final String sqrtToken = "sqrt";
    public static final String absToken = "abs";
    public static final char[] validTokenStartingSymbols = {'(', ')', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'x', 'y', 'z', '+', '-', '*', '/', '~', '|', '&', '^', 'c', ' ', 's', 'a', 'm'};

    @Override
    public TripleExpression parse(String expression) throws GrammarMismatchException {
        if (expression == null) {
            throw new GrammarMismatchException("Cannot parse NULL expression! Please ensure that expression is not null!");
        }

        if (expression.isEmpty()) {
            throw new GrammarMismatchException("Empty expression does not contain any tokens.", -1);
        }

        ParseResult parsedExpression = startParse(expression);

        if (parsedExpression.endedPosition != expression.length()) {
            throw new GrammarMismatchException("Unexpected symbol found which is not a valid prefix of any token", parsedExpression.endedPosition);
        }

        return parsedExpression.expression;
    }


    private ParseResult startParse(String expression) throws GrammarMismatchException {
        return parseMinMaxPriority(expression, 0);
    }


    private ParseResult parseMinMaxPriority(String expression, int position) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected token M ('max' or 'min' with argument), but reached EOS", position);
        }

        ParseResult O, preMinMax;

        O = parseORPriority(expression, position);
        preMinMax = parsePreMinMaxPriority(expression, O.endedPosition, O.expression);
        return new ParseResult(preMinMax.expression, preMinMax.endedPosition);
    }

    private ParseResult parsePreMinMaxPriority(String expression, int position, Term leftO) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            return new ParseResult(leftO, position);
        }

        ParseResult O;
        ParseResult preMinMaxRight;

        switch (expression.charAt(position)) {
            case 'm':
                MinOrMax nextOperator = ensureMinOrMax(expression, position);
                position += 3;  // will be at 'n' or 'x'

                if (position >= expression.length()) {
                    throw new GrammarMismatchException("Expected 'min' or 'max', but reached EOS", position);
                }

                if (!(Character.isWhitespace(expression.charAt(position)) ||
                        (expression.charAt(position) == '-'))) {
                    throw new GrammarMismatchException("Expected 'min' or 'max' with argument, but found unrecognized token");
                }


                O = parseORPriority(expression, position);
                if (nextOperator == MinOrMax.MIN) {
                    preMinMaxRight = parsePreMinMaxPriority(expression, O.endedPosition, new CheckedMinimum(leftO, O.expression));
                } else {
                    preMinMaxRight = parsePreMinMaxPriority(expression, O.endedPosition, new CheckedMaximum(leftO, O.expression));
                }
                return new ParseResult(preMinMaxRight.expression, preMinMaxRight.endedPosition);

            default:
                return new ParseResult(leftO, position);
        }
    }


    private ParseResult parsePreFirstPriority(String expression, int position, Term leftS) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            return new ParseResult(leftS, position);
        }

        ParseResult S;
        ParseResult preFRight;

        switch (expression.charAt(position)) {
            case '+':
                S = parseSecondPriority(expression, position + 1);
                preFRight = parsePreFirstPriority(expression, S.endedPosition, new CheckedAdd(leftS, S.expression));
                return new ParseResult(preFRight.expression, preFRight.endedPosition);
            case '-':
                S = parseSecondPriority(expression, position + 1);
                preFRight = parsePreFirstPriority(expression, S.endedPosition, new CheckedSubtract(leftS, S.expression));
                return new ParseResult(preFRight.expression, preFRight.endedPosition);
            default:
                return new ParseResult(leftS, position);

        }
    }


    private ParseResult parsePreORPriority(String expression, int position, Term leftX) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            return new ParseResult(leftX, position);
        }

        ParseResult X;
        ParseResult preORight;

        switch (expression.charAt(position)) {
            case '|':
                X = parseXORPriority(expression, position + 1);
                preORight = parsePreORPriority(expression, X.endedPosition, new BitwiseOR(leftX, X.expression));
                return new ParseResult(preORight.expression, preORight.endedPosition);
            default:
                return new ParseResult(leftX, position);
        }
    }


    private ParseResult parseORPriority(String expression, int position) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected token O (bitwise OR with an argument), but reached EOS", position);
        }

        ParseResult X, preO;

        X = parseXORPriority(expression, position);
        preO = parsePreORPriority(expression, X.endedPosition, X.expression);
        return new ParseResult(preO.expression, preO.endedPosition);
    }


    private ParseResult parsePreXORPriority(String expression, int position, Term leftA) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            return new ParseResult(leftA, position);
        }

        ParseResult A;
        ParseResult preXORight;

        switch (expression.charAt(position)) {
            case '^':
                A = parseANDPriority(expression, position + 1);
                preXORight = parsePreXORPriority(expression, A.endedPosition, new BitwiseXOR(leftA, A.expression));
                return new ParseResult(preXORight.expression, preXORight.endedPosition);
            default:
                return new ParseResult(leftA, position);
        }
    }


    private ParseResult parseXORPriority(String expression, int position) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected token X (bitwise XOR with an argument), but reached EOS", position);
        }

        ParseResult A, preX;

        A = parseANDPriority(expression, position);
        preX = parsePreXORPriority(expression, A.endedPosition, A.expression);
        return new ParseResult(preX.expression, preX.endedPosition);
    }


    private ParseResult parsePreANDPriority(String expression, int position, Term leftF) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            return new ParseResult(leftF, position);
        }

        ParseResult F;
        ParseResult preANDRight;

        switch (expression.charAt(position)) {
            case '&':
                F = parseFirstPriority(expression, position + 1);
                preANDRight = parsePreANDPriority(expression, F.endedPosition, new BitwiseAND(leftF, F.expression));
                return new ParseResult(preANDRight.expression, preANDRight.endedPosition);
            default:
                return new ParseResult(leftF, position);
        }
    }


    private ParseResult parseANDPriority(String expression, int position) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected token A (bitwise AND with an argument), but reached EOS", position);
        }

        ParseResult F, preA;

        F = parseFirstPriority(expression, position);
        preA = parsePreANDPriority(expression, F.endedPosition, F.expression);
        return new ParseResult(preA.expression, preA.endedPosition);
    }

    private ParseResult parseThirdPriority(String expression, int position) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected token T (a number, negation, parentheses, count with an argument or a variable), but reached EOS", position);
        }

        ParseResult T, F;

        char currentSymbol = expression.charAt(position);

        switch (currentSymbol) {
            case '-':
                if (position < expression.length() - 1) {
                    if (Character.isDigit(expression.charAt(position + 1))) {
                        return tryParseInt(expression, position);
                    } else {
                        T = parseThirdPriority(expression, position + 1);
                        return new ParseResult(new CheckedNegate(T.expression), T.endedPosition);
                    }
                } else {
                    throw new GrammarMismatchException("Found an unary minus without a valid argument!", position);
                }
            case '(':
                int openingBracketPosition = position;
                T = parseMinMaxPriority(expression, position + 1);
                position = skipWhiteSpace(expression, T.endedPosition);
                if (position == expression.length()) {
                    throw new GrammarMismatchException("Expected closing bracket ')' for pairing '(' at position " + openingBracketPosition + ", but reached EOS", T.endedPosition, false);
                }
                if (expression.charAt(position) != ')') {
                    if (!isValidTokenBeginnig(expression.charAt(position))) {
                        throw new GrammarMismatchException("Unexpected symbol found which is not a valid prefix of any token", position);
                    }
                    throw new GrammarMismatchException("Expected closing bracket ')' for pairing '(' at position " + openingBracketPosition + ", but none was found", position, false);
                }

                return new ParseResult(T.expression, position + 1);
            case 'a':
                ensureToken(expression, position, absToken);
                position += absToken.length();
                if (position == expression.length()) {
                    throw new GrammarMismatchException("Expected abs's argument, but reached EOS", position, false);
                }

                if (!(Character.isDigit(expression.charAt(position)) ||
                        Character.isWhitespace(expression.charAt(position)) ||
                        (expression.charAt(position) == '-'))) {
                    throw new GrammarMismatchException("Expected abs with argument, but found unrecognized token");
                }

                T = parseThirdPriority(expression, position);
                return new ParseResult(new CheckedAbs(T.expression), T.endedPosition);
            case 's':
                ensureToken(expression, position, sqrtToken);
                position += sqrtToken.length();
                T = parseThirdPriority(expression, position);
                return new ParseResult(new CheckedSqrt(T.expression), T.endedPosition);
            case 'x':
            case 'y':
            case 'z':
                return new ParseResult(new Variable(String.valueOf(expression.charAt(position))), position + 1);
            case '~':
                T = parseThirdPriority(expression, position + 1);
                return new ParseResult(new BitwiseNegation(T.expression), T.endedPosition);
            case 'c':
                ensureToken(expression, position, countToken);
                position += countToken.length();
                T = parseThirdPriority(expression, position);
                return new ParseResult(new BitwiseCount(T.expression), T.endedPosition);
            case ')':
                throw new GrammarMismatchException("Unexpected unpaired closing bracket ')'", position, false);
            default:
                if (Character.isDigit(expression.charAt(position))) {
                    return tryParseInt(expression, position);
                } else {
                    throw new UnexpectedSymbolException(currentSymbol, position);
                    //throw new GrammarMismatchException("Unexpected symbol starting new token", position, false);
                }
        }
    }


    private ParseResult parseFirstPriority(String expression, int position) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected token F (a sum or subtraction), but reached EOS", position, false);
        }

        ParseResult S = parseSecondPriority(expression, position);
        return parsePreFirstPriority(expression, S.endedPosition, S.expression);
    }


    private ParseResult parsePreSecondPriority(String expression, int position, Term leftT) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            return new ParseResult(leftT, position);
        }

        ParseResult T;
        ParseResult preSRight;

        switch (expression.charAt(position)) {
            case '*':
                T = parseThirdPriority(expression, position + 1);
                preSRight = parsePreSecondPriority(expression, T.endedPosition, new CheckedMultiply(leftT, T.expression));
                return new ParseResult(preSRight.expression, preSRight.endedPosition);
            case '/':
                T = parseThirdPriority(expression, position + 1);
                preSRight = parsePreSecondPriority(expression, T.endedPosition, new CheckedDivide(leftT, T.expression));
                return new ParseResult(preSRight.expression, preSRight.endedPosition);
            default:
                return new ParseResult(leftT, position);
        }
    }

    private ParseResult parseSecondPriority(String expression, int position) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);
        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected token T, but reached EOS", position, false);
        }

        ParseResult T = parseThirdPriority(expression, position);
        return parsePreSecondPriority(expression, T.endedPosition, T.expression);
    }

    private void ensureToken(String expression, int position, String testingToken) throws GrammarMismatchException {
        for (int i = 0; i < testingToken.length(); i++) {
            if (expression.charAt(position + i) != testingToken.charAt(i)) {
                throw new GrammarMismatchException("Probably misspelled '" + testingToken + "'", position, false);
            }
        }
    }

    private MinOrMax ensureMinOrMax(String expression, int position) throws GrammarMismatchException {
        if (position + 2 >= expression.length()) {
            throw new GrammarMismatchException("Expected 'min' or 'max', but reached EOS", position, false);
        }

        if (expression.charAt(position + 1) == 'i') {
            if (expression.charAt(position + 2) == 'n') {
                return MinOrMax.MIN;
            } else {
                throw new GrammarMismatchException("Probably misspelled 'min'", position, false);
            }
        } else if (expression.charAt(position + 1) == 'a') {
            if (expression.charAt(position + 2) == 'x') {
                return MinOrMax.MAX;
            } else {
                throw new GrammarMismatchException("Probably misspelled 'max'", position, false);
            }
        } else {
            throw new GrammarMismatchException("Expected 'min' or 'max', but none was detected", position);
        }
    }



    private ParseResult tryParseInt(String expression, int position) throws GrammarMismatchException {
        int answer = 0;
        int previousAnswer = 1;
        boolean needNegation = false;
        boolean answerWasModified = false;

        if (expression.charAt(position) == '-') {
            needNegation = true;
            position++;
            previousAnswer = -1;
        }

        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected a negative integer, but only minus was parsed before EOS", position);
        }

        while ((position < expression.length()) && Character.isDigit(expression.charAt(position))) {
            if (!needNegation) {
                answer = answer * 10 + (expression.charAt(position) - '0');
            } else {
                answer = answer * 10 - (expression.charAt(position) - '0');
            }

            if (!answerWasModified) {
                previousAnswer = answer;
                answerWasModified = true;
            }

            if (sign(answer) != sign(previousAnswer)) {
                throw new GrammarMismatchException("Number does not represent a valid integer. Valid integers range: -2147483648 <= int < 2147483648.", position, false);
            }

            position++;
        }

        if (!answerWasModified) {
            throw new GrammarMismatchException("Expected token I (a valid integer), but no number is represented", position);
        } else {
            return new ParseResult(new Const(answer), position);
        }
    }

    private boolean isValidTokenBeginnig(char testingChar) {
        for (char validChar : validTokenStartingSymbols) {
            if (testingChar == validChar) {
                return true;
            }
        }

        return false;
    }


//    private void ensureSqrt(String expression, int position) throws GrammarMismatchException {
//        if (position+sqrtToken.length() >= expression.length())
//        {
//            throw new GrammarMismatchException("Expected 'sqrt', but reached EOS", position);
//        }
//
//
//        for (int i = 1; i < sqrtToken.length(); i++) {
//            if (expression.charAt(position+i) != countToken.charAt(i))
//            {
//                throw new GrammarMismatchException("Probably misspelled 'sqrt'", position);
//            }
//        }
//
//
//    }

    private int skipWhiteSpace(String expression, int position) {
        while ((position < expression.length()) && Character.isWhitespace(expression.charAt(position))) {
            position++;
        }

        return position;
    }

    private int sign(int x) {
        if (x == 0) {
            return 0;
        } else {
            return (x > 0) ? 1 : (-1);
        }
    }

    private enum MinOrMax {
        MIN,
        MAX
    }

    private class ParseResult {
        Term expression;
        int endedPosition;

        public ParseResult(Term _expression, int _endedPosition) {
            expression = _expression;
            endedPosition = _endedPosition;
        }
    }


}