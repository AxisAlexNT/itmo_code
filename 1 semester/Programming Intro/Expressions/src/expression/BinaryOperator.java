package expression;

import expression.exceptions.ArithmeticEvaluationException;

public abstract class BinaryOperator extends Term {
    protected Term left;
    protected Term right;
    protected String operationSymbol;


    public BinaryOperator(Term _left, Term _right, String _operationSymbol) {
        left = _left;
        right = _right;
        operationSymbol = _operationSymbol;
    }


    protected abstract int operation(int l, int r) throws ArithmeticEvaluationException;

    protected abstract double operation(double l, double r) throws ArithmeticEvaluationException;


    @Override
    protected int doEvaluate(int x) throws ArithmeticEvaluationException {
        return operation(left.evaluate(x), right.evaluate(x));
    }

    @Override
    protected double doEvaluate(double x) {
        double result = 0;
        try {
            result = operation(left.evaluate(x), right.evaluate(x));
        } catch (ArithmeticEvaluationException e) {
            System.out.println("Caught an arithmetic exception while evaluating with doubles, but it was not in this task!");
        }
        return 0;
    }

    @Override
    protected int doEvaluate(int x, int y, int z) throws ArithmeticEvaluationException {
        return operation(left.evaluate(x, y, z), right.evaluate(x, y, z));
    }


    @Override
    protected String doToString() {
        return (new StringBuilder()).append('(').append(left.toString()).append(operationSymbol).append(right.toString()).append(')').toString();
    }
}
