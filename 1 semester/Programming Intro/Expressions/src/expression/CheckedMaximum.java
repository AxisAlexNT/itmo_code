package expression;

import expression.exceptions.ArithmeticEvaluationException;

public class CheckedMaximum extends BinaryOperator {
    public CheckedMaximum(Term _left, Term _right) {
        super(_left, _right, " max ");
    }

    @Override
    protected int operation(int l, int r) throws ArithmeticEvaluationException {
        return (l > r) ? l : r;
    }

    @Override
    protected double operation(double l, double r) throws ArithmeticEvaluationException {
        return (l > r) ? l : r;
    }
}
