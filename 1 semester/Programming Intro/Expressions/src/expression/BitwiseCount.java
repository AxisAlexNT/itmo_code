package expression;

public class BitwiseCount extends UnaryOperator {
    public BitwiseCount(Term argument) {
        super(argument, "count ");
    }

    @Override
    protected int operation(int x) {
        return Integer.bitCount(x);
    }

    @Override
    protected double operation(double x) {
        // throw new Exception();
        return Integer.bitCount((int) x);
    }
}
