package expression;

import expression.exceptions.ArithmeticEvaluationException;
import expression.exceptions.IntegerOverflowException;

public class CheckedSubtract extends BinaryOperator {
    public CheckedSubtract(Term _left, Term _right) {
        super(_left, _right, "-");
    }

    @Override
    protected int operation(int l, int r) throws ArithmeticEvaluationException {
        if (r > 0) {
            if (l < Integer.MIN_VALUE + r) {
                throw new IntegerOverflowException("Integer overflow in subtraction during evaluation", this, true);
            }
        } else {
            if (l > Integer.MAX_VALUE + r) {
                throw new IntegerOverflowException("Integer overflow in subtraction during evaluation", this, true);
            }
        }
        return l - r;
    }

    @Override
    protected double operation(double l, double r) {
        return l - r;
    }

}
