package expression.exceptions;

public class UnexpectedSymbolException extends GrammarMismatchException {
    public UnexpectedSymbolException(char unexpectedSymbol, int position) {
        super("Unexpected symbol: '" + unexpectedSymbol + "' found.", position, false);
    }
}
