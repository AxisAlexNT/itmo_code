package expression.exceptions;

public class UnexpectedEndedExpressionException extends GrammarMismatchException {
    public UnexpectedEndedExpressionException(String message, int position) {
        super(message, position);
    }

    public
}
