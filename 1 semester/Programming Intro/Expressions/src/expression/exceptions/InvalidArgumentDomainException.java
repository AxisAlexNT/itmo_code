package expression.exceptions;

import expression.Term;

public class InvalidArgumentDomainException extends ArithmeticEvaluationException {
    public InvalidArgumentDomainException(String message, Term nonEvaluatingTerm) {
        super(message, "Invalid argument domain", nonEvaluatingTerm);
    }

    public InvalidArgumentDomainException(String message, Term nonEvaluatingTerm, boolean needToPrintRanges) {
        super(message, "Invalid argument domain", nonEvaluatingTerm, needToPrintRanges);
    }
}
