package expression;

import expression.exceptions.ArithmeticEvaluationException;
import expression.exceptions.IntegerOverflowException;

public class CheckedAbs extends UnaryOperator {
    public CheckedAbs(Term argument) {
        super(argument, "abs");
    }

    @Override
    protected int operation(int x) throws ArithmeticEvaluationException {
        if (x == Integer.MIN_VALUE) {
            throw new IntegerOverflowException("Absolute value of Integer.MIN_VALUE exceeds the integer range", this, true);
        }

        return (x >= 0) ? x : (-x);
    }

    @Override
    protected double operation(double x) {
        return (x >= 0) ? x : (-x);
    }
}
