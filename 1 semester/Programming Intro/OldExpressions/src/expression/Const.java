package expression;

public class Const extends Term {
    protected int valueInt;
    protected double valueDouble;

    public Const(int _value) {
        valueInt = _value;
        valueDouble = (double) valueInt;
    }

    public Const(double _value) {
        valueDouble = _value;
        valueInt = (int) valueDouble;
    }

    @Override
    protected int doEvaluate(int x) {
        return valueInt;
    }

    @Override
    protected double doEvaluate(double x) {
        return valueDouble;
    }

    @Override
    protected int doEvaluate(int x, int y, int z) {
        return valueInt;
    }
}
