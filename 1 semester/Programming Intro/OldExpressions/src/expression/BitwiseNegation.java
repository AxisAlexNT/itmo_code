package expression;

public class BitwiseNegation extends UnaryOperator {
    public BitwiseNegation(Term argument) {
        super(argument);
    }

    @Override
    protected int operation(int x) {
        return ~x;
    }

    @Override
    protected double operation(double x) {
        // throw new Exception();
        return ~(int) x;
    }
}
