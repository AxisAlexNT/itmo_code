package expression;

public abstract class UnaryOperator extends Term {
    protected Term argument;

    public UnaryOperator(Term argument) {
        this.argument = argument;
    }

    @Override
    protected int doEvaluate(int x) {
        return operation(argument.evaluate(x));
    }

    protected abstract int operation(int x);

    @Override
    protected double doEvaluate(double x) {
        return operation(argument.evaluate(x));
    }

    protected abstract double operation(double x);

    @Override
    protected int doEvaluate(int x, int y, int z) {
        return operation(argument.evaluate(x, y, z));
    }
}
