package expression.parser;

/*
Grammar:

O' --> |XO' | empty
O  --> XO'
X' --> ^AX' | empty
X  --> AX'
A' --> &FA' | empty
A  --> FA'
F' --> (+|-)SF' | empty
F  --> SF'
S' --> ( *|/ )TS' | empty
S  --> TS'|empty
T  --> -T|~T|count T|(OХотя)|x|y|z|I|O
I  --> 0..9 | 0..9I


Starting token: O
*/


import expression.*;

public class ExpressionParser implements Parser {
    public static final String countToken = "count";


    @Override
    public TripleExpression parse(String expression) {
        assert (expression.length() > 0);

        try {
            return parseORPriority(expression, 0).expression;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    private ParseResult parsePreORPriority(String expression, int position, Term leftX) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            return new ParseResult(leftX, position);
        }

        ParseResult X;
        ParseResult preORight;

        switch (expression.charAt(position)) {
            case '|':
                X = parseXORPriority(expression, position + 1);
                preORight = parsePreORPriority(expression, X.endedPosition, new BitwiseOR(leftX, X.expression));
                return new ParseResult(preORight.expression, preORight.endedPosition);
            default:
                return new ParseResult(leftX, position);
        }
    }


    private ParseResult parseORPriority(String expression, int position) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected term O, found EOS", position);
        }

        ParseResult X, preO;

        X = parseXORPriority(expression, position);
        preO = parsePreORPriority(expression, X.endedPosition, X.expression);
        return new ParseResult(preO.expression, preO.endedPosition);
    }


    private ParseResult parsePreXORPriority(String expression, int position, Term leftA) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            return new ParseResult(leftA, position);
        }

        ParseResult A;
        ParseResult preXORight;

        switch (expression.charAt(position)) {
            case '^':
                A = parseANDPriority(expression, position + 1);
                preXORight = parsePreXORPriority(expression, A.endedPosition, new BitwiseXOR(leftA, A.expression));
                return new ParseResult(preXORight.expression, preXORight.endedPosition);
            default:
                return new ParseResult(leftA, position);
        }
    }


    private ParseResult parseXORPriority(String expression, int position) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected term X, found EOS", position);
        }

        ParseResult A, preX;

        A = parseANDPriority(expression, position);
        preX = parsePreXORPriority(expression, A.endedPosition, A.expression);
        return new ParseResult(preX.expression, preX.endedPosition);
    }


    private ParseResult parsePreANDPriority(String expression, int position, Term leftF) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            return new ParseResult(leftF, position);
        }

        ParseResult F;
        ParseResult preANDRight;

        switch (expression.charAt(position)) {
            case '&':
                F = parseFirstPriority(expression, position + 1);
                preANDRight = parsePreANDPriority(expression, F.endedPosition, new BitwiseAND(leftF, F.expression));
                return new ParseResult(preANDRight.expression, preANDRight.endedPosition);
            default:
                return new ParseResult(leftF, position);
        }
    }


    private ParseResult parseANDPriority(String expression, int position) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected term A, found EOS", position);
        }

        ParseResult F, preA;

        F = parseFirstPriority(expression, position);
        preA = parsePreANDPriority(expression, F.endedPosition, F.expression);
        return new ParseResult(preA.expression, preA.endedPosition);
    }





    private ParseResult parsePreFirstPriority(String expression, int position, Term leftS) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            return new ParseResult(leftS, position);
        }

        ParseResult S;
        ParseResult preFRight;

        switch (expression.charAt(position)) {
            case '+':
                S = parseSecondPriority(expression, position + 1);
                preFRight = parsePreFirstPriority(expression, S.endedPosition, new Add(leftS, S.expression));
                return new ParseResult(preFRight.expression, preFRight.endedPosition);
            case '-':
                S = parseSecondPriority(expression, position + 1);
                preFRight = parsePreFirstPriority(expression, S.endedPosition, new Subtract(leftS, S.expression));
                return new ParseResult(preFRight.expression, preFRight.endedPosition);
            default:
                return new ParseResult(leftS, position);

        }
    }


    private ParseResult parseFirstPriority(String expression, int position) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected token F, but string has ended", position);
        }

        ParseResult S = parseSecondPriority(expression, position);
        return parsePreFirstPriority(expression, S.endedPosition, S.expression);
    }


    private ParseResult parsePreSecondPriority(String expression, int position, Term leftT) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            return new ParseResult(leftT, position);
        }

        ParseResult T;
        ParseResult preSRight;

        switch (expression.charAt(position)) {
            case '*':
                T = parseThirdPriority(expression, position + 1);
                preSRight = parsePreSecondPriority(expression, T.endedPosition, new Multiply(leftT, T.expression));
                return new ParseResult(preSRight.expression, preSRight.endedPosition);
            case '/':
                T = parseThirdPriority(expression, position + 1);
                preSRight = parsePreSecondPriority(expression, T.endedPosition, new Divide(leftT, T.expression));
                return new ParseResult(preSRight.expression, preSRight.endedPosition);
            default:
                return new ParseResult(leftT, position);
        }
    }

    private ParseResult parseSecondPriority(String expression, int position) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);
        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected token T, but reached EOS", position);
        }

        ParseResult T = parseThirdPriority(expression, position);
        return parsePreSecondPriority(expression, T.endedPosition, T.expression);
    }


    private ParseResult parseThirdPriority(String expression, int position) throws GrammarMismatchException {
        position = skipWhiteSpace(expression, position);

        if (position == expression.length()) {
            throw new GrammarMismatchException("Expected term T, found EOS", position);
        }

        ParseResult T, F;

        switch (expression.charAt(position)) {
            case '-':
                T = parseThirdPriority(expression, position + 1);
                return new ParseResult(new UnaryMinus(T.expression), T.endedPosition);
            case '(':
                T = parseORPriority(expression, position + 1);
                if (T.endedPosition == expression.length()) {
                    throw new GrammarMismatchException("Expected closing bracket ')', but reached EOS", T.endedPosition);
                }
                position = skipWhiteSpace(expression, T.endedPosition);
                if (expression.charAt(position) != ')') {
                    throw new GrammarMismatchException("Expected closing bracket ')', but none found", position);
                }

                return new ParseResult(T.expression, position + 1);
            case 'x':
            case 'y':
            case 'z':
                return new ParseResult(new Variable(String.valueOf(expression.charAt(position))), position + 1);
            case '~':
                T = parseThirdPriority(expression, position + 1);
                return new ParseResult(new BitwiseNegation(T.expression), T.endedPosition);
            case 'c':
                boolean countTokenParsed = parseCountTerm(expression, position);
                if (!countTokenParsed) {
                    throw new GrammarMismatchException("Expected token 'count', but failed to parse it", position);
                }
                position += countToken.length();
                T = parseThirdPriority(expression, position);
                return new ParseResult(new BitwiseCount(T.expression), T.endedPosition);
            default:
                if (Character.isDigit(expression.charAt(position))) {
                    return tryParseInt(expression, position);
                } else {
                    return parseORPriority(expression, position);
                }
        }
    }


    private boolean parseCountTerm(String expression, int position) {
        for (int i = position; i < countToken.length(); i++) {
            if (expression.charAt(i) != countToken.charAt(i - position)) {
                return false;
            }
        }

        return true;
    }


    private ParseResult tryParseInt(String expression, int position) throws GrammarMismatchException {
        int answer = -1;

        while ((position < expression.length()) && Character.isDigit(expression.charAt(position))) {
            if (answer == -1) {
                answer = 0;
            }

            answer = answer * 10 + (expression.charAt(position) - '0');
            position++;
        }

        if (answer == -1) {
            throw new GrammarMismatchException("Expected token I, but no number is represented", position);
        } else {
            return new ParseResult(new Const(answer), position);
        }
    }


    private int skipWhiteSpace(String expression, int position) {
        while ((position < expression.length()) && Character.isWhitespace(expression.charAt(position))) {
            position++;
        }

        return position;
    }

    private class ParseResult {
        Term expression;
        int endedPosition;

        public ParseResult(Term _expression, int _endedPosition) {
            expression = _expression;
            endedPosition = _endedPosition;
        }
    }
}