package expression.parser;

public class GrammarMismatchException extends Exception {
    public GrammarMismatchException() {
        super();
    }


    public GrammarMismatchException(String message) {
        super(message);
    }


    public GrammarMismatchException(String message, int errorPosition) {
        super(new StringBuilder().append("At position: ").append(message).append("\nParser exception:\n").append(message).toString());
    }


    //public GrammarMismatchException(String message, String wantedTr)
}
