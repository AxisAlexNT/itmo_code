package expression;

public class BitwiseAND extends BinaryOperator {
    @Override
    protected int operation(int l, int r) {
        return l & r;
    }

    @Override
    protected double operation(double l, double r) {
        // throw new Exception();
        return ((int) l) & ((int) r);
    }

    public BitwiseAND(Term _left, Term _right) {
        super(_left, _right);
    }
}
