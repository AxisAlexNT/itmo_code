package expression;

public abstract class Term implements CommonExpression {
    protected abstract int doEvaluate(int x);

    protected abstract double doEvaluate(double x);

    protected abstract int doEvaluate(int x, int y, int z);

    @Override
    public int evaluate(int x) {
        return doEvaluate(x);
    }

    @Override
    public double evaluate(double x) {
        return doEvaluate(x);
    }

    @Override
    public int evaluate(int x, int y, int z) {
        return doEvaluate(x, y, z);
    }


}
