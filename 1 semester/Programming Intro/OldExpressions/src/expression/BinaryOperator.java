package expression;

public abstract class BinaryOperator extends Term {
    protected Term left;
    protected Term right;

    protected abstract int operation(int l, int r);

    protected abstract double operation(double l, double r);


    public BinaryOperator(Term _left, Term _right) {
        left = _left;
        right = _right;
    }


    @Override
    protected int doEvaluate(int x) {
        return operation(left.evaluate(x), right.evaluate(x));
    }

    @Override
    protected double doEvaluate(double x) {
        return operation(left.evaluate(x), right.evaluate(x));
    }

    @Override
    protected int doEvaluate(int x, int y, int z) {
        return operation(left.evaluate(x, y, z), right.evaluate(x, y, z));
    }
}
