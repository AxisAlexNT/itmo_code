package expression;

public class Multiply extends BinaryOperator {
    public Multiply(Term _left, Term _right) {
        super(_left, _right);
    }

    @Override
    protected int operation(int l, int r) {
        return l * r;
    }

    @Override
    protected double operation(double l, double r) {
        return l * r;
    }
}
