package expression;

public class UnaryMinus extends UnaryOperator {
    public UnaryMinus(Term argument) {
        super(argument);
    }

    @Override
    protected int operation(int x) {
        return (-x);
    }

    @Override
    protected double operation(double x) {
        return -x;
    }
}
