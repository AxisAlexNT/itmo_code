package expression;

public class Divide extends BinaryOperator {
    @Override
    protected int operation(int l, int r) {
        return l / r;
    }

    @Override
    protected double operation(double l, double r) {
        return l / r;
    }

    public Divide(Term _left, Term _right) {
        super(_left, _right);
    }
}
