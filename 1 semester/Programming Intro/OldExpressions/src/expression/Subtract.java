package expression;

public class Subtract extends BinaryOperator {
    @Override
    protected int operation(int l, int r) {
        return l - r;
    }

    @Override
    protected double operation(double l, double r) {
        return l - r;
    }

    public Subtract(Term _left, Term _right) {
        super(_left, _right);
    }

}
