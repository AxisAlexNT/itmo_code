import expression.*;

public class BasicTest {
    public static void main(String[] args) {
        int res = new Subtract(
                new Multiply(
                        new Const(3),
                        new Variable("x")
                ),
                new Const(3)
        ).evaluate(5);

        System.out.println(res);
    }
}
