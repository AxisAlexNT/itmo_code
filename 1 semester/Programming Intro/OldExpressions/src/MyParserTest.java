import expression.*;
import expression.parser.ExpressionParser;

public class MyParserTest {
    public static void main(String[] args) {
        ExpressionParser parser = new ExpressionParser();

        //TripleExpression parseResult = parser.parse("-(-(-		-5 + 16   *x*y) + 1 * z) -(((-11)))");
        TripleExpression parseResult = parser.parse("y|z");

        System.out.println(parseResult.evaluate(0, 1, 2));
    }
}
