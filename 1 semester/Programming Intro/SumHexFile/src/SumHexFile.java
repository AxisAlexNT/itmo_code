import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Pattern;

public class SumHexFile {


    private static void fallWithErrorMessage(int line, String element) {
        System.out.print("Line #");
        System.out.print(line);
        System.out.print(" Element '");
        System.out.print(element);
        System.out.println("' does not represent a valid decimal or hexadecimal number");
    }

    public static void main(String[] args) {

        if (args.length != 2) {
            System.out.println("Usage: java SumHexFile <input file> <output file>");
            return;
        }


        try (Scanner inputScanner = new Scanner(new File(args[0]))) {


            Pattern numbersPattern = Pattern.compile("\\p{Space}", Pattern.UNICODE_CHARACTER_CLASS);

            int sum = 0;

            int currentLine = 0;

            inputScanner.hasNextInt();


            while (inputScanner.hasNextLine()) {
                String nextLine = inputScanner.nextLine().toLowerCase();
                currentLine++;

                String[] inputRow = numbersPattern.split(nextLine);


                for (String numberString : inputRow) {
                    if (numberString.length() > 0) {

                        try {

                            boolean curNumIsNegative, curNumIsInHex;

                            curNumIsNegative = (Character.getType(numberString.charAt(0)) == Character.DASH_PUNCTUATION);

                            if (!numberString.contains("0x")) {
                                for (int i = 0; i < numberString.length(); i++) {
                                    char digit = numberString.charAt(i);

                                    if ((!Character.isDigit(digit)) && (Character.getType(digit) != Character.DASH_PUNCTUATION)) {
                                        fallWithErrorMessage(currentLine, numberString);
                                        return;
                                    }
                                }
                            } else {
                                if (numberString.substring(2).contains("x")) {
                                    fallWithErrorMessage(currentLine, numberString);
                                    return;
                                }
                            }



                            if (numberString.length() > 2 + (curNumIsNegative ? 1 : 0)) {
                                curNumIsInHex = numberString.substring(curNumIsNegative ? 1 : 0).startsWith("0x");

                                int radix = curNumIsInHex ? 16 : 10;

                                String numberWithoutPrefix = numberString.substring((curNumIsNegative ? 1 : 0) + (curNumIsInHex ? 2 : 0));

                                sum += (curNumIsNegative ? (-1) : 1) * Long.parseLong(numberWithoutPrefix, radix);

                            } else {
                                sum += Long.parseLong(numberString);
                            }
                        } catch (InputMismatchException ime) {
                            fallWithErrorMessage(currentLine, numberString);
//                            System.out.println("Techincal details:");
//                            System.out.println(ime.getMessage());
                            return;
                        }

                    }
                }
            }

            try (PrintWriter outputWriter = new PrintWriter(new File(args[1]))) {

                outputWriter.println(sum);


            } catch (FileNotFoundException ex) {
                System.out.println("Failed to create an output file.");
//                System.out.println("Techincal details:");
//                System.out.println(ex.getMessage());
            } catch (InputMismatchException ime) {
                System.out.println("Your input does not contain separated decimal or hexadecimal numbers.");
//                System.out.println("Techincal details:");
//                System.out.println(ime.getMessage());
            }
        } catch (FileNotFoundException e) {
            System.out.println("Cannot find input file!");
//            System.out.println("Techincal details:");
//            System.out.println(e.getMessage());
        }
    }
}
