import java.util.ArrayList;
import java.util.Scanner;

public class ReverseMax {

    public static void main(String[] args) {

        ArrayList<ArrayList<Integer>> inputMatrix = new ArrayList<>();

        Scanner inputScanner = new Scanner(System.in);

        int x = 0;

        int maxLineWidth = 0;

        //inputScanner.


        while (inputScanner.hasNextLine()) {
            String currentLine = inputScanner.nextLine();
            String[] numbersInLine = currentLine.split("([^-0-9])");

            inputMatrix.add(new ArrayList<Integer>());


            for (int i = 0; i < numbersInLine.length; i++) {
                if (numbersInLine[i].length() > 0) {
                    inputMatrix.get(x).add((Integer.valueOf(numbersInLine[i])));
                }
            }

            if (inputMatrix.get(x).size() > maxLineWidth) {
                maxLineWidth = inputMatrix.get(x).size();
            }

            x++;
        }


        Integer[] lineMaximum = new Integer[inputMatrix.size()];

        for (int i = 0; i < inputMatrix.size(); i++) {
            lineMaximum[i] = null;
        }


        for (int i = 0; i < inputMatrix.size(); i++) {
            for (int j = 0; j < inputMatrix.get(i).size(); j++) {
                if ((lineMaximum[i] == null) || (inputMatrix.get(i).get(j) > lineMaximum[i])) {
                    lineMaximum[i] = inputMatrix.get(i).get(j);
                }
            }
        }


        Integer[] rowMaximum = new Integer[maxLineWidth];

        for (int i = 0; i < maxLineWidth; i++) {
            rowMaximum[i] = null;
        }


        for (int rowIndex = 0; rowIndex < maxLineWidth; rowIndex++) {
            for (int lineIndex = 0; lineIndex < inputMatrix.size(); lineIndex++) {
                if (rowIndex < inputMatrix.get(lineIndex).size()) {
                    if ((rowMaximum[rowIndex] == null) || (inputMatrix.get(lineIndex).get(rowIndex) > rowMaximum[rowIndex])) {
                        rowMaximum[rowIndex] = inputMatrix.get(lineIndex).get(rowIndex);
                    }
                }
            }
        }


        for (int lineIndex = 0; lineIndex < inputMatrix.size(); lineIndex++) {
            for (int rowIndex = 0; rowIndex < inputMatrix.get(lineIndex).size(); rowIndex++) {
                if (rowMaximum[rowIndex] == null) {
                    System.out.print(lineMaximum[lineIndex]);
                } else if (lineMaximum[lineIndex] == null) {
                    System.out.print(rowMaximum[rowIndex]);
                } else {
                    System.out.print(Math.max(rowMaximum[rowIndex], lineMaximum[lineIndex]));
                }

                System.out.print(" ");
            }
            System.out.println();
        }

    }
}
