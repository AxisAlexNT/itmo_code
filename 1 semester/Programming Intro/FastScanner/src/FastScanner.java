import java.io.*;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.InputMismatchException;


public class FastScanner implements Closeable {
    public static final int BUFFER_SIZE = (1 << 14);
    private byte[] buffer;
    private String currentLine = null;
    private String nextLine = null;
    private InputStream inputStream = null;
    private int numberOfBytesRead = -2;
    private int currentPositionInBuffer;
    private boolean streamEnded = false;
    private ArrayDeque<String> currentLineTokens = null;


    public FastScanner(InputStream _inputStream) {
        inputStream = _inputStream;
        buffer = new byte[BUFFER_SIZE];
        currentPositionInBuffer = 0;
        bufferize();
        currentLineTokens = new ArrayDeque<>();
    }


    public FastScanner(String _s) {
        buffer = _s.getBytes();
        currentPositionInBuffer = 0;
        numberOfBytesRead = _s.length();
        streamEnded = true;
    }


    public FastScanner(File f) throws FileNotFoundException {
        inputStream = new FileInputStream(f);
        buffer = new byte[BUFFER_SIZE];
        currentPositionInBuffer = 0;
        bufferize();
        currentLineTokens = new ArrayDeque<>();
    }


    @Override
    public void close() throws IOException {
        if (inputStream != null) {
            inputStream.close();
        }
    }


    private void bufferize() {
        if (((!streamEnded) && (numberOfBytesRead >= 0)) || (numberOfBytesRead == -2)) {
            try {
                numberOfBytesRead = inputStream.read(buffer);

                if (numberOfBytesRead == -1) {
                    streamEnded = true;
                    inputStream.close();
                    inputStream = null;
                } else {
                    currentPositionInBuffer = 0;
                }
            } catch (IOException e) {
                streamEnded = true;
                try {
                    inputStream.close();
                } catch (IOException e1) {
                    inputStream = null;
                }
                inputStream = null;
            }
        }
    }


    private boolean bufferIsNotEnded() {
        return (currentPositionInBuffer < numberOfBytesRead);
    }


    private void ifNeedBufferize() {
        if (!bufferIsNotEnded() && !streamEnded) {
            bufferize();
        }
    }


    private void skipToNextCharStart() {
        ifNeedBufferize();
        while (bufferIsNotEnded() && ((buffer[currentPositionInBuffer] & 0b11000000) == 0b10000000)) {
            currentPositionInBuffer++;
            ifNeedBufferize();
        }
    }


    private char getNextChar() throws IOException, InputMismatchException {
        skipToNextCharStart();

        if (!bufferIsNotEnded() && streamEnded) {
            throw new IOException("Next character is not available: stream ended");
        }

        byte t = buffer[currentPositionInBuffer];
        int lengthInBytes = -1;
        int charValue = 0;


        if ((t & 0b10000000) == 0b00000000) {
            lengthInBytes = 1;
        } else if ((t & 0b11110000) == 0b11110000) {
            lengthInBytes = 4;
        } else if ((t & 0b11100000) == 0b11100000) {
            lengthInBytes = 3;
        } else if ((t & 0b11000000) == 0b11000000) {
            lengthInBytes = 2;
        } else {
            throw new InputMismatchException("Not a valid UTF-8 character");
        }

        int[] bytesOfChar = new int[lengthInBytes];

        for (int i = 0; i < lengthInBytes; i++) {
            bytesOfChar[i] = 256 + buffer[currentPositionInBuffer];
            currentPositionInBuffer++;
            ifNeedBufferize();
        }


        switch (lengthInBytes) {
            case 1:
                charValue = bytesOfChar[0] & 0b01111111;
                break;
            case 2:
                charValue = ((bytesOfChar[0] & 0b00011111) << 6) |
                        ((bytesOfChar[1] & 0b00111111));
                break;
            case 3:
                charValue = ((bytesOfChar[0] & 0b00001111) << 12) |
                        ((bytesOfChar[1] & 0b00111111) << 6) |
                        ((bytesOfChar[2] & 0b00111111));
                break;
            case 4:
                charValue = ((bytesOfChar[0] & 0b00000111) << 18) |
                        ((bytesOfChar[1] & 0b00111111) << 12) |
                        ((bytesOfChar[2] & 0b00111111) << 6) |
                        ((bytesOfChar[3] & 0b00111111));
                break;
            default:
                break;
        }
        return (char) charValue;
    }


    private String getNextLine() {
        char currentCharInStream = '\n';

        StringBuilder nextLineBuilder;
        nextLineBuilder = new StringBuilder();

        do {        // Construct a line of read characters
            try {
                currentCharInStream = getNextChar();
                nextLineBuilder.append(currentCharInStream);
            } catch (InputMismatchException ime) {
                ime.printStackTrace();
            } catch (IOException ioe) {
                break;
            }

        } while ((currentCharInStream != '\n') && (currentCharInStream != 0x04));

        // If there were no next characters, we've reached the end of stream and current line is null:

        if ((currentCharInStream == 0x04) || (nextLineBuilder.length() == 0)) {
            streamEnded = true;
            return null;
        }


        // As the last character could be whitespace (CR-LF), we need to delete it
        if (nextLineBuilder.length() > 0) {
            if (Character.isWhitespace(nextLineBuilder.charAt(nextLineBuilder.length() - 1))) {
                nextLineBuilder.deleteCharAt(nextLineBuilder.length() - 1);
            }
        }


        return nextLineBuilder.toString();
    }


    private void buildTokensInLine(String line, ArrayDeque<String> tokensDeque) {
        char currentCharInLine;
        StringBuilder tokensBuilder = new StringBuilder();

        for (int i = 0; i < line.length(); i++) {
            currentCharInLine = line.charAt(i);
            if (!Character.isWhitespace(currentCharInLine)) {
                tokensBuilder.append(currentCharInLine);
            } else {
                if (tokensBuilder.length() > 0) {
                    tokensDeque.addLast(tokensBuilder.toString());
                    tokensBuilder = new StringBuilder();
                }
            }
        }

        if (tokensBuilder.length() > 0) {
            tokensDeque.addLast(tokensBuilder.toString());
        }

    }


    public boolean hasNextLine() {
        if (currentLine == null) {
            nextLine = getNextLine();
            if (nextLine == null) {
                return false;
            }
            return true;
        } else {
            if (nextLine != null) {
                return true;
            } else {
                nextLine = getNextLine();
                return (nextLine != null);
            }
        }
    }


    public String nextLine() throws IOException {
        if (nextLine == null) {
            nextLine = getNextLine();
        }

        if (nextLine == null) {
            throw new IOException("No next line is available!");
        }

        String retLine = nextLine;
        nextLine = null;
        return retLine;
    }


    private void rebuildTokens() {
        if (currentLineTokens == null) {
            currentLineTokens = new ArrayDeque<>();
            currentLineTokens.clear();
            buildTokensInLine(currentLine, currentLineTokens);
        }
    }

    private boolean ifNeedUpdateLines() {
        if (currentLine == null) {
            if (nextLine != null) {
                currentLine = nextLine;
                nextLine = null;
            } else {
                currentLine = getNextLine();
            }

            if (currentLine != null) {
                rebuildTokens();
                return true;
            } else {
                return false;
            }

        } else {
            rebuildTokens();
            return true;
        }

    }


    public boolean hasNextInt(int radix) {
        boolean linesUpdated = ifNeedUpdateLines();

        if (!linesUpdated) {
            return false;
        }

        if ((currentLineTokens == null) || (currentLineTokens.isEmpty())) {
            return false;
        } else {
            try {
                int t = Integer.parseInt(currentLineTokens.peekFirst(), radix);
                return true;
            } catch (Exception e) {
                return false;
            }
        }
    }


    public int nextInt(int radix) throws NoSuchFieldException {
        if (!hasNextInt(radix)) {
            throw new NoSuchFieldException("Next token is not a valid number!");
        }

        int t = Integer.parseInt(currentLineTokens.pop(), radix);
        return t;
    }


    public int nextInt() throws Exception {
        return nextInt(10);
    }


    public boolean hasNextInt() {
        return hasNextInt(10);
    }


    public void goOnNextLine() {
        if (nextLine != null) {
            currentLine = nextLine;
        } else {
            currentLine = getNextLine();
        }

        currentLineTokens.clear();
        buildTokensInLine(currentLine, currentLineTokens);
        nextLine = null;
    }


    public ArrayDeque<String> getTokensInLine() {
        ifNeedUpdateLines();
        return currentLineTokens.clone();
    }


    public ArrayList<Integer> getIntegersInLine() throws Exception {
        ArrayList<Integer> result = new ArrayList<>();
        StringBuilder intBuilder = new StringBuilder();
        int tokenIndex = 0;
        int i = 0;
        if (currentLine == null) {
            if (nextLine != null) {
                currentLine = nextLine;
                nextLine = null;
            } else {
                currentLine = getNextLine();
            }
        }

        if (currentLine != null) {
            while (i < currentLine.length()) {
                char ch = currentLine.charAt(i);
                if (!Character.isWhitespace(ch)) {
                    intBuilder.append(ch);
                } else {
                    if (intBuilder.length() > 0) {
                        result.add(tokenIndex, Integer.parseInt(intBuilder.toString()));
                        tokenIndex++;
                        intBuilder = new StringBuilder();
                    }
                }
                i++;
            }
        }

        if (intBuilder.length() > 0) {
            result.add(tokenIndex, Integer.parseInt(intBuilder.toString()));
        }
        return result;
    }


    public boolean hasNextString() {
        boolean linesUpdated = ifNeedUpdateLines();

        if (!linesUpdated) {
            return false;
        }

        return ((currentLineTokens != null) && (currentLineTokens.size() > 0));
    }


    public String nextString() throws NoSuchFieldException {
        boolean linesUpdated = ifNeedUpdateLines();

        if ((!linesUpdated) || (currentLineTokens == null) || (currentLineTokens.isEmpty())) {
            throw new NoSuchFieldException("There is no next string in current line!");
        } else {
            return currentLineTokens.pop();
        }
    }


    private boolean characterIsAPartOfWord(char c) {
        return ((Character.isAlphabetic(c)) || (Character.getType(c) == Character.DASH_PUNCTUATION) || (c == '\''));
    }


    public boolean hasNextWord() {
        boolean linesUpdated = ifNeedUpdateLines();

        if ((!linesUpdated) || (currentLineTokens == null) || currentLineTokens.isEmpty()) {
            return false;
        } else {
            String word = currentLineTokens.peekFirst();

            for (int i = 0; i < word.length(); i++) {
                char c = word.charAt(i);
                if (characterIsAPartOfWord(c)) {
                    return true;
                }
            }
            return false;
        }
    }


    public String nextWord() throws NoSuchFieldException {
        if (!hasNextWord()) {
            throw new NoSuchFieldException("Next token is not a word!");
        } else {
            String currentToken = currentLineTokens.pop();

            int positionInToken = 0;

            while ((positionInToken < currentToken.length()) && !characterIsAPartOfWord(currentToken.charAt(positionInToken))) {
                positionInToken++;
            }

            StringBuilder wordBuilder = new StringBuilder();
            StringBuilder otherBuilder = new StringBuilder();

            for (; positionInToken < currentToken.length(); positionInToken++) {
                if (characterIsAPartOfWord(currentToken.charAt(positionInToken))) {
                    wordBuilder.append(currentToken.charAt(positionInToken));
                } else {
                    break;
                }
            }

            for (; positionInToken < currentToken.length(); positionInToken++) {
                otherBuilder.append(currentToken.charAt(positionInToken));
            }

            if (otherBuilder.length() > 0) {
                currentLineTokens.addFirst(otherBuilder.toString());
            }

            return wordBuilder.toString();
        }
    }


    public int numberOfTokensAvailable() {
        if (currentLineTokens == null) {
            return 0;
        } else {
            return currentLineTokens.size();
        }
    }


    public void skipNextToken() {
        if ((currentLineTokens == null) || currentLineTokens.isEmpty()) {
            return;
        }
        currentLineTokens.pop();
    }


}


