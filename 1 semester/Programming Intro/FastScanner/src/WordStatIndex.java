import java.io.*;
import java.nio.charset.Charset;
import java.util.LinkedHashMap;
import java.util.LinkedList;

class WordData {
    String word = "";
    int occurenceNumber = 1;
    LinkedList<Integer> positions;

    public WordData() {
        positions = new LinkedList<>();
    }

    public WordData(String _word, int firstOccurrencePosition) {
        word = _word;
        positions = new LinkedList<>();
        positions.add(firstOccurrencePosition);
    }

    public void addOccurrence(int pos) {
        //System.out.println("Another occurrence: " + word + " " + occurenceNumber + " " + pos);
        occurenceNumber++;
        positions.add(pos);
    }

    @Override
    public int hashCode() {
        return word.hashCode();
    }
}


public class WordStatIndex {
    public static void main(String[] args) {

        if (args.length != 2) {
            System.out.println("Usage: java WordStatIndex <input file> <output file>");
            return;
        }

        LinkedHashMap<String, WordData> stats = new LinkedHashMap<>();

        String currentWord = null;
        int currentPosition = 1;


        try (FastScanner scanner = new FastScanner(new File(args[0]))) {
            while (scanner.hasNextLine()) {
                scanner.goOnNextLine();

                while (scanner.numberOfTokensAvailable() > 0) {
                    try {
                        currentWord = scanner.nextWord().toLowerCase();

                        //System.out.println("New word: " + currentWord);

                        if (stats.containsKey(currentWord)) {
                            stats.get(currentWord).addOccurrence(currentPosition);
                        } else {
                            stats.put(currentWord, new WordData(currentWord, currentPosition));
                        }

                        currentPosition++;

                    } catch (NoSuchFieldException e) {
                        scanner.skipNextToken();
                    }

                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Input file cannot be found!");
        } catch (IOException e) {
            System.out.println("Exception during scanning file:");
            e.printStackTrace();
        }


        StringBuilder outputBuilder = new StringBuilder();


        stats.forEach((word, wordData) ->
        {
            outputBuilder.append(word).append(' ');
            outputBuilder.append(wordData.occurenceNumber);
            for (int wordOccurrencePosition : wordData.positions) {
                outputBuilder.append(' ').append(wordOccurrencePosition);
            }
            outputBuilder.append('\n');
        });


        try (PrintWriter outputWriter = new PrintWriter(args[1], "UTF8")) {
            outputWriter.print(outputBuilder.toString());
        } catch (FileNotFoundException e) {
            System.out.println("Cannot create output file!");
            System.out.println(e.getMessage());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }
}
