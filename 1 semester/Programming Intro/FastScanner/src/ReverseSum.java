import java.io.IOException;
import java.util.ArrayList;

public class ReverseSum {
    public static void main(String[] args) {
        ArrayList<ArrayList<Integer>> matrix = new ArrayList<>();

        int numberOfRows = 0;
        int numberOfColumns = 0;

        try (FastScanner scanner = new FastScanner(System.in)) {

            while (scanner.hasNextLine()) {
                try {
                    scanner.goOnNextLine();
                    ArrayList<Integer> intsInLine = scanner.getIntegersInLine();
                    matrix.add(numberOfRows, intsInLine);

                    if (intsInLine.size() > numberOfColumns) {
                        numberOfColumns = intsInLine.size();
                    }

                    numberOfRows++;
                } catch (Exception e) {
                    System.out.println("Line #" + numberOfRows + " contains non-integer element:");
                    System.out.println(e.getMessage());
                    scanner.close();
                    return;
                }
            }
        } catch (IOException e) {
            System.out.println("Cannot create FastScanner on given InputStream:");
            System.out.println(e.getMessage());
        }


        int[] rowSum = new int[numberOfRows];
        int[] colSum = new int[numberOfColumns];


        for (int rowIndex = 0; rowIndex < numberOfRows; rowIndex++) {
            for (int columnIndex = 0; columnIndex < matrix.get(rowIndex).size(); columnIndex++) {
                int t = matrix.get(rowIndex).get(columnIndex);
                rowSum[rowIndex] += t;
                colSum[columnIndex] += t;
            }
        }


        StringBuilder outputBuilder = new StringBuilder();

        for (int rowIndex = 0; rowIndex < numberOfRows; rowIndex++) {
            for (int columnIndex = 0; columnIndex < matrix.get(rowIndex).size(); columnIndex++) {
                outputBuilder.append(rowSum[rowIndex] + colSum[columnIndex] - matrix.get(rowIndex).get(columnIndex));
                outputBuilder.append(' ');
            }
            outputBuilder.append('\n');
        }

        System.out.print(outputBuilder.toString());


    }
}
