import java.io.*;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;


class LineIndexOccurrence {
    int line;
    int position;

    public LineIndexOccurrence(int _line, int _position) {
        line = _line;
        position = _position;
    }
}


class WordDataLine implements Comparable<WordDataLine> {
    String word;
    int occurenceNumber = 1;
    LinkedHashSet<LineIndexOccurrence> positions;


    public WordDataLine(String _word, int firstOccurrenceLine, int firstOccurrenceIndex) {
        word = _word;
        positions = new LinkedHashSet<>();
        positions.add(new LineIndexOccurrence(firstOccurrenceLine, firstOccurrenceIndex));
    }

    public void addOccurrence(int line, int pos) {
        occurenceNumber++;
        positions.add(new LineIndexOccurrence(line, pos));
    }

    @Override
    public int hashCode() {
        return word.hashCode();
    }

    @Override
    public int compareTo(WordDataLine o) {
        return word.compareTo(o.word);
    }
}



public class WordStatLineIndex {

    public static void main(String[] args) {

        if (args.length != 2) {
            System.out.println("Usage: java WordStatIndex <input file> <output file>");
            return;
        }


        LinkedHashMap<String, WordDataLine> stats = new LinkedHashMap<>();


        try (FastScanner scanner = new FastScanner(new File(args[0]))) {

            String nextWord;
            int currentLineNumber = 0;
            int currentPositionInLine;

            while (scanner.hasNextLine()) {
                scanner.goOnNextLine();

                currentLineNumber++;
                currentPositionInLine = 1;

                while (scanner.numberOfTokensAvailable() > 0) {
                    try {
                        nextWord = scanner.nextWord().toLowerCase();

                        if (stats.containsKey(nextWord)) {
                            stats.get(nextWord).addOccurrence(currentLineNumber, currentPositionInLine);
                        } else {
                            stats.put(nextWord, new WordDataLine(nextWord, currentLineNumber, currentPositionInLine));
                        }

                        currentPositionInLine++;

                    } catch (NoSuchFieldException e) {
                        scanner.skipNextToken();
                    }

                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Input file cannot be found!");
        } catch (IOException e) {
            System.out.println("I/O Exception during scanning file:");
            e.printStackTrace();
        }


        LinkedList<WordDataLine> sortedStats = new LinkedList<>(stats.values());
        Collections.sort(sortedStats);


        StringBuilder outputBuilder = new StringBuilder();

        sortedStats.forEach(wordDataLine ->
        {
            outputBuilder.append(wordDataLine.word).append(' ').append(wordDataLine.occurenceNumber);

            wordDataLine.positions.forEach(lineIndexOccurrence ->
            {
                outputBuilder.append(' ').append(lineIndexOccurrence.line);
                outputBuilder.append(':').append(lineIndexOccurrence.position);
            });
            outputBuilder.append('\n');
        });


        try (PrintWriter outputWriter = new PrintWriter(args[1], "UTF8")) {
            outputWriter.print(outputBuilder.toString());
        } catch (FileNotFoundException e) {
            System.out.println("Cannot create output file!");
            System.out.println(e.getMessage());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }
}
