package ru.ifmo.rain.serdiukov.walk;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Walk {
    public static void main(String[] args) {
//        RecursiveWalk.main(args);

        if ((args == null) || (args.length != 2) || (args[0] == null) || (args[1] == null)) {
            System.out.print("Wrong number of arguments.");
            printUsageMessage();
            return;
        }

        Path inputFilePath, outputFilePath, outputFileDirectory;

        try {
            inputFilePath = Paths.get(args[0]);
        } catch (InvalidPathException invalidInputPath) {
            System.out.printf("Invalid input path: %s.", invalidInputPath.getMessage());
            printUsageMessage();
            return;
        }

        try {
            outputFilePath = Paths.get(args[1]);
            outputFileDirectory = outputFilePath.getParent();
        } catch (InvalidPathException invalidOutputPath) {
            System.out.printf("Invalid output path: %s.", invalidOutputPath.getMessage());
            printUsageMessage();
            return;
        }


        if (outputFileDirectory == null) {
            System.out.print("Invalid output file path: it should be non-root.");
            printUsageMessage();
            return;
        }


        if (!Files.exists(inputFilePath)) {
            System.out.print("Input file does not exist.");
            printUsageMessage();
            return;
        }

        // Could be implemented with lazy streams in order not to get OoM
        List<String> inputFileLines;
        try {
            inputFileLines = Files.readAllLines(inputFilePath);
        } catch (IOException e) {
            System.out.printf("Cannot read lines from input file: %s%n", e.getMessage());
            return;
        }


        if (!Files.isDirectory(outputFileDirectory)) {
            try {
                Files.createDirectories(outputFileDirectory);
            } catch (IOException e) {
                System.out.printf("Cannot create directory for an output file: %s%n", e.getMessage());
                return;
            }
        }

        try (BufferedWriter outputWriter = Files.newBufferedWriter(outputFilePath)) {
            for (String fileName : inputFileLines) {
                Path filePath = Paths.get(fileName);
                outputWriter.write(String.format("%08x %s%n", FNVHasher.getFileHash(filePath), fileName));
            }
        } catch (IOException e) {
            System.out.printf("Cannot create output file: %s%n", e.getMessage());
//            return;
        }
    }

    private static void printUsageMessage() {
        System.out.printf("%nUsage: java Walk <input file name> <output file name>%n");
        System.out.flush();
    }
}
