package ru.ifmo.rain.serdiukov.walk;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

public class RecursiveWalk {
    public static void main(String[] args) {
        if ((args == null) || (args.length != 2) || (args[0] == null) || (args[1] == null)) {
            System.out.print("Wrong number of arguments.");
            printUsageMessage();
            return;
        }

        Path inputFilePath, outputFilePath, outputFileDirectory;

        try {
            inputFilePath = Paths.get(args[0]);
        } catch (InvalidPathException invalidInputPath) {
            System.out.printf("Invalid input path: %s.", invalidInputPath.getMessage());
            printUsageMessage();
            return;
        }

        try {
            outputFilePath = Paths.get(args[1]);
            outputFileDirectory = outputFilePath.getParent();
        } catch (InvalidPathException invalidOutputPath) {
            System.out.printf("Invalid output path: %s.", invalidOutputPath.getMessage());
            printUsageMessage();
            return;
        }


        if (outputFileDirectory == null) {
            System.out.print("Invalid output file path: it should be non-root.");
            printUsageMessage();
            return;
        }


        if (!Files.exists(inputFilePath)) {
            System.out.print("Input file does not exist.");
            printUsageMessage();
            return;
        }

        // Could be implemented with lazy streams in order not to get OoM
        List<String> inputFileLines;
        try {
            inputFileLines = Files.readAllLines(inputFilePath);
        } catch (IOException e) {
            System.out.printf("Cannot read lines from input file: %s%n", e.getMessage());
            return;
        }


        if (!Files.isDirectory(outputFileDirectory)) {
            try {
                Files.createDirectories(outputFileDirectory);
            } catch (IOException e) {
                System.out.printf("Cannot create directory for an output file: %s%n", e.getMessage());
                return;
            }
        }

        try (BufferedWriter outputWriter = Files.newBufferedWriter(outputFilePath)) {
            for (String fileName : inputFileLines) {
                try {
                    Path requiredPath = Paths.get(fileName);
                    if (Files.isReadable(requiredPath)) {
                        Files.walkFileTree(requiredPath, new FileVisitorPrinter(outputWriter));
                    } else {
                        outputWriter.write(getErrorStringOnFileIsNotAccessible(fileName));
                    }
                } catch (InvalidPathException invalidPathException) {
                    outputWriter.write(getErrorStringOnFileIsNotAccessible(fileName));
                }
            }
        } catch (FileVisitorPrinter.WalkerWriteException walkerWriteException) {
            System.out.printf("Cannot write to output file: %s%n", walkerWriteException.getMessage());
            //return;
        } catch (IOException ioException) {
            System.out.printf("Cannot create output file: %s%n", ioException.getMessage());
            //return;
        }
    }

    protected static String getHashString(Path filePath) {
        return String.format("%08x %s%n", FNVHasher.getFileHash(filePath), filePath);
    }

    protected static String getErrorStringOnFileIsNotAccessible(String filePathString) {
        return String.format("%08x %s%n", 0, filePathString);
    }

    private static void printUsageMessage() {
        System.out.printf("%nUsage: java RecursiveWalk <input file name> <output file name>%n");
        System.out.flush();
    }

    protected static class FileVisitorPrinter extends SimpleFileVisitor<Path> {
        private Writer outputWriter;

        FileVisitorPrinter(Writer outputWriter) {
            this.outputWriter = outputWriter;
        }

        @Override
        public FileVisitResult visitFile(Path filePath, BasicFileAttributes attrs) throws WalkerWriteException {
            try {
                outputWriter.write(getHashString(filePath));
            } catch (IOException e) {
                throw new WalkerWriteException(e.getMessage());
            }
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFileFailed(Path filePath, IOException exc) throws WalkerWriteException {
            try {
                outputWriter.write(getErrorStringOnFileIsNotAccessible(filePath.toString()));
            } catch (IOException e) {
                throw new WalkerWriteException(e.getMessage());
            }
            return FileVisitResult.CONTINUE;
        }

        public static class WalkerWriteException extends IOException {
            WalkerWriteException(String message) {
                super(message);
            }
        }
    }
}
