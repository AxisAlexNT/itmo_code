package ru.ifmo.rain.serdiukov.walk;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FNVHasher {
    private static final int FNV_32_PRIME = 0x01000193;
    private static final int FNV1_START_VALUE = 0x811c9dc5;

    public static int getFileHash(Path hashingFilePath) {
        if (!Files.isRegularFile(hashingFilePath)) {
            return 0;
        } else {
            try (BufferedInputStream fileStream = new BufferedInputStream(Files.newInputStream(hashingFilePath))) {
                int hval = FNV1_START_VALUE;

                int c;

                while ((c = fileStream.read()) >= 0) {
                    hval *= FNV_32_PRIME;
                    hval ^= c;
                }

                return hval;

            } catch (IOException e) {
                // In case of any trouble reading file
                return 0;
            }
        }
    }
}
