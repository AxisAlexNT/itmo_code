package ru.ifmo.rain.serdiukov.arrayset;

import java.util.List;
import java.util.NavigableSet;
import java.util.SortedSet;

public class Target {
    public static void main(String[] args) {

        ArraySet<Integer> set = new ArraySet<>(List.of(-198, -148, -118, -79, -1, 49, 60, 153, 197));
        NavigableSet<Integer> descSet = set.descendingSet();

        System.out.printf("First test, forward iteration: %n");
        for (int i : set) {
            System.out.println(i);
        }

        System.out.printf("Second test, descending iteration: %n");
        for (int i : descSet) {
            System.out.println(i);
        }

        SortedSet<Integer> testSet = set.subSet(-27, 60);

        System.out.printf("Third test: %n");
        for (int i : testSet) {
            System.out.println(i);
        }


//        NavigableSet<Integer> testDescSet = set.descendingSet().descendingSet();//.descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet().descendingSet();
//
//        System.out.printf("Fourth test: %n");
//        for (int i : testDescSet) {
//            System.out.println(i);
//        }

    }
}
