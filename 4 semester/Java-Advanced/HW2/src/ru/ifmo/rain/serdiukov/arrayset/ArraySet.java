package ru.ifmo.rain.serdiukov.arrayset;

import java.util.*;

import static java.util.Collections.*;

//public class ArraySet<T extends Comparable<? super T>>
public class ArraySet<T> extends AbstractSet<T> implements NavigableSet<T> {
    private BidirectionalArrayListView<T> storage;
    private Comparator<? super T> comparator;

    public ArraySet() {
        doInit(new ArrayList<>());
    }

    public ArraySet(Iterable<T> source, Comparator<? super T> givenComparator) {
        comparator = givenComparator;
        Optional<ArrayList<T>> sanitizedResult = sourceSanityCheck(source);
        if (sanitizedResult.isEmpty()) {
            TreeSet<T> treeForSorting = new TreeSet<>(givenComparator);
            source.forEach(treeForSorting::add);
            doInit(new ArrayList<>(treeForSorting));
            comparator = givenComparator;
        } else {
            doInit(sanitizedResult.get());
        }
    }

    public ArraySet(Iterable<T> source) {
        this(source, null);
    }

    public ArraySet(Comparator<? super T> givenComparator) {
        this();
        this.comparator = givenComparator;
    }


    public ArraySet(Collection<T> source, Comparator<? super T> givenComparator) {
        this((Iterable<T>) source, givenComparator);
    }

    public ArraySet(Collection<T> source) {
        this((Iterable<T>) source, null);
    }


    protected ArraySet(BidirectionalArrayListView<T> storageCollection, Comparator<? super T> givenComparator) {
        this.comparator = givenComparator;
        this.storage = storageCollection;
    }

    private void doInit(ArrayList<T> temporary) {
        temporary.trimToSize();
        storage = new BidirectionalArrayListView<>(temporary, BidirectionalArrayListView.ListViewDirection.FORWARD);
    }


    @SuppressWarnings("unchecked")
    protected Optional<ArrayList<T>> sourceSanityCheck(Iterable<T> source) {
        T previous = null;
        ArrayList<T> temporary = new ArrayList<>();


        int comparisonResult;
        int lastElementIndex = -1;

        // Two if-branches because this condition cannot change during after collection was created
        if (comparator == null) {
            for (T t : source) {
                if (t == null) {
                    throw new IllegalArgumentException("ArraySet does not permit storage of null elements");
                }

                temporary.add(t);
                ++lastElementIndex;

                if (previous != null) {
                    comparisonResult = ((Comparable<T>) t).compareTo(previous);
                    if (comparisonResult < 0) {
                        return Optional.empty();
                    } else if (comparisonResult == 0) {
                        temporary.remove(lastElementIndex);
                        --lastElementIndex;
                    }
                }
                previous = t;
            }
        } else {
            for (T t : source) {
                if (t == null) {
                    throw new IllegalArgumentException("ArraySet does not permit storage of null elements");
                }

                temporary.add(t);
                ++lastElementIndex;

                if (previous != null) {
                    comparisonResult = comparator.compare(t, previous);
                    if (comparisonResult < 0) {
                        return Optional.empty();
                    } else if (comparisonResult == 0) {
                        temporary.remove(lastElementIndex);
                        --lastElementIndex;
                    }
                }
                previous = t;
            }
        }

        return Optional.of(temporary);
    }

    protected int findIndexGreaterOrEquals(T key) {
        return Collections.binarySearch(storage, key, comparator);
    }

    private int findIndex(T element, GreaterOrLess mode) {

        int preIndex = findIndexGreaterOrEquals(element);

        if (preIndex >= 0) {
            switch (mode) {
                case LESS_OR_EQUALS:
                case GREATER_OR_EQUALS:
                    return preIndex;
                case LESS:
                    return preIndex - 1;
                case GREATER:
                    return Integer.min(preIndex + 1, storage.size());
                default:
                    throw new IllegalArgumentException("Wrong mode was passed to findIndex method");
            }
        } else {
            preIndex = -preIndex - 1;

            switch (mode) {
                case GREATER:
                case GREATER_OR_EQUALS:
                    return preIndex;
                case LESS:
                case LESS_OR_EQUALS:
                    return preIndex - 1;
                default:
                    throw new IllegalArgumentException("Wrong mode was passed to findIndex method");
            }
        }
    }

    @Override
    public T lower(T t) {
        int pi = findIndex(t, GreaterOrLess.LESS);

        if (pi == -1) {
            return null;
        }

        return storage.get(pi);
    }

    @Override
    public T floor(T t) {
        int pi = findIndex(t, GreaterOrLess.LESS_OR_EQUALS);

        if (pi == -1) {
            return null;
        }

        return storage.get(pi);
    }

    @Override
    public T ceiling(T t) {
        int pi = findIndex(t, GreaterOrLess.GREATER_OR_EQUALS);

        if ((pi == -1) || (pi == storage.size())) {
            return null;
        }

        return storage.get(pi);

    }

    @Override
    public T higher(T t) {
        int pi = findIndex(t, GreaterOrLess.GREATER);

        if ((pi == -1) || (pi == storage.size())) {
            return null;
        }

        return storage.get(pi);
    }

    @Override
    public T pollFirst() {
        throw new UnsupportedOperationException("Modifiable methods are not allowed in ArraySet");
    }

    @Override
    public T pollLast() {
        throw new UnsupportedOperationException("Modifiable methods are not allowed in ArraySet");
    }

    @Override
    public Iterator<T> iterator() {
        return unmodifiableList(storage).iterator();
    }

    @Override
    public NavigableSet<T> descendingSet() {
        return new ArraySet<>(storage.reversedView(), reverseOrder(comparator));
    }

    @Override
    public Iterator<T> descendingIterator() {
        return descendingSet().iterator();
    }

    @SuppressWarnings("unchecked")
    @Override
    public NavigableSet<T> subSet(T fromElement, boolean fromInclusive, T toElement, boolean toInclusive) throws IllegalArgumentException, ClassCastException {
        // [fromIndex, toIndex)
        // null means an infinity of required sign

        if ((fromElement != null) && (toElement != null)) {
            if (comparator != null) {
                if (comparator.compare(fromElement, toElement) > 0) {
                    throw new IllegalArgumentException("Wrong subSet range: from > to");
                }
            } else {
                try {
                    if (fromElement instanceof Comparable) {
                        if (((Comparable<T>) fromElement).compareTo(toElement) > 0) {
                            throw new IllegalArgumentException("Wrong subSet range: from > to");
                        }
                    }
                } catch (ClassCastException classCastException) {
                    throw new IllegalArgumentException("Elements are not comparable and no comparator was provided!", classCastException.getCause());
                }

            }
        }


        int fromIndex = 0;
        if (fromElement != null) {
            fromIndex = findIndex(fromElement, fromInclusive ? GreaterOrLess.GREATER_OR_EQUALS : GreaterOrLess.GREATER);
        }

        int toIndex = storage.size();

        if (toElement != null) {
            toIndex = 1 + findIndex(toElement, toInclusive ? GreaterOrLess.LESS_OR_EQUALS : GreaterOrLess.LESS);
        }

        fromIndex = Integer.max(fromIndex, 0);
        toIndex = Integer.max(Integer.min(toIndex, storage.size()), 0);

        if (fromIndex == 1 + toIndex) {
            return new ArraySet<>(comparator);
        }

        BidirectionalArrayListView<T> subView = storage.subView(fromIndex, toIndex);
        return new ArraySet<>(subView, comparator);
    }

    @Override
    public NavigableSet<T> headSet(T toElement, boolean inclusive) {
        return subSet(null, true, toElement, inclusive);
    }

    @Override
    public NavigableSet<T> tailSet(T fromElement, boolean inclusive) {
        return subSet(fromElement, inclusive, null, false);
    }

    @Override
    public SortedSet<T> subSet(T fromElement, T toElement) {
        return subSet(fromElement, true, toElement, false);
    }

    @Override
    public SortedSet<T> headSet(T toElement) {
        return headSet(toElement, false);
    }

    @Override
    public SortedSet<T> tailSet(T fromElement) {
        return tailSet(fromElement, true);
    }

    @Override
    public int size() {
        return storage.size();
    }

    @Override
    public Comparator<? super T> comparator() {
        return comparator;
    }

    @Override
    public T first() {
        if (!storage.isEmpty()) {
            return storage.get(0);
        } else {
            throw new NoSuchElementException("ArraySet is empty");
        }
    }

    @Override
    public T last() {
        if (!storage.isEmpty()) {
            return storage.get(storage.size() - 1);
        } else {
            throw new NoSuchElementException("ArraySet is empty");
        }
    }

    //TODO: try to eliminate unchecked cast
    @SuppressWarnings("unchecked")
    @Override
    public boolean contains(Object o) {
        try {
            return binarySearch(storage, (T) o, comparator) >= 0;
        } catch (ClassCastException e) {
            return false;
        }
    }


    protected enum GreaterOrLess {
        GREATER,
        LESS,
        GREATER_OR_EQUALS,
        LESS_OR_EQUALS
    }

    protected static class BidirectionalArrayListView<T> implements List<T>, RandomAccess {
        private List<T> storage;
        private ListViewDirection viewDirection;

        public BidirectionalArrayListView(ArrayList<T> source, ListViewDirection dir) {
            storage = source;
            viewDirection = dir;
        }

        protected BidirectionalArrayListView(List<T> source, ListViewDirection dir) {
            storage = source;
            viewDirection = dir;
        }

        protected static ListViewDirection getReverseDirection(ListViewDirection d) {
            return (d == ListViewDirection.FORWARD) ? ListViewDirection.BACKWARD : ListViewDirection.FORWARD;
        }


        public BidirectionalArrayListView<T> reversedView() {
            return new BidirectionalArrayListView<>(storage, getReverseDirection(viewDirection));
        }

        public BidirectionalArrayListView<T> subView(int fromIndex, int toIndex) {
            List<T> temporary = subList(fromIndex, toIndex);
            return new BidirectionalArrayListView<>(temporary, viewDirection);
        }

        @Override
        public int size() {
            return storage.size();
        }

        @Override
        public boolean isEmpty() {
            return storage.isEmpty();
        }

        @Override
        public boolean contains(Object o) {
            return storage.contains(o);
        }

        @Override
        public Iterator<T> iterator() {
            return listIterator();
        }

        @Override
        public Object[] toArray() {
            return storage.toArray();
        }

        @Override
        public <T1> T1[] toArray(T1[] a) {
            return storage.toArray(a);
        }

        @Override
        public boolean add(T t) {
            throw new UnsupportedOperationException("Adding to BidirectionalListView is not supported");
        }

        @Override
        public boolean remove(Object o) {
            throw new UnsupportedOperationException("Removing from BidirectionalListView is not supported");
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return storage.containsAll(c);
        }

        @Override
        public boolean addAll(Collection<? extends T> c) {
            throw new UnsupportedOperationException("Adding to BidirectionalListView is not supported");
        }

        @Override
        public boolean addAll(int index, Collection<? extends T> c) {
            throw new UnsupportedOperationException("Adding to BidirectionalListView is not supported");
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            throw new UnsupportedOperationException("Removing from BidirectionalListView is not supported");
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException("Retaining from BidirectionalListView is not supported");
        }

        @Override
        public void clear() {
            throw new UnsupportedOperationException("Clearing BidirectionalListView is not supported");
        }

        @Override
        public T get(int index) {
            if ((index < 0) || (index >= size())) {
                throw new IndexOutOfBoundsException("Index out of bounds for BidirectionalListView");
            }

            if (viewDirection == ListViewDirection.FORWARD) {
                return storage.get(index);
            } else {
                return storage.get(size() - index - 1);
            }
        }

        @Override
        public T set(int index, T element) {
            throw new UnsupportedOperationException("Setting BidirectionalListView is not supported");
        }

        @Override
        public void add(int index, T element) {
            throw new UnsupportedOperationException("Adding BidirectionalListView is not supported");
        }

        @Override
        public T remove(int index) {
            throw new UnsupportedOperationException("Removing from BidirectionalListView is not supported");
        }

        @Override
        public int indexOf(Object o) {
            if (viewDirection == ListViewDirection.FORWARD) {
                return storage.indexOf(o);
            } else {
                return size() - storage.indexOf(o) - 1;
            }
        }

        @Override
        public int lastIndexOf(Object o) {
            if (viewDirection == ListViewDirection.FORWARD) {
                return storage.lastIndexOf(o);
            } else {
                return size() - storage.lastIndexOf(o) - 1;
            }
        }

        @Override
        public ListIterator<T> listIterator() {
            return listIterator(0);
        }

        @Override
        public ListIterator<T> listIterator(int index) {
            if (viewDirection == ListViewDirection.FORWARD) {
                return storage.listIterator(index);
            } else {
                return new ListIterator<>() {
                    private ListIterator<T> localIterator = storage.listIterator(size() - index);

                    @Override
                    public boolean hasNext() {
                        return localIterator.hasPrevious();
                    }

                    @Override
                    public boolean hasPrevious() {
                        return localIterator.hasNext();
                    }

                    @Override
                    public T next() {
                        return localIterator.previous();
                    }

                    @Override
                    public T previous() {
                        return localIterator.next();
                    }

                    @Override
                    public int nextIndex() {
                        return localIterator.previousIndex();
                    }

                    @Override
                    public int previousIndex() {
                        return localIterator.nextIndex();
                    }

                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException("Removing from BidirectionalListView is not supported");
                    }

                    @Override
                    public void set(T t) {
                        throw new UnsupportedOperationException("Setting in BidirectionalListView is not supported");
                    }

                    @Override
                    public void add(T t) {
                        throw new UnsupportedOperationException("Adding to BidirectionalListView is not supported");
                    }
                };
            }
        }

        @Override
        public List<T> subList(int fromIndex, int toIndex) {
            if (viewDirection == ListViewDirection.FORWARD) {
                return storage.subList(fromIndex, toIndex);
            } else {
                return storage.subList(size() - toIndex, size() - fromIndex);
            }
        }

        protected enum ListViewDirection {
            FORWARD,
            BACKWARD
        }
    }
}