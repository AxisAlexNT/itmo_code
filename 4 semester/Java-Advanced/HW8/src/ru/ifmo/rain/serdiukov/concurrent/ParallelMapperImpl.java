package ru.ifmo.rain.serdiukov.concurrent;

import info.kgeorgiy.java.advanced.mapper.ParallelMapper;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

public class ParallelMapperImpl implements ParallelMapper {
    @SuppressWarnings("FieldCanBeLocal")
    private final int numberOfThreads;
    private final List<Thread> pool;
    private final LinkedList<Task<?, ?>> jobs;


    /**
     * Constructs an instance of ParallelMapper that uses given number of threads.
     *
     * @param numberOfThreads size of thread pool to operate with.
     */
    public ParallelMapperImpl(int numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
        pool = new ArrayList<>(numberOfThreads);
        jobs = new LinkedList<>();

        for (int i = 0; i < numberOfThreads; i++) {
            Thread subThread = new Thread(() -> {
                Task<?, ?> task = null;

                while (true) {
                    synchronized (jobs) {
                        while (jobs.isEmpty()) {
                            if (Thread.currentThread().isInterrupted()) {
                                return;
                            }

                            try {
                                jobs.wait();
                            } catch (InterruptedException e) {
                                return;
                            }
                        }

                        task = jobs.poll();
                    }

                    if (task != null) {
                        synchronized (task.executed) {
                            task.execute();
                            task.executed.notifyAll();
                        }
                    }

                    synchronized (jobs) {
                        jobs.notifyAll();
                    }

                }

            }
            );

            subThread.setDaemon(true);
            subThread.setName(String.format("Worker-%d", i));
            subThread.start();
            pool.add(subThread);
        }
    }


    @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
    @Override
    public <T, R> List<R> map(Function<? super T, ? extends R> f, List<? extends T> args) throws InterruptedException {
        final List<R> result = new ArrayList<>(args.size());


        if (args.isEmpty()) {
            return result;
        }

        // initialize result:
        for (int i = 0; i < args.size(); i++) {
            result.add(null);
        }

        final Semaphore semaphore = new Semaphore(args.size());

        int argumentIndex = 0;

        for (T argument : args) {
            final Task<T, R> t = new Task<T, R>(f, argument, result, argumentIndex, semaphore);

            synchronized (jobs) {
                jobs.add(t);
                jobs.notify();
            }

            ++argumentIndex;
        }

        synchronized (semaphore) {
            while (!semaphore.testZero()) {
                semaphore.wait();
            }
        }


        return result;
    }

    @Override
    public void close() {
        synchronized (jobs) {
            jobs.clear();
        }
        for (Thread t : pool) {
            try {
                t.interrupt();
                synchronized (jobs) {
                    jobs.notifyAll();
                }
                t.join();
            } catch (InterruptedException e) {
                // Nothing can be done
            }
        }
    }

    /**
     * Describes Task for ParallelMapperImpl.
     * In this context, task is computing function value on a given argument.
     *
     * @param <T> source data type.
     * @param <R> result data type.
     */
    private static class Task<T, R> {
        private final Function<? super T, ? extends R> f;
        private final T argument;
        private final List<R> resultStorage;
        private final Flag executed;
        private final int argumentIndex;
        private final Semaphore semaphore;
        private R result;


        /**
         * Main constructor that fills all data needed to create a self-sufficient task.
         *
         * @param f             a function which would be calculated on an {@code argument}.
         * @param argument      an argument on which to compute given function {@code f}.
         * @param resultStorage a reference to the list where to put computed result.
         * @param argumentIndex an index of that result in the list.
         * @param semaphore     a semaphore object, that is decremented once when this task is completed.
         */
        public Task(Function<? super T, ? extends R> f, T argument, List<R> resultStorage, int argumentIndex, Semaphore semaphore) {
            this.f = Objects.requireNonNull(f);
            this.argument = Objects.requireNonNull(argument);
            this.resultStorage = resultStorage;
            this.argumentIndex = argumentIndex;
            this.executed = new Flag();
            this.executed.unset();
            this.semaphore = semaphore;
        }

        /**
         * Executes the given task. This method should be called in a thread which you want to calculate result in.
         * Task could be executed only once, next calls would be ignored.
         * This method changes internal state of {@code Task} object: after execution {@code isExecuted()} will return true, a given semaphore is decremented only once.
         * Result of applying {@code f} to {@code argument} would be stored in {@code resultStorage} at position {@code argumentPosition}.
         */
        public void execute() {
            if (!this.executed.get()) {
                result = f.apply(argument);
                synchronized (resultStorage) {
                    resultStorage.set(argumentIndex, result);
                }

                semaphore.decrement();
                this.executed.set();
            }
        }
    }

    /**
     * Represents a synchronized wrapper for a boolean. Acts as a basic flag.
     */
    private static class Flag {
        private boolean state;

        /**
         * Constructs flag, initial state is {@code false}.
         */
        public Flag() {
            this.state = false;
        }

        /**
         * Sets the flag to {@code true}.
         */
        public synchronized void set() {
            state = true;
        }

        /**
         * Sets the flag to {@code false}.
         */
        public synchronized void unset() {
            state = false;
        }


        /**
         * Retrieves the flag state.
         *
         * @return state of the flag: {@code true} for set and {@code false} for unset.
         */
        public synchronized boolean get() {
            return state;
        }
    }


    /**
     * Represents a simple semaphore.
     * Its can atomically decrement and check its state.
     */
    private static class Semaphore {
        private int position;

        /**
         * Creates {@code Semaphore] instance for a given number of steps.
         *
         * @param position a number of steps as an initial value.
         */
        public Semaphore(int position) {
            this.position = position;
        }


        /**
         * Atomically decrements the state of this {@code Semaphore}.
         * {@code Semaphore} value could not be less than 0, provided that initial value was greater or equal to 0.
         */
        private synchronized void decrement() {
            --position;
            if (position < 0) {
                position = 0;
            }
            this.notifyAll();
        }

        /**
         * Tests {@code Semaphore} value for being zero.
         *
         * @return {@code true} if and only if the state of this {@code Semaphore} is zero.
         */
        private synchronized boolean testZero() {
            return position == 0;
        }
    }
}
