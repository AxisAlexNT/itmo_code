package ru.ifmo.rain.serdiukov.concurrent;

import info.kgeorgiy.java.advanced.concurrent.AdvancedIP;
import info.kgeorgiy.java.advanced.concurrent.ListIP;
import info.kgeorgiy.java.advanced.concurrent.ScalarIP;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@SuppressWarnings("StringBufferReplaceableByString")
public class IterativeParallelism implements ScalarIP, ListIP, AdvancedIP {

    /**
     * Applies function to elements of list, then reduces result by given fold function in parallel.
     *
     * @param threads      Number of threads to create for this task.
     * @param values       Source data.
     * @param subMapper    Function to calculate over source data.
     * @param resultFolder Associative binary operator that's used for reducing subtotals of each thread into one result.
     * @param <T>          Type parameter of source data.
     * @param <R>          Type parameter of result.
     * @return Result that's obtained by applying {@code subMapper} to each element of {@code values} sublists, then reduced by {@code resultFolder}.
     * @throws InterruptedException in case this task was interrupted.
     */
    private <T, R> R subListFold(int threads, List<T> values, Function<List<T>, R> subMapper, BinaryOperator<R> resultFolder) throws InterruptedException {
        if (values.isEmpty()) {
            throw new NoSuchElementException("Given empty list");
        }

        int numberOfThreads = Math.min(threads, values.size());
        //FIXME: add +1?
        int sizeOfSublist = values.size() / numberOfThreads;


        // Effectively-final wrappers
        final ArrayList<R> threadSubtotals = new ArrayList<>(Collections.nCopies(numberOfThreads, null));
        final ArrayList<Thread> threadPool = new ArrayList<>(numberOfThreads);

        //FIXME: should be applied to sublist, not to the whole list!!!
        for (int i = 0; (i < numberOfThreads); ++i) {
            final int threadNumber = i;
            Thread t = new Thread(() -> {
                int leftBorder;
                if (threadNumber != numberOfThreads - 1) {
                    leftBorder = Math.min((threadNumber + 1) * sizeOfSublist, values.size());
                } else {
                    leftBorder = values.size();
                }

                threadSubtotals.set(threadNumber, subMapper.apply(values.subList(threadNumber * sizeOfSublist, leftBorder)));

            });
            t.start();
            threadPool.add(t);
        }

        if (Thread.interrupted()) { // Clears interrupted status!
            throw new InterruptedException();
        }

        // All threads are created, we can be replaced by any other thread
        Thread.yield();

        final InterruptedException[] ie = {null};
        final boolean[] wasInterrupted = {false};

        // Wait for threads to finish:
        threadPool.forEach(t ->
                {
                    try {
                        t.join();
                    } catch (InterruptedException e) {
                        if (wasInterrupted[0]) {
                            e.addSuppressed(ie[0]);
                            ie[0] = e;
                        }
                        ie[0] = e;
                        wasInterrupted[0] = true;
                    }
                }
        );

        if (wasInterrupted[0]) {
            throw ie[0];
        }

        return threadSubtotals.stream().reduce(resultFolder).orElseThrow(NoSuchElementException::new);
    }

    //NOTE: Other javadocs are copied from interface

    @Override
    public <T> T maximum(int threads, List<? extends T> values, Comparator<? super T> comparator) throws InterruptedException {
        return subListFold(threads, values, list -> list.stream().max(comparator).orElse(null), BinaryOperator.maxBy(comparator));
    }

    @Override
    public <T> T minimum(int threads, List<? extends T> values, Comparator<? super T> comparator) throws InterruptedException {
        return subListFold(threads, values, list -> list.stream().min(comparator).orElse(null), BinaryOperator.minBy(comparator));
    }

    @Override
    public <T> boolean all(int threads, List<? extends T> values, Predicate<? super T> predicate) throws InterruptedException {
        return subListFold(threads, values, list -> list.stream().allMatch(predicate), (a, b) -> a && b);
    }

    @Override
    public <T> boolean any(int threads, List<? extends T> values, Predicate<? super T> predicate) throws InterruptedException {
        return subListFold(threads, values, list -> list.stream().anyMatch(predicate), (a, b) -> a || b);
    }

    @Override
    public String join(int threads, List<?> values) throws InterruptedException {
        return subListFold(threads, values,
                list -> list.stream().map(Object::toString).collect(Collectors.joining()),
                (a, b) -> (new StringBuilder()).append(a).append(b).toString()
        );
    }

    @Override
    public <T> List<T> filter(int threads, List<? extends T> values, Predicate<? super T> predicate) throws InterruptedException {
        return subListFold(threads, values,
                list -> list.stream().filter(predicate).collect(Collectors.toList()),
                (a, b) -> {
                    a.addAll(b);
                    return a;
                }
        );
    }


    @Override
    public <T, U> List<U> map(int threads, List<? extends T> values, Function<? super T, ? extends U> f) throws InterruptedException {
        return subListFold(threads, values,
                list -> list.stream().map(f).collect(Collectors.toList()),
                (a, b) -> {
                    a.addAll(b);
                    return a;
                }
        );
    }

    @Override
    public <T> T reduce(int threads, List<T> values, Monoid<T> monoid) throws InterruptedException {
        if (values.isEmpty()) {
            return monoid.getIdentity();
        } else {
            return subListFold(threads, values, list -> list.stream().reduce(monoid.getIdentity(), monoid.getOperator()), monoid.getOperator());
        }
    }

    @Override
    public <T, R> R mapReduce(int threads, List<T> values, Function<T, R> lift, Monoid<R> monoid) throws InterruptedException {
        if (values.isEmpty()) {
            return monoid.getIdentity();
        } else {
            return subListFold(threads, values, list -> list.stream().map(lift).reduce(monoid.getIdentity(), monoid.getOperator()), monoid.getOperator());
        }
    }
}
