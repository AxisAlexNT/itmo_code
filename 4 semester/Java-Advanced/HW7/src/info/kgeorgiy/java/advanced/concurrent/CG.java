package info.kgeorgiy.java.advanced.concurrent;

/**
 * Certificate generator.
 *
 * @author Georgiy Korneev (kgeorgiy@kgeorgiy.info)
 */
@FunctionalInterface
public interface CG {
    void certify(final Class<?> token, final String salt);
}
