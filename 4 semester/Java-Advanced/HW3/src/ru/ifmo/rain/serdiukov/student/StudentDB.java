package ru.ifmo.rain.serdiukov.student;

import info.kgeorgiy.java.advanced.student.*;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StudentDB implements StudentQuery, StudentGroupQuery, AdvancedStudentGroupQuery {
    private static final Student EMPTY_STUDENT = new Student(-1, "", "", "");
    private static final Group EMPTY_GROUP = new Group("", new ArrayList<>());

    @SuppressWarnings("StringBufferReplaceableByString")
    protected String getFullName(Student student) {
        return (new StringBuilder()).append(student.getFirstName()).append(' ').append(student.getLastName()).toString();
    }

    protected <E, F> List<F> getFieldValues(Collection<E> elements, Function<E, F> fieldGetter) {
        return Objects.requireNonNull(elements).parallelStream().
                filter(Objects::nonNull).
                map(fieldGetter).
                collect(Collectors.toList());
    }

    protected <E, F> Set<F> getDistinctFieldValues(Collection<E> elements, Function<E, F> fieldGetter) {
        return getFieldValues(elements, fieldGetter).parallelStream().collect(Collectors.toSet());
    }

    protected <E> List<E> sortElementsByComparator(Collection<E> elements, Comparator<E> comparator) {
        return Objects.requireNonNull(elements).parallelStream().
                filter(Objects::nonNull).
                sorted(comparator).
                collect(Collectors.toList());
    }

    protected <E, F> List<E> filterElementsByGivenFieldValue(Collection<E> elements, Function<E, F> fieldGetter, F givenValue) {
        return Objects.requireNonNull(elements).parallelStream().
                filter(Objects::nonNull).
                filter(s -> givenValue.equals(fieldGetter.apply(s))).
                collect(Collectors.toList());
    }


    @Override
    public List<String> getFirstNames(List<Student> students) {
        return getFieldValues(students, Student::getFirstName);
    }

    @Override
    public List<String> getLastNames(List<Student> students) {
        return getFieldValues(students, Student::getLastName);
    }

    @Override
    public List<String> getGroups(List<Student> students) {
        return getFieldValues(students, Student::getGroup);
    }

    @Override
    public List<String> getFullNames(List<Student> students) {
        return getFieldValues(students, this::getFullName);
    }

    @Override
    public Set<String> getDistinctFirstNames(List<Student> students) {
        return new TreeSet<>(getDistinctFieldValues(students, Student::getFirstName));
    }

    @Override
    public String getMinStudentFirstName(List<Student> students) {
        return students.parallelStream().sorted().findFirst().orElse(EMPTY_STUDENT).getFirstName();
    }

    @Override
    public List<Student> sortStudentsById(Collection<Student> students) {
        return sortElementsByComparator(students, Comparator.comparing(Student::getId));
    }

    @Override
    public List<Student> sortStudentsByName(Collection<Student> students) {
        return sortElementsByComparator(students,
                Comparator.
                        comparing(Student::getLastName).
                        thenComparing(Student::getFirstName).
                        thenComparing(Student::getId)
        );
    }

    @Override
    public List<Student> findStudentsByFirstName(Collection<Student> students, String name) {
        return sortElementsByComparator(filterElementsByGivenFieldValue(students, Student::getFirstName, name),
                Comparator.comparing(this::getFullName).thenComparing(Student::getId));
    }

    @Override
    public List<Student> findStudentsByLastName(Collection<Student> students, String name) {
        return sortElementsByComparator(filterElementsByGivenFieldValue(students, Student::getLastName, name),
                Comparator.comparing(this::getFullName).thenComparing(Student::getId));
    }

    @Override
    public List<Student> findStudentsByGroup(Collection<Student> students, String group) {
        return sortElementsByComparator(
                filterElementsByGivenFieldValue(students, Student::getGroup, group),
                Comparator.
                        comparing(Student::getLastName).
                        thenComparing(Student::getFirstName).
                        thenComparing(Student::getId)
        );
    }

    @Override
    public Map<String, String> findStudentNamesByGroup(Collection<Student> students, String group) {
        return findStudentsByGroup(students, group).parallelStream().
                sorted(
                        Comparator.
                                comparing(Student::getLastName).
                                thenComparing(Student::getFirstName)).
                collect(Collectors.toMap(
                        Student::getLastName,
                        Student::getFirstName,
                        (studentWithMinimalName, studentWithBiggerName) -> studentWithMinimalName
                ));
    }


    protected Map<String, List<Student>> uniteStudentsIntoMapByGroupName(Collection<Student> students) {
        return Objects.requireNonNull(students).parallelStream().
                filter(Objects::nonNull).
                collect(
                        Collectors.groupingBy(Student::getGroup)
                );
    }

    protected List<Group> getGroupsWhereStudentsAreOrderedByComparator(Collection<Student> students, Comparator<Student> orderOfStudentsInGroup) {
        return uniteStudentsIntoMapByGroupName(students).
                entrySet().parallelStream().sorted(Map.Entry.comparingByKey()).
                map(entry ->
                        new Group(
                                entry.getKey(),
                                entry.getValue().parallelStream().
                                        sorted(orderOfStudentsInGroup).
                                        collect(Collectors.toList()
                                        )
                        )
                ).
                collect(Collectors.toList());
    }

    @Override
    public List<Group> getGroupsByName(Collection<Student> students) {
        List<Group> groupsWhereStudentsAreOrderedByComparator = getGroupsWhereStudentsAreOrderedByComparator(
                students,
                Comparator.
                        comparing(Student::getLastName).
                        thenComparing(Student::getFirstName).
                        thenComparing(Student::getId)
        );
        return groupsWhereStudentsAreOrderedByComparator;
    }

    @Override
    public List<Group> getGroupsById(Collection<Student> students) {
        return getGroupsWhereStudentsAreOrderedByComparator(students,
                Comparator.
                        comparing(Student::getId).
                        thenComparing(Student::getLastName).
                        thenComparing(Student::getFirstName));
    }

    protected Group getLargestGroupByCriterion(Collection<Student> students, Comparator<Group> criterion) {
        return getGroupsByName(students).parallelStream().
                max(criterion).
                orElse(EMPTY_GROUP);
    }

    @Override
    public String getLargestGroup(Collection<Student> students) {
        return getLargestGroupByCriterion(
                students,
                Comparator.
                        comparing(
                                (Group g) -> g.getStudents().size()
                        ).
                        thenComparing(
                                Comparator.comparing(Group::getName).reversed()
                        )
        ).
                getName();
    }

    @Override
    public String getLargestGroupFirstName(Collection<Student> students) {
        return getLargestGroupByCriterion(students,
                Comparator.comparing(
                        (Group g) -> g.
                                getStudents().
                                parallelStream().
                                map(Student::getFirstName).
                                distinct().
                                count()
                ).
                        thenComparing(
                                Comparator.comparing(Group::getName).reversed()
                        )
        ).getName();
    }

    private List<String> getByIndices(Collection<Student> students, int[] indices, Function<Student, String> fieldGetter) {
        return Arrays.stream(indices).mapToObj(index -> students.parallelStream().map(fieldGetter).collect(Collectors.toList()).get(index)).collect(Collectors.toList());
    }

    @Override
    public List<String> getFullNames(Collection<Student> students, int[] indices) {
        return getByIndices(students, indices, this::getFullName);
    }

    @Override
    public String getMostPopularName(Collection<Student> students) {
        return students.parallelStream().collect(Collectors.groupingBy(this::getFullName)).entrySet().parallelStream().
                map(nameStudentsPair -> Map.entry(nameStudentsPair.getKey(), nameStudentsPair.getValue().parallelStream().map(Student::getGroup).distinct().count())).
                sorted((nameGroupsCount1, nameGroupsCount2) ->
                        {
                            if (nameGroupsCount1.getValue().equals(nameGroupsCount2.getValue())) {
                                return -nameGroupsCount1.getKey().compareTo(nameGroupsCount2.getKey());
                            } else {
                                return nameGroupsCount1.getValue().compareTo(nameGroupsCount2.getValue());
                            }
                        }
                ).max(Map.Entry.comparingByValue()).orElseGet(() -> Map.entry("", 0L)).getKey();
    }

    @Override
    public List<String> getFirstNames(Collection<Student> students, int[] indices) {
        return getByIndices(students, indices, Student::getFirstName);
    }

    @Override
    public List<String> getLastNames(Collection<Student> students, int[] indices) {
        return getByIndices(students, indices, Student::getLastName);
    }

    @Override
    public List<String> getGroups(Collection<Student> students, int[] indices) {
        return getByIndices(students, indices, Student::getGroup);
    }
}