#!/bin/bash
javac -cp . -d ../out/ "$(pwd)/ru/ifmo/rain/serdiukov/implementor/Implementor.java"
javac -cp . -d ../out/ "$(pwd)/info/kgeorgiy/java/advanced/implementor/Impler.java" "$(pwd)/info/kgeorgiy/java/advanced/implementor/JarImpler.java" "$(pwd)/info/kgeorgiy/java/advanced/implementor/ImplerException.java"

mkdir -p ../out/jar
jar --create --file ../out/jar/Implementor.jar --main-class ru.ifmo.rain.serdiukov.implementor.Implementor --manifest MANIFEST.MF -C ../out/ ru/ifmo/rain/serdiukov/implementor/Implementor.class -C ../out/ ru/ifmo/rain/serdiukov/implementor/'Implementor$RecursiveFileDeleter.class' -C ../out/ ru/ifmo/rain/serdiukov/implementor/'Implementor$RecursiveFileAdder.class' -C ../out/ info/kgeorgiy/java/advanced/implementor/Impler.class -C ../out/ info/kgeorgiy/java/advanced/implementor/JarImpler.class -C ../out/ info/kgeorgiy/java/advanced/implementor/ImplerException.class

java -cp . -jar jar/Implementor.jar ru.ifmo.rain.serdiukov.implementor.Implementor test.jar