package ru.ifmo.rain.serdiukov.implementor;


import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.*;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.jar.Attributes;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;

@SuppressWarnings("StringBufferReplaceableByString")
public class Implementor implements Impler, JarImpler {
    public static void main(String[] args) {
        if (args.length < 1 || args.length > 2) {
            System.out.printf("Wrong number of arguments passed.%nUsage: java -jar Implementor.jar <Class Name>%njava -jar Implementor -jar <Class Name> <FileName.jar>%n");
            return;
        }

        if (args.length == 1) {
            try {
                Class<?> token = Class.forName(args[0]);

                (new Implementor()).implement(token, Path.of(""));
            } catch (ClassNotFoundException e) {
                System.out.println("Cannot find class:");
                e.printStackTrace();
            } catch (ImplerException e) {
                System.out.println("Error during implementation:");
                e.printStackTrace();
            }
        } else {
            try {
                Class<?> token = Class.forName(args[0]);
                Path jarFilePath = Path.of(args[1]).toAbsolutePath();

                (new Implementor()).implementJar(token, jarFilePath);
            } catch (ClassNotFoundException e) {
                System.out.println("Cannot find class:");
                e.printStackTrace();
            } catch (ImplerException e) {
                System.out.println("Error during implementation:");
                e.printStackTrace();
            }
        }
    }

    /**
     * Returns string describing upper bounds of given variable.
     *
     * @param typeVariable variable whose upper bounds should be stringified.
     * @return String, describing upper bounds of given variable.
     */
    private static String typeVariableBounds(TypeVariable<?> typeVariable) {
        Type[] bounds = typeVariable.getBounds();
        if ((bounds.length == 1) && bounds[0].equals(Object.class)) {
            return typeVariable.getName(); // No upper bound
        } else {
            // Has upper bound
            return typeVariable.getName() + " extends " +
                    Arrays.stream(bounds)
                            .map(Type::getTypeName)
                            .collect(Collectors.joining(" & "));
        }
    }

    /**
     * Generates string containing field's modifiers.
     *
     * @param f field to generate modifiers of.
     * @return String containing field's modifiers.
     */
    private static String generateModifiers(Field f) {
        return Modifier.toString(f.getModifiers() & Modifier.fieldModifiers());
    }

    /**
     * Generates string containing method's modifiers.
     *
     * @param m method to generate modifiers of.
     * @return String containing method's modifiers.
     */
    private static String generateModifiers(Method m) {
        return Modifier.toString((m.getModifiers() & Modifier.methodModifiers()) & ~(Modifier.TRANSIENT | Modifier.ABSTRACT | Modifier.NATIVE));
    }

    /**
     * Generates string containing constructor's modifiers.
     *
     * @param c constructor to generate modifiers of.
     * @return String containing constructor's modifiers.
     */
    private static String generateModifiers(Constructor<?> c) {
        return Modifier.toString((c.getModifiers() & Modifier.constructorModifiers()) & ~(Modifier.TRANSIENT | Modifier.ABSTRACT | Modifier.NATIVE));
    }

    /**
     * Generates string of modifiers that describe modifiers of class instantiated from a given interface or class.
     *
     * @param token interface or class which is being implemented.
     * @return String containing right modifiers.
     */
    private static String generateModifiers(Class<?> token) {
        assert !token.isPrimitive();
        return Modifier.toString((token.getModifiers() & Modifier.classModifiers()) & ~(Modifier.ABSTRACT | Modifier.TRANSIENT | Modifier.NATIVE | Modifier.INTERFACE | Modifier.STATIC));
    }

    /**
     * Generates string of modifiers that describe modifiers of class instantiated from a given enumeration.
     *
     * @param token enumeration which is being implemented.
     * @return String containing right modifiers.
     */
    private static String generateModifiersForEnumImpl(Class<?> token) {
        assert token.isEnum();

        return Modifier.toString((token.getModifiers() & Modifier.classModifiers()) & ~(Modifier.ABSTRACT | Modifier.TRANSIENT | Modifier.FINAL | Modifier.NATIVE | Modifier.INTERFACE));
    }


    /**
     * Generates string representing zero of required type.
     *
     * @param type a reference type whose zero should be generated.
     * @return String representing zero of required type.
     */
    private static String generateZeroOfRequiredType(Class<?> type) {
        if (type.isPrimitive()) {
            if (type == Boolean.TYPE) {
                return "false";
            } else {
                return "0";
            }
        } else {
            return "null";
        }
    }


    /**
     * Generates string describing all generic types in declaration.
     *
     * @param d entity containing type variables.
     * @return String of format {@code <T1, T2, T3, ...Tk>} or empty string, if no generic parameters are present.
     */
    private static String generateGenericsDeclaration(GenericDeclaration d) {
        // Add all generic type parameter names
        TypeVariable<?>[] typeParameters = d.getTypeParameters();
        if (typeParameters.length > 0) {
            return Arrays.stream(typeParameters)
                    .map(Implementor::typeVariableBounds)
                    .collect(Collectors.joining(",", "<", "> "));
        }

        return "";
    }

    /**
     * Generates a name for field.
     *
     * @param f a field whose name to generate.
     * @return A string, containing field name.
     */
    private static String generateName(Field f) {
        return f.getName();
    }

    /**
     * Generates a string that describes generic return type of method.
     *
     * @param m a method whose generic return type is processed.
     * @return A string, containing valid generic return type representation.
     */
    private static String generateGenericReturnType(Method m) {
        return getCorrectNestedTypeName(m.getGenericReturnType());
    }

    private static String generateName(Executable e) {
        return e.getName();
    }


    /**
     * Generates a string, containing a parameter list for a functional-like object.
     *
     * @param e an executable object.
     * @return String representing parameter list in declaration.
     */
    private static String generateParametersList(Executable e) {
        StringBuilder sb = new StringBuilder();
        Type[] parameterTypes = e.getGenericParameterTypes();
        int counter = 0;

        // Add all parameters except last which can be varargs
        for (; counter < parameterTypes.length - 1; ++counter) {
            sb.append(getParameterDeclaration(parameterTypes[counter], counter));
        }

        // If last parameter is varargs -- replace [] with ...
        if (e.isVarArgs()) {
            sb.
                    append(getCorrectNestedTypeName(parameterTypes[counter])).  // Get type name for varargs
                    delete(sb.length() - 2, sb.length()).           // Delete wrongly placed []
                    append("... ").                                 // Mark as varargs
                    append("param").                                // Place parameter name
                    append(counter);
            ++counter;
        } else
            // If it is not varargs, but has 'last' parameter -- add it as a simple parameter
            if (parameterTypes.length > 0) {
                sb.append(getParameterDeclaration(parameterTypes[counter], counter));
                deleteLastComma(sb);
                ++counter;
            }
        return sb.toString();
    }

    /**
     * Generates method body.
     *
     * @param m method whose body should be generated.
     * @return A valid string representation for this method body.
     */
    private static String generateMethodBody(Method m) {
        if (m.getReturnType() == Void.TYPE) {
            return "{}";
        } else {
            return String.format("{ return %s; }", generateZeroOfRequiredType(m.getReturnType()));
        }
    }

    /**
     * Generates constructor body.
     *
     * @param c constructor to generate body for.
     * @return Valid string representation for constructor body.
     */
    private static String generateConstructorBody(Constructor<?> c, boolean addImpl) {
        if (addImpl) {
            StringBuilder sb = new StringBuilder();
            sb.append("{ super(");
            if (c.getParameterCount() > 0) {
                Type[] genericParameterTypes = c.getGenericParameterTypes();
                for (int i = 0; i < genericParameterTypes.length; ++i) {
                    sb.append("(").append(getCorrectNestedTypeName(genericParameterTypes[i])).append(") ").append(generateZeroOfRequiredType(c.getParameterTypes()[i])).append(", ");
                }

                deleteLastComma(sb);
            }
            sb.append("); }");
            return sb.toString();
        } else {
            return "{}";
        }
    }

    /**
     * Given a field constructs valid Java code declaring this field.
     * Field is assigned from a zero of required type for a final fields.
     *
     * @param f field to generate code for.
     * @return A valid Java code representing this field.
     */
    private static String generateFieldCode(Field f) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%s %s %s", generateModifiers(f), getCorrectNestedTypeName(f.getGenericType()), generateName(f)));
        if (Modifier.isFinal(f.getModifiers())) {
            sb.append(String.format(" = %s", generateZeroOfRequiredType(f.getType())));
        }
        sb.append(';');
        return sb.toString();
    }


    /**
     * Given a method generates a valid Java code declaring this method.
     * Places return statement returning zero of required type.
     * If flag {@code overrides} is set to true and method is not static, not abstract and not final it places {@code @Override} annotation. Otherwise it is not placed.
     *
     * @param m         a method whose code should be generated.
     * @param overrides a flag controlling {@code @Override} annotation.
     * @return
     */
    private static String generateMethodCode(Method m, boolean overrides) {
        StringBuilder sb = new StringBuilder();
        if (overrides && !Modifier.isStatic(m.getModifiers()) && !Modifier.isAbstract(m.getModifiers()) && !Modifier.isFinal(m.getModifiers())) {
            sb.append(String.format("@Override%n"));
        }


        sb.
                append(generateModifiers(m)).append(' ').
                append(generateGenericsDeclaration(m)).append(' ').
                append(generateGenericReturnType(m)).append(' ').
                append(generateName(m)).append('(').
                append(generateParametersList(m)).append(')').
                append(generateThrowsBlock(m));

        sb.
                append(generateMethodBody(m)).
                append(String.format("%n"));

        return sb.toString();
    }


    /**
     * Generates valid Java constructor code.
     *
     * @param c constructor to generate code for.
     * @return A string containing valid Java code that produces this constructor.
     */
    private static String generateConstructorCode(Constructor<?> c, boolean addImpl) {
        StringBuilder sb = new StringBuilder();
        sb.
                append(generateModifiers(c)).append(' ').
                append(generateGenericsDeclaration(c)).append(' ').
                append(c.getDeclaringClass().getSimpleName()).append(addImpl ? "Impl" : "").append('(').
                append(generateParametersList(c)).append(')').
                append(generateThrowsBlock(c)).
                append(generateConstructorBody(c, addImpl)).append(String.format("%n"));

        return sb.toString();
    }


    /**
     * Given an executable generates {@code throws} block and places exceptions that are declared by that executable.
     *
     * @param e an executable to generate {@code throws} block for
     * @return A code containing {@code throws} block for that executable.
     */
    private static String generateThrowsBlock(Executable e) {
        final StringBuilder sb = new StringBuilder();
        Type[] exceptions = e.getGenericExceptionTypes();
        if (exceptions.length > 0) {
            sb.append(" throws ");
            Arrays.stream(exceptions).forEachOrdered(exception -> sb.append(getCorrectNestedTypeName(exception)).append(", "));
            deleteLastComma(sb);
        }
        return sb.toString();
    }


    /**
     * Generates a fully qualified type name considering hierarchy.
     * Nested classes are split from their declaring classes using a period ({@code .}) rather than a {@code $} sign.
     *
     * @param t a type whose name to generate.
     * @return A string containing valid type name.
     */
    private static String getCorrectNestedTypeName(Type t) {
        return t.getTypeName().replaceAll("\\$", ".");
    }

    /**
     * Generates a valid parameter declaration to be placed into {@link java.lang.reflect.Executable}'s parameter list generator.
     *
     * @param t  a type of parameter.
     * @param id an index of parameter in parameter list.
     * @return String, containing valid parameter declaration.
     */
    private static String getParameterDeclaration(Type t, int id) {
        return String.format("%s param%d, ", getCorrectNestedTypeName(t), id); // Add parameter type name then parameter name
    }

    /**
     * Removes last comma added by other generator.
     *
     * @param sb a {@link java.lang.StringBuilder} that builds a comma-space separated list.
     */
    private static void deleteLastComma(StringBuilder sb) {
        sb.delete(sb.length() - 2, sb.length());
    }


    /**
     * Generates a valid Java code describing a class that implements given interface.
     *
     * @param token an interface type token to generate class for.
     * @return A String containing valid Java code representing a class that implements given interface.
     */
    private static String implementInterface(Class<?> token) throws ImplerException {
        assert token.isInterface();

        /*
        Interface implementation:
        <modifiers> class <interfaceName+Impl> implements <interfaceName> {
            #Fields declarations
            #Final fields definitions (0's of required type: 0/false/null)

            #public constructors

            #methods with respective modifiers (except for transient/abstract/native)

            #Nested members
        }
         */


        final StringBuilder sb = new StringBuilder();

        sb.
                append("public").
                append(" class ").
                append(token.getSimpleName()).append("Impl").
                append(generateGenericsDeclaration(token)).
                append(" implements ").append(token.getCanonicalName()).
                append(String.format("{%n"));

        HashSet<Field> fields = new HashSet<>();
        fields.addAll(Arrays.asList(token.getDeclaredFields()));
        fields.addAll(Arrays.asList(token.getFields()));
        fields.forEach(f -> sb.append(String.format("%s%n", generateFieldCode(f))));

        HashSet<Constructor<?>> constructors = new HashSet<>();
        constructors.addAll(Arrays.asList(token.getDeclaredConstructors()));
        constructors.addAll(Arrays.asList(token.getConstructors()));
        constructors.forEach(c -> sb.append(String.format("%s%n", generateConstructorCode(c, false))));

        HashSet<Method> methods = new HashSet<>();
        methods.addAll(Arrays.asList(token.getDeclaredMethods()));
        methods.addAll(Arrays.asList(token.getMethods()));
        methods.forEach(m -> sb.append(String.format("%s%n", generateMethodCode(m, true))));

        sb.append(generateNestedEntitiesCode(token));

        sb.append("}");

        return sb.toString();
    }


    /**
     * Generates a string hash for a method.
     * This hash is used to unique describe overloaded versions of method in hierarchy.
     * @param m A method to generate hash for.
     * @return A hash for the given method.
     */
    private static String methodHash(Method m) {
        return m.getName() + generateParametersList(m);
    }


    /**
     * Generates a valid Java code describing a non-abstract class that extends given type-token.
     *
     * @param token an interface type token to generate class for.
     * @throws ImplerException In case given class is final, is Enum or does not contain accessible constructors.
     * @return A String containing valid Java code representing a class that implements given interface.
     */
    private static String implementClass(Class<?> token) throws ImplerException {
        assert !token.isPrimitive();

        if (Modifier.isFinal(token.getModifiers())) {
            throw new ImplerException("Cannot implement final class!");
        }

        if (token == java.lang.Enum.class) {
            throw new ImplerException("Classes cannot directly extend enums!");
        }

        /*
        Interface implementation:
        <modifiers> class <interfaceName+Impl> implements <interfaceName> {
            #Fields declarations
            #Final fields definitions (0's of required type: 0/false/null)

            #public constructors

            #methods with respective modifiers (except for transient/abstract/native)

            #Nested members
        }
         */


        final StringBuilder sb = new StringBuilder();

        sb.
                append(generateModifiers(token)).
                append(" class ").
                append(token.getSimpleName()).append("Impl").
                append(generateGenericsDeclaration(token)).
                append(" extends ").append(token.getCanonicalName()).
                append(String.format("{%n"));

        HashSet<Field> fields = new HashSet<>();
        fields.addAll(Arrays.asList(token.getDeclaredFields()));
        fields.addAll(Arrays.asList(token.getFields()));
        fields.forEach(f -> sb.append(String.format("%s%n", generateFieldCode(f))));

        HashSet<Constructor<?>> constructors = new HashSet<>();
        constructors.addAll(Arrays.asList(token.getDeclaredConstructors()));
        constructors.addAll(Arrays.asList(token.getConstructors()));
        constructors.removeIf(c -> Modifier.isPrivate(c.getModifiers()));
        if (constructors.isEmpty()) {
            throw new ImplerException("No constructors are available for impler");
        }
        constructors.forEach(c -> sb.append(String.format("%s%n", generateConstructorCode(c, true))));

        TreeMap<String, Method> methods = new TreeMap<>();

        // Method name, return type, parameters list identify method unique

        Class<?> p = token;

        while (p != Object.class) {
            Arrays.stream(p.getDeclaredMethods()).forEachOrdered(m -> methods.putIfAbsent(methodHash(m), m));
            Arrays.stream(p.getMethods()).forEachOrdered(m -> methods.putIfAbsent(methodHash(m), m));
            p = p.getSuperclass();
        }


        methods.values().stream().
                filter(m -> !(
                        Modifier.isFinal(m.getModifiers())
                                || Modifier.isPrivate(m.getModifiers())
                                || !Modifier.isAbstract(m.getModifiers()))
                ).forEach(m -> sb.append(String.format("%s%n", generateMethodCode(m, true))));

        sb.append(generateNestedEntitiesCode(token));

        sb.append("}");

        return sb.toString();
    }


    /**
     * Generates a valid Java code for a given enumeration.
     *
     * @param token an {@link java.lang.Enum} type token to generate code for.
     * @return A String containing valid Java code representing given enumeration.
     */
    private static String implementEnum(Class<?> token) throws ImplerException {
        assert token.isEnum();
        /*
            <modifiers> enum Name{          <-- Cannot contain type parameters
                # List of states, delimiter is comma, ends with semicolon;
                # Only private constructors are possible
                # Any type of methods are possible
            }
         */

        final StringBuilder sb = new StringBuilder();

        sb.
                append(generateModifiersForEnumImpl(token)).
                append(" enum ").
                append(token.getSimpleName()).append(' ').
                append(generateImplementInterfaces(token)).
                append(String.format("{%n"));

        // All final fields are initialized, added default zero-pos constructor that does nothing, all states are zero-arg

        Arrays.stream(token.getEnumConstants()).forEach(c -> sb.append(c.toString()).append(", "));
        deleteLastComma(sb);
        sb.append(String.format(";%n"));

        Arrays.stream(token.getFields()).skip(token.getEnumConstants().length).forEachOrdered(f -> sb.append(String.format("%s%n", generateFieldCode(f))));


        HashSet<Constructor<?>> constructors = new HashSet<>(Arrays.asList(token.getDeclaredConstructors()));
        constructors.addAll(Arrays.asList(token.getConstructors()));
        boolean hasZeroArgConstructor = constructors.parallelStream().anyMatch(c -> c.getParameterCount() == 0);

        if (!hasZeroArgConstructor) {
            sb.append(generateZeroArgConstructor(token, false));
        }

        constructors.forEach(c -> sb.append(String.format("%s%n", generateConstructorCode(c, false))));

        HashSet<Method> methods = new HashSet<>(Arrays.asList(token.getDeclaredMethods()));
//        methods.addAll(Arrays.asList(token.getMethods()));
        methods.stream().filter(m -> !(Modifier.isStatic(m.getModifiers()) || Modifier.isFinal(m.getModifiers()))).forEach(m -> sb.append(String.format("%s%n", generateMethodCode(m, false))));

        sb.append(generateNestedEntitiesCode(token));

        sb.append("}");

        return sb.toString();
    }


    /**
     * Recursively generates a code for all nested members of given entity.
     *
     * @param token a type-token for an entity that can contain nested members.
     * @return A valid Java code for all nested members of given entity.
     */
    private static String generateNestedEntitiesCode(Class<?> token) throws ImplerException {
        StringBuilder sb = new StringBuilder();
        for (Class<?> m : token.getDeclaredClasses()) {
            if (m.isEnum()) {                               //TODO: check for the members you can implement
                String entityCode = implementEntity(m);
                sb.append(entityCode).append(String.format("%n"));
            }
        }

        return sb.toString();
    }

    /**
     * Generates a zero-arguments constructor for a given type-token.
     *
     * @param token a type token to generate constructor for.
     * @return A valid Java code that represents zero-arguments constructor for a given type-token.
     */
    private static String generateZeroArgConstructor(Class<?> token, boolean addImpl) {
        return String.format("private %s%s(){}%n", token.getSimpleName(), addImpl ? "Impl" : "");
    }

    /**
     * Generates an {@code implements} block for a class or enum.
     *
     * @param token a type token to generate {@code implements} block for.
     * @return A string, consisting of {@code implements} block for a class or enum.
     */
    private static String generateImplementInterfaces(Class<?> token) {
        if (token.getGenericInterfaces().length > 0) {
            final StringBuilder sb = new StringBuilder();

            sb.append("implements ");
            Arrays.stream(token.getGenericInterfaces()).forEachOrdered(t -> sb.append(getCorrectNestedTypeName(t)).append(", "));
            deleteLastComma(sb);

            return sb.toString();
        }

        return "";
    }


    /**
     * Generates a {@code package} header for a class in a new file.
     *
     * @param token a type token to generate header for.
     * @return A string contating {@code package} header for a class in a new file.
     */
    private static String generateRootHeader(Class<?> token) {
        return String.format("package %s;%n%n", token.getPackageName());
    }


    /**
     * Recursively generates code for a given interface or enum.
     * Entrance point for a recursive geerator.
     *
     * @param token a type token to generate implementation for.
     * @return A valid Java code containing implementation for a given type token.
     */
    private static String implementEntity(Class<?> token) throws ImplerException {
        if (token.isInterface()) {
            return implementInterface(token);
        } else if (token.isEnum()) {
            return implementEnum(token);
        } else if (!token.isPrimitive()) {
            return implementClass(token);
        }

        return "// TODO: FIXME!";
    }


    /**
     * Generates an implementation for a class in a new file.
     *
     * @param token a type token main in this class.
     * @return A string containing valid Java code for class in this filename file.
     */
    private static String doImplement(Class<?> token) throws ImplerException {
        return generateRootHeader(token) + implementEntity(token);
    }


    /**
     * Checks whether this implementor can implement given type token.
     *
     * @param token A type token to check.
     * @throws ImplerException In case given class is not found, is primitive or is not accessible.
     */
    private static void checkImplementingToken(Class<?> token) throws ImplerException {
        // Load class info:
        try {
            ClassLoader.getSystemClassLoader().loadClass(token.getName());
        } catch (ClassNotFoundException e) {
            throw new ImplerException(e.getMessage(), e);
        }

        // Cannot implement primitives:
        if (token.isPrimitive()) {
            throw new ImplerException("Given class is primitive!");
        }

        // Cannot implement private interfaces or extend from private classes:
        if (Modifier.isPrivate(token.getModifiers())) {
            throw new ImplerException("Given token is private!");
        }
    }


    /**
     * Generates path, taking care of package name, given path and extension.
     *
     * @param token     A type token which implementation would be saved.
     * @param root      A root directory to generate implementations in.
     * @param extension File extension.
     * @return Path to the correct location of generated implementation, started at {@code root}.
     */
    private static Path generateSourcePath(Class<?> token, Path root, String extension) {
        String canonicalPath = (token.getPackageName() + "." + token.getSimpleName()).replace('.', '/') + "Impl";
        return Path.of(root.toAbsolutePath().toString(), canonicalPath + extension);
    }

    /**
     * An interface method proposed by {@author Georgiy Korneev} for our homework.
     *
     * @param token type token to create implementation for.
     * @param root  root directory.
     * @throws ImplerException in case it is impossible to generate valid Java code for that entity.
     */
    @Override
    public void implement(Class<?> token, Path root) throws ImplerException {
        checkImplementingToken(token);


        Path outputFileName = generateSourcePath(token, root, ".java");

        try {
            Files.createDirectories(outputFileName.getParent());
        } catch (IOException e) {
            System.out.printf("Can't create output file directory: %s%n", outputFileName.toString());
            throw new ImplerException(e.getMessage(), e);
        }


        String implementation = String.format("// Auto-generated by Implementor for class/interface: %s%n", token.getName());


        implementation += doImplement(token);


        try (BufferedWriter outputWriter = Files.newBufferedWriter(outputFileName, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.WRITE)) {
            outputWriter.write(implementation);
        } catch (IOException e) {
            System.out.printf("Cannot create source file: %s%nException:%s%n", outputFileName.toString(), e.getCause());
            throw new ImplerException(e.getMessage(), e);
        }
    }


    /**
     * Generates jar implementation for a given token.
     *
     * @param token   type token to create implementation for.
     * @param jarFile target <var>.jar</var> file.
     * @throws ImplerException
     */
    @Override
    public void implementJar(Class<?> token, Path jarFile) throws ImplerException {
        checkImplementingToken(token);


        final JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        if (compiler == null) {
            throw new ImplerException("Cannot find java compiler!");
        }

        // Will be used as the temporary directory for compilation:
        Path temp = Path.of(jarFile.getParent().toString(), "tempForCompilation").normalize();
        try {
            Files.createDirectories(temp.getParent());
            if (Files.exists(temp)) {
                Files.walkFileTree(temp, new RecursiveFileDeleter());
            }
            Files.createDirectory(temp).toFile().deleteOnExit();
        } catch (IOException e) {
            System.out.printf("Can't create temporary directory: %s%n", temp.toString());
            throw new ImplerException(e.getMessage(), e);
        }


        // Save implementation to temporary directory
        implement(token, temp);

        Path outputFileName = generateSourcePath(token, temp, ".java");

        final ArrayList<String> arguments = new ArrayList<>();

        arguments.add("-encoding");
        arguments.add(StandardCharsets.UTF_8.name());
        arguments.add("-cp");
        try {
            arguments.add("." + File.pathSeparator
                    + temp + File.pathSeparator
                    + System.getProperty("java.class.path") + File.pathSeparator
                    + Path.of(token.getProtectionDomain().getCodeSource().getLocation().toURI()).toString());
        } catch (final URISyntaxException e) {
            throw new AssertionError(e);
        }

        arguments.add(outputFileName.toAbsolutePath().toString());

        System.out.println("Arguments:");
        arguments.forEach(s -> {
            System.out.print(s);
            System.out.print(" | ");
        });
        System.out.println("\nEnd arguments");

        final int compilerExitCode = compiler.run(null, null, null, arguments.toArray(String[]::new));

        if (compilerExitCode != 0) {
            throw new ImplerException("Ааа-а-а-а-а, мой собстсвенный код не компилируется!!! :(");
        }


        final ArrayList<Path> classFilePaths = new ArrayList<>();

        try {
            Files.walkFileTree(temp, new RecursiveFileAdder(classFilePaths, ".class"));
        } catch (IOException e) {
            throw new ImplerException("Cannot recursively add all classfiles to classpath!", e);
        }


        Manifest manifest = new Manifest();
        Attributes attributes = manifest.getMainAttributes();
        attributes.put(Attributes.Name.MANIFEST_VERSION, "1.0");


        try {
            Files.deleteIfExists(jarFile);
        } catch (IOException e) {
            throw new ImplerException("Cannot delete jar file!", e);
        }

        try (JarOutputStream jos = new JarOutputStream(Files.newOutputStream(jarFile), manifest)) {
            for (Path classFilePath : classFilePaths) {
                String path =
                        temp.toAbsolutePath().
                                relativize(classFilePath.toAbsolutePath()).
                                toString().
                                replace('\\', '/');
                ZipEntry ze = new ZipEntry(path);
                jos.putNextEntry(ze);
                Files.copy(classFilePath, jos);
            }

        } catch (IOException e) {
            throw new ImplerException("Cannot create output jar file!", e);
        }
    }


    /**
     * This class is used to add paths with given extension to the given list.
     */
    private static class RecursiveFileAdder extends SimpleFileVisitor<Path> {
        private List<Path> filePaths;
        private String extension;


        /**
         * Constructs file adder that adds paths with given extension to the given list.
         * @param filePaths A list to add paths to.
         * @param extension An extension of files to add.
         */
        RecursiveFileAdder(List<Path> filePaths, String extension) {
            this.filePaths = filePaths;
            this.extension = extension;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
            if (file.toAbsolutePath().toString().endsWith(extension)) {
                filePaths.add(file.toAbsolutePath());
            }
            return FileVisitResult.CONTINUE;
        }
    }

    /**
     * This class is used to recursively delete files and directory in a given path.
     */
    private static class RecursiveFileDeleter extends SimpleFileVisitor<Path> {
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            Files.deleteIfExists(file);
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
            Files.deleteIfExists(dir);
            return FileVisitResult.CONTINUE;
        }
    }
}
