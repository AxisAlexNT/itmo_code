type expr = IMPL of expr*expr | OR of expr*expr | AND of expr*expr | NEG of expr | Var of string;;

(*

EXPR::= OR | OR '->' EXPR
OR  ::= AND OR1
OR1 ::= '|' AND OR1 | empty
AND ::= NEG AND1
AND1::= '&' NEG AND1 | empty
NEG ::= '!'NEG | '('EXPR')' | VAR
VAR ::= ('A'...'Z')VAR1
VAR1::={'A'...'Z'|'0'...'9'|‘'’}*


Each function for term greedy eats string as it can starting from given position and returns pair (pared expr * first non-term position)
*)


let prettyPrint term' =

    let rec print term =
    match term with
    |IMPL(a, b) -> "(->,"^(print a)^","^(print b)^")"
    |OR(a,b)    -> "(|,"^(print a)^","^(print b)^")"
    |AND(a,b)   -> "(&,"^(print a)^","^(print b)^")"
    |NEG(a)     -> "(!"^(print a)^")"
    |Var(name)  -> name
    in

    print term'
;;

let isValidVariableNameChar ch = (('A' <= ch)&&(ch <= 'Z')) || (('0' <= ch)&&(ch <= '9')) || (ch = '\'');;
let isValidGrammarChar ch = (isValidVariableNameChar ch) || (ch = '-') || (ch = '>') || (ch = '!') || (ch = '&') || (ch = '|') || (ch = '(') || (ch = ')');;


let rec removeWhiteSpaces' str position prefix =
    if (position >= String.length str) then prefix
    else if (isValidGrammarChar str.[position]) then removeWhiteSpaces' str (position+1) (prefix^(String.make 1 str.[position]))
    else removeWhiteSpaces' str (position+1) prefix;;

let removeWhiteSpaces str = removeWhiteSpaces' str 0 "";;

let parse s =
    if ((String.length s) = 0) then
        failwith "Empty string given!";

    let s' = removeWhiteSpaces s in
    let st = s'^";;;;" in (*In order to reduce bounds checks*)

    let rec parseEXPR str position =
        let (orTerm, orNextPosition) = parseOR str position in
        if (str.[orNextPosition] = '-') then
        (
            if (str.[orNextPosition+1] = '>') then
            (
                let (exprTerm, exprNextPosition) = parseEXPR str (orNextPosition+2) in
                (IMPL(orTerm, exprTerm), exprNextPosition)
            ) else (
                failwith ("Unexpected symbol at position "^(string_of_int orNextPosition)^": "^(String.make 1 str.[orNextPosition+1]))
            )
        ) else (
            (orTerm, orNextPosition)
        )
    and parseOR str position =
        let (leftANDTerm, leftANDTermNextPosition) = parseAND str position in
        let (rightOR1Term, rightOR1NextPosition) = parseOR1 str leftANDTermNextPosition leftANDTerm in
        (rightOR1Term, rightOR1NextPosition)
    and parseOR1 str position leftANDTerm =
        if (position = String.length str) then
        (
            (leftANDTerm, position)
        )
        else (
            if (str.[position] = '|') then (
                let (rightANDTerm, rightANDNextPosition) = parseAND str (position+1) in
                let (lastOR1, lastOR1NextPosition) = parseOR1 str rightANDNextPosition (OR(leftANDTerm, rightANDTerm)) in
                (lastOR1, lastOR1NextPosition)
            ) else (
                (leftANDTerm, position)
            )
        )
    and parseAND str position =
        let (leftNEGTerm, leftNEGTermNextPosition) = parseNEG str position in
        let (rightAND1Term, rightAND1TermNextPosition) = parseAND1 str leftNEGTermNextPosition leftNEGTerm in
        (rightAND1Term, rightAND1TermNextPosition)
    and parseAND1 str position leftNEGTerm =
        if (position >= String.length str) then
        (
            (leftNEGTerm, position)
        )
        else (
            if (str.[position] = '&') then (
                let (rightNEGTerm, rightNEGNextPosition) = parseNEG str (position+1) in
                let (lastAND1, lastAND1NextPosition) = parseAND1 str rightNEGNextPosition (AND(leftNEGTerm, rightNEGTerm)) in
                (lastAND1, lastAND1NextPosition)
            ) else (
                (leftNEGTerm, position)
            )
        )
    and parseNEG str position =
        if (str.[position] = '!') then
        (
            let (innerNEG, innerNEGNextPosition) = parseNEG str (position+1) in
            (NEG(innerNEG), innerNEGNextPosition)
        ) else if (str.[position] = '(') then (
            let (innerEXPR, innerEXPRNextPosition) = parseEXPR str (position+1) in
            if (str.[innerEXPRNextPosition] <> ')') then failwith ("Odd brackets at positions: "^(string_of_int position)^" and "^(string_of_int innerEXPRNextPosition));
            (innerEXPR, innerEXPRNextPosition+1)
        ) else (
            parseVAR str position
        )
    and parseVAR str position =
        if (String.length str <= position) then failwith "Grammar mismatch: expected VAR, but reached end of string";
        if ((str.[position] < 'A') || (str.[position] > 'Z')) then failwith ("Grammar mismatch: expected start of variable name ('A'..'Z'), but found character: "^(String.make 1 str.[position]));
        parseVAR1 str (position+1) (String.make 1 str.[position])
    and parseVAR1 str position namePrefix =
        if (isValidVariableNameChar str.[position]) then (
            parseVAR1 str (position+1) (namePrefix^(String.make 1 str.[position]))
        ) else (
            (Var(namePrefix), position)
        )
    in

    let (term, _) = parseEXPR st 0 in
    term;;

(*parse "A&B->C->C'";;*)



(*prettyPrint (parse "A&B->C->C'");;*)


let solve () =
    let s = read_line () in
    let expression = parse s in
    let answer = prettyPrint expression in
    print_string answer
;;

let _ = solve ();;
